import React from 'react'
import SignUpForm from '../Components/SignUpForm/SignUpForm'

const SignUpPage = () => {
    return (
        <SignUpForm />
    )
}

export default SignUpPage