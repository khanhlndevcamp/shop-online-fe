import React, { useEffect, useState } from 'react'
import DashboardContent from '../Components/Dashboard/DashboardContent/DashboardContent'
import DashboardHeader from '../Components/Dashboard/DashboardHeader/DashboardHeader'
import DashboardSideBar from '../Components/Dashboard/DashboardSideBar/DashboardSideBar'
import { useDispatch, useSelector } from 'react-redux'
import { closeSidebarWhenResize, openSidebarWhenResize, resetDataDashboardGeneral } from '../actions/dashboardAction'
import { Container } from 'reactstrap'
import { useNavigate } from 'react-router-dom'

const DashboardPage = ({children}) => {

  const { dashboardReducer, userReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [isMobile, setIsMobile] = useState(window.innerWidth < 1000)

  const handleClickSideBarWrapper = (e) => {
    let elmSidebarWrapper = document.getElementById('sidebar-wrapper')
    if (e.target === elmSidebarWrapper) {
      //just close side bar
      dispatch(closeSidebarWhenResize())
    }
  }

  useEffect(() => {
    if (!userReducer.user) {
      navigate('/login/dashboard')
    } else {
      // console.log('case have user')
      // navigate('/dashboard')
      // dispatch(fetchOrderCustomer(userReducer.user.email))
    }
  }, [userReducer.user])

  useEffect(() => {
    const handleResizeDashboardSidebar = (e) => {
      if (window.innerWidth < 1000) {
        setIsMobile(true)
        dispatch(closeSidebarWhenResize())
      } else {
        dispatch(openSidebarWhenResize())
        setIsMobile(false)
      }
    }

    window.addEventListener('resize', handleResizeDashboardSidebar)

    return () => {
      window.removeEventListener('resize', handleResizeDashboardSidebar)
      dispatch(resetDataDashboardGeneral())
    }
  }, [])

  return (
    <>
      {userReducer.user ?
        userReducer.user.role === 'admin'
          ?
          <div style={{ width: '100%', margin: '-60px 0 -70px' }}>
            <div className='d-flex' style={{ position: 'relative', height: '100%' }}>
              <div
                id='sidebar-wrapper'
                onClick={isMobile ? handleClickSideBarWrapper : null}
                style={{
                  transition: 'all ease .3s',
                  overflow: 'hidden',
                  position: isMobile ? 'fixed' : 'relative',
                  top: isMobile ? '95px' : '0',
                  bottom: 0,
                  left: 0,
                  right: dashboardReducer.openSidebar ? '0' : '100%',
                  background: 'rgba(0,0,0,0.2)',
                  zIndex: 20
                }}
              >
                <DashboardSideBar />
              </div>
              <div style={{ flex: 1, width: '100%' }}>
                <DashboardHeader />
                <DashboardContent>{children}</DashboardContent>
              </div>
            </div>

          </div>
          :
          <Container>
            <h3 style={{ textAlign: 'center' }}>You have no permission to access this page!</h3>
          </Container>
        :
        null
      }
    </>
  )
}

export default DashboardPage