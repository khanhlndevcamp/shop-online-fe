import React from 'react'
import AllProducts from '../Components/AllProducts/AllProducts'
import BreadCrumb from '../Components/BreadCrumb/BreadCrumb'
import { Container } from 'reactstrap'

const ProductsListPage = () => {

  const breadCrumbList = [
    {
      url: '/',
      name: 'Home'
    },
    {
      url: '/products',
      name: 'All products'
    }
  ]

  return (
    <Container>
      <BreadCrumb breadCrumbList={breadCrumbList} />
      <AllProducts/>
    </Container>
  )
}

export default ProductsListPage