import React from 'react'
import Checkout from '../Components/Checkout/Checkout'

const CheckOutPage = () => {
  return (
    <Checkout />
  )
}

export default CheckOutPage