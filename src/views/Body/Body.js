import React from 'react'
import { Route, Routes } from 'react-router-dom'
import HomePage from '../../pages/HomePage'
import LoginPage from '../../pages/LoginPage'
import ProductsListPage from '../../pages/ProductsListPage'
import ProductInfoPage from '../../pages/ProductInfoPage'
import CartPage from '../../pages/CartPage'
import CheckOutPage from '../../pages/CheckOutPage'
import OrdersPage from '../../pages/OrdersPage'
import DashboardPage from '../../pages/DashboardPage'
import ContentDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentDashboard/ContentDashboard'
import ContentProducts from '../../Components/Dashboard/DashboardContent/Content/ContentProducts/ContentProducts'
import ContentCustomers from '../../Components/Dashboard/DashboardContent/Content/ContentCustomer/ContentCustomers'
import ContentOrders from '../../Components/Dashboard/DashboardContent/Content/ContentOrder/ContentOrders'
import EditProductDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentProducts/EditProductDashboard/EditProductDashboard'
import AddProductDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentProducts/AddProductDashboard/AddProductDashboard'
import EditOrderDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentOrder/EditOrderDashboard/EditOrderDashboard'
import AddOrderDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentOrder/AddOrderDashboard/AddOrderDashboard'
import AddCustomerDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentCustomer/AddCustomerDashboard/AddCustomerDashboard'
import EditCustomerDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentCustomer/EditCustomerDashboard/EditCustomerDashboard'
import DetailProductDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentProducts/DetailProductDashboard/DetailProductDashboard'
import DetailCustomerDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentCustomer/DetailCustomerDashboard/DetailCustomerDashboard'
import DetailOrderDashboard from '../../Components/Dashboard/DashboardContent/Content/ContentOrder/DetailOrderDashboard/DetailOrderDashboard'
import SignUpPage from '../../pages/SignUpPage'

const Body = ({ heightHeader }) => {
    return (
        <div className="content-wrapper" style={{ paddingTop: `${heightHeader + 60}px`, paddingBottom: '70px' }}>
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='*' element={<HomePage />} />
                <Route path='/products' element={<ProductsListPage />} />
                <Route path='/login' element={<LoginPage />} />
                <Route path='/login/:pageBack' element={<LoginPage />} />
                <Route path='/sign-up' element={<SignUpPage/>}/>
                <Route path='/products/:productId' element={<ProductInfoPage />} />
                <Route path='/cart' element={<CartPage />} />
                <Route path='/checkout' element={<CheckOutPage />} />
                <Route path='/orders' element={<OrdersPage />} />
                <Route path='/dashboard' element={<DashboardPage><ContentDashboard/></DashboardPage>} />
                <Route path='/dashboard/products' element={<DashboardPage><ContentProducts/></DashboardPage>} />
                <Route path='/dashboard/products/add' element={<DashboardPage><AddProductDashboard/></DashboardPage>} />
                <Route path='/dashboard/products/edit/:productId' element={<DashboardPage><EditProductDashboard/></DashboardPage>} />
                <Route path='/dashboard/products/detail/:productId' element={<DashboardPage><DetailProductDashboard/></DashboardPage>} />
                <Route path='/dashboard/customers' element={<DashboardPage><ContentCustomers/></DashboardPage>} />
                <Route path='/dashboard/customers/add' element={<DashboardPage><AddCustomerDashboard/></DashboardPage>} />
                <Route path='/dashboard/customers/edit/:customerId' element={<DashboardPage><EditCustomerDashboard/></DashboardPage>} />
                <Route path='/dashboard/customers/detail/:customerId' element={<DashboardPage><DetailCustomerDashboard/></DashboardPage>} />
                <Route path='/dashboard/orders' element={<DashboardPage><ContentOrders/></DashboardPage>} />
                <Route path='/dashboard/orders/add' element={<DashboardPage><AddOrderDashboard/></DashboardPage>} />
                <Route path='/dashboard/orders/edit/:orderCode/:orderId' element={<DashboardPage><EditOrderDashboard/></DashboardPage>} />
                <Route path='/dashboard/orders/detail/:orderCode' element={<DashboardPage><DetailOrderDashboard/></DashboardPage>} />
            </Routes>
        </div>
    )
}

export default Body