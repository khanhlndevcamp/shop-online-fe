import classNames from "classnames/bind"
import { Container } from "reactstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useEffect, useMemo, useRef, useState } from "react"
import LocalMallIcon from '@mui/icons-material/LocalMall';
import LogoutIcon from '@mui/icons-material/Logout';
import { Link, useNavigate } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { onAuthStateChanged, signOut } from "firebase/auth"
import RoofingIcon from '@mui/icons-material/Roofing';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import DashboardIcon from '@mui/icons-material/Dashboard';
import styles from './Header.module.css'
import auth from "../../app/firebase"
import { changeStatusExpiredDateAccessToken, logInSuccess, logOutAccount, logOutSuccess } from "../../actions/userAction"
import { changeInputSearchHeader, resetFilterCondition } from "../../actions/filterAction"
import { resetStatusChangeCartProduct } from "../../actions/cartAction"
import { fetchProduct } from "../../actions/productAction";

let cx = classNames.bind(styles)

function Header({ updateHeightHeader }) {

    const filterReducer = useSelector(reduxData => reduxData.filterReducer)
    const userStore = useSelector(reduxData => reduxData.userReducer)
    const cartReducer = useSelector(reduxData => reduxData.cartReducer)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const headerWrapper = useRef()
    const [statusClickAvatar, setStatusClickAvatar] = useState(false)
    const [isMobile, setIsMobile] = useState(window.innerWidth < 770)
    const [showModalSearch, setShowModalSearch] = useState(false)


    let heightHeaderComponent = useRef('')
    useEffect(() => {
        heightHeaderComponent.current = headerWrapper.current.offsetHeight
        updateHeightHeader(heightHeaderComponent.current)
    })

    const handleClickAvatar = (e) => {
        setStatusClickAvatar((prev) => !prev)
    }

    const handleClickLogOut = () => {
        setStatusClickAvatar(false)
        //google account
        signOut(auth)
            .then(() => {
                console.log('log out success')
                dispatch(logOutSuccess())
            })
            .catch((err) => {
                console.log('err log out', err)
            })
        //account mongo
        dispatch(logOutAccount())
    }

    const handleClickOrders = (e) => {
        if (cartReducer.statusChangeCartProduct) {
            if (window.confirm('Your change have been not save, go to another page?')) {
                navigate('/orders')
                dispatch(resetStatusChangeCartProduct())
            }
        }
        else {
            navigate('/orders')
        }
    }

    const handleClickBtnSearch = (e) => {
        if (filterReducer.name) {
            navigate(`/products?keyword=${filterReducer.name}`)
        } else {
            navigate(`/products`)
        }
        if (window.innerWidth <= 575) {
            //params: current page, limitItem, filterCondtion
            dispatch(fetchProduct(1, 5, filterReducer))
        }
        else if (window.innerWidth <= 765) {
            dispatch(fetchProduct(1, 8, filterReducer))
        }
        else if (window.innerWidth <= 1200) {
            dispatch(fetchProduct(1, 9, filterReducer))
        }
        else if (window.innerWidth > 1200) {
            dispatch(fetchProduct(1, 8, filterReducer))
        }
    }

    const handleClickBtnSearchMobile = (e) => {
        if (filterReducer.name) {
            navigate(`/products?keyword=${filterReducer.name}`)
        } else {
            navigate(`/products`)
        }
        setShowModalSearch(false)
        if (window.innerWidth <= 575) {
            //params: current page, limitItem, filterCondtion
            dispatch(fetchProduct(1, 5, filterReducer))
        }
        else if (window.innerWidth <= 765) {
            dispatch(fetchProduct(1, 8, filterReducer))
        }
        else if (window.innerWidth <= 1200) {
            dispatch(fetchProduct(1, 9, filterReducer))
        }
        else if (window.innerWidth > 1200) {
            dispatch(fetchProduct(1, 8, filterReducer))
        }
    }

    const handleClickIconSearchAtBottom = (e) => {
        setShowModalSearch(prev => !prev)
    }

    const handleCloseModalSearch = (e) => {
        setShowModalSearch(false)
    }

    const handleChangeInputSearch = (e) => {
        dispatch(resetFilterCondition())
        dispatch(changeInputSearchHeader(e.target.value))
    }

    const handleKeyUpInput = (e) => {
        if (e.keyCode === 13) {
            if (filterReducer.name) {
                navigate(`/products?keyword=${filterReducer.name}`)
            } else {
                navigate(`/products`)
            }
            if (window.innerWidth <= 575) {
                //params: current page, limitItem, filterCondtion
                dispatch(fetchProduct(1, 5, filterReducer))
            }
            else if (window.innerWidth <= 765) {
                dispatch(fetchProduct(1, 8, filterReducer))
            }
            else if (window.innerWidth <= 1200) {
                dispatch(fetchProduct(1, 9, filterReducer))
            }
            else if (window.innerWidth > 1200) {
                dispatch(fetchProduct(1, 8, filterReducer))
            }
        }
    }

    const handleClickDashBoard = () => {
        if (cartReducer.statusChangeCartProduct) {
            if (window.confirm('Your change have been not save, go to another page?')) {
                navigate('/dashboard')
                dispatch(resetStatusChangeCartProduct())
            }
        }
        else {
            navigate('/dashboard')
        }
    }

    const handleKeyUpInputMobile = (e) => {
        if (e.keyCode === 13) {
            if (filterReducer.name) {
                navigate(`/products?keyword=${filterReducer.name}`)
            } else {
                navigate(`/products`)
            }
            setShowModalSearch(false)
            if (window.innerWidth <= 575) {
                //params: current page, limitItem, filterCondtion
                dispatch(fetchProduct(1, 5, filterReducer))
            }
            else if (window.innerWidth <= 765) {
                dispatch(fetchProduct(1, 8, filterReducer))
            }
            else if (window.innerWidth <= 1200) {
                dispatch(fetchProduct(1, 9, filterReducer))
            }
            else if (window.innerWidth > 1200) {
                dispatch(fetchProduct(1, 8, filterReducer))
            }
        }
    }

    const handleClickLogInWhenExpiredLogInSession = (e) => {
        // navigate('/login')
        dispatch(changeStatusExpiredDateAccessToken(false))
    }

    useMemo(() => {
        const tokenLocalStorage = localStorage.getItem('token')
        if (!tokenLocalStorage) { //not login by gg account
            onAuthStateChanged(auth, (user) => {
                if (user) {
                    console.log(user)
                    dispatch(logInSuccess(user))
                } else {
                    dispatch(logInSuccess(null))
                }
            })
        }
    }, [])

    const handleClickLogoBrand = (e) => {
        window.scrollTo(0, 0)
        dispatch(resetFilterCondition())
        if (cartReducer.statusChangeCartProduct) {
            if (window.confirm('Your change have been not save, go to another page?')) {
                navigate('/')
                dispatch(resetStatusChangeCartProduct())
            }
        }
        else {
            navigate('/')
        }
    }

    useEffect(() => {
        window.addEventListener('click', (e) => {
            if (!e.target.closest('#avatar')) {
                setStatusClickAvatar(false)
            }
        })
        window.addEventListener('resize', (e) => {
            setIsMobile(window.innerWidth < 770)
        })
    }, [])
    return (
        <>
            <div style={{
                display: userStore.statusAccessTokenExpired ? 'flex': 'none' , 
                position: 'absolute', 
                top:0, 
                right: 0, 
                left: 0, 
                bottom: 0, 
                zIndex: 9999, 
                background: 'rgba(0,0,0,0.3)'}}>
                <div style={{margin: 'auto', background: 'white', padding: '30px 50px', textAlign: 'center'}}>
                    <h3>Expired log in session</h3>
                    <button
                    onClick={handleClickLogInWhenExpiredLogInSession}
                    style={{
                        border: 'none',
                        borderRadius: '5px',
                        outline: 'none',
                        background: '#269D41',
                        padding: '5px 12px',
                        width: '50%',
                        color: 'white',
                        fontWeight: 500,
                        fontSize: '19px',
                        marginTop: '12px'
                    }}
                    className="hover-basic"
                    >
                        Log in</button>
                </div>
            </div>
            {isMobile ?
                <>
                    <div ref={headerWrapper} className={cx('header-wrapper')}>
                        <Container>
                            <div className="justify-content-center align-items-center d-flex">
                                <div onClick={handleClickLogoBrand} className={cx('brand')}>
                                    <img src="https://xgear.net/wp-content/uploads/elementor/thumbs/cropped-Logo-Web-pz1a16lc97w34f9qk0qor8elt1iokzfxsurlcm35ba.png" alt='logo' />
                                </div>
                            </div>
                        </Container>
                    </div>
                    <div className={cx('header-wrapper-mobile')}>
                        <RoofingIcon style={{ color: 'orange' }} onClick={handleClickLogoBrand} />
                        {userStore.user ?
                            <div style={{ position: 'relative' }}>
                                <div id='avatar' onClick={handleClickAvatar} className={cx('ml-sm', 'avatar-wrapper')}>
                                    <img src={userStore.user.photoURL} className={cx('avatar')} alt='avatar' width={'23px'} />
                                </div>
                                <div style={{ display: statusClickAvatar ? 'block' : 'none' }} className={cx('info-user-mobile')}>
                                    <p style={{ display: userStore.user.role === 'admin' ? 'flex' : 'none' }} onClick={handleClickDashBoard} className={cx('info-user__item')}>
                                        <DashboardIcon style={{ fontSize: '18px', marginRight: '5px' }} /> Dashboard
                                    </p>
                                    <p onClick={handleClickOrders} className={cx('info-user__item')}>
                                        <LocalMallIcon style={{ fontSize: '18px', marginRight: '5px' }} /> Orders
                                    </p>
                                    <p onClick={handleClickLogOut} className={cx('info-user__item')}>
                                        <LogoutIcon style={{ fontSize: '18px', marginRight: '5px' }} /> Log out
                                    </p>
                                </div>
                            </div>

                            :
                            <Link to='/login' >
                                <FontAwesomeIcon icon="fa-regular fa-circle-user" className={cx('ml-sm', 'icon-header')} />
                            </Link>
                        }
                        <SearchIcon
                            onClick={handleClickIconSearchAtBottom}
                            style={{ fontSize: '30px' }}
                            className={cx('ml-sm', 'icon-header', 'hover-basic')}
                        />
                        <div style={{ position: 'fixed', background: 'rgba(0, 0, 0, 0.7)', display: showModalSearch ? 'flex' : 'none', zIndex: 99999, top: 0, right: 0, bottom: '50px', left: 0, transition: 'all ease .3s' }}>
                            <span
                                onClick={handleCloseModalSearch}
                                style={{ position: 'absolute', top: '20px', right: '20px' }}
                            >
                                <CloseIcon style={{ color: 'white' }} />
                            </span>
                            <div style={{ margin: 'auto', width: '90%', maxWidth: '550px', display: 'flex', alignItems: 'center' }}>
                                <input
                                    onKeyUp={handleKeyUpInputMobile}
                                    value={filterReducer.name ? filterReducer.name : ''}
                                    onChange={handleChangeInputSearch}
                                    className={cx('input-search-mobile')}
                                    placeholder='What are you looking for?'
                                />
                                <SearchIcon
                                    onClick={handleClickBtnSearchMobile}
                                    style={{ fontSize: '30px' }}
                                    className={cx('ml-sm', 'icon-header', 'hover-basic')}
                                />
                            </div>
                        </div>
                        <Link to='/cart'>
                            <span className="hover-basic" style={{ position: 'relative' }}>
                                <FontAwesomeIcon icon="fa-solid fa-cart-shopping" className={cx('ml-sm', 'icon-header')} />
                                {cartReducer.totalQuantityProduct > 0 ?
                                    <span className={cx('quanity')}>{cartReducer.totalQuantityProduct}</span>
                                    :
                                    null
                                }
                            </span>
                        </Link>
                    </div>
                </>
                :
                <div ref={headerWrapper} className={cx('header-wrapper')}>
                    <Container>
                        <div className="justify-content-between align-items-center d-flex ">
                            <div onClick={handleClickLogoBrand} className={cx('brand')}>
                                <img src="https://xgear.net/wp-content/uploads/elementor/thumbs/cropped-Logo-Web-pz1a16lc97w34f9qk0qor8elt1iokzfxsurlcm35ba.png" alt='logo' />
                            </div>
                            <div style={{ flex: 1 }}>
                                <div className="d-flex align-items-center justify-content-center">
                                    <input
                                        onKeyUp={handleKeyUpInput}
                                        className={cx('input-search')}
                                        value={filterReducer.name ? filterReducer.name : ''}
                                        onChange={handleChangeInputSearch}
                                        placeholder="What are you looking for?"
                                    />
                                    <SearchIcon
                                        onClick={handleClickBtnSearch}
                                        style={{ fontSize: '30px' }}
                                        className={cx('ml-sm', 'icon-header', 'hover-basic')}
                                    />
                                </div>
                            </div>

                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                {/* <FontAwesomeIcon icon="fa-regular fa-bell" className={cx('ml-sm', 'icon-header')} /> */}
                                <Link to='/cart'>
                                    <span className="hover-basic" style={{ position: 'relative' }}>
                                        <FontAwesomeIcon icon="fa-solid fa-cart-shopping" className={cx('ml-sm', 'icon-header')} />
                                        {cartReducer.totalQuantityProduct > 0 ?
                                            <span className={cx('quanity')}>{cartReducer.totalQuantityProduct}</span>
                                            :
                                            null
                                        }
                                    </span>
                                </Link>

                                {userStore.user ?
                                    <div style={{ position: 'relative' }}>
                                        <div id='avatar' onClick={handleClickAvatar} className={cx('ml-sm', 'avatar-wrapper')}>
                                            <img src={userStore.user.photoURL} className={cx('avatar')} alt='avatar' width={'23px'} />
                                            <p className={cx('name-user')} style={{ display: 'inline-block', margin: 0 }}>Hi <b>{userStore.user.displayName}</b>,</p>
                                        </div>
                                        <div style={{ display: statusClickAvatar ? 'block' : 'none' }} className={cx('info-user')}>
                                            <p style={{ display: userStore.user.role === 'admin' ? 'flex' : 'none' }} onClick={handleClickDashBoard} className={cx('info-user__item')}>
                                                <DashboardIcon style={{ fontSize: '18px', marginRight: '5px' }} /> Dashboard
                                            </p>
                                            <p onClick={handleClickOrders} className={cx('info-user__item')}>
                                                <LocalMallIcon style={{ fontSize: '18px', marginRight: '5px' }} /> Orders
                                            </p>
                                            <p onClick={handleClickLogOut} className={cx('info-user__item')}>
                                                <LogoutIcon style={{ fontSize: '18px', marginRight: '5px' }} /> Log out
                                            </p>
                                        </div>
                                    </div>

                                    :
                                    <Link to='/login' >
                                        <FontAwesomeIcon icon="fa-regular fa-circle-user" className={cx('ml-sm', 'icon-header')} />
                                    </Link>
                                }

                            </div>
                        </div>
                    </Container>
                </div>

            }


        </>
    )
}

export default Header