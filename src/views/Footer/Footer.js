import { Col, Container, Row } from "reactstrap"
import classNames from "classnames/bind"
import styles from './Footer.module.css'
import InforFooter1 from "./InforFooter1/InforFooter1.js"
import InforFooter2 from "./InforFooter2/InforFooter2.js"
import InforFooter3 from "./InforFooter3/InforFooter3.js"
import SocialFooter from './SocialFooter/SocialFooter.js'

let cx = classNames.bind(styles)
function Footer() {

    return (
        <div className={cx('bg-light', 'footer-wrapper')}>
            <Container>
                <Row className='align-items-center'>
                    <Col md='9'>
                        <Row className="justify-content-between">
                            <Col>
                                <InforFooter1 />
                            </Col>
                            <Col>
                                <InforFooter2/>
                            </Col>
                            <Col>
                                <InforFooter3 />
                            </Col>
                        </Row>
                    </Col>
                    <Col md='3' className="justify-content-center d-flex">
                        <SocialFooter/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Footer