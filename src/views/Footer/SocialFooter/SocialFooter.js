import classNames from "classnames/bind"
import styles from '../Footer.module.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useDispatch, useSelector } from "react-redux"
import { resetFilterCondition } from "../../../actions/filterAction"
import { resetStatusChangeCartProduct } from "../../../actions/cartAction"
import { useNavigate } from "react-router-dom"

let cx = classNames.bind(styles)

function SocialFooter() {
    const {cartReducer} = useSelector(reduxData => reduxData)
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const handleClickLogoBrand = (e) => {
        window.scrollTo(0, 0)
        dispatch(resetFilterCondition())
        if (cartReducer.statusChangeCartProduct) {
            if (window.confirm('Your change have been not save, go to another page?')) {
                navigate('/')
                dispatch(resetStatusChangeCartProduct())
            }
        }
        else {
            navigate('/')
        }
    }
    return (
        <div>
            <div className="hover-basic" onClick={handleClickLogoBrand}>
                <img src="https://xgear.net/wp-content/uploads/2022/10/cropped-cropped-cropped-Avata-Xgear-Black.png" alt='logo' />
            </div>
            <div className="d-flex justify-content-between p-1 mt-4">
                <FontAwesomeIcon className={cx('size-lg-icon')} icon="fa-brands fa-facebook" />
                <FontAwesomeIcon className={cx('size-lg-icon')} icon="fa-brands fa-square-instagram" />
                <FontAwesomeIcon className={cx('size-lg-icon')} icon="fa-brands fa-youtube" />
                <FontAwesomeIcon className={cx('size-lg-icon')} icon="fa-brands fa-twitter" />
            </div>
        </div>
    )
}

export default SocialFooter