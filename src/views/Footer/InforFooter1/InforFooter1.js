import classNames from 'classnames/bind'
import styles from '../Footer.module.css'

let cx = classNames.bind(styles)

function InforFooter1 () {

    return (
        <ul className={cx('footer-info')}>
            <li>products</li>
            <li ><span className='hover-underline'>help center</span></li>
            <li ><span className='hover-underline'>Contact Us</span></li>
            <li ><span className='hover-underline'>Warranty</span></li>
            <li ><span className='hover-underline'>Order status</span></li>
        </ul>
    )
}

export default InforFooter1