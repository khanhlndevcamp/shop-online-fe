import { applyMiddleware, combineReducers, legacy_createStore } from "redux";
import userReducer from "../reducers/userReducer";
import productReducer from "../reducers/productRuducer";
import filterReducer from '../reducers/filterReducer'
import cartReducer from "../reducers/cartReducer";
import orderReducer from "../reducers/orderReducer"
import dashboardReducer from "../reducers/dashboardReducer";
import thunk from "redux-thunk";

const appReducers = combineReducers({userReducer, productReducer, filterReducer, cartReducer, orderReducer, dashboardReducer})


const store = legacy_createStore(appReducers, applyMiddleware(thunk))

export default store