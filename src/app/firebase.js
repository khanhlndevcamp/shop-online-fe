import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCNKkDThINbvyzAV2-gghvnxY3_O04T1nA",
  authDomain: "devcamp-shop24h-765a0.firebaseapp.com",
  projectId: "devcamp-shop24h-765a0",
  storageBucket: "devcamp-shop24h-765a0.appspot.com",
  messagingSenderId: "379336055391",
  appId: "1:379336055391:web:458f21f002b9df6e6259ea"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app)

export default auth