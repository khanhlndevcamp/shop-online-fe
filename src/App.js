import 'bootstrap/dist/css/bootstrap.min.css'
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

import './App.css';
import Header from './views/Header/Header';
import Footer from './views/Footer/Footer';
import { useEffect, useState } from 'react'
import Body from './views/Body/Body';
import { useDispatch } from 'react-redux';
import { loadDataLocalStorage } from './actions/cartAction';
import { loadTokenLocalStorage } from './actions/userAction';

library.add(fas, far, fab)

function App() {

  const [heightHeader, setHeightHeader] = useState('')
  const dispatch = useDispatch()

  const updateHeightHeader = (newHeight) => {
    setHeightHeader(newHeight)
  }
  
  useEffect(() => {
    dispatch(loadDataLocalStorage())
    dispatch(loadTokenLocalStorage())
  }, [])

  return (
    <div className="App" >
      <Header updateHeightHeader = {updateHeightHeader}/>      
      <Body heightHeader={heightHeader}/>
      <Footer/>
    </div>
  );
}

export default App;
