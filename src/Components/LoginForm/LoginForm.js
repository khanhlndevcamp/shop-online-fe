import classNames from 'classnames/bind'
import React, { useEffect } from 'react'
import { Container } from 'reactstrap'
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";

import styles from './LoginForm.Style.module.css'
import auth from '../../app/firebase';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { callApiLogInWithAccount, changeInputPassword, changeInputUsername, logInSuccess, resetStatusLogin } from '../../actions/userAction';

const cx = classNames.bind(styles)

const LoginForm = () => {

    const provider = new GoogleAuthProvider();
    const userStore = useSelector(reduxData => reduxData.userReducer)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const { pageBack } = useParams()

    const handleClickSignInWithGoogle = () => {
        signInWithPopup(auth, provider)
            .then(result => {
                dispatch(logInSuccess(result.user))

            })
            .catch(err => {
                dispatch(logInSuccess(null))

            })
    }

    const handleChangeInputUsername = (e) => {
        dispatch(changeInputUsername(e.target.value))
    }

    const handleChangeInputPassword = (e) => {
        dispatch(changeInputPassword(e.target.value))
    }

    const handleClickSignIn = () => {
        dispatch(callApiLogInWithAccount(userStore.username, userStore.password))
    }

    useEffect(() => {
        if(!userStore.statusAccessTokenExpired) {
            if (userStore.user) {
                if (pageBack) {
                    navigate(`/${pageBack}`)
                } else {
                    navigate('/')
                }
            } else {
                if (pageBack) {
                    navigate(`/login/${pageBack}`)
                } else {
                    console.log('testtttttttttttttttttttttttttttttttttt')
                    navigate('/login')
                }
            }
        }

    }, [userStore.user])

    useEffect(() => {
        window.scrollTo(0, 0)

        return () => {
            dispatch(resetStatusLogin())
        }
    }, [])

    return (
        <Container>
            <div style={{ width: '100%' }}>
                <div className={cx('form-wrapper')}>
                    <button onClick={handleClickSignInWithGoogle} className={cx('btn-form', 'btn-first')}>Sign in with Google</button>
                    <div className={cx('horizon')}>
                        <div className={cx('content-horizon')}>
                            <p style={{ marginBottom: '4px' }}>or</p>
                        </div>
                    </div>
                    <input onChange={handleChangeInputUsername} value={userStore.username} className={cx('input')} placeholder='Username' />
                    <input type='password' onChange={handleChangeInputPassword} value={userStore.password} className={cx('input')} placeholder='Password' />
                    <span style={{color:'red', display: userStore.statusLogin ? 'none' : 'block', margin:'15px 0'}}>{'* Invalid username or password!'}</span>
                    <button onClick={handleClickSignIn} className={cx('btn-form', 'btn-second')}>Sign in</button>
                </div>
                <p style={{ textAlign: 'center', color: '#787272', marginTop: '30px', fontWeight: '500', fontSize: '18px' }}>Don't have an account? <Link className={cx('sign-up')} to='/sign-up'>Sign up here</Link></p>
            </div>
        </Container>
    )
}

export default LoginForm