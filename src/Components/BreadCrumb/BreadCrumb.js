import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { useNavigate } from "react-router-dom";
import './BreadCrumb.css'
import { useDispatch } from 'react-redux';
import { resetFilterCondition } from '../../actions/filterAction';
import { resetStatusChangeCartProduct } from '../../actions/cartAction';

const BreadCrumb = ({ breadCrumbList, statusChangeCartProduct }) => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const handleClickBreadCrumItem = (url) => {
        dispatch(resetFilterCondition())
        if(statusChangeCartProduct) {
            if(window.confirm('Your change have been not save, go to another page?')) {
                navigate(url)
                dispatch(resetStatusChangeCartProduct())
            }
        }
        else {
            navigate(url)
        }
    }
    return (
        <Breadcrumb>
            {breadCrumbList.map((item, index) => {
                if (index !== breadCrumbList.length - 1) {
                    return (
                        <BreadcrumbItem key={index}>
                            <span style={{fontWeight: 500, fontSize: '16px'}} onClick={() => handleClickBreadCrumItem(item.url)}>{item.name}</span>
                        </BreadcrumbItem>
                    )
                } else {
                    return <BreadcrumbItem key={index} active>{item.name}</BreadcrumbItem>
                }
            })}
        </Breadcrumb>
    )
}

export default BreadCrumb