import { Pagination } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProduct } from '../../actions/productAction'

const PaginationProducts = ({numberPage, page}) => {
    const dispatch = useDispatch()
    
    const fetchProductInfo = useSelector(reduxData => reduxData.productReducer)
    const filterReducer = useSelector(reduxData => reduxData.filterReducer)
    const handleChangePage = (event, value) => {
        dispatch(fetchProduct(value, fetchProductInfo.itemPerPage, filterReducer))
    }
  return (
    <Pagination onChange={handleChangePage} page={+page} count={numberPage} />
  )
}

export default PaginationProducts