import styled from "styled-components";

export const InforWrapper = styled.div`
    margin: 16px 0;
`
export const LabelCheckOut = styled.label`
    margin-bottom: 12px
`

export const InputCheckOut = styled.input`
    padding: 5px 10px;
    background-color: #F7F7F7;
    border: none;
    border-radius: 3px;
    outline: unset;
    display: block;
    width: 100%
`

export const TextAreaCheckOut = styled.textarea`
    padding: 5px 10px;
    background-color: #F7F7F7;
    border: none;
    border-radius: 3px;
    outline: unset;
    display: block;
    width: 100%
`