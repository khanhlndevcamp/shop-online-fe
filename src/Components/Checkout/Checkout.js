import React, { useEffect, useState } from 'react'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Col, Container, Row } from 'reactstrap'
import { InforWrapper, InputCheckOut, LabelCheckOut, TextAreaCheckOut } from './CheckoutStyledComponent'
import { useDispatch, useSelector } from 'react-redux'
import { callApiCreateOrder, changeValueInputInfoOrder, getInfoOrder, resetStatusCreateOrder } from '../../actions/cartAction'
import { Link, useNavigate } from 'react-router-dom'
import { CircularProgress } from '@mui/material';

const Checkout = () => {
    const { cartReducer, userReducer } = useSelector(reduxData => reduxData)
    const [resultValidateFullName, setResultValidateFullname] = useState(true)
    const [resultValidatePhone, setResultValidatePhone] = useState(true)
    const [resultValidateEmail, setResultValidateEmail] = useState(true)
    const [resultValidateAddress, setResultValidateAddress] = useState(true)
    const navigate = useNavigate()

    const dispatch = useDispatch()

    const handleChangeInput = (e) => {
        dispatch(changeValueInputInfoOrder(e.target.getAttribute('data-key'), e.target.value))
    }

    const handleClickPurchase = (e) => {

        //validate
        if (!cartReducer.infoOrder.fullName) {
            setResultValidateFullname(false)
        }
        else if (isNaN(+cartReducer.infoOrder.phone) ||
            cartReducer.infoOrder.phone.length < 9 ||
            cartReducer.infoOrder.phone.length > 11
        ) {
            setResultValidatePhone(false)
        }
        else if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(cartReducer.infoOrder.email)) {
            setResultValidateEmail(false)
        }
        else if (!cartReducer.infoOrder.address) {
            setResultValidateAddress(false)
        }
        else {
            dispatch(callApiCreateOrder(cartReducer))
        }
    }

    const handleBlurInputFullname = (e) => {
        if (!e.target.value) {
            setResultValidateFullname(false)
        } else {
            setResultValidateFullname(true)
        }
    }

    const handleBlurInputPhone = (e) => {
        if (isNaN(+cartReducer.infoOrder.phone) ||
            cartReducer.infoOrder.phone.length < 9 ||
            cartReducer.infoOrder.phone.length > 11
        ) {
            setResultValidatePhone(false)
        } else {
            setResultValidatePhone(true)
        }
    }

    const handleBlurInputEmail = (e) => {
        if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(cartReducer.infoOrder.email)) {
            setResultValidateEmail(false)
        } else {
            setResultValidateEmail(true)
        }
    }

    const handleBlurInputAddress = (e) => {
        if (!cartReducer.infoOrder.address) {
            setResultValidateAddress(false)
        } else {
            setResultValidateAddress(true)
        }
    }

    useEffect(() => {
        dispatch(getInfoOrder(userReducer.user))
        console.log('checkout js: user', userReducer.user)
        if (userReducer.user === null) {
            navigate('/login/checkout')
        }
        window.scrollTo(0, 0)
    }, [userReducer.user])

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [cartReducer.statusCreateOrder])

    useEffect(() => {
        window.scrollTo(0, 0)
        return () => {
            dispatch(resetStatusCreateOrder())
        }
    }, [])

    return (
        <Container>
            <h3 style={{ textAlign: 'center' }}>Check out</h3>
            <div className='text-center'
                style={{display: cartReducer.callPendingCreateVoucher ? 'block' : 'none'}}
            >
                <CircularProgress />
            </div>
            {cartReducer.statusCreateOrder ?
                <Row className='d-flex justify-content-center'>
                    <div style={{ width: '100%', maxWidth: '600px', border: '1px solid #ccc', borderRadius: '4px', boxShadow: '0 0 5px rgba(0,0,0,0.2)', padding: '20px' }}>
                        <div className='text-center' style={{ marginBottom: '30px' }}>
                            <CheckCircleIcon style={{ fontSize: '70px', color: '#209F85' }} />
                            <h5 style={{ color: '#333' }}>Create order successfully!</h5>
                        </div>
                        <div className='d-flex justify-content-between align-items-center'>
                            <p style={{ fontSize: '18px', fontWeight: 500, color: '#F47E44', margin: 0, paddingRight: '20px' }}>Your status order: Waiting for confirmation</p>
                            <span style={{ color: '#999999', fontSize: '18px', fontWeight: 500 }}>Order code: <span style={{ fontWeight: 700, color: '#7e7b7b' }}>{cartReducer.orderCode}</span></span>
                        </div>
                        <h5 style={{ marginTop: '20px', color: '#333' }}>Thank you for your purchasing!</h5>
                        <p style={{ color: '#777777', textAlign: 'justify', marginBottom: '25px' }}>
                            We have received your order, we will check and confirm the order within 12 business hours. You can click the "Order tracking" button below to see the status of your order
                        </p>
                        <Link to='/orders'>
                            <button
                                className='hover-basic'
                                style={
                                    {
                                        margin: '0 auto',
                                        borderRadius: '5px',
                                        width: '90%',
                                        display: 'block',
                                        border: 'none',
                                        background: '#F46119',
                                        padding: '8px 0',
                                        color: 'white',
                                        fontWeight: 500
                                    }
                                }
                            >
                                Order tracking
                            </button>
                        </Link>
                        <p style={{ color: '#999999', fontSize: '20px', fontWeight: 500, textAlign: 'center', margin: '15px 0' }}>or</p>
                        <Link to={'/products'}>
                            <button
                                className='hover-basic'
                                style={
                                    {
                                        margin: '0 auto',
                                        borderRadius: '5px',
                                        width: '90%',
                                        display: 'block',
                                        border: 'none',
                                        background: '#109ED8',
                                        padding: '8px 0',
                                        color: 'white',
                                        fontWeight: 500
                                    }
                                }
                            >
                                Countinue shopping
                            </button>
                        </Link>
                    </div>
                </Row>
                :
                <Row >
                    <Col className='p-2' lg='6' xs='12' >
                        <div style={{ border: '1px solid #ccc', borderRadius: '5px', padding: '20px 30px' }}>
                            <h5 style={{ marginBottom: '40px' }}>Billing Information</h5>
                            <InforWrapper>
                                <LabelCheckOut>Fullname <span style={{ color: 'red' }}>{'(*)'}</span></LabelCheckOut>
                                <InputCheckOut
                                    onBlur={handleBlurInputFullname}
                                    onChange={handleChangeInput}
                                    data-key='fullName'
                                    value={cartReducer.infoOrder.fullName ? cartReducer.infoOrder.fullName : ''}
                                    placeholder='Your full name' />
                                <span style={{ color: 'red', display: resultValidateFullName ? 'none' : 'block' }}>*Invalid full name</span>
                            </InforWrapper>
                            <InforWrapper>
                                <LabelCheckOut>Phone Number <span style={{ color: 'red' }}>{'(*)'}</span></LabelCheckOut>
                                <InputCheckOut
                                    onBlur={handleBlurInputPhone}
                                    onChange={handleChangeInput}
                                    data-key='phone'
                                    value={cartReducer.infoOrder.phone ? cartReducer.infoOrder.phone : ''}
                                    placeholder='Your phone number' />
                                <span style={{ color: 'red', display: resultValidatePhone ? 'none' : 'block' }}>*Invalid phone {'(9 - 11 characters)'} </span>
                            </InforWrapper>
                            <InforWrapper>
                                <LabelCheckOut>Email <span style={{ color: 'red' }}>{'(*)'}</span></LabelCheckOut>
                                <InputCheckOut
                                    onBlur={handleBlurInputEmail}
                                    onChange={handleChangeInput}
                                    data-key='email'
                                    value={cartReducer.infoOrder.email ? cartReducer.infoOrder.email : ''}
                                    placeholder='Your email' />
                                <span style={{ color: 'red', display: resultValidateEmail ? 'none' : 'block' }}>*Invalid email</span>
                            </InforWrapper>
                            <InforWrapper>
                                <LabelCheckOut>Address <span style={{ color: 'red' }}>{'(*)'}</span></LabelCheckOut>
                                <InputCheckOut
                                    onBlur={handleBlurInputAddress}
                                    onChange={handleChangeInput}
                                    data-key='address'
                                    value={cartReducer.infoOrder.address ? cartReducer.infoOrder.address : ''}
                                    placeholder='Your address' />
                                <span style={{ color: 'red', display: resultValidateAddress ? 'none' : 'block' }}>*Invalid address</span>
                            </InforWrapper>
                            <InforWrapper>
                                <LabelCheckOut>Note</LabelCheckOut>
                                <TextAreaCheckOut
                                    onChange={handleChangeInput}
                                    data-key='note'
                                    value={cartReducer.infoOrder.note ? cartReducer.infoOrder.note : ''}
                                    placeholder='Your note' />
                            </InforWrapper>
                        </div>
                    </Col>
                    <Col className='p-2' lg='6' xs='12'>
                        <div style={{ border: '1px solid #ccc', borderRadius: '5px', padding: '20px 30px' }}>
                            <h5 style={{ marginBottom: '40px' }}>Your order</h5>
                            <Row style={{ marginBottom: '30px' }}>
                                <Col xs='7'><b>Product</b></Col>
                                <Col xs='2' className='text-center' ><b>Quanity</b></Col>
                                <Col xs='3' className='text-center' ><b>Price</b></Col>
                            </Row>
                            {cartReducer.cartProducts.length > 0 ?
                                cartReducer.cartProducts.map((product, index) => {
                                    return (
                                        <Row key={index} className='my-3'>
                                            <Col xs='7' style={{ paddingRight: '20px', color: '#7B5555', fontWeight: 600 }}>{product.name}</Col>
                                            <Col xs='2' className='text-center' style={{ fontWeight: 600 }} >{product.quanityProduct}</Col>
                                            <Col xs='3' className='text-center' style={{ color: '#7B5555', fontWeight: 600 }} >${Math.round(product.quanityProduct * product.promotionPrice).toLocaleString()}</Col>
                                        </Row>
                                    )
                                })
                                :
                                <div className='text-center' style={{ fontWeight: 500 }}>Have no product!</div>
                            }

                        </div>
                        <Row>
                            <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                                <div style={{ background: '#f7f0f0', padding: '20px 45px' }}>
                                    <p><b>Cart total</b></p>
                                    <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                                        <span><b>Total Quanity</b></span>
                                        <span
                                            style={{ color: 'red' }}
                                        >
                                            <b>{cartReducer.totalQuanity} items</b>
                                        </span>
                                    </div>
                                    <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                                        <span><b>Subtotal</b></span>
                                        <span
                                            style={{ color: 'red' }}
                                        >
                                            <b>${cartReducer.subTotalPrice.toLocaleString()}</b>
                                        </span>
                                    </div>
                                    <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                                        <span><b>Discount percent</b></span>
                                        <span
                                            style={{ color: 'red' }}
                                        >
                                            <b>{cartReducer.discountVoucher}%</b>
                                        </span>
                                    </div>
                                    <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                                        <span><b>Discount price</b></span>
                                        <span
                                            style={{ color: 'red' }}
                                        >
                                            <b>- ${Math.round(cartReducer.subTotalPrice * (cartReducer.discountVoucher / 100)).toLocaleString()}</b>
                                        </span>
                                    </div>
                                    <hr />
                                    <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                                        <span><b>Total</b></span>
                                        <span
                                            style={{ color: 'red' }}
                                        >
                                            <b>
                                                ${cartReducer.totalPrice.toLocaleString()}
                                            </b>
                                        </span>
                                    </div>
                                    <button
                                        onClick={handleClickPurchase}
                                        className='hover-basic'
                                        style={
                                            {
                                                width: '100%',
                                                display: 'block',
                                                border: 'none',
                                                background: '#F46119',
                                                padding: '8px 0',
                                                color: 'white',
                                                fontWeight: 500
                                            }
                                        }
                                    >
                                        Purchase
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            }

        </Container>
    )
}

export default Checkout