import Carousel from "./Carousel/Carousel"

import LatestProduct from "./LatestProduct/LatestProduct";
import ViewAll from "./ViewAll/ViewAll";


function ContentHomepage () {
    return (
        <div>
            <Carousel/>
            <LatestProduct/>
            <ViewAll/>
        </div>
    )
}

export default ContentHomepage