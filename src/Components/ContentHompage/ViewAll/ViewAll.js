import classNames from "classnames/bind"
import styles from '../Content.module.css'
import { Link } from "react-router-dom"

let cx = classNames.bind(styles)

function ViewAll() {

    return (
        <div className="text-center">
            <Link to={'/products'}>
                <button className={cx('btn-view-all')}>
                    View All
                </button>
            </Link>
        </div>
    )
}

export default ViewAll