import { Container, Row, Col } from "reactstrap";
import CardProduct from "../../AllProducts/CardProduct/CardProduct";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProduct } from "../../../actions/productAction";
import { CircularProgress } from '@mui/material';


function LatestProduct() {

    const fetchProductInfo = useSelector((reduxData) => reduxData.productReducer)
    const filterReducer = useSelector((reduxData) => reduxData.filterReducer)
    const dispatch = useDispatch()
    useEffect(() => {
        // dispatch(fetchProduct(fetchProductInfo.limitItem))  
        dispatch(fetchProduct(1, 8, filterReducer))

    }, [])

    return (
        <Container style={{ transition: 'all ease 0.3s' }} className="mt-5">
            <h1 className="text-center mb-5">LATEST PRODUCT</h1>
            <div style={{ textAlign: 'center', display: fetchProductInfo.fetchPending ? 'block' : 'none' }}>
                <CircularProgress />
            </div>
            <Row>
                {fetchProductInfo.productsList.map((product, index) => {
                    return (
                        <Col md='3' key={index}>
                            <CardProduct
                                idProduct={product._id}
                                name={product.name}
                                imgUrl={product.imageUrl}
                                buyPrice={product.buyPrice}
                                promotionPrice={product.promotionPrice}
                            />
                        </Col>
                    )
                })}
            </Row>
        </Container>
    )
}

export default LatestProduct