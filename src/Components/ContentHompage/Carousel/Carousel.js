import Slider from "react-slick";
import classNames from "classnames/bind";
import { Row, Col } from "reactstrap";

import styles from '../Content.module.css'
import NextArrow from "./Slider/NextArrow";
import PrevArrow from "./Slider/PrevArrow";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

let cx = classNames.bind(styles)

function Carousel() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 800,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay:true,
    autoplaySpeed: 5000,
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />
  };

  const fetchProductList = useSelector(reduxData => reduxData.productReducer)

  return (
    <div className={cx('carousel-wrapper')}>
      <Slider {...settings}>
        {fetchProductList.productsList.map((product, index) => {
          if (index < 4) {
            return (
              <div key={index}>
                <Row className={cx('slide-wrapper')}>
                  <Col lg='7' md='12' className={cx('slide-info')}>
                    <div className={cx('slide-info-wrapper')}>
                      <p className={cx('sub-title')}>Headphone</p>
                      <h1 className={cx('title')}>{product.name}</h1>
                      <p className={cx('content')}>
                        THX 7.1 Surround Sound Capable: Provides industry-leading audio realism for in-game immersion by providing accurate spatial audio information beyond standard 7.1 surround sound directional cues. Triforce Titanium 50mm High-End Sound Drivers: Outfitted with cutting-edge, 50mm drivers divided into 3 parts for individual tuning of highs, mids, and lows
                      </p>
                      <Link to = {`/products/${product._id}`}>
                        <button className="btn-shop">SHOP NOW</button>
                      </Link>
                    </div>
                  </Col>
                  <Col lg='5' md='12' className={cx('slide-image')}>
                    <img alt='slide' src={product.imageUrl} />
                  </Col>
                </Row>
              </div>
            )
          }
          return null
        })}

      </Slider>
    </div>
  )
}

export default Carousel