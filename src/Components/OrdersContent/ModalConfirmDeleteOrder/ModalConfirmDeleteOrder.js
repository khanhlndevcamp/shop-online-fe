import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import ClearIcon from '@mui/icons-material/Clear';
import { ButtonOrderContentCancel, ButtonOrdersContent } from '../OrdersContentStyledComponent';
import { useDispatch, useSelector } from 'react-redux';
import { deleteOderOfCustomer } from '../../../actions/orderAction';
import { CircularProgress } from '@mui/material';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    maxWidth: '100%',
    bgcolor: 'background.paper',
    border: '1px solid #ccc',
    boxShadow: 24,
    p: 4,
};

export default function ModalConfirmDelete({ currentOrderId, onCloseModalConfirmDelete, openModalConfirmDelete }) {
    const { orderReducer } = useSelector(reduxData => reduxData)
    const dispatch = useDispatch()


    const handleClickConfirmDelete = (e) => {
        //params: idCustomer, idOder, emailCustomer, currentPage, limitItem, orderCode
        dispatch(deleteOderOfCustomer(
            orderReducer.customerInfo._id, 
            currentOrderId, 
            orderReducer.customerInfo.email,
            orderReducer.currentPage,
            orderReducer.itemPerPage,
            orderReducer.valueFilterOrder
            ))

    }
 
    console.log('modal confirm orderReducer.statusDeleteOrder', orderReducer.statusDeleteOrder)
    return (
        <div>
            <Modal
                open={openModalConfirmDelete}
                onClose={onCloseModalConfirmDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <span onClick={onCloseModalConfirmDelete} className='hover-basic' style={{ position: 'absolute', top: '10px', right: '10px' }}><ClearIcon /></span>
                    {/* <div style={{textAlign: 'center', display: orderReducer.statusPendingFetchOrder ? 'block' : 'none'}}><CircularProgress/></div> */}
                    {orderReducer.statusDeleteOrder ?
                        <Typography textAlign={'center'} id="modal-modal-title" variant="h6" component="h2">
                            Delete order successfully!
                        </Typography>
                        :
                        <>
                            <Typography textAlign={'center'} id="modal-modal-title" variant="h6" component="h2">
                                Confirm delete your order
                            </Typography>
                            <div style={{ marginTop: '20px', textAlign: 'center' }}>
                                <ButtonOrderContentCancel onClick={handleClickConfirmDelete}>Confirm</ButtonOrderContentCancel>
                                <ButtonOrdersContent onClick={onCloseModalConfirmDelete}>Cancel</ButtonOrdersContent>
                            </div>
                        </>

                    }

                </Box>
            </Modal>
        </div>
    );
}