import styled from "styled-components";

export const ButtonOrdersContent = styled.button`
    padding: 8px;
    color: #fff;
    font-weight: 500;
    background-color: #4d4d6c;
    border: none;
    border-radius: 4px;
    margin: 5px;
    width: 103px;
    max-width: 100%;
`

export const ButtonOrderContentCancel = styled(ButtonOrdersContent)`
    background-color: red
`

export const ProcessOrderWrapper = styled.div`
    margin-top: 20px;
    border-left: 1px dashed #ccc;
    padding-left: 20px;
    position: relative;
`

export const ProcessOrderItem = styled.div`
    margin: 40px 0;
    display: flex;
    align-items: center;
    position: relative;
    &:last-child {
        margin: 0;
        margin-top: 40px;
        margin-bottom: 20px;
    }
    &:first-child {
        margin: 0;
        margin-top: 30px;
        margin-bottom: 40px;
    }
`

export const IconCompleteProgress = styled.span`
    display: block;
    position: absolute;
    transform: translateY(-50%);
    top: 50%;
    left: -33px;
    width: 25px;
    height: 25px;
    border: none;
    border-radius: 50%;
    background: orange;
    font-weight: 700;
    color: #fff;
    text-align: center
`

export const IconPendingProress = styled(IconCompleteProgress)`
    background: #ccc;

`