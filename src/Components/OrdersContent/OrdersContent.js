import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'reactstrap'
import { ButtonOrderContentCancel, ButtonOrdersContent, IconCompleteProgress, IconPendingProress, ProcessOrderItem, ProcessOrderWrapper } from './OrdersContentStyledComponent'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { changeValueInputOrderCode, fetchOrderCustomer, getCurrentOrderInfoDetail, resetDataOrderCustomer, resetStatusDeleteOrder } from '../../actions/orderAction'
import { CircularProgress } from '@mui/material'
import GradingIcon from '@mui/icons-material/Grading';
import ReceiptIcon from '@mui/icons-material/Receipt';
import HourglassTopIcon from '@mui/icons-material/HourglassTop';
import PaginationOrders from './PaginationOrders/PaginationOrders'
import WidgetsIcon from '@mui/icons-material/Widgets';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import HouseIcon from '@mui/icons-material/House';
import '../../App.css'
import ModalConfirmDeleteOrder from './ModalConfirmDeleteOrder/ModalConfirmDeleteOrder'

const OrdersContent = () => {

    const { userReducer, orderReducer } = useSelector(reduxData => reduxData)
    const [heightElementBillingInfo, setHeightElementBillingInfo] = useState(0)
    const [openModalConfirmDelete, setOpenModalConfirmDelete] = useState(false)
    const [currentOrderId, setCurrentOrderId] = useState('')
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleChangeValueInputOrderCode = (e) => {
        dispatch(changeValueInputOrderCode(e.target.value))
        dispatch(fetchOrderCustomer(userReducer.user.email, 1, orderReducer.itemPerPage, e.target.value))
    }

    const handleClickCancelOrder = (e) => {
        setOpenModalConfirmDelete(true)
        setCurrentOrderId(e.target.getAttribute('data-order-id'))
    }

    const handleCloseModalConfirmDelete = () => {
        setOpenModalConfirmDelete(false)
        dispatch(resetStatusDeleteOrder())
    }

    const handleClickBtnViewMoreOrder = (e) => {
        let listElementInfoOrderDetail = document.querySelectorAll('.info-order-detail')
        let arrElementInfoOrderDetail = Array.from(listElementInfoOrderDetail)
        let elementOrderProcess = document.getElementById(`order-process-wrapper-${e.target.id}`)
        setHeightElementBillingInfo(elementOrderProcess.offsetHeight - 360)
        arrElementInfoOrderDetail.forEach((element) => {
            //show-order just a mark class to trigger effect, don't have any css
            if (e.target.getAttribute('data-order-code') === element.getAttribute('data-order-code')) {
                element.classList.toggle('show-order')
            } else {
                element.classList.remove('show-order')
            }
            if (Array.from(element.classList).includes('show-order')) {
                //waiting for rendering of element to get exact height
                setTimeout(() => {
                    element.style.height = `${element.children[0].offsetHeight + 18}px`
                }, 100)
            } else {
                element.style.height = 0
            }
        })
        let listElementShowInfoOrderDetail = document.querySelectorAll('.info-order-detail.show-order')
        if(listElementShowInfoOrderDetail.length > 0) {
            dispatch(getCurrentOrderInfoDetail(e.target.getAttribute('data-order-id')))
        }

    }

    useEffect(() => {
        if (!userReducer.user) {
            navigate('/login/orders')
        } else {
            //param email, currentPage, limitItem, orderCode
            dispatch(fetchOrderCustomer(userReducer.user.email, 1, orderReducer.itemPerPage, orderReducer.valueFilterOrder))
        }
    }, [userReducer.user])

    useEffect(() => {
        const handleResizeOrdersConent = (e) => {
            let nodeListElementInfoOrderDetail = document.querySelectorAll('.info-order-detail.show-order')

            if (nodeListElementInfoOrderDetail.length > 0) {
                let arrElementInfoOrderDetail = Array.from(nodeListElementInfoOrderDetail)
                let currentOderCode = arrElementInfoOrderDetail[0].getAttribute('data-order-code')
                let elementOrderProcess = document.getElementById(`order-process-wrapper-${currentOderCode}`)
                setHeightElementBillingInfo(elementOrderProcess.offsetHeight - 360)
                arrElementInfoOrderDetail[0].style.height = `${arrElementInfoOrderDetail[0].children[0].offsetHeight + 18}px`
            }
        }
        window.addEventListener('resize', handleResizeOrdersConent)

        window.scrollTo(0, 0)
        return () => {
            dispatch(resetDataOrderCustomer())
            window.removeEventListener('resize', handleResizeOrdersConent)
        }
    }, [])


    return (
        <Container>
            <div className='text-center'>
                <h3 style={{marginBottom: '20px'}}>Orders Tracking</h3>
                <input
                    value={orderReducer.valueFilterOrder ? orderReducer.valueFilterOrder : ''}
                    onChange={handleChangeValueInputOrderCode}
                    placeholder='Enter your order code'
                    style={{
                        padding: '5px 15px',
                        border: '1px solid #ccc',
                        borderRadius: '5px',
                        width: '100%',
                        maxWidth: '800px',
                        outline: 'none'
                    }}
                />
            </div>
            <div 
            className='text-center' 
            style={{margin: '12px 0', display: orderReducer.statusPendingFetchOrder ? 'block' : 'none'}}
            >
                <CircularProgress />
            </div>
            <Row style={{ marginTop: '50px' }}>
                <Col xs='3'>
                    <h5 style={{ margin: '0', fontSize: '18px', color: 'gray' }}>Order Code</h5>
                </Col>
                <Col xs='3'>
                    <h5 style={{ margin: '0', fontSize: '18px', color: 'gray' }}>Total Price</h5>
                </Col>
                <Col xs='3'>
                    <h5 style={{ margin: '0', fontSize: '18px', color: 'gray' }}>Create Date</h5>
                </Col>
                <Col xs='3'>
                    <h5 style={{ margin: '0', fontSize: '18px', color: 'gray' }}>Details</h5>
                </Col>
                <hr style={{ margin: '15px 0' }} />
            </Row>

            {/* <div className='text-center' style={{ display: orderReducer.statusPendingFetchOrder ? 'block' : 'none' }}>
                <CircularProgress />
            </div> */}

            {orderReducer.ordersOfCustomer.length > 0 ?
                <>
                    {
                        orderReducer.ordersOfCustomer.map((order) => {
                            let createDate = new Date(order.createdAt)
                            return (
                                <Row key={order.orderCode}>
                                    <Col xs='3' className='d-flex align-items-center'>
                                        <h5 style={{ margin: '0', fontSize: '18px' }}>{order.orderCode}</h5>
                                    </Col>
                                    <Col xs='3' className='d-flex align-items-center'>
                                        <h5 style={{ margin: '0', fontSize: '18px' }}>${order.cost}</h5>
                                    </Col>
                                    <Col xs='3' className='d-flex align-items-center'>
                                        <h5 style={{ margin: '0', fontSize: '18px' }}>{`${createDate.getDate()}-${createDate.getMonth()}-${createDate.getFullYear()}`}</h5>
                                    </Col>
                                    <Col xs='3'>
                                        <ButtonOrdersContent
                                            data-order-code={order.orderCode}
                                            data-order-id={order._id}
                                            onClick={handleClickBtnViewMoreOrder}
                                            id={order.orderCode}
                                            className='hover-basic'>
                                            View more
                                        </ButtonOrdersContent>
                                        <ButtonOrderContentCancel
                                            data-order-code={order.orderCode}
                                            data-order-id={order._id}
                                            onClick={handleClickCancelOrder}
                                            className='hover-basic'
                                            style={{display: order.step > 2 ? 'none' : 'block'}}
                                            >
                                            Cancel
                                        </ButtonOrderContentCancel>
                                        <ButtonOrderContentCancel
                                            data-order-code={order.orderCode}
                                            data-order-id={order._id}
                                            style={{display: order.step > 2 ? 'block' : 'none', background: '#ccc'}}
                                            >
                                            Cancel
                                        </ButtonOrderContentCancel>
                                    </Col>
                                    <Col xs='12'
                                        className='info-order-detail'
                                        data-order-code={order.orderCode}
                                        style={{
                                            display: 'block',
                                            height: 0,
                                            overflow: 'hidden',
                                            transition: 'height ease 0.3s',
                                            padding: '0 30px'
                                        }}
                                    >
                                        <Row
                                            style={{
                                                marginTop: '15px',
                                                padding: '20px 5px',
                                                background: '#fff',
                                                border: '1px solid #ccc',
                                                borderRadius: '3px',
                                                boxShadow: '0 0 5px rgba(0,0,0,0.2)'
                                            }}>
                                            <div style={{textAlign: 'center', display: orderReducer.statusPedingGetInfoCurrentOrder ? 'block' : 'none'}} ><CircularProgress/></div>

                                            <Col lg='6' xs='12' style={{ padding: '20px 15px', }} >
                                                <div id={`order-process-wrapper-${order.orderCode}`} style={{ border: '1px solid #ccc', width: '100%', padding: '12px 30px' }} >
                                                    <h5>Order Process</h5>
                                                    <ProcessOrderWrapper>
                                                        <span
                                                            style={{
                                                                display: 'block',
                                                                borderLeft: '1px dashed #ff1100',
                                                                position: 'absolute',
                                                                top: 0,
                                                                left: '-1px',
                                                                height: orderReducer.infoCurrentOrder.order.progressPercent,
                                                            }}
                                                        ></span>
                                                        <ProcessOrderItem>
                                                            <ReceiptIcon style={{ fontSize: '50px' }} />
                                                            <div style={{ marginLeft: '15px' }}>
                                                                <span style={{ display: 'block', fontWeight: 500, fontSize: '18px' }}>
                                                                    Created Order
                                                                </span>
                                                                <span style={{ display: 'block', fontSize: '15px', color: 'rgb(153 130 130)' }}>
                                                                    Your order has been created
                                                                </span>
                                                            </div>
                                                            {orderReducer.infoCurrentOrder.order.step >= 1 ?
                                                                <IconCompleteProgress>
                                                                    &#10003;
                                                                </IconCompleteProgress>
                                                                :
                                                                <IconPendingProress />
                                                            }

                                                        </ProcessOrderItem>
                                                        <ProcessOrderItem>

                                                            <HourglassTopIcon style={{ fontSize: '50px' }} />
                                                            <div style={{ marginLeft: '15px' }}>
                                                                <span style={{ display: 'block', fontWeight: 500, fontSize: '18px' }}>
                                                                    Waiting for confirmation
                                                                </span>
                                                                <span style={{ display: 'block', fontSize: '15px', color: 'rgb(153 130 130)' }}>
                                                                    Your order on waiting for confirmation
                                                                </span>
                                                            </div>
                                                            {orderReducer.infoCurrentOrder.order.step >= 2 ?
                                                                <IconCompleteProgress>
                                                                    &#10003;
                                                                </IconCompleteProgress>
                                                                :
                                                                <IconPendingProress />
                                                            }
                                                        </ProcessOrderItem>
                                                        <ProcessOrderItem>
                                                            <GradingIcon style={{ fontSize: '50px' }} />
                                                            <div style={{ marginLeft: '15px' }}>
                                                                <span style={{ display: 'block', fontWeight: 500, fontSize: '18px' }}>
                                                                    Confirmed
                                                                </span>
                                                                <span style={{ display: 'block', fontSize: '15px', color: 'rgb(153 130 130)' }}>
                                                                    Your order has been confirmed
                                                                </span>
                                                            </div>
                                                            {orderReducer.infoCurrentOrder.order.step >= 3 ?
                                                                <IconCompleteProgress>
                                                                    &#10003;
                                                                </IconCompleteProgress>
                                                                :
                                                                <IconPendingProress />
                                                            }
                                                        </ProcessOrderItem>
                                                        <ProcessOrderItem>
                                                            <WidgetsIcon style={{ fontSize: '50px' }} />
                                                            <div style={{ marginLeft: '15px' }}>
                                                                <span style={{ display: 'block', fontWeight: 500, fontSize: '18px' }}>
                                                                    Packed
                                                                </span>
                                                                <span style={{ display: 'block', fontSize: '15px', color: 'rgb(153 130 130)' }}>
                                                                    Your order has been packed
                                                                </span>
                                                            </div>
                                                            {orderReducer.infoCurrentOrder.order.step >= 4 ?
                                                                <IconCompleteProgress>
                                                                    &#10003;
                                                                </IconCompleteProgress>
                                                                :
                                                                <IconPendingProress />
                                                            }
                                                        </ProcessOrderItem>
                                                        <ProcessOrderItem>
                                                            <LocalShippingIcon style={{ fontSize: '50px' }} />
                                                            <div style={{ marginLeft: '15px' }}>
                                                                <span style={{ display: 'block', fontWeight: 500, fontSize: '18px' }}>
                                                                    Shipping
                                                                </span>
                                                                <span style={{ display: 'block', fontSize: '15px', color: 'rgb(153 130 130)' }}>
                                                                    Now, your order is on the way. Our partner will soon deliver the product to you
                                                                </span>
                                                            </div>
                                                            {orderReducer.infoCurrentOrder.order.step >= 5 ?
                                                                <IconCompleteProgress>
                                                                    &#10003;
                                                                </IconCompleteProgress>
                                                                :
                                                                <IconPendingProress />
                                                            }
                                                        </ProcessOrderItem>
                                                        <ProcessOrderItem>
                                                            <HouseIcon style={{ fontSize: '50px', }} />
                                                            <div style={{ marginLeft: '15px' }}>
                                                                <span style={{ display: 'block', fontWeight: 500, fontSize: '18px' }}>
                                                                    Delivered
                                                                </span>
                                                                <span style={{ display: 'block', fontSize: '15px', color: 'rgb(153 130 130)' }}>
                                                                    Your order has been done
                                                                </span>
                                                            </div>
                                                            {orderReducer.infoCurrentOrder.order.step >= 6 ?
                                                                <IconCompleteProgress>
                                                                    &#10003;
                                                                </IconCompleteProgress>
                                                                :
                                                                <IconPendingProress />
                                                            }
                                                        </ProcessOrderItem>
                                                    </ProcessOrderWrapper>
                                                </div>
                                            </Col>
                                            <Col lg='6' xs='12' style={{ padding: '20px 15px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }} >
                                                <div style={{ border: '1px solid #ccc', width: '100%', height: '350px', overflow: 'auto', padding: '12px 30px' }} >
                                                    <h5 style={{ marginBottom: '25px' }}>Order Information</h5>
                                                    <Row style={{ marginBottom: '15px' }}>
                                                        <Col xs='6' ><b>Product</b></Col>
                                                        <Col xs='3' className='text-center' ><b>Quanity</b></Col>
                                                        <Col xs='3' className='text-center' ><b>Price</b></Col>
                                                    </Row>
                                                    {orderReducer.infoCurrentOrder.orderDetails.length > 0 ?
                                                        orderReducer.infoCurrentOrder.orderDetails.map((orderDetail, index) => {
                                                            return (
                                                                <Row key={index} className='my-3'>
                                                                    <Col xs='6' style={{ paddingRight: '20px', color: '#7B5555', fontWeight: 600 }}>{orderDetail.product.name}</Col>
                                                                    <Col xs='3' className='text-center' style={{ fontWeight: 600 }} >{orderDetail.quantity}</Col>
                                                                    <Col
                                                                        xs='3'
                                                                        className='text-center'
                                                                        style={{ color: '#7B5555', fontWeight: 600 }}
                                                                    >
                                                                        ${Math.round(+orderDetail.quantity * +orderDetail.product.promotionPrice)}
                                                                    </Col>
                                                                </Row>
                                                            )
                                                        })
                                                        :
                                                        null
                                                    }

                                                    <Row>
                                                        <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                                                            <div style={{ background: '#f7f0f0', padding: '20px' }}>
                                                                <p><b>Cart total</b></p>
                                                                <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                                                                    <Col style={{ padding: 0 }} xs='6'><b>Total Quantity</b></Col>
                                                                    <Col
                                                                        xs='6'
                                                                        style={{ color: 'red', padding: 0, textAlign: 'right' }}
                                                                    >
                                                                        <b>{orderReducer.infoCurrentOrder.totalQuantity} items</b>
                                                                    </Col>
                                                                </Row>

                                                                <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                                                                    <Col style={{ padding: 0 }} xs='6'><b>Discount percent</b></Col>
                                                                    <Col
                                                                        xs='6'
                                                                        style={{ color: 'red', padding: 0, textAlign: 'right' }}
                                                                    >
                                                                        <b>{orderReducer.infoCurrentOrder.discountPercent}%</b>
                                                                    </Col>
                                                                </Row>
                                                                <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                                                                    <Col style={{ padding: 0 }} xs='6'><b>Discount price</b></Col>
                                                                    <Col
                                                                        xs='6'
                                                                        style={{ color: 'red', padding: 0, textAlign: 'right' }}
                                                                    >
                                                                        <b>- ${orderReducer.infoCurrentOrder.discountPrice}</b>
                                                                    </Col>
                                                                </Row>

                                                                <hr />
                                                                <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                                                                    <Col style={{ padding: 0 }} xs='6'><b>Total</b></Col>
                                                                    <Col
                                                                        xs='6'
                                                                        style={{ color: 'red', padding: 0, textAlign: 'right' }}
                                                                    >
                                                                        <b>${orderReducer.infoCurrentOrder.totalPriceOfOrder}</b>
                                                                    </Col>
                                                                </Row>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>
                                                <div style={{ marginTop: '10px', border: '1px solid #ccc', width: '100%', height: `${heightElementBillingInfo}px`, padding: '12px 12px 12px 30px', overflow: 'auto' }}>
                                                    <h5 style={{ marginBottom: '25px' }}>Billing Information</h5>
                                                    <p>
                                                        <b>Fullname:</b> {orderReducer.infoCurrentOrder.order.customerName}
                                                    </p>
                                                    <p>
                                                        <b>Phone number:</b> {orderReducer.infoCurrentOrder.order.phoneNumber}
                                                    </p>
                                                    <p>
                                                        <b>Email:</b> {orderReducer.infoCurrentOrder.order.email}
                                                    </p>
                                                    <p>
                                                        <b>Address:</b> {orderReducer.infoCurrentOrder.order.address}
                                                    </p>
                                                    <p>
                                                        <b>Note:</b> {orderReducer.infoCurrentOrder.order.note}
                                                    </p>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <hr style={{ margin: '15px 0' }} />
                                </Row>
                            )
                        })
                    }
                    <Row>
                        <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }} className='d-flex justify-content-end'>
                            <PaginationOrders numberPage={orderReducer.numberPage} page={orderReducer.currentPage} />
                        </Col>
                    </Row>
                </>

                :
                <div className='text-center'>
                    <h5>Have no orders!</h5>
                </div>
            }
            <ModalConfirmDeleteOrder currentOrderId={currentOrderId} openModalConfirmDelete={openModalConfirmDelete} onCloseModalConfirmDelete={handleCloseModalConfirmDelete} />
        </Container>
    )
}

export default OrdersContent