import { Pagination } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { changePageOrdersCustomer, fetchOrderCustomer } from '../../../actions/orderAction'

const PaginationOrders = ({numberPage, page}) => {
  const { userReducer, orderReducer } = useSelector(reduxData => reduxData)
    const dispatch = useDispatch()
    const handleChangePage = (event, value) => {
        // dispatch(changePageOrdersCustomer(value))
            //param email, currentPage, limitItem, orderCode
        dispatch(fetchOrderCustomer(userReducer.user.email, value, orderReducer.itemPerPage, orderReducer.valueFilterOrder))
    }
    console.log('Page pagination', page)
  return (
    <Pagination onChange={handleChangePage} page={+page} count={numberPage ? numberPage : 1} />
  )
}

export default PaginationOrders