import React from 'react'
// import ContentDashboard from './Content/ContentDashboard/ContentDashboard'
// import ContentProducts from './Content/ContentProducts/ContentProducts'
// import ContentOrders from './Content/ContentOrder/ContentOrders'
// import ContentCustomers from './Content/ContentCustomer/ContentCustomers'
import { useSelector } from 'react-redux'
import { useState } from 'react'
import { useEffect } from 'react'

const DashboardContent = ({children}) => {
  const [isMobile, setIsMobile] = useState(window.innerWidth < 1000)
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  // const dispatch = useDispatch()

  useEffect(() => {
    const handleResizeDashboardHeader = (e) => {
      setIsMobile(window.innerWidth < 1000)
    }
    window.addEventListener('resize', handleResizeDashboardHeader)

    return () => {
      window.removeEventListener('resize', handleResizeDashboardHeader)
    }
  }, [])
  return (
    <div
      style={{
        padding: '15px 20px',
        background: '#F4F6F9',
        marginTop: '50px',
        marginLeft: dashboardReducer.openSidebar && !isMobile ? '230px' : '0px',
        transition: 'all ease 0.3s'
      }}>
      {children}
    </div>
  )
}

export default DashboardContent