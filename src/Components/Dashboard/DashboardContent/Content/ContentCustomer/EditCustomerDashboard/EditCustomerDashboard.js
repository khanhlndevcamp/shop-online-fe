import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { Col, Row } from 'reactstrap'
import { ButtonAction, InputDashboardContent, SelectDashboardContent } from '../../../DashboardContentStyleComponent'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import { callApiAddCusomerDashboard, callApiGetInfoAcccountToEditCusomerDashboard, callApiUpdateCustomerDashboard, changeInputAddCustomerDashboard, changeSelectRoleAddCustomerDashboard, changeStatusChangeInputEmailCustomerDashboard, changeStatusChangeInputPhoneCustomerDashboard, changeStatusChangeInputUsernameCustomerDashboard, checkDupEmailAddcustomerDashboard, checkDupPhoneAddcustomerDashboard, checkDupUsernameAddcustomerDashboard, resetDataAddCustomerDashboard } from '../../../../../../actions/dashboardAction'
import { CircularProgress } from '@mui/material'

const EditCustomerDashboard = () => {
  const { customerId } = useParams()
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  const [resultValidateUsername, setResultValidateUsername] = useState(true)
  const [resultValidatePhone, setResultValidatePhone] = useState(true)
  const [resultValidateEmail, setResultValidateEmail] = useState(true)
  const [resultValidatePassword, setResultValidatePassword] = useState(true)
  const [resultValidateFullname, setResultValidateFullname] = useState(true)
  const [resultValidateRole, setResultValidateRole] = useState(true)

  const handleClickGoBack = (e) => {
    navigate('/dashboard/customers')
  }

  const handleChangeInputAddCustomerDashboard = (e) => {
    dispatch(changeInputAddCustomerDashboard(e.target.value, e.target.getAttribute('data-type-input')))
  }

  const handleChangeInputUsername = (e) => {
    dispatch(changeInputAddCustomerDashboard(e.target.value, e.target.getAttribute('data-type-input')))
    //params: username
    if (e.target.value) { //user !== ''
      //params: username, currentUsername
      dispatch(checkDupUsernameAddcustomerDashboard(e.target.value, dashboardReducer.dataOfContentCustomer.infoEditCustomer.currentUsername))
      //params: status
      dispatch(changeStatusChangeInputUsernameCustomerDashboard(true))
      setResultValidateUsername(true)
    } else {
      dispatch(changeStatusChangeInputUsernameCustomerDashboard(false))
      setResultValidateUsername(false)
    }
  }

  const handleChangeInputPhone = (e) => {
    dispatch(changeInputAddCustomerDashboard(e.target.value, e.target.getAttribute('data-type-input')))
    //params: Phone
    if (
      isNaN(+e.target.value) ||
      e.target.value.length < 9 ||
      e.target.value.length > 11
    ) {
      dispatch(changeStatusChangeInputPhoneCustomerDashboard(false))
      setResultValidatePhone(false)
    } else {
      //params: phone, currentPhone
      dispatch(checkDupPhoneAddcustomerDashboard(e.target.value, dashboardReducer.dataOfContentCustomer.infoEditCustomer.currentPhone))
      dispatch(changeStatusChangeInputPhoneCustomerDashboard(true))
      setResultValidatePhone(true)
    }
  }

  const handleChangeInputEmail = (e) => {
    dispatch(changeInputAddCustomerDashboard(e.target.value, e.target.getAttribute('data-type-input')))
    //params: Email
    if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(e.target.value)) {
      dispatch(changeStatusChangeInputEmailCustomerDashboard(false))
      setResultValidateEmail(false)
    } else {
      //params: email, currentEmail
      dispatch(checkDupEmailAddcustomerDashboard(e.target.value, dashboardReducer.dataOfContentCustomer.infoEditCustomer.currentEmail))
      dispatch(changeStatusChangeInputEmailCustomerDashboard(true))
      setResultValidateEmail(true)
    }
  }
  const handleBlurInputAddCustomer = (e) => {
    const typeInput = e.target.getAttribute('data-type-input')
    switch (typeInput) {
      case 'username':
        if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.username === '') {
          setResultValidateUsername(false)
        } else {
          setResultValidateUsername(true)
        }
        break;
      case 'phone':
        if (
          isNaN(+dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone) ||
          dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone.length < 9 ||
          dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone.length > 11
        ) {
          setResultValidatePhone(false)
        } else {
          setResultValidatePhone(true)
        }
        break;
      case 'email':
        if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(dashboardReducer.dataOfContentCustomer.infoAddCustomer.email)) {
          setResultValidateEmail(false)
        } else {
          setResultValidateEmail(true)
        }
        break;
      case 'password':
        if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.password !== '' &&
          dashboardReducer.dataOfContentCustomer.infoAddCustomer.password.length < 4) {
          setResultValidatePassword(false)
        } else {
          setResultValidatePassword(true)
        }
        break;
      case 'fullName':
        if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.fullName === '') {
          setResultValidateFullname(false)
        } else {
          setResultValidateFullname(true)
        }
        break;
      default:
        break;
    }
  }

  const handleChangeSelectRoleAddProduct = (e) => {
    dispatch(changeSelectRoleAddCustomerDashboard(e.target.value))

    if (e.target.value !== '') {
      setResultValidateRole(true)
    }
  }

  const handleClickBtnAddCustomer = () => {
    let resultValidateUsernameBlock = false
    let resultValidatePasswordBlock = false
    let resultValidateFullnameBlock = false
    let resultValidatePhoneBlock = false
    let resultValidateEmailBlock = false
    let resultValidateRoleBlock = false
    if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.username === '') {
      setResultValidateUsername(false)
      resultValidateUsernameBlock = false
    } else {
      setResultValidateUsername(true)
      resultValidateUsernameBlock = true
    }

    if (
      isNaN(+dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone) ||
      dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone.length < 9 ||
      dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone.length > 11
    ) {
      setResultValidatePhone(false)
      resultValidatePhoneBlock = false
    } else {
      setResultValidatePhone(true)
      resultValidatePhoneBlock = true
    }

    if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(dashboardReducer.dataOfContentCustomer.infoAddCustomer.email)) {
      setResultValidateEmail(false)
      resultValidateEmailBlock = false
    } else {
      setResultValidateEmail(true)
      resultValidateEmailBlock = true
    }

    if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.password !== '' &&
      dashboardReducer.dataOfContentCustomer.infoAddCustomer.password.length < 4
    ) {
      setResultValidatePassword(false)
      resultValidatePasswordBlock = false
    } else {
      setResultValidatePassword(true)
      resultValidatePasswordBlock = true
    }

    if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.fullName === '') {
      setResultValidateFullname(false)
      resultValidateFullnameBlock = false
    } else {
      setResultValidateFullname(true)
      resultValidateFullnameBlock = true
    }

    if (dashboardReducer.dataOfContentCustomer.infoAddCustomer.role === '') {
      setResultValidateRole(false)
      resultValidateRoleBlock = false
    } else {
      setResultValidateRole(true)
      resultValidateRoleBlock = true
    }

    if (
      resultValidateUsernameBlock && resultValidatePasswordBlock &&
      resultValidateFullnameBlock && resultValidatePhoneBlock &&
      resultValidateEmailBlock && resultValidateRoleBlock &&
      dashboardReducer.dataOfContentCustomer.resultCheckDupEmail &&
      dashboardReducer.dataOfContentCustomer.resultCheckDupPhone &&
      dashboardReducer.dataOfContentCustomer.resultCheckDupUsername
    ) {
      console.log('call api update customer')
      // dispatch(callApiAddCusomerDashboard(dashboardReducer.dataOfContentCustomer.infoAddCustomer))
      //params: infoCustomer, account id
      dispatch(callApiUpdateCustomerDashboard(dashboardReducer.dataOfContentCustomer.infoAddCustomer, dashboardReducer.dataOfContentCustomer.infoEditCustomer.currentAccountId))
    }
  }

  console.log('content add customer dashboard:', dashboardReducer.dataOfContentCustomer)
  useEffect(() => {
    const handleResizeAddProductDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeAddProductDashboard)
    dispatch(callApiGetInfoAcccountToEditCusomerDashboard(customerId))
    return () => {
      window.removeEventListener('resize', handleResizeAddProductDashboard)
      dispatch(resetDataAddCustomerDashboard())
    }
  }, [])
  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Edit Customer</h3>
      <div
        className='text-center'
        style={{ display: dashboardReducer.dataOfContentCustomer.fetchPendingContentCustomerDashboard ? 'block' : 'none' }}
      >
        <CircularProgress />
      </div>
      <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Usename:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddCustomer}
              onChange={handleChangeInputUsername}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.username}
              data-type-input='username'
              placeholder='Enter username'
              disabled={true}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{
              color: dashboardReducer.dataOfContentCustomer.resultCheckDupUsername ? 'green' : 'red',
              display: resultValidateUsername && dashboardReducer.dataOfContentCustomer.statusChangeInputUsernameAddCustomer ? 'block' : 'none'
            }}>
              {dashboardReducer.dataOfContentCustomer.resultCheckDupUsername ?
                'Valid username'
                :
                'Username already exist'
              }
            </span>
            <span style={{ color: 'red', display: resultValidateUsername ? 'none' : 'block' }}>
              Enter username
            </span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0', display: 'none' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>New password:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddCustomer}
              onChange={handleChangeInputAddCustomerDashboard}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.password}
              type='password'
              data-type-input='password'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%',
              }}
            />
            {/* <span style={{ color: 'red', display: resultValidatePassword ? 'none' : 'block' }}>Invalid password</span> */}
          </Col>
        </Row>

        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Phone number:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onChange={handleChangeInputPhone}
              onBlur={handleBlurInputAddCustomer}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone}
              data-type-input='phone'
              placeholder='Enter phone number'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{
              color: dashboardReducer.dataOfContentCustomer.resultCheckDupPhone ? 'green' : 'red',
              display: resultValidatePhone && dashboardReducer.dataOfContentCustomer.statusChangeInputPhoneAddCustomer ? 'block' : 'none'
            }}>
              {dashboardReducer.dataOfContentCustomer.resultCheckDupPhone ?
                'Valid phone number'
                :
                'Phone number already exist'
              }
            </span>
            <span style={{ color: 'red', display: resultValidatePhone ? 'none' : 'block' }}>
              Invalid phone number
            </span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Email:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onChange={handleChangeInputEmail}
              onBlur={handleBlurInputAddCustomer}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.email}
              data-type-input='email'
              placeholder='Enter email'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{
              color: dashboardReducer.dataOfContentCustomer.resultCheckDupEmail ? 'green' : 'red',
              display: resultValidateEmail && dashboardReducer.dataOfContentCustomer.statusChangeInputEmailAddCustomer ? 'block' : 'none'
            }}>
              {dashboardReducer.dataOfContentCustomer.resultCheckDupEmail ?
                'Valid email'
                :
                'Email already exist'
              }
            </span>
            <span style={{ color: 'red', display: resultValidateEmail ? 'none' : 'block' }}>
              Invalid email
            </span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Full name:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddCustomer}
              onChange={handleChangeInputAddCustomerDashboard}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.fullName}
              data-type-input='fullName'
              placeholder='Enter full name'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateFullname ? 'none' : 'block' }}>Enter name</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Role:</label>
          </Col>
          <Col md='8' xs='12'>
            <SelectDashboardContent
              onChange={handleChangeSelectRoleAddProduct}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.role}
            >
              <option value=''>Choose role</option>
              <option value='user'>User</option>
              <option value='admin'>Admin</option>
            </SelectDashboardContent>
            <span style={{ color: 'red', display: resultValidateRole ? 'none' : 'block' }}>Choose role</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Avatar URL:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onChange={handleChangeInputAddCustomerDashboard}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.imageURL}
              data-type-input='imageURL'
              placeholder='Enter avatar URL'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>

        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Address:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onChange={handleChangeInputAddCustomerDashboard}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.address}
              // onBlur={handleBlurInputAddProduct}
              // onChange={handleChangeInputAddProduct}
              // value={dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice}
              data-type-input='address'
              placeholder='Enter address'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>

        <div style={{
          display: dashboardReducer.dataOfContentCustomer.fetchPendingContentCustomerDashboard === false &&
            dashboardReducer.dataOfContentCustomer.statusChangeInputAddCustomer === false &&
            dashboardReducer.dataOfContentCustomer.statusChangeInputEmailAddCustomer === false &&
            dashboardReducer.dataOfContentCustomer.statusChangeInputPhoneAddCustomer === false &&
            dashboardReducer.dataOfContentCustomer.statusChangeInputUsernameAddCustomer === false
            ? 'flex' : 'block',
          justifyContent: 'space-between',
          alignItems: 'center',
          textAlign: 'right',
          padding: '0 12px'
        }}>
          <div

            style={{
              display:
                dashboardReducer.dataOfContentCustomer.fetchPendingContentCustomerDashboard === false &&
                  dashboardReducer.dataOfContentCustomer.statusChangeInputAddCustomer === false &&
                  dashboardReducer.dataOfContentCustomer.statusChangeInputEmailAddCustomer === false &&
                  dashboardReducer.dataOfContentCustomer.statusChangeInputPhoneAddCustomer === false &&
                  dashboardReducer.dataOfContentCustomer.statusChangeInputUsernameAddCustomer === false
                  ? 'flex' : 'none',
              margin: 0,
              color: dashboardReducer.dataOfContentCustomer.statusCallApiAddCustomer ? 'green' : 'red',
              fontSize: '18px',
              fontWeight: 500
            }}
            className='align-items-center'>

            {dashboardReducer.dataOfContentCustomer.statusCallApiAddCustomer ?
              <>
                <CheckCircleIcon style={{ marginRight: '8px' }} />
                Update customer successfully
              </>
              :
              <>
                <CancelIcon style={{ marginRight: '8px' }} />
                Update customer fail
              </>
            }
          </div>

          <ButtonAction
            onClick={handleClickBtnAddCustomer}
            className='hover-basic'
            bgColor='#FFA500'
            color='white'
          >
            Update
          </ButtonAction>
        </div>

      </div>
    </div>
  )
}

export default EditCustomerDashboard