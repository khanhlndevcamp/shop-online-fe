import React from 'react'
import { Col, Row } from 'reactstrap'
import { ButtonAction, SelectDashboardContent } from '../../DashboardContentStyleComponent'
import { useState } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { callApiUpdateStatusCustomerDashboard, changeInputFilterEmailCustomerDashboard, changeInputFilterPhoneNumberCustomerDashboard, changeSelectSortCustomerDashboard, changeSelectStatusCustomerFilter, changeStatusCallApiDeleteCustomerDashboard, fetchDataOfContentCustomerDashboard } from '../../../../../actions/dashboardAction'
import PaginationContentCustomerDashboard from './PaginationContentCustomerDashboard'
import { useNavigate } from 'react-router-dom'
import ModalConfirmDeleteCustomerDashboard from './DeleteCustomerDashboard/ModalConfirmDeleteCustomer'
import { CircularProgress } from '@mui/material'
const ContentCustomers = () => {
  const [isSmallMobile, setIsSmallMobile] = useState(window.innerWidth < 470)
  // const [isMobile, setIsMobile] = useState(window.innerWidth < 670)
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const [openModalConfirmDelete, setOpenModalConfirmDelete] = useState(false)
  const [currentCustomerId, setCurrentCustomerId] = useState('')

  const dispatch = useDispatch()
  const navigate = useNavigate()

  console.log('data content customer:', dashboardReducer.dataOfContentCustomer)

  const handleChangeInputEmailConentCustomer = (e) => {
    dispatch(changeInputFilterEmailCustomerDashboard(
      e.target.value
    ))
    const filterConditon = {
      email: e.target.value,
      phoneNumber: dashboardReducer.dataOfContentCustomer.filterConditon.phoneNumber,
      status: dashboardReducer.dataOfContentCustomer.filterConditon.status,
    }
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentCustomerDashboard(1, dashboardReducer.dataOfContentCustomer.limitItem, filterConditon, dashboardReducer.dataOfContentCustomer.sortCondition))
  }

  const handleChangeInputPhoneNumberConentCustomer = (e) => {
    dispatch(changeInputFilterPhoneNumberCustomerDashboard(
      e.target.value
    ))
    const filterConditon = {
      email: dashboardReducer.dataOfContentCustomer.filterConditon.email,
      phoneNumber: e.target.value,
      status: dashboardReducer.dataOfContentCustomer.filterConditon.status,
    }
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentCustomerDashboard(1, dashboardReducer.dataOfContentCustomer.limitItem, filterConditon, dashboardReducer.dataOfContentCustomer.sortCondition))
  }

  const handleChangeSelectStatusCustomerFilter = (e) => {
    dispatch(changeSelectStatusCustomerFilter(e.target.value))
    const filterConditon = {
      email: dashboardReducer.dataOfContentCustomer.filterConditon.email,
      phoneNumber: dashboardReducer.dataOfContentCustomer.filterConditon.phoneNumber,
      status: e.target.value,
    }
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentCustomerDashboard(1, dashboardReducer.dataOfContentCustomer.limitItem, filterConditon, dashboardReducer.dataOfContentCustomer.sortCondition))
  }

  const handleClickAddNewCustomer = () => {
    navigate('/dashboard/customers/add')
  }

  const handleClickEditCustomer = (e) => {
    navigate(`/dashboard/customers/edit/${e.target.getAttribute('data-customer-id')}`)
  }

  const handleClickDeleteCustomer = (e) => {
    setOpenModalConfirmDelete(true)
    setCurrentCustomerId(e.target.getAttribute('data-customer-id'))
  }

  const handleCloseModalConfirmDelete = () => {
    setOpenModalConfirmDelete(false)
    dispatch(changeStatusCallApiDeleteCustomerDashboard(false))
  }

  const handleClickBtnActiveCustomer = (e) => {
    //param status, customerId, currentPage, limitItem, filterConditon, sortCondition
    let customerId = e.target.getAttribute('data-customer-id')
    dispatch(callApiUpdateStatusCustomerDashboard(
      true,
      customerId,
      dashboardReducer.dataOfContentCustomer.currentPage,
      dashboardReducer.dataOfContentCustomer.limitItem,
      dashboardReducer.dataOfContentCustomer.filterConditon,
      dashboardReducer.dataOfContentCustomer.sortCondition
    ))
  }

  const handleClickBtnDisableCustomer = (e) => {

    let customerId = e.target.getAttribute('data-customer-id')
    dispatch(callApiUpdateStatusCustomerDashboard(
      false,
      customerId,
      dashboardReducer.dataOfContentCustomer.currentPage,
      dashboardReducer.dataOfContentCustomer.limitItem,
      dashboardReducer.dataOfContentCustomer.filterConditon,
      dashboardReducer.dataOfContentCustomer.sortCondition
    ))
  }

  const handleChangeSelectSortCustomer = (e) => {
    console.log('change select sort:', e.target.options[e.target.selectedIndex].getAttribute('data-type-option'))
    console.log('change select sort:', e.target.value)
    let typeOption = e.target.options[e.target.selectedIndex].getAttribute('data-type-option')
    //param typeOption, value
    dispatch(changeSelectSortCustomerDashboard(typeOption, e.target.value))
    let sortCondition = {
      nameSort: '',
      reveneuSort: '',
      orderSort: '',
    }
    switch (e.target.value) {
      case 'nameSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'nameSort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'reveneuSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'reveneuSort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'orderSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'orderSort-1':
        sortCondition[typeOption] = '-1'
        break;
      default:
        break;
    }
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentCustomerDashboard(1, dashboardReducer.dataOfContentCustomer.limitItem, dashboardReducer.dataOfContentCustomer.filterConditon, sortCondition))
  }


  const handleClickItemTable = (e) => {
    console.log('customer id', e.target.closest('.btn-action-customer-dashboard') === e.target)
    if (e.target.closest('.btn-action-customer-dashboard') !== e.target) {
      let customerId = e.target.closest('.content-customer-dashboard-wrapper').getAttribute('data-customer-id')
      // dispatch(fetchInfoToEditProductDashboard(customerId))
      navigate(`/dashboard/customers/detail/${customerId}`)
    }
  }


  useEffect(() => {
    const handleResizeConententProducts = (e) => {
      setIsSmallMobile(window.innerWidth < 470)
      // setIsMobile(window.innerWidth < 670)
    }
    window.scrollTo(0, 0)
    window.addEventListener('resize', handleResizeConententProducts)
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentCustomerDashboard(
      dashboardReducer.dataOfContentCustomer.currentPage,
      dashboardReducer.dataOfContentCustomer.limitItem,
      dashboardReducer.dataOfContentCustomer.filterConditon,
      dashboardReducer.dataOfContentCustomer.sortCondition
    ))
    return () => {
      window.removeEventListener('resize', handleResizeConententProducts)
    }
  }, [])
  return (
    <div>
      <h3 style={{ textAlign: 'center', margin: '20px 0' }}>Customer</h3>
      <Row style={{ width: '100%', maxWidth: '800px', margin: '20px auto', justifyContent: 'center' }}>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} md='4' xs='12' >
          <input
            data-type-input='email'
            onChange={handleChangeInputEmailConentCustomer}
            value={dashboardReducer.dataOfContentCustomer.filterConditon.email}
            style={{
              border: '1px solid #ccc',
              outline: 'none',
              padding: '5px 12px',
              borderRadius: '5px',
              width: '100%'
            }}
            placeholder='Email customer'
          />
        </Col>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} md='4' xs='12' >
          <input
            data-type-input='phoneNumber'
            value={dashboardReducer.dataOfContentCustomer.filterConditon.phoneNumber}
            onChange={handleChangeInputPhoneNumberConentCustomer}
            style={{
              border: '1px solid #ccc',
              outline: 'none',
              padding: '5px 12px',
              borderRadius: '5px',
              width: '100%'
            }}
            placeholder='Phone number'
          />
        </Col>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} md='4' xs='12' >
          <SelectDashboardContent
            value={dashboardReducer.dataOfContentCustomer.filterConditon.status}
            onChange={handleChangeSelectStatusCustomerFilter}
          >
            <option value=''>All status</option>
            <option value={true} >Active</option>
            <option value={false} >Disable</option>
          </SelectDashboardContent>
        </Col>
      </Row>
      <div
        className='text-center'
        style={{ display: dashboardReducer.dataOfContentCustomer.fetchPendingContentCustomerDashboard ? 'block' : 'none' }}
      >
        <CircularProgress />
      </div>
      <div style={{ width: '100%', padding: '20px 0' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginBottom: '0px', padding: '0 0px' }}>
          <div>
            <label style={{ fontSize: '18px', display: 'inline-block', margin: '5px' }}><b>Sort:</b></label>
            <SelectDashboardContent
              className='hover-basic'
              onChange={handleChangeSelectSortCustomer}
              value={dashboardReducer.dataOfContentCustomer.valueSelectSort}
            >
              <option data-type-option='nameSort' value={'nameSort1'} >{`Name A - Z`}</option>
              <option data-type-option='nameSort' value={'nameSort-1'}>{`Name Z - A`}</option>
              <option data-type-option='reveneuSort' value={'reveneuSort1'}>{`Ascending reveneu`}</option>
              <option data-type-option='reveneuSort' value={'reveneuSort-1'}>{`Descending reveneu`}</option>
              <option data-type-option='orderSort' value={'orderSort1'}>{`Ascending order quantity`}</option>
              <option data-type-option='orderSort' value={'orderSort-1'}>{`Descending order quantity`}</option>
            </SelectDashboardContent>
          </div>

          <ButtonAction
            className='hover-basic'
            bgColor='#28A544'
            style={{ fontSize: '16px' }}
            color='white'
            onClick={handleClickAddNewCustomer}
          >
            + Add new
          </ButtonAction>
        </div>
        <div style={{ width: '100%', padding: '20px 0', overflowX: 'auto' }}>
          <div style={{ width: '98%', minWidth: '1600px', margin: '0 auto' }}>
            <Row
              style={{ padding: '12px 0', borderBottom: '1px solid #ccc', background: '#32343A' }}
              className='align-items-center'
            >
              <Col xs='1' className='d-flex justify-content-center'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Name</h5>
              </Col>
              <Col xs='2'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Email</h5>
              </Col>
              <Col xs='1'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Phone</h5>
              </Col>
              <Col xs='1'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Address</h5>
              </Col>
              <Col xs='1'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Reveneu</h5>
              </Col>
              <Col xs='1'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Total Orders</h5>
              </Col>
              <Col xs='2'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Create Date</h5>
              </Col>
              <Col xs='2'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Update Date</h5>
              </Col>
              <Col xs='1'>
                <h5 style={{ fontSize: '19px', color: 'white', margin: 0, transition: 'all ease 0.3s' }}>Action</h5>
              </Col>
            </Row>
            {dashboardReducer.dataOfContentCustomer.customerList.length > 0 ?
              dashboardReducer.dataOfContentCustomer.customerList.map((customer, index) => {
                let objCreateDate = new Date(customer.createdAt)
                let createDate = `${objCreateDate.getDate()}-${objCreateDate.getMonth() + 1}-${objCreateDate.getFullYear()} ${objCreateDate.getHours()}:${objCreateDate.getMinutes()}:${objCreateDate.getSeconds()}`
                let objUpdateDate = new Date(customer.updatedAt)
                let updateDate = `${objUpdateDate.getDate()}-${objUpdateDate.getMonth() + 1}-${objUpdateDate.getFullYear()} ${objUpdateDate.getHours()}:${objUpdateDate.getMinutes()}:${objUpdateDate.getSeconds()}`

                return (
                  <Row
                    onClick={handleClickItemTable}
                    className='hover-color content-customer-dashboard-wrapper'
                    data-customer-id={customer._id}
                    key={index}
                    style={{
                      padding: '12px 0',
                      borderBottom: '1px solid #ccc',
                      alignItems: 'center',
                      background: customer.active ? 'unset' : '#dbd0d0',
                      color: customer.active ? 'unset' : '#958686'
                    }}
                  >
                    <Col xs='1' style={{ overflow: 'auto' }} className='d-flex justify-content-center'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {customer.fullName}
                      </h5>
                    </Col>
                    <Col xs='2' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {customer.email}
                      </h5>
                    </Col>
                    <Col xs='1' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {customer.phone}
                      </h5>
                    </Col>
                    <Col xs='1' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {customer.address}
                      </h5>
                    </Col>
                    <Col xs='1' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s', paddingLeft: '19px' }}>
                        {`$${customer.totalReveneu.toLocaleString()}`}
                      </h5>
                    </Col>
                    <Col xs='1' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s', paddingLeft: '50px' }}>
                        {customer.totalOrder}
                      </h5>
                    </Col>
                    <Col xs='2' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {createDate}
                      </h5>
                    </Col>
                    <Col xs='2' style={{ overflow: 'auto' }}>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {updateDate}
                      </h5>
                    </Col>
                    <Col xs='1'>
                      <ButtonAction
                        data-customer-id={customer._id}
                        className='hover-basic btn-action-customer-dashboard'
                        bgColor='orange'
                        style={{ fontSize: '19px' }}
                        color='white'
                        onClick={handleClickEditCustomer}
                      >
                        Edit
                      </ButtonAction>
                      <ButtonAction
                        onClick={handleClickDeleteCustomer}
                        data-customer-id={customer._id}
                        className='hover-basic btn-action-customer-dashboard'
                        bgColor='red'
                        style={{ fontSize: '19px', display: 'none' }}
                      >
                        Delete
                      </ButtonAction>
                      <ButtonAction
                        onClick={handleClickBtnActiveCustomer}
                        data-customer-id={customer._id}
                        className='hover-basic btn-action-customer-dashboard'
                        bgColor='violet'
                        style={{ fontSize: '19px', display: customer.active ? 'none' : 'inline-block' }}
                        color='white'
                      >
                        Active
                      </ButtonAction>
                      <ButtonAction
                        onClick={handleClickBtnDisableCustomer}
                        data-customer-id={customer._id}
                        className='hover-basic btn-action-customer-dashboard'
                        bgColor='red'
                        style={{ fontSize: '19px', display: customer.active ? 'inline-block' : 'none' }}
                        color='white'
                      >
                        Disable
                      </ButtonAction>
                    </Col>

                  </Row>
                )
              })
              :
              <Row style={{ paddingTop: '30px' }}>
                <h3 style={{ textAlign: 'center' }}>Have no customer!</h3>
              </Row>
            }
          </div>
        </div>

        <Row style={{
          paddingTop: '30px',
          alignItems: 'center',
          display: dashboardReducer.dataOfContentCustomer.customerList.length > 0 ? 'flex' : 'none'
        }}>
          <Col xs='12' className='d-flex' style={{ justifyContent: 'flex-end' }}>
            <PaginationContentCustomerDashboard
              currentPage={dashboardReducer.dataOfContentCustomer.currentPage}
              numberPage={dashboardReducer.dataOfContentCustomer.numberPage}
            />
          </Col>
        </Row>
      </div>
      <ModalConfirmDeleteCustomerDashboard currentCustomerId={currentCustomerId} openModalConfirmDelete={openModalConfirmDelete} onCloseModalConfirmDelete={handleCloseModalConfirmDelete} />
    </div>
  )
}

export default ContentCustomers