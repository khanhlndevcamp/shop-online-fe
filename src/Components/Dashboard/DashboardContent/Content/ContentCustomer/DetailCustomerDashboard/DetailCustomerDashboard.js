import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { Col, Row } from 'reactstrap'
import { InputDashboardContent } from '../../../DashboardContentStyleComponent'
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import { callApiGetInfoAcccountToEditCusomerDashboard, resetDataAddCustomerDashboard } from '../../../../../../actions/dashboardAction'

const DetailCustomerDashboard = () => {
  const { customerId } = useParams()
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)

  const handleClickGoBack = (e) => {
    navigate('/dashboard/customers')
  }

  useEffect(() => {
    const handleResizeAddProductDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeAddProductDashboard)
    dispatch(callApiGetInfoAcccountToEditCusomerDashboard(customerId))
    return () => {
      window.removeEventListener('resize', handleResizeAddProductDashboard)
      dispatch(resetDataAddCustomerDashboard())
    }
  }, [])
  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Detail Customer</h3>
      <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Usename:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.username}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />

          </Col>
        </Row>

        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Phone number:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.phone}
              data-type-input='phone'
              placeholder='Enter phone number'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />

          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Email:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.email}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />

          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Full name:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.fullName}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Role:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.role}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Avatar URL:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.imageURL}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>

        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label style={{ fontWeight: 500 }}>Address:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentCustomer.infoAddCustomer.address}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>

      </div>
    </div>
  )
}

export default DetailCustomerDashboard