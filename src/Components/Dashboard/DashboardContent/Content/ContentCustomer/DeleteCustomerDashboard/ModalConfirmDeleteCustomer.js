import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import ClearIcon from '@mui/icons-material/Clear';
import { useDispatch, useSelector } from 'react-redux';
import { ButtonOrderContentCancel, ButtonOrdersContent } from '../../../../../OrdersContent/OrdersContentStyledComponent';
import { callApiDeleteCustomerDashboard, callApiDeleteOrderDashboard } from '../../../../../../actions/dashboardAction';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    maxWidth: '100%',
    bgcolor: 'background.paper',
    border: '1px solid #ccc',
    boxShadow: 24,
    p: 4,
};

export default function ModalConfirmDeleteCustomerDashboard({ currentCustomerId, onCloseModalConfirmDelete, openModalConfirmDelete }) {
    const { dashboardReducer } = useSelector(reduxData => reduxData)
    const dispatch = useDispatch()


    const handleClickConfirmDelete = (e) => {
        dispatch(callApiDeleteCustomerDashboard(
            currentCustomerId,
            dashboardReducer.dataOfContentCustomer.currentPage,
            dashboardReducer.dataOfContentCustomer.limitItem,
            dashboardReducer.dataOfContentCustomer.filterConditon,
            dashboardReducer.dataOfContentCustomer.sortCondition
        ))
        // console.log('call api delete customer', currentCustomerId)
    }


    return (
        <div>
            <Modal
                open={openModalConfirmDelete}
                onClose={onCloseModalConfirmDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <span onClick={onCloseModalConfirmDelete} className='hover-basic' style={{ position: 'absolute', top: '10px', right: '10px' }}><ClearIcon /></span>
                    {dashboardReducer.dataOfContentCustomer.statusCallApiDeleteCustomer ?
                        <Typography textAlign={'center'} id="modal-modal-title" variant="h6" component="h2">
                            Delete customer successfully!
                        </Typography>
                        :
                        <>
                            <Typography textAlign={'center'} id="modal-modal-title" variant="h6" component="h2">
                                Confirm delete customer
                            </Typography>
                            <div style={{ marginTop: '20px', textAlign: 'center' }}>
                                <ButtonOrderContentCancel onClick={handleClickConfirmDelete}>Confirm</ButtonOrderContentCancel>
                                <ButtonOrdersContent onClick={onCloseModalConfirmDelete}>Cancel</ButtonOrdersContent>
                            </div>
                        </>

                    }

                </Box>
            </Modal>
        </div>
    );
}