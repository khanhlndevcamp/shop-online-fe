import { Pagination } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataOfContentCustomerDashboard } from '../../../../../actions/dashboardAction'

const PaginationContentCustomerDashboard = ({ currentPage, numberPage }) => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)

  const dispatch = useDispatch()

  const handleChangePageCustomerDashboard = (e, value) => {
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentCustomerDashboard(value, dashboardReducer.dataOfContentCustomer.limitItem, dashboardReducer.dataOfContentCustomer.filterConditon, dashboardReducer.dataOfContentCustomer.sortCondition))
  }
  return (
    <div>
      <Pagination onChange={handleChangePageCustomerDashboard} count={numberPage} page={currentPage} />
    </div>
  )
}

export default PaginationContentCustomerDashboard