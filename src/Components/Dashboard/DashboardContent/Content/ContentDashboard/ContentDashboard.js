import React, { useEffect } from 'react'
import { InfoOverview, SelectDashboardContent } from '../../DashboardContentStyleComponent'
import { Col, Row } from 'reactstrap'
import LocalMallIcon from '@mui/icons-material/LocalMall';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import WidgetsIcon from '@mui/icons-material/Widgets';
import { useState } from 'react';
import { CChartBar, CChartDoughnut } from '@coreui/react-chartjs';
import { useDispatch, useSelector } from 'react-redux';
import { callApiGetReveneuByDayContentDashboard, callApiGetReveneuByMonthContentDashboard, changeSelectMonthChartReveneuByDayContentDashboard, changeSelectYearChartReveneuByDayContentDashboard, changeSelectYearChartReveneuContentDashboard, clickChooseCustomer, clickChooseOrder, clickChooseProduc, fetchDataOfContentDashboard } from '../../../../../actions/dashboardAction';
import { useNavigate } from 'react-router-dom';

const ContentDashboard = () => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [statusClickMoreInfoAPT, setStatusClickMoreInfoAPT] = useState(false)
  const [statusClickMoreInfoUPT, setStatusClickMoreInfoUPT] = useState(false)
  const [statusClickMoreInfoAPU, setStatusClickMoreInfoAPU] = useState(false)

  // chart reveneu by month
  const listOfYear = []
  const listOfMonth = []

  const currentYear = new Date()
  for (let i = 0; i < 5; i++) {
    listOfYear.push(currentYear.getFullYear() - i)
  }
  for (let i = 0; i < 12; i++) {
    listOfMonth.push(i + 1)
  }
  const listDataReveneu = []
  dashboardReducer.dataOfContentDashboard.chartReveneuByMonth.listOfReveneuByMonth.forEach(reveneuByMonth => {
    listDataReveneu.push(reveneuByMonth.reveneu)
  })
  //end char reveneu by month

  // chart reveneu by day
  const listOfDay = []
  const listDataReveneuByDay = []
  dashboardReducer.dataOfContentDashboard.chartReveneuByDay.listOfReveneuByDay.forEach((data, index) => {
    listOfDay.push(index +1)
    listDataReveneuByDay.push(data.reveneu)
  })
  //end char renveneu by day
  const handleChangeSelectYearChartReveneu = (e) => {
    console.log(e.target.value)
    //param year
    dispatch(changeSelectYearChartReveneuContentDashboard(e.target.value))
    //param year
    dispatch(callApiGetReveneuByMonthContentDashboard(e.target.value))
  }

  const handleChangeSelectYearChartReveneuByDay = (e) => {
    console.log(e.target.value)

    //param year
    dispatch(changeSelectYearChartReveneuByDayContentDashboard(e.target.value))
    //param year month
    dispatch(callApiGetReveneuByDayContentDashboard(e.target.value, dashboardReducer.dataOfContentDashboard.chartReveneuByDay.currentMonth))

  }

  const handleChangeSelectMonthChartReveneuByDay = (e) => {
    console.log(e.target.value)
    //param year
    dispatch(changeSelectMonthChartReveneuByDayContentDashboard(e.target.value))
    //param year month
    dispatch(callApiGetReveneuByDayContentDashboard(dashboardReducer.dataOfContentDashboard.chartReveneuByDay.currentYear, e.target.value))

  }
  const handleClickItemProduct = (e) => {
    dispatch(clickChooseProduc())
    navigate('/dashboard/products')
  }

  const handleClickItemCustomer = (e) => {
    dispatch(clickChooseCustomer())
    navigate('/dashboard/customers')

  }

  const handleClickItemOrder = (e) => {
    dispatch(clickChooseOrder())
    navigate('/dashboard/orders')
  }
  useEffect(() => {
    dispatch(fetchDataOfContentDashboard())
    //param year
    dispatch(callApiGetReveneuByMonthContentDashboard(dashboardReducer.dataOfContentDashboard.chartReveneuByMonth.currentYear))
    //param year, month
    dispatch(callApiGetReveneuByDayContentDashboard(dashboardReducer.dataOfContentDashboard.chartReveneuByDay.currentYear, dashboardReducer.dataOfContentDashboard.chartReveneuByDay.currentMonth))
  }, [])

  return (
    <div>
      <Row>
        <h3>Overview</h3>
        <hr />
        <Col style={{ margin: '10px 0' }} sm='6' xs='12'>
          <InfoOverview bgColor='#17A2B8' color='white' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '33px' }}>
              <div>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {`$${(dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu).toLocaleString()}`}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total Reveneu</p>
              </div>
              <AttachMoneyIcon style={{ fontSize: '75px', opacity: '0.8', color: '#148A9D' }} />
            </div>
            <div
              className='d-flex justify-content-center align-items-center hover-basic'
              style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#148A9D', height: '30px' }}>
              <span style={{ fontWeight: 500 }}>
                More info
              </span>
              <ArrowForwardIcon style={{ fontSize: '20px', margin: '0px 4px' }} />
            </div>
          </InfoOverview>
        </Col>
        <Col style={{ margin: '10px 0' }} sm='6' xs='12'>
          <InfoOverview bgColor='#28A745' color='white' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '33px' }}>
              <div>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {(dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders).toLocaleString()}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total Orders</p>
              </div>
              <LocalMallIcon style={{ fontSize: '75px', opacity: '0.8', color: '#218A39' }} />
            </div>
            <div
              onClick={handleClickItemOrder}
              className='d-flex justify-content-center align-items-center hover-basic'
              style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#218A39', height: '30px' }}>
              <span style={{ fontWeight: 500 }}>
                More info
              </span>
              <ArrowForwardIcon style={{ fontSize: '20px', margin: '0px 4px' }} />
            </div>
          </InfoOverview>
        </Col>
        <Col style={{ margin: '10px 0' }} sm='6' xs='12'>
          <InfoOverview bgColor='#FFC107' color='#1F2D3D' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '33px' }}>
              <div>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>{dashboardReducer.dataOfContentDashboard.numberOfTotalProduct}</h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total Products</p>
              </div>
              <WidgetsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#D9A406' }} />
            </div>
            <div
              onClick={handleClickItemProduct}
              className='d-flex justify-content-center align-items-center hover-basic'
              style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#D9A406', height: '30px' }}>
              <span style={{ fontWeight: 500 }}>
                More info
              </span>
              <ArrowForwardIcon style={{ fontSize: '20px', margin: '0px 4px' }} />
            </div>
          </InfoOverview>
        </Col>
        <Col style={{ margin: '10px 0' }} sm='6' xs='12'>
          <InfoOverview bgColor='#DC3545' color='white' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '33px' }}>
              <div>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {dashboardReducer.dataOfContentDashboard.numberOfTotalCustomer}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total Customers</p>
              </div>
              <PeopleAltIcon style={{ fontSize: '75px', opacity: '0.8', color: '#BB2D3B' }} />
            </div>
            <div
              onClick={handleClickItemCustomer}
              className='d-flex justify-content-center align-items-center hover-basic'
              style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#BB2D3B', height: '30px' }}>
              <span style={{ fontWeight: 500 }}>
                More info
              </span>
              <ArrowForwardIcon style={{ fontSize: '20px', margin: '0px 4px' }} />
            </div>
          </InfoOverview>
        </Col>
      </Row>

      <Row style={{ marginTop: '30px' }}>
        <h3>KPI</h3>
        <hr />
        <Col style={{ margin: '10px 0' }} xl='4' xs='12'>
          <InfoOverview bgColor='#1385FF' color='white' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '20px' }}>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {(dashboardReducer.dataOfContentDashboard.orderOverview.totalSoldProducts / dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders) ? (dashboardReducer.dataOfContentDashboard.orderOverview.totalSoldProducts / dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders).toFixed(2) : 0}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>UPT</p>
                <p style={{ minHeight: '48px', marginBottom: '8px' }}>Average quanitity of products per transactions</p>
                <div style={{ height: statusClickMoreInfoUPT ? '65px' : '0', overflow: 'hidden', transition: 'all ease 0.3s' }}>
                  <p style={{ margin: 0, fontWeight: 500, fontSize: '18px' }}>
                    Total sold products: {dashboardReducer.dataOfContentDashboard.orderOverview.totalSoldProducts.toLocaleString()}
                  </p>
                  <p style={{ margin: 0, fontWeight: 500, fontSize: '18px' }}>
                    Total transactions: {dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders.toLocaleString()}
                  </p>
                </div>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#0069D9' }} />
              <div
                onClick={() => { setStatusClickMoreInfoUPT(prev => !prev) }}
                className='d-flex justify-content-center align-items-center hover-basic'
                style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#0069D9', height: '30px' }}>
                <span style={{ fontWeight: 500 }}>
                  {statusClickMoreInfoUPT ? 'Less info' : 'More info'}
                </span>
                <ArrowDownwardIcon style={{ fontSize: '20px', margin: '0px 4px', transform: statusClickMoreInfoUPT ? 'rotate(180deg)' : 'rotate(0deg)', transition: 'all ease 0.3s' }} />
              </div>
            </div>
          </InfoOverview>
        </Col>


        <Col style={{ margin: '10px 0' }} xl='4' xs='12'>
          <InfoOverview bgColor='#37AFC2' color='white' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '20px' }}>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {`$${(dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu / dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders) ? (dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu / dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders).toFixed(2).toLocaleString() : 0}`}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>APT</p>
                <p style={{ minHeight: '48px', marginBottom: '8px' }}>Average price per transaction</p>
                <div style={{ height: statusClickMoreInfoAPT ? '65px' : '0', overflow: 'hidden', transition: 'all ease 0.3s' }}>
                  <p style={{ margin: 0, fontWeight: 500, fontSize: '18px' }}>
                    Total reveneu: ${dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu.toLocaleString()}
                  </p>
                  <p style={{ margin: 0, fontWeight: 500, fontSize: '18px' }}>
                    Total transactions: {dashboardReducer.dataOfContentDashboard.orderOverview.totalOrders}
                  </p>
                </div>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#117A8B' }} />
              <div
                onClick={() => { setStatusClickMoreInfoAPT(prev => !prev) }}
                className='d-flex justify-content-center align-items-center hover-basic'
                style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#117A8B', height: '30px' }}>
                <span style={{ fontWeight: 500 }}>
                  {statusClickMoreInfoAPT ? 'Less info' : 'More info'}
                </span>
                <ArrowDownwardIcon style={{ fontSize: '20px', margin: '0px 4px', transform: statusClickMoreInfoAPT ? 'rotate(180deg)' : 'rotate(0deg)', transition: 'all ease 0.3s' }} />
              </div>
            </div>
          </InfoOverview>
        </Col>


        <Col style={{ margin: '10px 0' }} xl='4' xs='12'>
          <InfoOverview bgColor='#4F6AA3' color='white' >
            <div className='d-flex justify-content-between align-items-center' style={{ marginBottom: '20px' }}>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {`$${(dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu / dashboardReducer.dataOfContentDashboard.orderOverview.totalSoldProducts) ? (dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu / dashboardReducer.dataOfContentDashboard.orderOverview.totalSoldProducts).toFixed(2).toLocaleString() : 0}`}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>APU</p>
                <p style={{ minHeight: '48px', marginBottom: '8px' }}>Average price per a product</p>
                <div style={{ height: statusClickMoreInfoAPU ? '65px' : '0', overflow: 'hidden', transition: 'all ease 0.3s' }}>
                  <p style={{ margin: 0, fontWeight: 500, fontSize: '18px' }}>
                    Total reveneu: ${dashboardReducer.dataOfContentDashboard.orderOverview.totalReveneu.toLocaleString()}
                  </p>
                  <p style={{ margin: 0, fontWeight: 500, fontSize: '18px' }}>
                    Total sold products: {dashboardReducer.dataOfContentDashboard.orderOverview.totalSoldProducts.toLocaleString()}
                  </p>
                </div>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#3B5998' }} />
              <div
                onClick={() => { setStatusClickMoreInfoAPU(prev => !prev) }}
                className='d-flex justify-content-center align-items-center hover-basic'
                style={{ position: 'absolute', bottom: '0px', left: 0, right: 0, background: '#3B5998', height: '30px' }}>
                <span style={{ fontWeight: 500 }}>
                  {statusClickMoreInfoAPU ? 'Less info' : 'More info'}
                </span>
                <ArrowDownwardIcon style={{ fontSize: '20px', margin: '0px 4px', transform: statusClickMoreInfoAPU ? 'rotate(180deg)' : 'rotate(0deg)', transition: 'all ease 0.3s' }} />
              </div>
            </div>
          </InfoOverview>
        </Col>
      </Row>

      <Row style={{ marginTop: '30px' }}>
        <h3>Pending Orders Rate</h3>
        <hr />
        <Col xs='12'>
          <div className='d-flex justify-content-center'>
            <CChartDoughnut
              style={{ maxWidth: '500px', width: '100%' }}
              data={{
                labels: ['Completed Orders', 'Pending Orders'],
                datasets: [
                  {
                    backgroundColor: ['#41B883', '#E46651'],
                    data: [
                      dashboardReducer.dataOfContentDashboard.orderOverview.completedOrders,
                      dashboardReducer.dataOfContentDashboard.orderOverview.pendingOrders
                    ],
                  },
                ],
              }}
            />
          </div>
        </Col>
      </Row>
      <Row style={{ marginTop: '30px' }}>
        <h3>Reveneu by months</h3>
        <hr />
        <div>
          <label style={{ fontSize: '19px', fontWeight: 500 }}>Year:</label>
          <SelectDashboardContent
            style={{ width: '100px', margin: '0 12px' }}
            onChange={handleChangeSelectYearChartReveneu}
            value={dashboardReducer.dataOfContentDashboard.chartReveneuByMonth.currentYear}
          >
            {listOfYear.map((year, index) => {
              return (<option key={index} value={year}>{year}</option>)
            })}
          </SelectDashboardContent>
        </div>
        <Col xs='12'>
          <div className='d-flex justify-content-center'>
            <CChartBar
              style={{ maxWidth: '1200px', width: '100%' }}
              data={{
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Agust', 'September', 'Octorber', 'November', 'December'],
                datasets: [
                  {
                    label: 'Reveneu',
                    backgroundColor: '#f87979',
                    data: listDataReveneu,
                  },
                ],
              }}
              labels="months"
            />
          </div>
        </Col>
      </Row>
      <Row style={{ marginTop: '30px' }}>
        <h3>Reveneu by day</h3>
        <hr />
        <div>
          <label style={{ fontSize: '19px', fontWeight: 500 }}>Month:</label>
          <SelectDashboardContent
            style={{ width: '100px', margin: '0 12px' }}
            onChange={handleChangeSelectMonthChartReveneuByDay}
            value={dashboardReducer.dataOfContentDashboard.chartReveneuByDay.currentMonth}
          >
            {listOfMonth.map((month, index) => {
              return (<option key={index} value={month}>{month}</option>)
            })}
          </SelectDashboardContent>
          <label style={{ fontSize: '19px', fontWeight: 500 }}>Year:</label>
          <SelectDashboardContent
            style={{ width: '100px', margin: '0 12px' }}
            onChange={handleChangeSelectYearChartReveneuByDay}
            value={dashboardReducer.dataOfContentDashboard.chartReveneuByDay.currentYear}
          >
            {listOfYear.map((year, index) => {
              return (<option key={index} value={year}>{year}</option>)
            })}
          </SelectDashboardContent>
        </div>
        <Col xs='12'>
          <div className='d-flex justify-content-center'>
            <CChartBar
              style={{ maxWidth: '1200px', width: '100%' }}
              data={{
                labels: listOfDay,
                datasets: [
                  {
                    label: 'Reveneu',
                    backgroundColor: '#f87979',
                    data: listDataReveneuByDay,
                  },
                ],
              }}
              labels="months"
            />
          </div>
        </Col>
      </Row>
    </div>
  )
}

export default ContentDashboard