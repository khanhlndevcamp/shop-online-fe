import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import { Col, Row } from 'reactstrap';
import { InputDashboardContent, SelectDashboardContent } from '../../../DashboardContentStyleComponent';
import { useEffect } from 'react';
import { useState } from 'react';
import {
  fetchInfoToEditOrderDashboard,
  resetDataEditOrderDashboard
} from '../../../../../../actions/dashboardAction';

const DetailOrderDashboard = () => {
  const { orderCode } = useParams()
  const navigate = useNavigate()
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)



  const handleClickGoBack = (e) => {
    navigate('/dashboard/orders')
  }



  useEffect(() => {
    const handleResizeEditOrderDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeEditOrderDashboard)
    dispatch(fetchInfoToEditOrderDashboard(orderCode))

    return () => {
      window.removeEventListener('resize', handleResizeEditOrderDashboard)
      dispatch(resetDataEditOrderDashboard())
    }
  }, [])
  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Detail Order</h3>
      <Row>
        <Col lg='6' md='12'>
          <div style={{ margin: '30px auto', width: '100%', maxWidth: '1200px' }}>
            <h5>Billing infomation</h5>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Customer name:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.customerName}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                  disabled={true}
                />
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Phone number:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  disabled={true}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Email:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  disabled={true}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.email}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Address:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  disabled={true}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.address}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Note:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  disabled={true}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.note}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Status order:</label>
              </Col>
              <Col md='8' xs='12'>
                <SelectDashboardContent
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.status.toLocaleLowerCase().trim()}
                  disabled={true}
                >
                  <option value=''>Choose status order</option>
                  <option value='created order'>Created order</option>
                  <option value='waiting for confirmation'>Waiting for confirmation</option>
                  <option value='confirmed'>Confirmed</option>
                  <option value='packed'>Packed</option>
                  <option value='shipping'>Shipping</option>
                  <option value='delivered'>Delivered</option>
                </SelectDashboardContent>
              </Col>
            </Row>
          </div>
        </Col>

        {/* <hr style={{ width: '100%', margin: 'auto', maxWidth: '700px' }} /> */}
        <Col lg='6' md='12'>
          <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
            <h5 style={{ marginBottom: '30px' }}>Order infomation</h5>
            <div style={{ border: '1px solid #ccc', padding: '12px 15px' }}>
              <Row style={{ marginBottom: '30px' }}>
                <Col xs='5' ><b>Product</b></Col>
                <Col xs='3' className='text-center' ><b>Quanity</b></Col>
                <Col xs='4' className='text-center' ><b>Price</b></Col>
              </Row>
              {dashboardReducer.dataOfContentOrder.infoEditOrder.orderDetail.map((orderDetail, index) => {
                return (
                  <Row key={index} className='my-3'>
                    <Col xs='5' style={{ paddingRight: '20px', color: '#7B5555', fontWeight: 600 }}>
                      {orderDetail.product.name}
                    </Col>
                    <Col xs='3' className='text-center' style={{ fontWeight: 600 }} >
                      <input
                        style={{
                          display: 'inline',
                          width: '35px',
                          outline: 'none',
                          border: '1px solid #ccc',
                          borderRadius: '5px',
                          paddingLeft: '4px'
                        }}
                        value={orderDetail.quantity}
                        type='number'
                        disabled={true}
                      />
                    </Col>
                    <Col
                      xs='4'
                      className='text-center'
                      style={{ color: '#7B5555', fontWeight: 600 }}
                    >
                      ${Math.round(orderDetail.quantity * orderDetail.product.promotionPrice).toLocaleString()}
                    </Col>
                  </Row>
                )
              })}
            </div>
            {/* <Row style={{ margin: '30px 0', alignItems: 'center' }}>
              <Col md='8' xs='12'>
                <div
                  style={{
                    border: '1px solid #ccc',
                    outline: 'none',
                    borderRadius: '5px',
                    width: '100%',
                    padding: '5px 12px',
                    background: 'white',
                    position: 'relative',
                    margin: 0
                  }}
                >
                  <div
                    onClick={() => { setOpenSelectProduct(prev => !prev) }}
                    className='hover-basic'
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                    }}
                  >
                    <span>{dashboardReducer.dataOfContentOrder.infoEditOrder.infoProductAddMore.productName}</span>
                    <span>
                      <KeyboardArrowDownIcon />
                    </span>
                  </div>

                  <div style={{
                    display: openSelectProduct ? 'block' : 'none',
                    position: 'absolute',
                    background: 'white',
                    top: '100%',
                    left: 0,
                    right: 0,
                    border: '1px solid #ccc',
                    zIndex: 20,
                    maxHeight: '300px',
                    overflow: 'auto',
                    transition: 'all ease .1s'
                  }}>
                    <p
                      onClick={handleClickChooseProductInSelect}
                      data-product-id=''
                      className='hover-basic'
                      data-product-name='Choose product'
                      data-product-price={0}
                      style={{ margin: '12px', borderBottom: '1px solid #ccc', paddingBottom: '5px' }}>
                      Choose product
                    </p>
                    {dashboardReducer.dataOfContentOrder.infoEditOrder.productList.map((product, index) => {
                      return (
                        <p
                          onClick={handleClickChooseProductInSelect}
                          className='hover-basic'
                          data-product-id={product._id}
                          data-product-name={product.name}
                          data-product-price={product.promotionPrice}
                          key={index}
                          style={{ margin: '12px', borderBottom: '1px solid #ccc', paddingBottom: '5px' }}
                        >
                          {product.name} / ${product.promotionPrice}
                        </p>
                      )
                    })}
                  </div>
                </div>
                <span style={{ color: 'red', display: showAlertAddMoreProduct ? 'inline-block' : 'none' }}>Please choose product!</span>

              </Col>
              <Col md='4' xs='12' className='d-flex align-items-center justify-content-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <ButtonAction
                  bgColor='rgb(111, 111, 111)'
                  className='hover-basic'
                  onClick={handleClickAddMoreProduct}
                >
                  Add more product
                </ButtonAction>
              </Col>
            </Row> */}
            {/* <Row style={{ margin: '30px 0', alignItems: 'center' }}>

              <Col md='8' xs='12'>
                <InputDashboardContent
                  onChange={handleChangeInputEditOrder}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.voucherCode}
                  data-type-input='voucherCode'
                  placeholder='Voucher'
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
                <span
                  style={{
                    color: dashboardReducer.dataOfContentOrder.resultCallApiCheckVoucher ? 'green' : 'red',
                    display: dashboardReducer.dataOfContentOrder.statusClickCallApiCheckVoucherOrder ? 'block' : 'none'
                  }}
                >
                  {dashboardReducer.dataOfContentOrder.resultCallApiCheckVoucher ? 'Valid voucher' : 'Invalid voucher'}
                </span>
              </Col>
              <Col md='4' xs='12' className='d-flex align-items-center justify-content-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <ButtonAction
                  bgColor='green'
                  className='hover-basic'
                  onClick={handleClickApplyVoucher}
                >
                  Apply voucher
                </ButtonAction>
              </Col>
            </Row> */}
            <Row >
              <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                <div style={{ background: '#f7f0f0', padding: '20px' }}>
                  <p><b>Cart total</b></p>
                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Total Quantity</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>{dashboardReducer.dataOfContentOrder.infoEditOrder.totalQuantity} items</b>
                    </Col>
                  </Row>

                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Discount percent</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>{dashboardReducer.dataOfContentOrder.infoEditOrder.discountPercent}%</b>
                    </Col>
                  </Row>
                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Discount price</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>- ${dashboardReducer.dataOfContentOrder.infoEditOrder.discountPrice}</b>
                    </Col>
                  </Row>

                  <hr />
                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Total</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>${dashboardReducer.dataOfContentOrder.infoEditOrder.totalPrice}</b>
                    </Col>
                  </Row>
                </div>
              </Col>

            </Row>
          </div>
        </Col>

      </Row>
    </div >
  )
}

export default DetailOrderDashboard