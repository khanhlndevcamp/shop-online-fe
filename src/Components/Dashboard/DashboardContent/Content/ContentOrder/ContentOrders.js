import React, { useEffect, useState } from 'react'
import { Col, Row } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { CircularProgress } from '@mui/material'
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import '../../../../../App.css'
import PaginationContentOrderDashboard from './PaginationContentOrderDashboard'
import { changeInputEmailCustomerOrderFilter, changeInputOrderCodeOrderFilter, changeSelectRangeDateOrderFilter, changeSelectSortOrderDashboard, changeSelectStatusOrderFilter, changeStatusCallApiDeleteOrderDashboard, fetchDataOfContentOrder, fetchInfoToEditOrderDashboard } from '../../../../../actions/dashboardAction'
import { ButtonAction, InfoOverview, SelectDashboardContent } from '../../DashboardContentStyleComponent'
import ModalConfirmDeleteOrderDashboard from './DeleteOrderDashboard/ModalConfirmDeleteOrderDashboard'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import 'bootstrap-daterangepicker/daterangepicker.css';




const ContentOrders = () => {
  const { orderReducer, dashboardReducer } = useSelector(reduxData => reduxData)
  const [openModalConfirmDelete, setOpenModalConfirmDelete] = useState(false)
  const [currentOrderId, setCurrentOrderId] = useState('')
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleChangeValueInputFilterOrderCode = (e) => {
    dispatch(changeInputOrderCodeOrderFilter(e.target.value))
    const filterConditon = {
      orderCode: e.target.value,
      email: dashboardReducer.dataOfContentOrder.filterConditon.email,
      statusOrder: dashboardReducer.dataOfContentOrder.filterConditon.statusOrder,
      fromDate: dashboardReducer.dataOfContentOrder.filterConditon.fromDate,
      toDate: dashboardReducer.dataOfContentOrder.filterConditon.toDate,
    }
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
  }

  const handleChangeValueInputFilterEmailCustomer = (e) => {
    dispatch(changeInputEmailCustomerOrderFilter(e.target.value))
    const filterConditon = {
      email: e.target.value,
      orderCode: dashboardReducer.dataOfContentOrder.filterConditon.orderCode,
      statusOrder: dashboardReducer.dataOfContentOrder.filterConditon.statusOrder,
      fromDate: dashboardReducer.dataOfContentOrder.filterConditon.fromDate,
      toDate: dashboardReducer.dataOfContentOrder.filterConditon.toDate,
    }
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
  }

  const handleChangeSelectStatusFilterOrder = (e) => {
    dispatch(changeSelectStatusOrderFilter(e.target.value))
    const filterConditon = {
      email: dashboardReducer.dataOfContentOrder.filterConditon.email,
      orderCode: dashboardReducer.dataOfContentOrder.filterConditon.orderCode,
      statusOrder: e.target.value,
      fromDate: dashboardReducer.dataOfContentOrder.filterConditon.fromDate,
      toDate: dashboardReducer.dataOfContentOrder.filterConditon.toDate,
    }
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
  }

  //date range picker
  const handleApplyFilterDateOrder = (event, picker) => {
    //assign value to input
    picker.element.val(
      picker.startDate.format('MM/DD/YYYY') +
      ' - ' +
      picker.endDate.format('MM/DD/YYYY')
    );

    let fromDate = picker.startDate.format('MM-DD-YYYY')
    let toDate = picker.endDate.format('MM-DD-YYYY')
    const filterConditon = {
      email: dashboardReducer.dataOfContentOrder.filterConditon.email,
      orderCode: dashboardReducer.dataOfContentOrder.filterConditon.orderCode,
      statusOrder: dashboardReducer.dataOfContentOrder.filterConditon.statusOrder,
      fromDate: fromDate,
      toDate: toDate,
    }

    console.log('handle apply start date', fromDate)
    console.log('handle apply end date', toDate)
    dispatch(changeSelectRangeDateOrderFilter(fromDate, toDate))
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
  };
  const handleCancelFilterDateOrder = (event, picker) => {
    //assign value to input
    picker.element.val('');

    let fromDate = '01-01-2023'
    let toDate = '12-31-2023'
    const filterConditon = {
      email: dashboardReducer.dataOfContentOrder.filterConditon.email,
      orderCode: dashboardReducer.dataOfContentOrder.filterConditon.orderCode,
      statusOrder: dashboardReducer.dataOfContentOrder.filterConditon.statusOrder,
      fromDate: fromDate,
      toDate: toDate,
    }
    console.log('handle cancel start date', fromDate)
    console.log('handle cancel end date', toDate)
    dispatch(changeSelectRangeDateOrderFilter(fromDate, toDate))
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
  };
  //close date range picker /////

  console.log('dashboard data content of order', dashboardReducer.dataOfContentOrder)
  const handleClickCancelOrder = (e) => {
    setOpenModalConfirmDelete(true)
    setCurrentOrderId(e.target.getAttribute('data-order-id'))
  }



  const handleCloseModalConfirmDelete = () => {
    setOpenModalConfirmDelete(false)
    dispatch(changeStatusCallApiDeleteOrderDashboard(false))
  }

  const handleClickBtnEditOrder = (e) => {
    // dispatch(fetchInfoToEditOrderDashboard(e.target.getAttribute('data-order-code')))
    navigate(`/dashboard/orders/edit/${e.target.getAttribute('data-order-code')}/${e.target.getAttribute('data-order-id')}`)
  }

  const handleClickBtnAddOrder = (e) => {
    navigate('/dashboard/orders/add')
  }

  const handleChangeSelectSortOrder = (e) => {
    console.log('change select sort:', e.target.options[e.target.selectedIndex].getAttribute('data-type-option'))
    console.log('change select sort:', e.target.value)
    let typeOption = e.target.options[e.target.selectedIndex].getAttribute('data-type-option')
    //param typeOption, value
    dispatch(changeSelectSortOrderDashboard(typeOption, e.target.value))
    let sortCondition = {
      orderCodeSort: '',
      priceSort: '',
      orderQuantitySort: '',
      createDateSort: ''
    }
    switch (e.target.value) {
      case 'orderCodeSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'orderCodeSort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'priceSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'priceSort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'orderQuantitySort1':
        sortCondition[typeOption] = '1'
        break;
      case 'orderQuantitySort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'createDateSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'createDateSort-1':
        sortCondition[typeOption] = '-1'
        break;
      default:
        break;
    }
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, dashboardReducer.dataOfContentOrder.filterConditon, sortCondition))

  }

  const handleClickItemTable = (e) => {
    if (e.target.closest('.btn-action-order-dashboard') !== e.target) {
      let orderCode = e.target.closest('.content-order-dashboard-wrapper').getAttribute('data-order-code')
      //param order code  
      navigate(`/dashboard/orders/detail/${orderCode}`)
    }
  }

  useEffect(() => {
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentOrder(1, dashboardReducer.dataOfContentOrder.limitItem, dashboardReducer.dataOfContentOrder.filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
    window.scrollTo(0, 0)
    return () => {
    }
  }, [])


  return (
    <div>
      <h3 style={{ textAlign: 'center', margin: '20px 0' }}>Order</h3>

      <div style={{ width: '80%', margin: '0 auto' }}>
        <Row>
          <Col md='6' xs='12' style={{ margin: '12px 0' }}>
            <input
              value={dashboardReducer.dataOfContentOrder.filterConditon.orderCode}
              onChange={handleChangeValueInputFilterOrderCode}
              placeholder='Enter order code'
              style={{
                padding: '5px 15px',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%',
                maxWidth: '800px',
                outline: 'none'
              }}
            />
          </Col>

          <Col md='6' xs='12' style={{ margin: '12px 0' }}>
            <input
              value={dashboardReducer.dataOfContentOrder.filterConditon.email}
              onChange={handleChangeValueInputFilterEmailCustomer}
              placeholder='Enter email customer'
              style={{
                padding: '5px 15px',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%',
                maxWidth: '800px',
                outline: 'none'
              }}
            />
          </Col>

          <Col md='6' xs='12' style={{ margin: '12px 0' }}>
            <SelectDashboardContent
              className='hover-basic'
              onChange={handleChangeSelectStatusFilterOrder}
              value={dashboardReducer.dataOfContentOrder.filterConditon.statusOrder}
            >
              <option value={''} >All status</option>
              <option value={'created order'} >Created order</option>
              <option value={'waiting for confirmation'}>Waiting for confirmation</option>
              <option value={'confirmed'}>Confirmed</option>
              <option value={'packed'}>Packed</option>
              <option value={'shipping'}>Shipping</option>
              <option value={'delivered'}>Delivered</option>
            </SelectDashboardContent>
          </Col>

          <Col md='6' xs='12' style={{ margin: '12px 0' }}>
            <DateRangePicker
              initialSettings={{ startDate: dashboardReducer.dataOfContentOrder.filterConditon.fromDate, endDate: dashboardReducer.dataOfContentOrder.filterConditon.toDate }}
              onApply={handleApplyFilterDateOrder}
              onCancel={handleCancelFilterDateOrder}
            >
              <input type="text" className="form-control col-4" style={{ height: '36px' }} placeholder='Created date order: from - to (mm-dd-yyyy)' />
            </DateRangePicker>
          </Col>
        </Row>
      </div>
      <div
        className='text-center'
        style={{ display: dashboardReducer.dataOfContentOrder.fetchPendingContentOrderDashboard ? 'block' : 'none' }}
      >
        <CircularProgress />
      </div>
      <Row style={{ marginTop: '30px' }}>
        <h3>Overal</h3>
        <hr />
        <Col style={{ margin: '10px 0' }} xl='4' xs='12'>
          <InfoOverview bgColor='#1385FF' color='white' >
            <div className='d-flex justify-content-between align-items-center'>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  ${dashboardReducer.dataOfContentOrder.overal.totalPrice.toLocaleString()}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total price</p>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#0069D9' }} />
            </div>
          </InfoOverview>
        </Col>

        <Col style={{ margin: '10px 0' }} xl='4' xs='12'>
          <InfoOverview bgColor='#37AFC2' color='white' >
            <div className='d-flex justify-content-between align-items-center'>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {dashboardReducer.dataOfContentOrder.overal.totalQuantitySold}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total quantity sold</p>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#117A8B' }} />
            </div>
          </InfoOverview>
        </Col>

        <Col style={{ margin: '10px 0' }} xl='4' xs='12'>
          <InfoOverview bgColor='#4F6AA3' color='white' >
            <div className='d-flex justify-content-between align-items-center'>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {dashboardReducer.dataOfContentOrder.overal.totalOrders}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total orders</p>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#3B5998' }} />
            </div>
          </InfoOverview>
        </Col>
      </Row>
      <div style={{ width: '100%', padding: '20px 0' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginBottom: '0px', padding: '0 0px' }}>
          <div>
            <label style={{ fontSize: '18px', display: 'inline-block', margin: '5px' }}><b>Sort:</b></label>
            <SelectDashboardContent
              className='hover-basic'
              onChange={handleChangeSelectSortOrder}
              value={dashboardReducer.dataOfContentOrder.valueSelectSort}
            >
              <option data-type-option='orderCodeSort' value={'orderCodeSort1'} >{`Order code A - Z`}</option>
              <option data-type-option='orderCodeSort' value={'orderCodeSort-1'}>{`Order code Z - A`}</option>
              <option data-type-option='priceSort' value={'priceSort1'}>{`Ascending price`}</option>
              <option data-type-option='priceSort' value={'priceSort-1'}>{`Descending price`}</option>
              <option data-type-option='orderQuantitySort' value={'orderQuantitySort1'}>{`Ascending order quantity`}</option>
              <option data-type-option='orderQuantitySort' value={'orderQuantitySort-1'}>{`Descending order quantity`}</option>
              <option data-type-option='createDateSort' value={'createDateSort1'}>{`Ascending create date`}</option>
              <option data-type-option='createDateSort' value={'createDateSort-1'}>{`Descending create date`}</option>
            </SelectDashboardContent>
          </div>

          <ButtonAction
            className='hover-basic'
            bgColor='#28A544'
            style={{ fontSize: '16px' }}
            color='white'
            onClick={handleClickBtnAddOrder}
          >
            + Add new
          </ButtonAction>
        </div>
        <div style={{ width: '100%', padding: '20px 0', overflowX: 'auto' }}>
          <div style={{ width: '98%', minWidth: '1600px', margin: '0 auto' }}>
            <Row
              style={{ padding: '12px 0', borderBottom: '1px solid #ccc', background: '#32343A' }}
            >
              <Col className='d-flex align-items-center justify-content-center' xs='1'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Order Code</h5>
              </Col>
              <Col className='d-flex align-items-center justify-content-center' xs='1'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Total Price</h5>
              </Col>
              <Col className='d-flex align-items-center justify-content-center' xs='1'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Quantity</h5>
              </Col>
              <Col className='d-flex align-items-center' xs='2'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Email</h5>
              </Col>
              <Col className='d-flex align-items-center' xs='1'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Address</h5>
              </Col>
              <Col className='d-flex align-items-center' xs='1'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Status</h5>
              </Col>
              <Col className='d-flex align-items-center' xs='2'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Create Date</h5>
              </Col>
              <Col className='d-flex align-items-center' xs='2'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Update Date</h5>
              </Col>
              <Col className='d-flex align-items-center' xs='1'>
                <h5 style={{ margin: '0', fontSize: '18px', color: 'white' }}>Action</h5>
              </Col>
            </Row>
            {dashboardReducer.dataOfContentOrder.orderList.length > 0 ?
              <>
                {
                  dashboardReducer.dataOfContentOrder.orderList.map((order) => {
                    let objCreateDate = new Date(order.createdAt)
                    let createDate = `${objCreateDate.getDate()}-${objCreateDate.getMonth() + 1}-${objCreateDate.getFullYear()} ${objCreateDate.getHours()}:${objCreateDate.getMinutes()}:${objCreateDate.getSeconds()}`
                    let objUpdateDate = new Date(order.updatedAt)
                    let updateDate = `${objUpdateDate.getDate()}-${objUpdateDate.getMonth() + 1}-${objUpdateDate.getFullYear()} ${objUpdateDate.getHours()}:${objUpdateDate.getMinutes()}:${objUpdateDate.getSeconds()}`

                    return (
                      <Row
                        onClick={handleClickItemTable}
                        data-order-id={order._id}
                        data-order-code={order.orderCode}
                        className='hover-color content-order-dashboard-wrapper'
                        key={order.orderCode}
                        style={{ padding: '12px 0', borderBottom: '1px solid #ccc', alignItems: 'center' }}
                      >
                        <Col xs='1' className='d-flex align-items-center justify-content-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{order.orderCode}</h5>
                        </Col>
                        <Col xs='1' className='d-flex align-items-center justify-content-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>${order.cost.toLocaleString()}</h5>
                        </Col>
                        <Col xs='1' className='d-flex align-items-center justify-content-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{order.totalQuantity}</h5>
                        </Col>
                        <Col xs='2' className='d-flex align-items-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{order.email}</h5>
                        </Col>
                        <Col xs='1' className='d-flex align-items-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{order.address}</h5>
                        </Col>
                        <Col xs='1' className='d-flex align-items-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{order.status}</h5>
                        </Col>
                        <Col xs='2' className='d-flex align-items-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{createDate}</h5>
                        </Col>
                        <Col xs='2' className='d-flex align-items-center'>
                          <h5 style={{ margin: '0', fontSize: '18px' }}>{updateDate}</h5>
                        </Col>
                        <Col xs='1'>
                          {/* btn edit step < 6 => can edit*/}
                          <ButtonAction
                            data-order-code={order.orderCode}
                            data-order-id={order._id}
                            onClick={handleClickBtnEditOrder}
                            id={order.orderCode}
                            bgColor='#FFA500'
                            color='white'
                            className='hover-basic btn-action-order-dashboard'
                            style={{
                              display: order.step < 6 ? 'block' : 'none'
                            }}
                          >
                            Edit
                          </ButtonAction>
                          <ButtonAction
                            data-order-code={order.orderCode}
                            data-order-id={order._id}
                            id={order.orderCode}
                            bgColor='#ccc'
                            color='white'
                            className='btn-action-order-dashboard'
                            style={{
                              display: order.step === 6 ? 'block' : 'none'
                            }}
                          >
                            Edit
                          </ButtonAction>
                          {/* end btn edit */}

                          {/* btn delete: step =< 2 => can delete */}
                          <ButtonAction
                            data-order-code={order.orderCode}
                            data-order-id={order._id}
                            onClick={handleClickCancelOrder}
                            bgColor='red'
                            color='white'
                            className='hover-basic btn-action-order-dashboard'
                            style={{
                              display: order.step < 3 ? 'block' : 'none'
                            }}
                          >
                            Cancel
                          </ButtonAction>
                          <ButtonAction
                            data-order-code={order.orderCode}
                            data-order-id={order._id}
                            bgColor='#ccc'
                            color='white'
                            className='btn-action-order-dashboard'
                            style={{
                              display: order.step > 2 ? 'block' : 'none'
                            }}
                          >
                            Cancel
                          </ButtonAction>
                          {/* end btn delete */}
                        </Col>
                      </Row>
                    )
                  })
                }
              </>
              :
              <div className='text-center' style={{ padding: '30px 0' }}>
                <h4 style={{ margin: '0' }}>Have no orders!</h4>
              </div>
            }
          </div>
        </div>
        <Row style={{
          paddingTop: '30px',
          alignItems: 'center',
          display: dashboardReducer.dataOfContentOrder.orderList.length > 0 ? 'flex' : 'none'
        }}>
          <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }} className='d-flex justify-content-end'>
            <PaginationContentOrderDashboard numberPage={dashboardReducer.dataOfContentOrder.numberPage} currentPage={dashboardReducer.dataOfContentOrder.currentPage} />
          </Col>
        </Row>
      </div>

      {/* <div className='text-center' style={{ display: orderReducer.statusPendingFetchOrder ? 'block' : 'none' }}>
        <CircularProgress />
      </div> */}

      <ModalConfirmDeleteOrderDashboard currentOrderId={currentOrderId} openModalConfirmDelete={openModalConfirmDelete} onCloseModalConfirmDelete={handleCloseModalConfirmDelete} />
    </div>
  )
}

export default ContentOrders