import { Pagination } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataOfContentOrder } from '../../../../../actions/dashboardAction'

const PaginationContentOrderDashboard = ({ currentPage, numberPage }) => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)

  const dispatch = useDispatch()

  const handleChangePageOrderDashboard = (e, value) => {
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentOrder(value, dashboardReducer.dataOfContentOrder.limitItem, dashboardReducer.dataOfContentOrder.filterConditon, dashboardReducer.dataOfContentOrder.sortCondition))
  }
  return (
    <div>
      <Pagination onChange={handleChangePageOrderDashboard} count={numberPage} page={currentPage} />
    </div>
  )
}

export default PaginationContentOrderDashboard