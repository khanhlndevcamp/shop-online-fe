import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import { Col, Row } from 'reactstrap';
import { ButtonAction, InputDashboardContent, SelectDashboardContent } from '../../../DashboardContentStyleComponent';
import { useEffect } from 'react';
import { useState } from 'react';
import {
  addMoreProductToOrderDetailEditOrderDashboard,
  callApiCheckVoucherEditOrderDashboard,
  callApiUpdateOrderEditOrderDashboard,
  changeInputEditOrderDashboard,
  changeInputQuantityEditOrderDashboard,
  changeSelectAddMoreProductEditOrderDashboard,
  changeSelectStatusEditOrderDashboard,
  changeStatusCallApiCheckVoucherEditOrderDashboard,
  changeStatusCallApiUpdateOrderDashboard,
  fetchInfoToEditOrderDashboard,
  removeProductToOrderDetailEditOrderDashboard,
  resetDataEditOrderDashboard,
  validateFalseBuyQuantityProductOrderDashboard,
  validateTrueBuyQuantityProductOrderDashboard
} from '../../../../../../actions/dashboardAction';
import { CircularProgress } from '@mui/material';

const EditOrderDashboard = () => {
  const { orderCode, orderId } = useParams()
  const navigate = useNavigate()
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  const [openSelectProduct, setOpenSelectProduct] = useState(false)
  const [showAlertAddMoreProduct, setShowAlertAddMoreProduct] = useState(false)
  const [resultValidateCustomerName, setResultValidateCustomerName] = useState(true)
  const [resultValidatePhoneNumber, setResultValidatePhoneNumber] = useState(true)
  const [resultValidateEmail, setResultValidateEmail] = useState(true)
  const [resultValidateAddress, setResultValidateAddress] = useState(true)
  const [resultValidateStatusOrder, setResultValidateStatusOrder] = useState(true)
  const [resultValidateQuantityProduct, setResultValidateQuantityProduct] = useState(true)


  console.log('edit order dashboard', dashboardReducer.dataOfContentOrder)

  const handleClickGoBack = (e) => {
    navigate('/dashboard/orders')
  }

  const handleClickChooseProductInSelect = (e) => {
    setOpenSelectProduct(false)
    dispatch(changeSelectAddMoreProductEditOrderDashboard(
      e.target.getAttribute('data-product-name'),
      e.target.getAttribute('data-product-price'),
      e.target.getAttribute('data-product-id')
    ))
    // dispatch(changeStatusCallApiUpdateOrderDashboard(false))
    setShowAlertAddMoreProduct(false)
  }

  const handleClickAddMoreProduct = (e) => {
    if (dashboardReducer.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId === '') {
      setShowAlertAddMoreProduct(true)
    } else {
      setShowAlertAddMoreProduct(false)
      dispatch(addMoreProductToOrderDetailEditOrderDashboard())
    }
  }

  const handleChangeInputEditOrder = (e) => {
    dispatch(changeInputEditOrderDashboard(e.target.getAttribute('data-type-input'), e.target.value))
    dispatch(changeStatusCallApiUpdateOrderDashboard(false))
    if (e.target.getAttribute('data-type-input') === 'voucherCode') {
      dispatch(changeStatusCallApiCheckVoucherEditOrderDashboard(false))
    }
  }

  const handleChangeSelectStatusOrder = (e) => {
    let step = +e.target[e.target.selectedIndex].getAttribute('data-step')
    console.log('step change select status', step)
    //param: value select, step
    dispatch(changeSelectStatusEditOrderDashboard(e.target.value, step))
    dispatch(changeStatusCallApiUpdateOrderDashboard(false))
    setResultValidateStatusOrder(true)
  }

  const handleChangeInputQuantityEditOrder = (e) => {
    dispatch(changeInputQuantityEditOrderDashboard(e.target.value, e.target.getAttribute('data-index')))
    dispatch(changeStatusCallApiUpdateOrderDashboard(false))
    setResultValidateQuantityProduct(true)
  }

  const handleClickApplyVoucher = (e) => {
    dispatch(callApiCheckVoucherEditOrderDashboard(dashboardReducer.dataOfContentOrder.infoEditOrder.voucherCode))
  }

  const handleClickBtnUpdateOrder = () => {
    let resultValidateCustomerNameBlock = true
    let resultValidatePhoneNumberBlock = true
    let resultValidateEmailBlock = true
    let resultValidateAddressBlock = true
    let resultValidateStatusOrderBlock = true
    let resultValidateQuantityProductBlock = true

    if (dashboardReducer.dataOfContentOrder.infoEditOrder.customerName === '') {
      resultValidateCustomerNameBlock = false
      setResultValidateCustomerName(false)
    } else {
      resultValidateCustomerNameBlock = true
      setResultValidateCustomerName(true)
    }

    if (isNaN(+dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber) ||
      dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber.length < 9 ||
      dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber.length > 11
    ) {
      resultValidatePhoneNumberBlock = false
      setResultValidatePhoneNumber(false)
    } else {
      resultValidatePhoneNumberBlock = true
      setResultValidatePhoneNumber(true)
    }

    if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(dashboardReducer.dataOfContentOrder.infoEditOrder.email)) {
      resultValidateEmailBlock = false
      setResultValidateEmail(false)
    } else {
      resultValidateEmailBlock = true
      setResultValidateEmail(true)
    }

    if (dashboardReducer.dataOfContentOrder.infoEditOrder.address === '') {
      resultValidateAddressBlock = false
      setResultValidateAddress(false)
    } else {
      resultValidateAddressBlock = true
      setResultValidateAddress(true)
    }

    if (dashboardReducer.dataOfContentOrder.infoEditOrder.status === '') {
      resultValidateStatusOrderBlock = false
      setResultValidateStatusOrder(false)
    } else {
      resultValidateStatusOrderBlock = true
      setResultValidateStatusOrder(true)
    }

    if (dashboardReducer.dataOfContentOrder.infoEditOrder.totalQuantity === 0) {
      resultValidateQuantityProductBlock = false
      setResultValidateQuantityProduct(false)
    } else {
      resultValidateQuantityProductBlock = true
      setResultValidateQuantityProduct(true)
    }

    if (resultValidateCustomerNameBlock &&
      resultValidatePhoneNumberBlock &&
      resultValidateEmailBlock &&
      resultValidateAddressBlock &&
      resultValidateStatusOrderBlock &&
      resultValidateQuantityProductBlock) {

      let flagResultCheckBuyQuantity = true;
      let failProduct = {};
      let buyQuantity = 0;
      (async () => {
        const token = localStorage.getItem('token')
        var headersCreateOrder = new Headers();
        headersCreateOrder.append("Authentication", `Bearer ${token}`);
        headersCreateOrder.append("Content-Type", "application/json");
        //check buy quantity vs stock of product
        var requestOptionsGetResultCheckBuyQuantity = {
          method: 'GET',
          headers: headersCreateOrder,
        };

        // console.log('1 dataResultCheckBuyQuantity', flagResultCheckBuyQuantity)
        for await (let orderDetailItem of dashboardReducer.dataOfContentOrder.infoEditOrder.cloneOrderDetail) {
          if (orderDetailItem.quantity !== 0) {
            const resResultCheckBuyQuantity = await fetch(`${process.env.REACT_APP_HOST_API}/products/validate-buy-quantity/${orderDetailItem.product._id}/${+orderDetailItem.quantity}`, requestOptionsGetResultCheckBuyQuantity)
            const dataResultCheckBuyQuantity = await resResultCheckBuyQuantity.json()
            // console.log('2 dataResultCheckBuyQuantity', dataResultCheckBuyQuantity)
            if (dataResultCheckBuyQuantity.result === false) {
              // await dispatch(changeStatusExpiredDateAccessToken(true))
              // return dispatch({
              //     type: EXPIRED_DATE_ACCESS_TOKEN
              // })
              flagResultCheckBuyQuantity = false
              failProduct = dataResultCheckBuyQuantity.product
              buyQuantity = dataResultCheckBuyQuantity.buyQuantity
              // console.log('3 set dataResultCheckBuyQuantity', dataResultCheckBuyQuantity)
            }
          }
        }

        //end check buy quantity vs stock of product
        // console.log('4 dataResultCheckBuyQuantity, flagResultCheckBuyQuantity', flagResultCheckBuyQuantity)
        if (flagResultCheckBuyQuantity) {
          // console.log('5 callApiCreateOrderDashboard')
          console.log('call api update order')
          //params infoEditOrder, orderId
          dispatch(validateTrueBuyQuantityProductOrderDashboard())
          dispatch(callApiUpdateOrderEditOrderDashboard(dashboardReducer.dataOfContentOrder.infoEditOrder, orderId))
        } else {
          console.log('6 dont call api')
          //param product, buyQuantity
          dispatch(validateFalseBuyQuantityProductOrderDashboard(failProduct, buyQuantity))
        }
      })()

    }

  }

  const handleBlurInputEditOrderDashboard = (e) => {
    const typeInput = e.target.getAttribute('data-type-input')
    switch (typeInput) {
      case 'customerName':
        if (dashboardReducer.dataOfContentOrder.infoEditOrder.customerName === '') {
          setResultValidateCustomerName(false)
        } else {
          setResultValidateCustomerName(true)
        }
        break;
      case 'phoneNumber':
        if (isNaN(+dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber) ||
          dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber.length < 9 ||
          dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber.length > 11
        ) {
          setResultValidatePhoneNumber(false)
        } else {
          setResultValidatePhoneNumber(true)
        }
        break;
      case 'email':
        if (!/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(dashboardReducer.dataOfContentOrder.infoEditOrder.email)) {
          setResultValidateEmail(false)
        } else {
          setResultValidateEmail(true)
        }
        break;
      case 'address':
        if (dashboardReducer.dataOfContentOrder.infoEditOrder.address === '') {
          setResultValidateAddress(false)
        } else {
          setResultValidateAddress(true)
        }
        break;
      default:
        break;
    }
  }

  const handleClickRemoveProductEditOrderDashboard = (e) => {
    setResultValidateQuantityProduct(true)
    const productId = e.target.getAttribute('data-product-id')
    dispatch(removeProductToOrderDetailEditOrderDashboard(productId))
  }

  useEffect(() => {
    const handleResizeEditOrderDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeEditOrderDashboard)
    dispatch(fetchInfoToEditOrderDashboard(orderCode))

    return () => {
      window.removeEventListener('resize', handleResizeEditOrderDashboard)
      dispatch(resetDataEditOrderDashboard())
    }
  }, [])
  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Edit Order</h3>
      <div
        className='text-center'
        style={{ display: dashboardReducer.dataOfContentOrder.fetchPendingContentOrderDashboard ? 'block' : 'none' }}
      >
        <CircularProgress />
      </div>
      <Row>
        <Col lg='6' md='12'>
          <div style={{ margin: '30px auto', width: '100%', maxWidth: '1200px' }}>
            <h5>Billing infomation</h5>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Customer name:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  onBlur={handleBlurInputEditOrderDashboard}
                  onChange={handleChangeInputEditOrder}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.customerName}
                  data-type-input='customerName'
                  placeholder='Customer name'
                  disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
                <span style={{ color: 'red', display: resultValidateCustomerName ? 'none' : 'block' }}>Invalid customer name</span>
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Phone number:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  onBlur={handleBlurInputEditOrderDashboard}
                  onChange={handleChangeInputEditOrder}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.phoneNumber}
                  disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                  data-type-input='phoneNumber'
                  placeholder='Phone number'
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
                <span style={{ color: 'red', display: resultValidatePhoneNumber ? 'none' : 'block' }}>Invalid phone number</span>
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Email:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  onBlur={handleBlurInputEditOrderDashboard}
                  onChange={handleChangeInputEditOrder}
                  disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.email}
                  data-type-input='email'
                  placeholder='Email'
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
                <span style={{ color: 'red', display: resultValidateEmail ? 'none' : 'block' }}>Invalid email</span>
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Address:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  onBlur={handleBlurInputEditOrderDashboard}
                  onChange={handleChangeInputEditOrder}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.address}
                  disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                  data-type-input='address'
                  placeholder='Address'
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
                <span style={{ color: 'red', display: resultValidateAddress ? 'none' : 'block' }}>Invalid address</span>
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Note:</label>
              </Col>
              <Col md='8' xs='12'>
                <InputDashboardContent
                  onChange={handleChangeInputEditOrder}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.note}
                  disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                  data-type-input='note'
                  placeholder={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? '' : 'Note'}
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
              </Col>
            </Row>
            <Row style={{ margin: '30px 0' }}>
              <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <label style={{ fontWeight: 500 }}>Status order:</label>
              </Col>
              <Col md='8' xs='12'>
                <SelectDashboardContent
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.status.toLocaleLowerCase().trim()}
                  onChange={handleChangeSelectStatusOrder}
                  disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step === 6 ? true : false}
                >
                  <option data-step='0' value=''>Choose status order</option>
                  <option
                    disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                    data-step='1'
                    value='created order'
                  >
                    Created order</option>
                  <option
                    disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                    data-step='2'
                    value='waiting for confirmation'
                  >Waiting for confirmation</option>
                  <option
                    disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 3 ? true : false}
                    data-step='3'
                    value='confirmed'
                  >Confirmed</option>
                  <option
                    disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 4 ? true : false}
                    data-step='4'
                    value='packed'
                  >Packed</option>
                  <option
                    disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 5 ? true : false}
                    data-step='5'
                    value='shipping'
                  >Shipping</option>
                  <option
                    data-step='6'
                    value='delivered'
                  >Delivered</option>
                </SelectDashboardContent>
                <span style={{ color: 'red', display: resultValidateStatusOrder ? 'none' : 'block' }}>Invalid status</span>
              </Col>
            </Row>
          </div>
        </Col>

        {/* <hr style={{ width: '100%', margin: 'auto', maxWidth: '700px' }} /> */}
        <Col lg='6' md='12'>
          <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
            <h5 style={{ marginBottom: '30px' }}>Order infomation</h5>
            <div style={{ border: '1px solid #ccc', padding: '12px 15px' }}>
              <Row style={{ marginBottom: '30px' }}>
                <Col xs='5' ><b>Product</b></Col>
                <Col xs='3' className='text-center' ><b>Quanity</b></Col>

                <Col
                  xs={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? '4' : '3'}
                  className='text-center' >
                  <b>Price</b>
                </Col>
                <Col
                  xs={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? '0' : '1'}
                  className='text-center' ></Col>
              </Row>
              {dashboardReducer.dataOfContentOrder.infoEditOrder.orderDetail.map((orderDetail, index) => {
                return (
                  <Row key={index} className='my-3'>
                    <Col xs='5' style={{ paddingRight: '20px', color: '#7B5555', fontWeight: 600 }}>
                      {orderDetail.product.name}
                    </Col>
                    <Col xs='3' className='text-center' style={{ fontWeight: 600 }} >
                      <input
                        onChange={handleChangeInputQuantityEditOrder}
                        style={{
                          display: 'inline',
                          width: '35px',
                          outline: 'none',
                          border: '1px solid #ccc',
                          borderRadius: '5px',
                          paddingLeft: '4px'
                        }}
                        disabled={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? true : false}
                        value={orderDetail.quantity}
                        type='number'
                        data-id-order-detail={orderDetail._id}
                        data-index={index}
                      />
                    </Col>
                    <Col
                      xs={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? '4' : '3'}
                      className='text-center'
                      style={{ color: '#7B5555', fontWeight: 600 }}
                    >
                      ${Math.round(orderDetail.quantity * orderDetail.product.promotionPrice).toLocaleString()}
                    </Col>
                    <Col
                      onClick={handleClickRemoveProductEditOrderDashboard}
                      xs={dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? '0' : '1'}
                      className='text-center hover-underline'
                      style={{
                        color: 'red',
                        fontWeight: 600,
                        display: dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? 'none' : 'block'
                      }}
                      data-index={index}
                      data-product-id={orderDetail.product._id}

                    >
                      x
                    </Col>
                  </Row>
                )
              })}
            </div>
            <span style={{ color: 'red', display: resultValidateQuantityProduct ? 'none' : 'block' }}>At least 1 product per a transaction</span>
            <div style={{
              color: 'red',
              display: dashboardReducer.dataOfContentOrder.resultCheckBuyQuantityProduct.result ? 'none' : 'block'
            }}
            >
              <p>The buy quantity larger than the quantity of stock</p>
              <p>Name product: {dashboardReducer.dataOfContentOrder.resultCheckBuyQuantityProduct.product.name}</p>
              <p>Buy quantity: {dashboardReducer.dataOfContentOrder.resultCheckBuyQuantityProduct.buyQuantity}</p>
              <p>Current stock: {dashboardReducer.dataOfContentOrder.resultCheckBuyQuantityProduct.product.amount}</p>
            </div>
            <Row
              style={{
                margin: '30px 0',
                alignItems: 'center',
                display: dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? 'none' : 'flex'
              }}>
              <Col md='8' xs='12'>
                <div
                  style={{
                    border: '1px solid #ccc',
                    outline: 'none',
                    borderRadius: '5px',
                    width: '100%',
                    padding: '5px 12px',
                    background: 'white',
                    position: 'relative',
                    margin: 0
                  }}
                >
                  <div
                    onClick={() => { setOpenSelectProduct(prev => !prev) }}
                    className='hover-basic'
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                    }}
                  >
                    <span>{dashboardReducer.dataOfContentOrder.infoEditOrder.infoProductAddMore.productName}</span>
                    <span>
                      <KeyboardArrowDownIcon />
                    </span>
                  </div>

                  <div style={{
                    display: openSelectProduct ? 'block' : 'none',
                    position: 'absolute',
                    background: 'white',
                    top: '100%',
                    left: 0,
                    right: 0,
                    border: '1px solid #ccc',
                    zIndex: 20,
                    maxHeight: '300px',
                    overflow: 'auto',
                    transition: 'all ease .1s'
                  }}>
                    <p
                      onClick={handleClickChooseProductInSelect}
                      data-product-id=''
                      className='hover-basic'
                      data-product-name='Choose product'
                      data-product-price={0}
                      style={{ margin: '12px', borderBottom: '1px solid #ccc', paddingBottom: '5px' }}>
                      Choose product
                    </p>
                    {dashboardReducer.dataOfContentOrder.infoEditOrder.productList.map((product, index) => {
                      return (
                        <p
                          onClick={handleClickChooseProductInSelect}
                          className='hover-basic'
                          data-product-id={product._id}
                          data-product-name={product.name}
                          data-product-price={product.promotionPrice}
                          key={index}
                          style={{ margin: '12px', borderBottom: '1px solid #ccc', paddingBottom: '5px' }}
                        >
                          {product.name} / ${product.promotionPrice}
                        </p>
                      )
                    })}
                  </div>
                </div>
                <span style={{ color: 'red', display: showAlertAddMoreProduct ? 'inline-block' : 'none' }}>Please choose product!</span>

              </Col>
              <Col md='4' xs='12' className='d-flex align-items-center justify-content-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <ButtonAction
                  bgColor='rgb(111, 111, 111)'
                  className='hover-basic'
                  onClick={handleClickAddMoreProduct}
                >
                  Add more product
                </ButtonAction>
              </Col>
            </Row>
            <Row
              style={{
                margin: '30px 0',
                alignItems: 'center',
                display: dashboardReducer.dataOfContentOrder.infoEditOrder.step > 2 ? 'none' : 'flex'
              }}>

              <Col md='8' xs='12'>
                <InputDashboardContent
                  onChange={handleChangeInputEditOrder}
                  value={dashboardReducer.dataOfContentOrder.infoEditOrder.voucherCode}
                  data-type-input='voucherCode'
                  placeholder='Voucher'
                  style={{
                    outline: 'none',
                    border: '1px solid #ccc',
                    borderRadius: '5px',
                    width: '100%'
                  }}
                />
                <span
                  style={{
                    color: dashboardReducer.dataOfContentOrder.resultCallApiCheckVoucher ? 'green' : 'red',
                    display: dashboardReducer.dataOfContentOrder.statusClickCallApiCheckVoucherOrder ? 'block' : 'none'
                  }}
                >
                  {dashboardReducer.dataOfContentOrder.resultCallApiCheckVoucher ? 'Valid voucher' : 'Invalid voucher'}
                </span>
              </Col>
              <Col md='4' xs='12' className='d-flex align-items-center justify-content-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
                <ButtonAction
                  bgColor='green'
                  className='hover-basic'
                  onClick={handleClickApplyVoucher}
                >
                  Apply voucher
                </ButtonAction>
              </Col>
            </Row>
            <Row >
              <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                <div style={{ background: '#f7f0f0', padding: '20px' }}>
                  <p><b>Cart total</b></p>
                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Total Quantity</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>{dashboardReducer.dataOfContentOrder.infoEditOrder.totalQuantity} items</b>
                    </Col>
                  </Row>

                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Discount percent</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>{dashboardReducer.dataOfContentOrder.infoEditOrder.discountPercent}%</b>
                    </Col>
                  </Row>
                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Discount price</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>- ${dashboardReducer.dataOfContentOrder.infoEditOrder.discountPrice}</b>
                    </Col>
                  </Row>

                  <hr />
                  <Row className='d-flex justify-content-between align-items-center' style={{ margin: '20px 0' }}>
                    <Col style={{ padding: 0 }} xs='6'><b>Total</b></Col>
                    <Col
                      xs='6'
                      style={{ color: 'red', padding: 0, textAlign: 'right' }}
                    >
                      <b>${dashboardReducer.dataOfContentOrder.infoEditOrder.totalPrice}</b>
                    </Col>
                  </Row>
                </div>
              </Col>
              <Col
                style={{
                  display: dashboardReducer.dataOfContentOrder.fetchPendingContentOrderDashboard === false &&
                    dashboardReducer.dataOfContentOrder.statusChangeInputEditOrder === false
                    ? 'flex' : 'block',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  textAlign: 'right',
                  padding: '0 12px'
                }}
              >
                <p
                  style={{
                    display: dashboardReducer.dataOfContentOrder.fetchPendingContentOrderDashboard === false &&
                      dashboardReducer.dataOfContentOrder.statusChangeInputEditOrder === false
                      ? 'flex' : 'none',
                    margin: 0,
                    color: dashboardReducer.dataOfContentOrder.statusCallApiUpdateOrder ? 'green' : 'red',
                    fontSize: '18px',
                    fontWeight: 500
                  }}
                  className='align-items-center'
                >
                  {dashboardReducer.dataOfContentOrder.statusCallApiUpdateOrder ?
                    <>
                      <CheckCircleIcon style={{ marginRight: '8px' }} />
                      Edit order successfully
                    </>
                    :
                    <>
                      <CancelIcon style={{ marginRight: '8px' }} />
                      Edit order fail
                    </>
                  }
                </p>
                <ButtonAction
                  onClick={handleClickBtnUpdateOrder}
                  className='hover-basic'
                  bgColor='#FFA500'
                  color='white'
                  style={{
                    display: dashboardReducer.dataOfContentOrder.infoEditOrder.step === 6 ? 'none' : 'inline-block'
                  }}
                >
                  Update
                </ButtonAction>
                <ButtonAction
                  bgColor='#ccc'
                  color='white'
                  style={{
                    display: dashboardReducer.dataOfContentOrder.infoEditOrder.step === 6 ? 'inline-block' : 'none'
                  }}
                >
                  Update
                </ButtonAction>
              </Col>
            </Row>
          </div>
        </Col>

      </Row>
    </div >
  )
}

export default EditOrderDashboard