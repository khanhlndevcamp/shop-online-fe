import React from 'react'
import { Col, Row } from 'reactstrap'
import { ButtonAction, InfoOverview, SelectDashboardContent } from '../../DashboardContentStyleComponent'
import { useEffect } from 'react'
import PaginantionContentProduct from './PaginationContentProducts'
import { useDispatch, useSelector } from 'react-redux'
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import {
  callApiUpdateStatusProductDashboard,
  changeInputBrandProductFilter,
  changeInputNameProductFilter,
  changeSelectSortProductDashboard,
  changeSelectStatusProductFilter,
  changeSelectTypeProductFilter,
  fetchDataOfContentProduct,
  fetchInfoToEditProductDashboard
} from '../../../../../actions/dashboardAction'
import { useNavigate } from 'react-router-dom'
import { CircularProgress } from '@mui/material'

const ContentProducts = () => {

  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleChangeInputNameProductFilter = (e) => {
    dispatch(changeInputNameProductFilter(e.target.value))
    const filterConditon = {
      name: e.target.value,
      brand: dashboardReducer.dataOfContentProduct.filterConditon.brand,
      type: dashboardReducer.dataOfContentProduct.filterConditon.type,
      status: dashboardReducer.dataOfContentProduct.filterConditon.status,
    }

    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(1, dashboardReducer.dataOfContentProduct.limitItem, filterConditon, dashboardReducer.dataOfContentProduct.sortCondition))
  }

  const handleChangeInputBrandProductFilter = (e) => {
    dispatch(changeInputBrandProductFilter(e.target.value))
    const filterConditon = {
      brand: e.target.value,
      name: dashboardReducer.dataOfContentProduct.filterConditon.name,
      type: dashboardReducer.dataOfContentProduct.filterConditon.type,
      status: dashboardReducer.dataOfContentProduct.filterConditon.status,
    }

    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(1, dashboardReducer.dataOfContentProduct.limitItem, filterConditon, dashboardReducer.dataOfContentProduct.sortCondition))
  }

  const handleChangeFilterTypeProduct = (e) => {
    dispatch(changeSelectTypeProductFilter(e.target.value))
    const filterConditon = {
      brand: dashboardReducer.dataOfContentProduct.filterConditon.brand,
      name: dashboardReducer.dataOfContentProduct.filterConditon.name,
      type: e.target.value,
      status: dashboardReducer.dataOfContentProduct.filterConditon.status,
    }

    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(1, dashboardReducer.dataOfContentProduct.limitItem, filterConditon, dashboardReducer.dataOfContentProduct.sortCondition))
  }

  const handleChangeFilterStatusProduct = (e) => {
    dispatch(changeSelectStatusProductFilter(e.target.value))
    const filterConditon = {
      brand: dashboardReducer.dataOfContentProduct.filterConditon.brand,
      name: dashboardReducer.dataOfContentProduct.filterConditon.name,
      type: dashboardReducer.dataOfContentProduct.filterConditon.type,
      status: e.target.value,
    }

    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(1, dashboardReducer.dataOfContentProduct.limitItem, filterConditon, dashboardReducer.dataOfContentProduct.sortCondition))
  }

  const handleClickBtnEditProduct = (e) => {
    // console.log('poruct id', e.target.getAttribute('data-product-id'))
    dispatch(fetchInfoToEditProductDashboard(e.target.getAttribute('data-product-id')))
    navigate(`/dashboard/products/edit/${e.target.getAttribute('data-product-id')}`)
  }

  const handleClickBtnAddProduct = (e) => {
    navigate('/dashboard/products/add')
  }

  const handleClickBtnActiveProduct = (e) => {
    //param status, productId, currentPage, limitItem, filterConditon, sortConditon
    dispatch(callApiUpdateStatusProductDashboard(
      true,
      e.target.getAttribute('data-product-id'),
      dashboardReducer.dataOfContentProduct.currentPage,
      dashboardReducer.dataOfContentProduct.limitItem,
      dashboardReducer.dataOfContentProduct.filterConditon,
      dashboardReducer.dataOfContentProduct.sortCondition
    ))
  }

  const handleClickBtnDisableProduct = (e) => {
    //param status, productId
    dispatch(callApiUpdateStatusProductDashboard(
      false,
      e.target.getAttribute('data-product-id'),
      dashboardReducer.dataOfContentProduct.currentPage,
      dashboardReducer.dataOfContentProduct.limitItem,
      dashboardReducer.dataOfContentProduct.filterConditon,
      dashboardReducer.dataOfContentProduct.sortCondition
    ))
  }



  const handleChangeSelectSortProduct = (e) => {
    console.log('change select sort:', e.target.options[e.target.selectedIndex].getAttribute('data-type-option'))
    console.log('change select sort:', e.target.value)
    let typeOption = e.target.options[e.target.selectedIndex].getAttribute('data-type-option')
    //param typeOption, value
    dispatch(changeSelectSortProductDashboard(typeOption, e.target.value))
    let sortCondition = {
      nameSort: '',
      priceSort: '',
      soldSort: '',
    }
    switch (e.target.value) {
      case 'nameSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'nameSort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'priceSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'priceSort-1':
        sortCondition[typeOption] = '-1'
        break;
      case 'soldSort1':
        sortCondition[typeOption] = '1'
        break;
      case 'soldSort-1':
        sortCondition[typeOption] = '-1'
        break;
      default:
        break;
    }
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(1, dashboardReducer.dataOfContentProduct.limitItem, dashboardReducer.dataOfContentProduct.filterConditon, sortCondition))
  }

  const handleClickItemTable = (e) => {
    console.log('poruct id', e.target.closest('.btn-edit-product-dashboard') === e.target)
    if (e.target.closest('.btn-edit-product-dashboard') !== e.target) {
      let productId = e.target.closest('.content-product-dashboard-wrapper').getAttribute('data-product-id')
      navigate(`/dashboard/products/detail/${productId}`)
    }
  }

  console.log('sort condtion:', dashboardReducer.dataOfContentProduct.sortCondition)
  console.log('fetchPendingDashboard:', dashboardReducer.fetchPendingDashboard)
  useEffect(() => {
    // const handleResizeConententProducts = (e) => {
    //   setIsSmallMobile(window.innerWidth < 470)
    //   setIsMobile(window.innerWidth < 670)
    // }
    window.scrollTo(0, 0)
    // window.addEventListener('resize', handleResizeConententProducts)
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(dashboardReducer.dataOfContentProduct.currentPage, dashboardReducer.dataOfContentProduct.limitItem, dashboardReducer.dataOfContentProduct.filterConditon, dashboardReducer.dataOfContentProduct.sortCondition))

    return () => {
      // dispatch(resetDataContentProductDashboard())
      // window.removeEventListener('resize', handleResizeConententProducts)
    }
  }, [])
  return (
    <div>
      <h3 style={{ textAlign: 'center', margin: '20px 0' }}>Product</h3>
      <Row style={{ width: '100%', maxWidth: '800px', margin: '20px auto', justifyContent: 'center' }}>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} sm='6' xs='12' >
          <input
            style={{
              border: '1px solid #ccc',
              outline: 'none',
              padding: '5px 12px',
              borderRadius: '5px',
              width: '100%'
            }}
            value={dashboardReducer.dataOfContentProduct.filterConditon.name}
            onChange={handleChangeInputNameProductFilter}
            placeholder='Name product'
          />
        </Col>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} sm='6' xs='12' >
          <input
            style={{
              border: '1px solid #ccc',
              outline: 'none',
              padding: '5px 12px',
              borderRadius: '5px',
              width: '100%'
            }}
            value={dashboardReducer.dataOfContentProduct.filterConditon.brand}
            onChange={handleChangeInputBrandProductFilter}
            placeholder='Brand product'
          />
        </Col>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} sm='6' xs='12' >
          <SelectDashboardContent
            value={dashboardReducer.dataOfContentProduct.filterConditon.status}
            onChange={handleChangeFilterStatusProduct}
          >
            <option value=''>All status</option>
            <option value={true} >Active</option>
            <option value={false} >Disable</option>
          </SelectDashboardContent>
        </Col>
        <Col className='d-flex justify-content-center' style={{ margin: '10px 0' }} sm='6' xs='12' >
          <SelectDashboardContent
            value={dashboardReducer.dataOfContentProduct.filterConditon.type}
            onChange={handleChangeFilterTypeProduct}
          >
            <option value=''>All type product</option>
            {dashboardReducer.dataOfContentProduct.allProductTypes.length > 0 ?
              dashboardReducer.dataOfContentProduct.allProductTypes.map((productType, index) => {
                return (
                  <option key={index} value={productType._id}>{productType.name}</option>
                )
              })
              :
              null
            }
          </SelectDashboardContent>
        </Col>
      </Row>
      <div 
      className='text-center'
      style={{display: dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard ? 'block' : 'none'}}
      >
        <CircularProgress />
      </div>
      <Row style={{ marginTop: '30px' }}>
        <h3>Overal</h3>
        <hr />
        <Col style={{ margin: '10px 0' }} xl='6' xs='12'>
          <InfoOverview bgColor='#1385FF' color='white' >
            <div className='d-flex justify-content-between align-items-center'>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {dashboardReducer.dataOfContentProduct.overal.totalProducts}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total products</p>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#0069D9' }} />
            </div>
          </InfoOverview>
        </Col>

        <Col style={{ margin: '10px 0' }} xl='6' xs='12'>
          <InfoOverview bgColor='#37AFC2' color='white' >
            <div className='d-flex justify-content-between align-items-center'>
              <div style={{ paddingRight: '12px', }}>
                <h4 style={{ fontWeight: 700, fontSize: '35px' }}>
                  {dashboardReducer.dataOfContentProduct.overal.totalQuantitySold}
                </h4>
                <p style={{ margin: 0, fontWeight: 500, fontSize: '20px' }}>Total quantity sold</p>
              </div>
              <QueryStatsIcon style={{ fontSize: '75px', opacity: '0.8', color: '#117A8B' }} />
            </div>
          </InfoOverview>
        </Col>
      </Row>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginBottom: '0px', padding: '0 0px' }}>
        <div>
          <label style={{ fontSize: '18px', display: 'inline-block', margin: '5px' }}><b>Sort:</b></label>
          <SelectDashboardContent
            className='hover-basic'
            onChange={handleChangeSelectSortProduct}
            value={dashboardReducer.dataOfContentProduct.valueSelectSort}
          >
            <option data-type-option='nameSort' value={'nameSort1'} >{`Name A - Z`}</option>
            <option data-type-option='nameSort' value={'nameSort-1'}>{`Name Z - A`}</option>
            <option data-type-option='priceSort' value={'priceSort1'}>{`Ascending price`}</option>
            <option data-type-option='priceSort' value={'priceSort-1'}>{`Descending price`}</option>
            <option data-type-option='soldSort' value={'soldSort1'}>{`Ascending sold quantity`}</option>
            <option data-type-option='soldSort' value={'soldSort-1'}>{`Descending sold quantity`}</option>
          </SelectDashboardContent>
        </div>
        <ButtonAction
          className='hover-basic'
          bgColor='#28A544'
          style={{ fontSize: '16px' }}
          color='white'
          onClick={handleClickBtnAddProduct}
        >
          + Add new
        </ButtonAction>
      </div>
      <div style={{ width: '100%', padding: '20px 0', overflowX: 'auto' }}>
        <div style={{ width: '98%', minWidth: '1600px', margin: '0 auto' }}>
          <Row
            style={{ padding: '12px 0', borderBottom: '1px solid #ccc', background: '#32343A' }}
          >
            <Col xs='2'>
              <h5 style={{ textAlign: 'center', fontSize: '18px', color: 'white', margin: '0' }}>Image</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Name</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Brand</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Color</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Type</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Price</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Sold</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Stock</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Create Date</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Update Date</h5>
            </Col>
            <Col xs='1'>
              <h5 style={{ fontSize: '19px', color: 'white', margin: '0', transition: 'all ease 0.3s' }}>Action</h5>
            </Col>
          </Row>
          {dashboardReducer.dataOfContentProduct.productList.length > 0 ?
            <>
              {dashboardReducer.dataOfContentProduct.productList.map((product, index) => {
                let objCreateDate = new Date(product.createdAt)
                let createDate = `${objCreateDate.getDate()}-${objCreateDate.getMonth() + 1}-${objCreateDate.getFullYear()} ${objCreateDate.getHours()}:${objCreateDate.getMinutes()}:${objCreateDate.getSeconds()}`
                let objUpdateDate = new Date(product.updatedAt)
                let updateDate = `${objUpdateDate.getDate()}-${objUpdateDate.getMonth() + 1}-${objUpdateDate.getFullYear()} ${objUpdateDate.getHours()}:${objUpdateDate.getMinutes()}:${objUpdateDate.getSeconds()}`
                return (
                  <Row
                    onClick={handleClickItemTable}
                    data-product-id={product._id}
                    className='hover-color content-product-dashboard-wrapper'
                    key={index}
                    style={{
                      padding: '12px 0',
                      borderBottom: '1px solid #ccc',
                      alignItems: 'center',
                      background: product.active ? 'unset' : '#dbd0d0',
                      color: product.active ? 'unset' : '#958686'
                    }}
                  >
                    <Col xs='2' className='d-flex justify-content-center'>
                      <img
                        alt='product'
                        width={'50%'}
                        style={{ minWidth: '70px' }}
                        src={product.imageUrl}
                      />
                    </Col>
                    <Col xs='1'>
                      <h5
                        style={{ fontSize: '19px', transition: 'all ease 0.3s' }}
                      >
                        {product.name}
                      </h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>
                        {product.brand}
                      </h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>{product.color}</h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>{product.type.name}</h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>${product.promotionPrice}</h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s', marginLeft:'10px'  }}>{product.quantitySold}</h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s', marginLeft:'20px' }}>{product.amount}</h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>{createDate}</h5>
                    </Col>
                    <Col xs='1'>
                      <h5 style={{ fontSize: '19px', transition: 'all ease 0.3s' }}>{updateDate}</h5>
                    </Col>
                    <Col xs='1'>
                      <ButtonAction
                        onClick={handleClickBtnEditProduct}
                        data-product-id={product._id}
                        className='hover-basic btn-edit-product-dashboard'
                        bgColor='#FFA500'
                        style={{ fontSize: '19px' }}
                        color='white'
                      >
                        Edit
                      </ButtonAction>
                      <ButtonAction
                        onClick={handleClickBtnActiveProduct}
                        data-product-id={product._id}
                        className='hover-basic btn-edit-product-dashboard'
                        bgColor='violet'
                        style={{ fontSize: '19px', display: product.active ? 'none' : 'inline-block' }}
                        color='white'
                      >
                        Active
                      </ButtonAction>
                      <ButtonAction
                        onClick={handleClickBtnDisableProduct}
                        data-product-id={product._id}
                        className='hover-basic btn-edit-product-dashboard'
                        bgColor='red'
                        style={{ fontSize: '19px', display: product.active ? 'inline-block' : 'none' }}
                        color='white'
                      >
                        Disable
                      </ButtonAction>
                    </Col>
                  </Row>
                )
              })}

            </>
            :
            <Row style={{ paddingTop: '30px' }}>
              <h3 style={{ textAlign: 'center' }}>Have no product!</h3>
            </Row>
          }

        </div>
      </div>
      <Row
        style={{
          paddingTop: '30px',
          alignItems: 'center',
          display: dashboardReducer.dataOfContentProduct.productList.length > 0 ? 'flex' : 'none'
        }}>
        <Col xs='12' className='d-flex' style={{ justifyContent: 'flex-end' }}>
          <PaginantionContentProduct
            numberPage={dashboardReducer.dataOfContentProduct.numberPage}
            currentPage={dashboardReducer.dataOfContentProduct.currentPage}
          />
        </Col>
      </Row>
    </div>
  )
}

export default ContentProducts