import { Pagination } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataOfContentProduct } from '../../../../../actions/dashboardAction'

const PaginantionContentProduct = ({ numberPage, currentPage }) => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()

  const handleChangePageContentProduct = (e, value) => {
    //param: current page, limitItem, filterCondition, sortCondition
    dispatch(fetchDataOfContentProduct(value, dashboardReducer.dataOfContentProduct.limitItem, dashboardReducer.dataOfContentProduct.filterConditon, dashboardReducer.dataOfContentProduct.sortCondition))
  }

  return (
    <div>
      <Pagination onChange={handleChangePageContentProduct} count={numberPage} page={currentPage} />
    </div>
  )
}

export default PaginantionContentProduct