import React from 'react'
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import { useNavigate, useParams } from 'react-router-dom';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import { Col, Row } from 'reactstrap';
import { ButtonAction, InputDashboardContent, SelectDashboardContent } from '../../../DashboardContentStyleComponent';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  callApiAddProductDashboard,
  callApitGetAllProductTypes,
  changeInputAddProductDashboard,
  changeSelectTypeAddProductDashboard,
  changeStatusCallApiAddProductDashboard,
  resetDataAddProductDashBoard,
} from '../../../../../../actions/dashboardAction';
import { CircularProgress } from '@mui/material';
const AddProductDashboard = () => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [resultValidateNameProduct, setResultValidateNameProduct] = useState(true)
  const [resultValidateDescriptionProduct, setResultValidateDescriptionProduct] = useState(true)
  const [resultValidateBrandProduct, setResultValidateBrandProduct] = useState(true)
  const [resultValidateColorProduct, setResultValidateColorProduct] = useState(true)
  const [resultValidateImageUrlProduct, setResultValidateImageUrlProduct] = useState(true)
  const [resultValidateBuyPriceProduct, setResultValidateBuyPriceProduct] = useState(true)
  const [resultValidatePromotionPriceProduct, setResultValidatePromotionPriceProduct] = useState(true)
  const [resultValidateTypeProduct, setResultValidateTypeProduct] = useState(true)
  const [resultValidateStockQuantityProduct, setResultValidateStockQuantityProduct] = useState(true)

  const handleClickGoBack = (e) => {
    navigate('/dashboard/products')
  }

  const handleChangeInputAddProduct = (e) => {
    dispatch(changeInputAddProductDashboard(e.target.getAttribute('data-type-input'), e.target.value))
    dispatch(changeStatusCallApiAddProductDashboard(false))
  }

  const handleChangeSelectTypeAddProduct = (e) => {
    dispatch(changeSelectTypeAddProductDashboard(e.target.value))
    dispatch(changeStatusCallApiAddProductDashboard(false))
  }

  const handleClickBtnAddProduct = (e) => {

    let resultValidateNameProductBlock = false
    let resultValidateDescriptionProductBlock = false
    let resultValidateBrandProductBlock = false
    let resultValidateColorProductBlock = false
    let resultValidateTypeProductBlock = false
    let resultValidateImageUrlProductBlock = false
    let resultValidateBuyPriceProductBlock = false
    let resultValidatePromotionPriceProductBlock = false
    let resultValidateStockQuantityProductBlock = false

    if (dashboardReducer.dataOfContentProduct.infoAddProduct.name === '') {
      setResultValidateNameProduct(false)
      resultValidateNameProductBlock = false
    } else {
      resultValidateNameProductBlock = true
      setResultValidateNameProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoAddProduct.description === '') {
      setResultValidateDescriptionProduct(false)
      resultValidateDescriptionProductBlock = false
    } else {
      resultValidateDescriptionProductBlock = true
      setResultValidateDescriptionProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoAddProduct.brand === '') {
      setResultValidateBrandProduct(false)
      resultValidateBrandProductBlock = false
    } else {
      resultValidateBrandProductBlock = true
      setResultValidateBrandProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoAddProduct.color === '') {
      setResultValidateColorProduct(false)
      resultValidateColorProductBlock = false
    } else {
      resultValidateColorProductBlock = true
      setResultValidateColorProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoAddProduct.imageUrl === '') {
      setResultValidateImageUrlProduct(false)
      resultValidateImageUrlProductBlock = false
    } else {
      resultValidateImageUrlProductBlock = true
      setResultValidateImageUrlProduct(true)
    }

    if (isNaN(+dashboardReducer.dataOfContentProduct.infoAddProduct.amount) ||
      dashboardReducer.dataOfContentProduct.infoAddProduct.amount === '' ||
      +dashboardReducer.dataOfContentProduct.infoAddProduct.amount < 0
    ) {
      setResultValidateStockQuantityProduct(false)
      resultValidateStockQuantityProductBlock = false
    } else {
      setResultValidateStockQuantityProduct(true)
      resultValidateStockQuantityProductBlock = true
    }

    if (dashboardReducer.dataOfContentProduct.infoAddProduct.type === '') {
      setResultValidateTypeProduct(false)
      resultValidateTypeProductBlock = false
    } else {
      resultValidateTypeProductBlock = true
      setResultValidateTypeProduct(true)
    }

    if (isNaN(+dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice) ||
      dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice === ''
    ) {

      setResultValidateBuyPriceProduct(false)
      resultValidateBuyPriceProductBlock = false
    } else {
      resultValidateBuyPriceProductBlock = true
      setResultValidateBuyPriceProduct(true)
    }

    if (isNaN(+dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice) ||
      dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice === '' ||
      +dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice > +dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice
    ) {
      setResultValidatePromotionPriceProduct(false)
      resultValidatePromotionPriceProductBlock = false
    } else {
      resultValidatePromotionPriceProductBlock = true
      setResultValidatePromotionPriceProduct(true)
    }

    if (resultValidateNameProductBlock && resultValidateDescriptionProductBlock &&
      resultValidateBrandProductBlock && resultValidateColorProductBlock &&
      resultValidateImageUrlProductBlock && resultValidateBuyPriceProductBlock &&
      resultValidatePromotionPriceProductBlock && resultValidateTypeProductBlock
    ) {
      dispatch(callApiAddProductDashboard(dashboardReducer.dataOfContentProduct.infoAddProduct))
    }
  }


  const handleBlurInputAddProduct = (e) => {
    const typeInput = e.target.getAttribute('data-type-input')
    switch (typeInput) {
      case 'name':
        if (dashboardReducer.dataOfContentProduct.infoAddProduct.name === '') {
          setResultValidateNameProduct(false)
        } else {
          setResultValidateNameProduct(true)
        }
        break;
      case 'description':
        if (dashboardReducer.dataOfContentProduct.infoAddProduct.description === '') {
          setResultValidateDescriptionProduct(false)
        } else {
          setResultValidateDescriptionProduct(true)
        }
        break;
      case 'brand':
        if (dashboardReducer.dataOfContentProduct.infoAddProduct.brand === '') {
          setResultValidateBrandProduct(false)
        } else {
          setResultValidateBrandProduct(true)
        }
        break;
      case 'color':
        if (dashboardReducer.dataOfContentProduct.infoAddProduct.color === '') {
          setResultValidateColorProduct(false)
        } else {
          setResultValidateColorProduct(true)
        }
        break;
      case 'type':
        if (dashboardReducer.dataOfContentProduct.infoAddProduct.type === '') {
          setResultValidateTypeProduct(false)
        } else {
          setResultValidateTypeProduct(true)
        }
        break;
      case 'imageUrl':
        if (dashboardReducer.dataOfContentProduct.infoAddProduct.imageUrl === '') {
          setResultValidateImageUrlProduct(false)
        } else {
          setResultValidateImageUrlProduct(true)
        }
        break;
      case 'amount':
        if (isNaN(+dashboardReducer.dataOfContentProduct.infoAddProduct.amount) ||
          dashboardReducer.dataOfContentProduct.infoAddProduct.amount === '' ||
          +dashboardReducer.dataOfContentProduct.infoAddProduct.amount < 0
        ) {
          setResultValidateStockQuantityProduct(false)
        } else {
          setResultValidateStockQuantityProduct(true)
        }
        break;
      case 'buyPrice':
        if (isNaN(+dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice) ||
          dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice === ''
        ) {
          setResultValidateBuyPriceProduct(false)
        } else {
          setResultValidateBuyPriceProduct(true)
        }
        break;
      case 'promotionPrice':
        if (isNaN(+dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice) ||
          dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice === '' ||
          +dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice > +dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice
        ) {
          setResultValidatePromotionPriceProduct(false)
        } else {
          setResultValidatePromotionPriceProduct(true)
        }

        break;

      default:
        break;
    }
  }



  useEffect(() => {
    const handleResizeAddProductDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeAddProductDashboard)
    dispatch(callApitGetAllProductTypes())

    return () => {
      window.removeEventListener('resize', handleResizeAddProductDashboard)
      dispatch(resetDataAddProductDashBoard())
    }
  }, [])
  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Add Product</h3>
      <div
        className='text-center'
        style={{ display: dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard ? 'block' : 'none' }}
      >
        <CircularProgress />
      </div>
      <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Name:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.name}
              data-type-input='name'
              placeholder='Name product'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateNameProduct ? 'none' : 'block' }}>Invalid name</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Description:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.description}
              data-type-input='description'
              placeholder='Description'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateDescriptionProduct ? 'none' : 'block' }}>Invalid description</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Brand:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.brand}
              data-type-input='brand'
              placeholder='Brand'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateBrandProduct ? 'none' : 'block' }}>Invalid brand</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Color:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.color}
              data-type-input='color'
              placeholder='Color'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateColorProduct ? 'none' : 'block' }}>Invalid color</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Type:</label>
          </Col>
          <Col md='8' xs='12'>
            <SelectDashboardContent
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.type}
              onChange={handleChangeSelectTypeAddProduct}
            >
              <option value=''>Choose product type</option>
              {dashboardReducer.dataOfContentProduct.allProductTypes.length > 0 ?
                dashboardReducer.dataOfContentProduct.allProductTypes.map((productType, index) => {
                  return (
                    <option key={index} value={productType._id}>{productType.name}</option>
                  )
                })
                :
                null
              }
            </SelectDashboardContent>
            <span style={{ color: 'red', display: resultValidateTypeProduct ? 'none' : 'block' }}>Invalid type</span>

          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Image URL:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.imageUrl}
              data-type-input='imageUrl'
              placeholder='Image URL'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateImageUrlProduct ? 'none' : 'block' }}>Invalid image url</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Stock:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.amount}
              data-type-input='amount'
              type='number'
              placeholder='Stock quantity'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateStockQuantityProduct ? 'none' : 'block' }}>Invalid stock quantity</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Buy price:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.buyPrice}
              data-type-input='buyPrice'
              placeholder='Buy price'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateBuyPriceProduct ? 'none' : 'block' }}>Invalid buy price</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Promotion price:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputAddProduct}
              onChange={handleChangeInputAddProduct}
              value={dashboardReducer.dataOfContentProduct.infoAddProduct.promotionPrice}
              data-type-input='promotionPrice'
              placeholder='Promotion price'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidatePromotionPriceProduct ? 'none' : 'block' }}>Invalid promotion price</span>
          </Col>
        </Row>
        <div style={{
          display: dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard === false &&
            dashboardReducer.dataOfContentProduct.statusChangeInputAddProduct === false
            ? 'flex' : 'block',
          justifyContent: 'space-between',
          alignItems: 'center',
          textAlign: 'right',
          padding: '0 12px'
        }}>
          <p

            style={{
              display:
                dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard === false &&
                  dashboardReducer.dataOfContentProduct.statusChangeInputAddProduct === false
                  ? 'flex' : 'none',
              margin: 0,
              color: dashboardReducer.dataOfContentProduct.statusCallApiAddProduct ? 'green' : 'red',
              fontSize: '18px',
              fontWeight: 500
            }}
            className='align-items-center'>

            {dashboardReducer.dataOfContentProduct.statusCallApiAddProduct ?
              <>
                <CheckCircleIcon style={{ marginRight: '8px' }} />
                Add product successfully
              </>
              :
              <>
                <CancelIcon style={{ marginRight: '8px' }} />
                Add product fail
              </>
            }
          </p>

          <ButtonAction
            onClick={handleClickBtnAddProduct}
            className='hover-basic'
            bgColor='#FFA500'
            color='white'
          >
            Add
          </ButtonAction>
        </div>

      </div>
    </div>
  )
}

export default AddProductDashboard