import React from 'react'
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import { useNavigate, useParams } from 'react-router-dom';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';
import { Col, Row } from 'reactstrap';
import { ButtonAction, InputDashboardContent, SelectDashboardContent } from '../../../DashboardContentStyleComponent';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { callApiUpdateProductDashboard, changeInputEditProductDashboard, changeSelectTypeEditProductDashboard, changeStatusCallApiUpdateProductDashboard, resetDataEditProductDashBoard } from '../../../../../../actions/dashboardAction';
import { CircularProgress } from '@mui/material';
const EditProductDashboard = () => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [resultValidateNameProduct, setResultValidateNameProduct] = useState(true)
  const [resultValidateDescriptionProduct, setResultValidateDescriptionProduct] = useState(true)
  const [resultValidateBrandProduct, setResultValidateBrandProduct] = useState(true)
  const [resultValidateColorProduct, setResultValidateColorProduct] = useState(true)
  const [resultValidateImageUrlProduct, setResultValidateImageUrlProduct] = useState(true)
  const [resultValidateStockQuantityProduct, setResultValidateStockQuantityProduct] = useState(true)
  const [resultValidateBuyPriceProduct, setResultValidateBuyPriceProduct] = useState(true)
  const [resultValidatePromotionPriceProduct, setResultValidatePromotionPriceProduct] = useState(true)
  const { productId } = useParams()

  const handleClickGoBack = (e) => {
    navigate('/dashboard/products')
  }

  const handleChangeInputEditProduct = (e) => {
    dispatch(changeInputEditProductDashboard(e.target.getAttribute('data-type-input'), e.target.value))
    dispatch(changeStatusCallApiUpdateProductDashboard(false))
  }

  const handleChangeSelectTypeEditProduct = (e) => {
    dispatch(changeSelectTypeEditProductDashboard(e.target.value))
    dispatch(changeStatusCallApiUpdateProductDashboard(false))
  }

  const handleClickBtnUpdateProduct = (e) => {

    let resultValidateNameProductBlock = false
    let resultValidateDescriptionProductBlock = false
    let resultValidateBrandProductBlock = false
    let resultValidateColorProductBlock = false
    let resultValidateImageUrlProductBlock = false
    let resultValidateStockQuantityProductBlock = false
    let resultValidateBuyPriceProductBlock = false
    let resultValidatePromotionPriceProductBlock = false

    if (dashboardReducer.dataOfContentProduct.infoEditProduct.name === '') {
      setResultValidateNameProduct(false)
      resultValidateNameProductBlock = false
    } else {
      resultValidateNameProductBlock = true
      setResultValidateNameProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoEditProduct.description === '') {
      setResultValidateDescriptionProduct(false)
      resultValidateDescriptionProductBlock = false
    } else {
      resultValidateDescriptionProductBlock = true
      setResultValidateDescriptionProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoEditProduct.brand === '') {
      setResultValidateBrandProduct(false)
      resultValidateBrandProductBlock = false
    } else {
      resultValidateBrandProductBlock = true
      setResultValidateBrandProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoEditProduct.color === '') {
      setResultValidateColorProduct(false)
      resultValidateColorProductBlock = false
    } else {
      resultValidateColorProductBlock = true
      setResultValidateColorProduct(true)
    }

    if (dashboardReducer.dataOfContentProduct.infoEditProduct.imageUrl === '') {
      setResultValidateImageUrlProduct(false)
      resultValidateImageUrlProductBlock = false
    } else {
      resultValidateImageUrlProductBlock = true
      setResultValidateImageUrlProduct(true)
    }

    if (isNaN(+dashboardReducer.dataOfContentProduct.infoEditProduct.amount) ||
      dashboardReducer.dataOfContentProduct.infoEditProduct.amount === '' ||
      +dashboardReducer.dataOfContentProduct.infoEditProduct.amount < 0
    ) {
      setResultValidateStockQuantityProduct(false)
      resultValidateStockQuantityProductBlock = false
    } else {
      setResultValidateStockQuantityProduct(true)
      resultValidateStockQuantityProductBlock = true
    }

    if (isNaN(+dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice) ||
      dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice === ''
    ) {
      setResultValidateBuyPriceProduct(false)
      resultValidateBuyPriceProductBlock = false
    } else {
      resultValidateBuyPriceProductBlock = true
      setResultValidateBuyPriceProduct(true)
    }

    if (isNaN(+dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice) ||
      dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice === '' ||
      +dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice > +dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice
    ) {
      setResultValidatePromotionPriceProduct(false)
      resultValidatePromotionPriceProductBlock = false
    } else {
      resultValidatePromotionPriceProductBlock = true
      setResultValidatePromotionPriceProduct(true)
    }

    if (resultValidateNameProductBlock && resultValidateDescriptionProductBlock &&
      resultValidateBrandProductBlock && resultValidateColorProductBlock &&
      resultValidateImageUrlProductBlock && resultValidateBuyPriceProductBlock &&
      resultValidatePromotionPriceProductBlock && resultValidateStockQuantityProductBlock
    ) {
      console.log('call api update product', productId)
      dispatch(callApiUpdateProductDashboard(dashboardReducer.dataOfContentProduct.infoEditProduct, productId))
    }
  }

  const handleBlurInputEditProduct = (e) => {
    const typeInput = e.target.getAttribute('data-type-input')
    switch (typeInput) {
      case 'name':
        if (dashboardReducer.dataOfContentProduct.infoEditProduct.name === '') {
          setResultValidateNameProduct(false)
        } else {
          setResultValidateNameProduct(true)
        }
        break;
      case 'description':
        if (dashboardReducer.dataOfContentProduct.infoEditProduct.description === '') {
          setResultValidateDescriptionProduct(false)
        } else {
          setResultValidateDescriptionProduct(true)
        }
        break;
      case 'brand':
        if (dashboardReducer.dataOfContentProduct.infoEditProduct.brand === '') {
          setResultValidateBrandProduct(false)
        } else {
          setResultValidateBrandProduct(true)
        }
        break;
      case 'color':
        if (dashboardReducer.dataOfContentProduct.infoEditProduct.color === '') {
          setResultValidateColorProduct(false)
        } else {
          setResultValidateColorProduct(true)
        }
        break;
      case 'imageUrl':
        if (dashboardReducer.dataOfContentProduct.infoEditProduct.imageUrl === '') {
          setResultValidateImageUrlProduct(false)
        } else {
          setResultValidateImageUrlProduct(true)
        }
        break;
      case 'amount':
        if (isNaN(+dashboardReducer.dataOfContentProduct.infoEditProduct.amount) ||
          dashboardReducer.dataOfContentProduct.infoEditProduct.amount === '' ||
          +dashboardReducer.dataOfContentProduct.infoEditProduct.amount < 0
        ) {
          setResultValidateStockQuantityProduct(false)
        } else {
          setResultValidateStockQuantityProduct(true)
        }
        break;
      case 'buyPrice':
        if (isNaN(+dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice) ||
          dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice === ''
        ) {
          setResultValidateBuyPriceProduct(false)
        } else {
          setResultValidateBuyPriceProduct(true)
        }
        break;
      case 'promotionPrice':
        if (isNaN(+dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice) ||
          dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice === '' ||
          +dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice > +dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice
        ) {
          setResultValidatePromotionPriceProduct(false)
        } else {
          setResultValidatePromotionPriceProduct(true)
        }

        break;

      default:
        break;
    }
  }

  useEffect(() => {
    const handleResizeEditProductDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeEditProductDashboard)

    return () => {
      window.removeEventListener('resize', handleResizeEditProductDashboard)
      dispatch(resetDataEditProductDashBoard())

    }
  }, [])


  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Edit Product</h3>
      <div
        className='text-center'
        style={{ display: dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard ? 'block' : 'none' }}
      >
        <CircularProgress />
      </div>
      <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Name:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.name}
              data-type-input='name'
              placeholder='Name product'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateNameProduct ? 'none' : 'block' }}>Invalid name</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Description:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.description}
              data-type-input='description'
              placeholder='Description'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateDescriptionProduct ? 'none' : 'block' }}>Invalid description</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Brand:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.brand}
              data-type-input='brand'
              placeholder='Brand'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateBrandProduct ? 'none' : 'block' }}>Invalid brand</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Color:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.color}
              data-type-input='color'
              placeholder='Color'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateColorProduct ? 'none' : 'block' }}>Invalid color</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Type:</label>
          </Col>
          <Col md='8' xs='12'>
            <SelectDashboardContent
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.type}
              onChange={handleChangeSelectTypeEditProduct}
            >
              {dashboardReducer.dataOfContentProduct.allProductTypes.length > 0 ?
                dashboardReducer.dataOfContentProduct.allProductTypes.map((productType, index) => {
                  return (
                    <option key={index} value={productType._id}>{productType.name}</option>
                  )
                })
                :
                null
              }
            </SelectDashboardContent>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Image URL:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.imageUrl}
              data-type-input='imageUrl'
              placeholder='Image URL'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateImageUrlProduct ? 'none' : 'block' }}>Invalid image url</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Stock:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.amount}
              data-type-input='amount'
              type='number'
              placeholder='Stock quantity'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateStockQuantityProduct ? 'none' : 'block' }}>Invalid stock quantity</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Buy price:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice}
              data-type-input='buyPrice'
              placeholder='Buy price'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidateBuyPriceProduct ? 'none' : 'block' }}>Invalid buy price</span>
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Promotion price:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              onBlur={handleBlurInputEditProduct}
              onChange={handleChangeInputEditProduct}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice}
              data-type-input='promotionPrice'
              placeholder='Promotion price'
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
            <span style={{ color: 'red', display: resultValidatePromotionPriceProduct ? 'none' : 'block' }}>Invalid promotion price</span>
          </Col>
        </Row>
        <div
          style={{
            display: dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard === false &&
              dashboardReducer.dataOfContentProduct.statusChangeInputEditProduct === false
              ? 'flex' : 'block',
            justifyContent: 'space-between',
            alignItems: 'center',
            textAlign: 'right',
            padding: '0 12px'
          }}
        >
          <p
            style={{
              display: dashboardReducer.dataOfContentProduct.fetchPendingContentProductDashboard === false &&
                dashboardReducer.dataOfContentProduct.statusChangeInputEditProduct === false
                ? 'flex' : 'none',
              margin: 0,
              color: dashboardReducer.dataOfContentProduct.statusCallApiUpdateProduct ? 'green' : 'red',
              fontSize: '18px',
              fontWeight: 500
            }}
            className='align-items-center'
          >
            {dashboardReducer.dataOfContentProduct.statusCallApiUpdateProduct ?
              <>
                <CheckCircleIcon style={{ marginRight: '8px' }} />
                Edit product successfully
              </>
              :
              <>
                <CancelIcon style={{ marginRight: '8px' }} />
                Edit product fail
              </>
            }
          </p>
          <ButtonAction
            onClick={handleClickBtnUpdateProduct}
            className='hover-basic'
            bgColor='#FFA500'
            color='white'
          >
            Update
          </ButtonAction>
        </div>

      </div>
    </div>
  )
}

export default EditProductDashboard