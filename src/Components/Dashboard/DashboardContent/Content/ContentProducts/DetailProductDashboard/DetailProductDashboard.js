import React from 'react'
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import { useNavigate, useParams } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import { InputDashboardContent } from '../../../DashboardContentStyleComponent';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchInfoToEditProductDashboard, resetDataEditProductDashBoard } from '../../../../../../actions/dashboardAction';
const DetailProductDashboard = () => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { productId } = useParams()

  console.log('all product type', dashboardReducer.dataOfContentProduct.allProductTypes)
  console.log('product type', dashboardReducer.dataOfContentProduct.infoEditProduct.type)
  
  let typeProduct = dashboardReducer.dataOfContentProduct.allProductTypes.filter((productType) => {
      return dashboardReducer.dataOfContentProduct.infoEditProduct.type === productType._id
  })
  console.log(' filter product type', typeProduct)

  const handleClickGoBack = (e) => {
    navigate('/dashboard/products')
  }

  useEffect(() => {
    const handleResizeEditProductDashboard = () => {
      setIsMobile(window.innerWidth < 768)
    }
    window.addEventListener('resize', handleResizeEditProductDashboard)
    dispatch(fetchInfoToEditProductDashboard(productId))

    return () => {
      window.removeEventListener('resize', handleResizeEditProductDashboard)
      dispatch(resetDataEditProductDashBoard())
    }
  }, [])

  return (
    <div>
      <p
        onClick={handleClickGoBack}
        style={{ fontWeight: 500, fontSize: '20px', display: 'flex', alignItems: 'center' }}
        className='hover-basic'>
        <ReplyAllIcon style={{ marginRight: '10px' }} />
        Go back
      </p>
      <h3 style={{ textAlign: 'center' }}>Detail Product</h3>
      <div style={{ margin: '30px auto', width: '100%', maxWidth: '600px' }}>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Name:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.name}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
              disabled={true}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Description:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.description}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Brand:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.brand}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Color:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.color}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Type:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={typeProduct.length > 0 ? typeProduct[0].name : ''} 
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Image URL:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.imageUrl}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Buy price:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.buyPrice}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>
        <Row style={{ margin: '30px 0' }}>
          <Col md='4' xs='12' className='d-flex align-items-center' style={{ marginBottom: isMobile ? '8px' : 0 }} >
            <label>Promotion price:</label>
          </Col>
          <Col md='8' xs='12'>
            <InputDashboardContent
              disabled={true}
              value={dashboardReducer.dataOfContentProduct.infoEditProduct.promotionPrice}
              style={{
                outline: 'none',
                border: '1px solid #ccc',
                borderRadius: '5px',
                width: '100%'
              }}
            />
          </Col>
        </Row>


      </div>
    </div>
  )
}

export default DetailProductDashboard