import styled from "styled-components";

export const InfoOverview = styled.div`
    background-color: ${props => props.bgColor ? props.bgColor : 'white'};
    color: ${props => props.color ? props.color : 'black'};
    border: none;
    border-radius: 5px;
    padding: 15px 10px;
    position: relative;
    overflow: hidden;
`

export const ItemTable = styled.p`
    font-weight: 400;
    font-size: 19px;
    margin: 0;
    line-height: 26px;
`

export const ButtonAction = styled.button`
    background: ${props => props.bgColor ? props.bgColor : 'violet'};
    color:  ${props => props.color ? props.color : 'white'};
    font-weight: 500;
    padding: 8px;
    border: none;
    border-radius: 5px;
    outline: none;
    width: 103px;
    margin: 5px;
    max-width: 100%;
`

export const InputDashboardContent = styled.input`
    outline: none;
    border: 1px solid #ccc;
    border-radius: 5px;
    width: 100%;
    padding: 5px 12px
`

export const SelectDashboardContent = styled.select`
        outline: none;
        border: 1px solid #ccc;
        border-radius: 5px;
        width: 100%;
        padding: 5px 12px
`