import { WrapperItemSideBar } from '../DashboardStyledComponent'
import { useDispatch, useSelector } from 'react-redux'
import { clickChooseCustomer, clickChooseDashboard, clickChooseOrder, clickChooseProduc, fetchDataOfContentDashboard, fetchDataOfContentProduct } from '../../../actions/dashboardAction'
import { useNavigate } from 'react-router-dom'

const DashboardSideBar = () => {

  const { dashboardReducer, userReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleClickItemDashboard = (e) => {
    dispatch(clickChooseDashboard())
    navigate('/dashboard')
  }

  const handleClickItemProduct = (e) => {
    dispatch(clickChooseProduc())
    navigate('/dashboard/products')
  }

  const handleClickItemCustomer = (e) => {
    dispatch(clickChooseCustomer())
    navigate('/dashboard/customers')

  }

  const handleClickItemOrder = (e) => {
    dispatch(clickChooseOrder())
    navigate('/dashboard/orders')
  }


  return (
    <div style={{ 
      width: dashboardReducer.openSidebar ? '230px' : 0,
      height: '100%',
      background: '#343A40', 
      color: 'white', 
      transition: 'all ease 0.3s', 
      overflow: 'hidden',
      zIndex: 20,
      position: 'fixed'
      }}>
      <div style={{ padding: '15px 20px', borderBottom: '1px solid #ccc' }}>
        <h3>Management</h3>
      </div>
      <div style={{ padding: '15px 20px', borderBottom: '1px solid #ccc' }}>
        {userReducer.user ?
          <div style={{ position: 'relative' }}>
            <div>
              <img style={{ borderRadius: '50%' }} src={userReducer.user.photoURL} alt='avatar' width={'35px'} />
              <p style={{ display: 'inline-block', margin: '0 12px ', fontSize: '20px' }}><b>{userReducer.user.displayName}</b></p>
            </div>
          </div>
          :
          null
        }
      </div>
      <div style={{ padding: '15px 12px' }}>
        <WrapperItemSideBar
          onClick={handleClickItemDashboard}
          style={{ background: dashboardReducer.chooseDashboard ? '#007BFF' : 'transparent', padding: dashboardReducer.chooseDashboard ? '10px 15px' : '10px 0' }}
          className='wrapper-item-side-bar hover-basic'>
          <p style={{ margin: 0 }}>Dashboard</p>
        </WrapperItemSideBar>
        <WrapperItemSideBar
          onClick={handleClickItemProduct}
          style={{ background: dashboardReducer.chooseProduct ? '#007BFF' : 'transparent', padding: dashboardReducer.chooseProduct ? '10px 15px' : '10px 0' }}
          className='wrapper-item-side-bar hover-basic'>
          <p style={{ margin: 0 }}>Product</p>
        </WrapperItemSideBar>
        <WrapperItemSideBar
          onClick={handleClickItemCustomer}
          style={{ background: dashboardReducer.chooseCustomer ? '#007BFF' : 'transparent', padding: dashboardReducer.chooseCustomer ? '10px 15px' : '10px 0' }}
          className='wrapper-item-side-bar hover-basic'>
          <p style={{ margin: 0 }}>Customer</p>
        </WrapperItemSideBar>
        <WrapperItemSideBar
          onClick={handleClickItemOrder}
          style={{ background: dashboardReducer.chooseOrder ? '#007BFF' : 'transparent', padding: dashboardReducer.chooseOrder ? '10px 15px' : '10px 0' }}
          className='wrapper-item-side-bar hover-basic'>
          <p style={{ margin: 0 }}>Order</p>
        </WrapperItemSideBar>
      </div>
    </div>
  )
}

export default DashboardSideBar