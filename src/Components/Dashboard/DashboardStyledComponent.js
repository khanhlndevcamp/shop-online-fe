import styled from "styled-components";


export const WrapperItemSideBar = styled.div`
    padding: 10px 0;
    border-radius: 5px;
    background-color: transparent;
    font-weight: 500;
    transition: all ease .3s;
`