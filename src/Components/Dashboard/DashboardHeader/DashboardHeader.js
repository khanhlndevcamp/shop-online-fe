import React, { useEffect, useState } from 'react'
import MenuIcon from '@mui/icons-material/Menu';
import { useDispatch, useSelector } from 'react-redux';
import { clickChooseCustomer, clickChooseDashboard, clickChooseOrder, clickChooseProduc, clickMenuDashboard, fetchDataOfContentDashboard, fetchDataOfContentProduct } from '../../../actions/dashboardAction';
import { useNavigate } from 'react-router-dom';
const DashboardHeader = () => {
  const { dashboardReducer } = useSelector(reduxData => reduxData)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [isSmallMobile, setIsSmallMobile] = useState(window.innerWidth < 470)
  const [isMobile, setIsMobile] = useState(window.innerWidth < 1000)

  const handleClickItemDashboard = (e) => {
    dispatch(clickChooseDashboard())
    navigate('/dashboard')
  }

  const handleClickItemProduct = (e) => {
    dispatch(clickChooseProduc())
    navigate('/dashboard/products')
  }

  const handleClickItemCustomer = (e) => {
    dispatch(clickChooseCustomer())
    navigate('/dashboard/customers')

  }

  const handleClickItemOrder = (e) => {
    dispatch(clickChooseOrder())
    navigate('/dashboard/orders')
  }

  const handleClickMenuDashboard = (e) => {
    dispatch(clickMenuDashboard())
  }

  useEffect(() => {
    const handleResizeDashboardHeader = (e) => {
      setIsSmallMobile(window.innerWidth < 470)
      setIsMobile(window.innerWidth < 1000)
    }
    window.addEventListener('resize', handleResizeDashboardHeader)

    return () => {
      window.removeEventListener('resize', handleResizeDashboardHeader)
    }
  }, [])
  return (
    <div
      className='d-flex justify-content-between align-items-center'
      style={{
        height: '50px',
        background: '#FFA500',
        padding: '15px 20px',
        color: '#343A40',
        fontWeight: 500,
        transition: 'all ease 0.3s',
        position: 'fixed',
        left: dashboardReducer.openSidebar && !isMobile ?  '230px' : '0px',
        right: '0',
        zIndex: 15
      }}>
      <div
        onClick={handleClickMenuDashboard}
        style={{ color: '#343A40', display: 'block' }}
        className='hover-basic'
      >
        <MenuIcon />
      </div>
      {isSmallMobile ?
        null
        :
        <div className='d-flex'>
          <div
            style={{ margin: '0 12px' }}
            onClick={handleClickItemDashboard}
            className='wrapper-item-side-bar hover-basic'
          >
            <p style={{ margin: 0 }}>Dashboard</p>
          </div>
          <div
            style={{ margin: '0 12px' }}
            onClick={handleClickItemProduct}
            className='wrapper-item-side-bar hover-basic'
          >
            <p style={{ margin: 0 }}>Product</p>
          </div>
          <div
            style={{ margin: '0 12px' }}
            onClick={handleClickItemCustomer}
            className='wrapper-item-side-bar hover-basic'
          >
            <p style={{ margin: 0 }}>Customer</p>
          </div>
          <div
            style={{ margin: '0 12px' }}
            onClick={handleClickItemOrder}
            className='wrapper-item-side-bar hover-basic'
          >
            <p style={{ margin: 0 }}>Order</p>
          </div>
        </div>
      }
    </div>
  )
}

export default DashboardHeader