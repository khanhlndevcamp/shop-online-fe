import React, { useEffect } from 'react'
import { Container } from 'reactstrap'
import BreadCrumb from '../BreadCrumb/BreadCrumb'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { fetchProductById, fetchProductListRelated } from '../../actions/productAction'
import InfoProduct from './InfoProduct/InfoProduct'
import { CircularProgress } from '@mui/material'

const ProductDetail = () => {

    const fetchProductInfo = useSelector(reduxData => reduxData.productReducer)
    const dispatch = useDispatch()
    const { productId } = useParams()


    const breadCrumbList = [
        {
            url: '/',
            name: 'Home'
        },
        {
            url: '/products',
            name: 'All products'
        },
        {
            name: fetchProductInfo.productById.name ? `${fetchProductInfo.productById.name}` : ''
        }
    ]
    
    useEffect(() => {
        window.scrollTo(0,0)
        dispatch(fetchProductById(productId))
        //params limit item related product
        dispatch(fetchProductListRelated(fetchProductInfo.limitRelateProduct))
    }, [productId])

    return (
        <Container>
            <BreadCrumb breadCrumbList={breadCrumbList} />
            <div style={{ marginTop: '70px' }}>
                {fetchProductInfo.fetchPending ?
                    <div style={{textAlign: 'center'}}>
                        <CircularProgress/>
                    </div>
                :
                    <>
                        <InfoProduct productById={fetchProductInfo.productById} idProduct = {productId} />
                    </>
        }
            </div>
        </Container>
    )
}

export default ProductDetail