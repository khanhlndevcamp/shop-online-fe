import React, { useEffect, useMemo, useRef, useState } from 'react'
import { Col, Row } from 'reactstrap'
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import classNames from 'classnames/bind'
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import styles from './InfoProduct.module.css'
import { ButtonInfoProduct, HeaderInfoImage, HeaderInfoProduct, HeaderInfoWrapper, IconWrapper, MainInfoWrapper, NewPrice, OldPrice, SubImage } from './InfoProductStyledComponent'
import { useDispatch, useSelector } from 'react-redux';
import CardProduct from '../../AllProducts/CardProduct/CardProduct';
import { addToCart } from '../../../actions/cartAction';

const cx = classNames.bind(styles)
const InfoProduct = ({ productById, idProduct }) => {

    const dispatch = useDispatch()
    const [urlMainImage, setUrlMainImage] = useState('')
    const fetchProductInfo = useSelector((reduxData) => reduxData.productReducer)
    const [openViewMore, setOpenViewMore] = useState(false)
    const [quanityProduct, setQuanityProduct] = useState(1)
    const infoProductElement = useRef()
    const [offsetHeight, setOffsetHeight] = useState(0)

    // const listRandomRender = useMemo(() => {
    //     const arrRandonNumber = fetchProductInfo.productsList.filter((product) => {
    //         return product._id !== idProduct
    //     })

    //     const randomNumber = Math.floor(Math.random() * (arrRandonNumber.length - 5))
    //     let listRandomRender = arrRandonNumber.slice(randomNumber, randomNumber + 6)
    //     return listRandomRender
    // }, [productById])

    const handleClickViewMore = (e) => {
        setOpenViewMore(prev => !prev)
    }

    const handleClickChangeImage = (e) => {
        setUrlMainImage(e.target.src)
    }

    const handleClickPlusQuanity = (e) => {
        let quantityStock = +e.target.closest('.plus-quantity-product-detail').getAttribute('data-stock-quantity')
        setQuanityProduct(prev => {
            if(prev >= quantityStock) {
                return prev
            } else {
                return prev + 1
            }
        })
    }

    const handleClickMinusQuanity = (e) => {
        setQuanityProduct(prev => {
            if (prev <= 1) {
                return 1
            } else {
                return prev - 1
            }
        })
    }

    const handleClickAddCart = (e) => {
        let toastInfoProduct = document.getElementById('toast-info-product')
        let messageToast = document.getElementById('toast-message-info-product')
        const clone = messageToast.cloneNode(true);
        clone.style.display = 'flex'
        clone.style.animation = `slideInLeft ease .3s, fadeOut linear 1s 3s forwards `
        toastInfoProduct.appendChild(clone)

        setTimeout(() => {
            toastInfoProduct.removeChild(clone)
        }, 4000)
        dispatch(addToCart(quanityProduct, productById))
    }

    useEffect(() => {
        setUrlMainImage(productById.imageUrl)
        setOpenViewMore(false)
        setOffsetHeight(infoProductElement.current.offsetHeight)
    }, [productById])

    useEffect(() => {
        setQuanityProduct(1)
    }, [idProduct])


    return (
        <>
            <div ref={infoProductElement} style={{ margin: '50px 20px' }}>
                <div id='toast-info-product' style={{ position: 'fixed', top: '130px', right: '0px', zIndex: 9999 }}></div>
                <p id='toast-message-info-product' className={cx('message-toast')} >
                    <CheckCircleIcon style={{color: 'green', marginRight: '10px'}} />
                    Add product successfully!
                </p>
                <div>
                    <HeaderInfoWrapper>
                        {productById._id ?
                            <Row>
                                <Col lg='5' md='6' xs='12'>
                                    <HeaderInfoImage>
                                        <div style={{ display: 'flex', height: '350px', justifyContent: 'center' }}>
                                            <img src={urlMainImage} alt='product' />
                                        </div>
                                        <div style={{ marginTop: '20px' }}>
                                            <span
                                                style={
                                                    {
                                                        margin: '12px',
                                                        display: 'inline-block',
                                                        height: '100px',
                                                        width: '100px'
                                                    }
                                                }
                                            >
                                                <SubImage
                                                    alt='product'
                                                    onClick={handleClickChangeImage}
                                                    style={
                                                        {
                                                            border: '1px solid #ccc',
                                                            display: 'inline-block'
                                                        }
                                                    }
                                                    width={'100%'}
                                                    src={productById.imageUrl}
                                                />
                                            </span>
                                            <span
                                                style={
                                                    {
                                                        margin: '12px',
                                                        display: 'inline-block',
                                                        height: '100px',
                                                        width: '100px'
                                                    }
                                                }
                                            >
                                                <SubImage
                                                    alt='product'
                                                    onClick={handleClickChangeImage}
                                                    style={
                                                        {
                                                            border: '1px solid #ccc',
                                                            display: 'inline-block'
                                                        }
                                                    }
                                                    width={'100%'}
                                                    src={productById.imageUrl}
                                                />
                                            </span>
                                            <span
                                                style={
                                                    {
                                                        margin: '12px',
                                                        display: 'inline-block',
                                                        height: '100px',
                                                        width: '100px'
                                                    }
                                                }
                                            >
                                                <SubImage
                                                    alt='product'
                                                    onClick={handleClickChangeImage}
                                                    style={
                                                        {
                                                            border: '1px solid #ccc',
                                                            display: 'inline-block'
                                                        }
                                                    }
                                                    width={'100%'}
                                                    src={productById.imageUrl}
                                                />
                                            </span>
                                        </div>
                                    </HeaderInfoImage>
                                </Col>

                                <Col lg='7' md='6' xs='12'>
                                    <HeaderInfoProduct>
                                        <h2 style={{ margin: '20px 0 30px' }}>{productById.name}</h2>
                                        <p style={{ fontSize: '18px' }}>
                                            <span style={{ color: '#808080', fontWeight: 500 }}>Brand:</span> {productById.brand}
                                        </p>
                                        <p style={{ fontSize: '18px' }}>
                                            <span style={{ color: '#808080', fontWeight: 500 }}>Color:</span> {productById.color}
                                        </p>
                                        <p style={{ textAlign: 'justify' }}>
                                            Products with electrical plugs are designed for use in the US. Outlets and voltage differ internationally and this product may require an adapter or converter for use in your destination. Please check compatibility before purchasing.
                                        </p>
                                        <p>
                                            <OldPrice>${productById.buyPrice}</OldPrice> <NewPrice>${productById.promotionPrice}</NewPrice>
                                        </p>
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            <IconWrapper onClick={handleClickMinusQuanity}>
                                                <RemoveCircleIcon htmlColor='#808080' />
                                            </IconWrapper>
                                            <span style={{ fontSize: '23px', margin: '0 12px' }}>{quanityProduct}</span>
                                            <IconWrapper onClick={handleClickPlusQuanity} className='plus-quantity-product-detail' data-stock-quantity = {productById.amount}>
                                                <AddCircleIcon  htmlColor='#808080' />
                                            </IconWrapper>
                                        </div>
                                        <p style={{ fontSize: '18px', margin: '20px 0', fontWeight: 500 }}>
                                            <span style={{ color: '#808080', fontWeight: 500 }}>Stock:</span> {productById.amount <= 0 ? 'Sold out' : productById.amount}
                                        </p>
                                        <ButtonInfoProduct onClick={handleClickAddCart}>Add to cart</ButtonInfoProduct>
                                    </HeaderInfoProduct>
                                </Col>
                            </Row>
                            :
                            null
                        }

                    </HeaderInfoWrapper>

                    <MainInfoWrapper>
                        <h4>Description</h4>
                        <ul style={{ listStyle: 'inside', margin: '40px 0' }}>
                            <li style={{ margin: '12px 0' }}>
                                EXQUISITELY CRAFTED 40 MM DRIVERS make for natural, perfectly balanced notes—tight bass, smooth, rounded midrange and high frequencies that sparkle with detail.
                            </li>
                            <li style={{ margin: '12px 0' }}>
                                STATE-OF-THE-ART HYBRID ACTIVE NOISE CANCELING (ANC): Two mics in each ear cup filter out unwanted sounds, leaving you free to immerse. Awareness mode is there to bring the outside world back to you when needed.
                            </li>
                            <li style={{ margin: '12px 0' }}>
                                A MORE BESPOKE LISTENING EXPERIENCE is made possible by the Philips Headphones app. You can fine-tune your headphones’ sound profile and switch between different levels of noise cancelation..Note:If you face issue in Bluetooth connectivity please turn off the Bluetooth function for a couple of minutes, then turn it back on
                            </li>
                            <li style={{ margin: '12px 0' }}>
                                BEST BLUETOOTH STREAMING SOUND QUALITY with AAC and aptX HD support. You can also connect to your high resolution equipment via a wired connection for even more details. These headphones are Hi-Res audio certified.
                            </li>
                        </ul>

                        <div style={{ textAlign: 'center' }}>
                            <img alt='product' src={urlMainImage} width={'50%'} />
                            <ul style={{ listStyle: 'inside', margin: '40px 0', textAlign: 'left' }}>
                                <li style={{ margin: '12px 0' }}>
                                    EXQUISITELY CRAFTED 40 MM DRIVERS make for natural, perfectly balanced notes—tight bass, smooth, rounded midrange and high frequencies that sparkle with detail.
                                </li>
                                <li style={{ margin: '12px 0' }}>
                                    STATE-OF-THE-ART HYBRID ACTIVE NOISE CANCELING (ANC): Two mics in each ear cup filter out unwanted sounds, leaving you free to immerse. Awareness mode is there to bring the outside world back to you when needed.
                                </li>
                                <li style={{ margin: '12px 0' }}>
                                    A MORE BESPOKE LISTENING EXPERIENCE is made possible by the Philips Headphones app. You can fine-tune your headphones’ sound profile and switch between different levels of noise cancelation..Note:If you face issue in Bluetooth connectivity please turn off the Bluetooth function for a couple of minutes, then turn it back on
                                </li>
                                <li style={{ margin: '12px 0' }}>
                                    BEST BLUETOOTH STREAMING SOUND QUALITY with AAC and aptX HD support. You can also connect to your high resolution equipment via a wired connection for even more details. These headphones are Hi-Res audio certified.
                                </li>
                            </ul>
                        </div>
                    </MainInfoWrapper>
                </div>
            </div>

            <div style={{ background: 'white', marginTop: openViewMore ? '0px' : `-${+offsetHeight * 2 / 5}px`, zIndex: 10, position: 'relative', transition: 'all ease 0.4s' }}>
                <div style={{ textAlign: 'center', marginBottom: '30px' }}>
                    <ButtonInfoProduct onClick={handleClickViewMore}>{openViewMore ? 'Show less' : 'View more'}</ButtonInfoProduct>
                </div>
                <h4>Related Products</h4>
                <Row>
                    {fetchProductInfo.productListRelated.map((product, index) => {
                        return (
                            <Col md='4' key={index}>
                                <CardProduct
                                    idProduct={product._id}
                                    name={product.name}
                                    imgUrl={product.imageUrl}
                                    buyPrice={product.buyPrice}
                                    promotionPrice={product.promotionPrice}
                                />
                            </Col>
                        )
                    })}
                </Row>
            </div>
        </>
    )
}

export default InfoProduct