import styled from "styled-components";


export const HeaderInfoWrapper = styled.div`
    margin-bottom: 80px;
`

export const MainInfoWrapper = styled.div`
    margin-top: 130px;
`

export const HeaderInfoImage = styled.div`
    text-align: center;
`
export const SubImage = styled.img`
    &:hover {
        cursor: pointer;
        opacity: 0.8
    }
`

export const OldPrice = styled.span`
    position: relative;
    color: #746464;
    font-weight: 500;
    font-size: 22px;
    &::after {
        content: '';
        position: absolute;
        width: 100%;
        border-top: 2px solid #2a2525;
        top: 50%;
        left: 0;
    }
`
export const NewPrice = styled.span`
    font-weight: 700;
    font-size: 30px;
    color: #C0392B;
    margin: 0 12px;
`

export const ButtonInfoProduct = styled.button`
    padding: 20px 40px;
    background-color: black;
    color: #fff;
    border: none;
    margin-top: 40px;
    text-transform: uppercase;
    &:hover {
        opacity: 0.8;
        font-size: 20px;
        padding: 21px 42px;
        transition: all ease 0.3s;
    }
`


export const IconWrapper = styled.span`
    &:hover {
        cursor: pointer;
        opacity: 0.8
    }
`

export const HeaderInfoProduct = styled.div`
`