import React, { useEffect, useState } from 'react'
import classNames from 'classnames/bind'
import { Container, Row, Col } from 'reactstrap'
import BreadCrumb from '../BreadCrumb/BreadCrumb'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProduct } from '../../actions/productAction'
import { changeValueInputVoucher, editMinusQuanity, editPlusQuanity, fetchCheckVoucherCode, loadDataLocalStorage, removeProductCart, resetStatusChangeCartProduct, updateCart, validateFalseBuyQuantityProductCartProduct, validateTrueBuyQuantityProductCartProduct } from '../../actions/cartAction'
import { useNavigate } from 'react-router-dom'
import PaginationCartProducts from './PaginationCartProducts/PaginationCartProducts'
import { CircularProgress } from '@mui/material'
import ErrorIcon from '@mui/icons-material/Error';
import '../../App.css'

import styles from './CartStyle.module.css'

const cx = classNames.bind(styles)

const CartProducts = () => {

    const dispatch = useDispatch()
    const { cartReducer, productReducer, userReducer } = useSelector(reduxData => reduxData)
    const [numberPage, setNumberPage] = useState(1)
    const [isSmallMobile, setIsSmallMobile] = useState(window.innerWidth < 470)
    // const productsList = productReducer.productsList
    const navigate = useNavigate()

    const breadCrumbList = [
        {
            url: '/',
            name: 'Home'
        },
        {
            name: 'Cart'
        }
    ]

    let productsInCartRender = [];

    // if (productsList.length > 0) {
    productsInCartRender = cartReducer.cartProducts.slice((cartReducer.currentPage - 1) * 4, cartReducer.currentPage * 4)
    // }


    const handleClickPlusQuanity = (e) => {
        dispatch(editPlusQuanity(e.target.getAttribute('data-id-product')))
    }

    const handleClickMinusQuanity = (e) => {
        dispatch(editMinusQuanity(e.target.getAttribute('data-id-product')))
    }

    const handleClickUpdateCart = (e) => {
        dispatch(updateCart())
    }

    const handleClickRemoveProduct = (e) => {
        dispatch(removeProductCart(e.target.getAttribute('data-id-product')))
    }

    const handleClickContinueShopping = () => {
        if (cartReducer.statusChangeCartProduct) {
            if (window.confirm('Your change have been not save, go to another page?')) {
                navigate('/products')
                dispatch(resetStatusChangeCartProduct())
            }
        }
        else {
            navigate('/products')
        }
    }

    const handleClickCheckOut = (e) => {
        if(!cartReducer.statusChangeCartProduct) {
            if (cartReducer.cartProducts.length === 0) {
                navigate('/cart')
                let toastCartProduct = document.getElementById('toast-cart-product')
                let messageToast = document.getElementById('toast-message-cart-product')
                const clone = messageToast.cloneNode(true)
                clone.style.display = 'flex'
                clone.style.animation = `slideInLeft ease .3s, fadeOut linear 1s 3s forwards `
                toastCartProduct.appendChild(clone)
    
                setTimeout(() => {
                    toastCartProduct.removeChild(clone)
                }, 4000)
                return;
            }
            if (userReducer.user === null) {
                alert('Please log in first!')
                navigate('/login/checkout')
            } else {
                console.log('cartReducer.cartProducts', cartReducer.cartProducts)
                let flagResultCheckBuyQuantity = true;
                let failProduct = {};
                let buyQuantity = 0;
                (async () => {
                    const token = localStorage.getItem('token')
                    var headersCreateOrder = new Headers();
                    headersCreateOrder.append("Authentication", `Bearer ${token}`);
                    headersCreateOrder.append("Content-Type", "application/json");
                    //check buy quantity vs stock of product
                    var requestOptionsGetResultCheckBuyQuantity = {
                        method: 'GET',
                        headers: headersCreateOrder,
                    };
    
                    // console.log('1 dataResultCheckBuyQuantity', flagResultCheckBuyQuantity)
                    for await (let productItem of cartReducer.cartProducts) {
                        if (productItem.quantity !== 0) {
                            const resResultCheckBuyQuantity = await fetch(`${process.env.REACT_APP_HOST_API}/products/validate-buy-quantity/${productItem._id}/${+productItem.quanityProduct}`, requestOptionsGetResultCheckBuyQuantity)
                            const dataResultCheckBuyQuantity = await resResultCheckBuyQuantity.json()
                            // console.log('2 dataResultCheckBuyQuantity', dataResultCheckBuyQuantity)
                            if (dataResultCheckBuyQuantity.result === false) {
                                // await dispatch(changeStatusExpiredDateAccessToken(true))
                                // return dispatch({
                                //     type: EXPIRED_DATE_ACCESS_TOKEN
                                // })
                                flagResultCheckBuyQuantity = false
                                failProduct = dataResultCheckBuyQuantity.product
                                buyQuantity = dataResultCheckBuyQuantity.buyQuantity
                                // console.log('3 set dataResultCheckBuyQuantity', dataResultCheckBuyQuantity)
                            }
                        }
                    }
    
                    //end check buy quantity vs stock of product
                    // console.log('4 dataResultCheckBuyQuantity, flagResultCheckBuyQuantity', flagResultCheckBuyQuantity)
                    if (flagResultCheckBuyQuantity) {
                        // console.log('5 callApiCreateOrderDashboard')
                        // console.log('call api create order')
                        navigate('/checkout')
                        //params infoEditOrder, orderId
                        dispatch(validateTrueBuyQuantityProductCartProduct())
                        // dispatch(callApiUpdateOrderEditOrderDashboard(dashboardReducer.dataOfContentOrder.infoEditOrder, orderId))
                    } else {
                        console.log('6 dont call api')
                        //param product, buyQuantity
                        dispatch(validateFalseBuyQuantityProductCartProduct(failProduct, buyQuantity))
                    }
                })()
            }
        }
    }

    const handleChangeInputVoucher = (e) => {
        dispatch(changeValueInputVoucher(e.target.value))
    }

    const handleClickApplyCoupon = (e) => {
        dispatch(fetchCheckVoucherCode(cartReducer.voucherCode))
    }

    useEffect(() => {
        const handleResizeWindow = (e) => {
            setIsSmallMobile(window.innerWidth < 470)
        }
        // dispatch(fetchProduct(0))
        dispatch(loadDataLocalStorage())
        window.scrollTo(0, 0)
        window.addEventListener('resize', handleResizeWindow)

    }, [])


    useEffect(() => {
        setNumberPage(Math.ceil(cartReducer.cartProducts.length / 4))
    }, [cartReducer.cartProducts.length])
    return (
        <Container>
            <BreadCrumb breadCrumbList={breadCrumbList} statusChangeCartProduct={cartReducer.statusChangeCartProduct} />
            <div id='toast-cart-product' style={{ position: 'fixed', top: '130px', right: '0px', zIndex: 9999 }}>
            </div>
            <p id='toast-message-cart-product' className={cx('message-toast')} >
                <ErrorIcon style={{ color: 'green', marginRight: '10px' }} />
                Have no product in cart!
            </p>
            <Row>
                <Col xs='12' style={{ borderBottom: '1px solid #ccc', paddingTop: '20px', paddingBottom: '20px' }}>
                    <Row>
                        <Col xs='5'>
                            <h5 style={{ fontSize: isSmallMobile ? '16px' : '1.25rem', transition: 'all ease 0.3s' }} >Products</h5>
                        </Col>
                        <Col xs='2' className='text-center'>
                            <h5 style={{ fontSize: isSmallMobile ? '16px' : '1.25rem', transition: 'all ease 0.3s' }} >Price</h5>
                        </Col>
                        <Col xs='2' className='text-center'>
                            <h5 style={{ fontSize: isSmallMobile ? '16px' : '1.25rem', transition: 'all ease 0.3s' }} >Quanity</h5>
                        </Col>
                        <Col xs='2' className='text-center'>
                            <h5 style={{ fontSize: isSmallMobile ? '16px' : '1.25rem', transition: 'all ease 0.3s' }} >Total</h5>
                        </Col>
                        <Col xs='1'>
                        </Col>
                    </Row>
                </Col>
                <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px', display: productReducer.fetchPending ? 'block' : 'none' }}>
                    <div style={{ textAlign: 'center' }}>
                        <CircularProgress />
                    </div>
                </Col>
                {productsInCartRender.length > 0 && !productReducer.fetchPending ?
                    <>
                        {
                            productsInCartRender.map((product, index) => {
                                return (
                                    <Col key={index} xs='12' style={{ borderBottom: '1px solid #ccc', paddingTop: '20px', paddingBottom: '20px' }}>
                                        <Row className='align-items-center'>
                                            <Col xs='5'>
                                                <div className='d-flex align-items-center'>
                                                    <span style={{ transition: 'all ease 0.3s', display: 'inline-block', marginRight: '12px', width: isSmallMobile ? '70px' : '100px', height: isSmallMobile ? '70px' : '100px' }}>
                                                        <img alt='product' width={isSmallMobile ? '70px' : '100px'} src={product.imageUrl} />
                                                    </span>
                                                    <span style={{ fontSize: isSmallMobile ? '14px' : '16px', transition: 'all ease 0.3s' }}>{product.name}</span>
                                                </div>
                                            </Col>
                                            <Col xs='2' className='text-center'>
                                                <span><b>${product.promotionPrice.toLocaleString()}</b></span>
                                            </Col>
                                            <Col xs='2'>
                                                <div className='d-flex justify-content-center align-items-center'>
                                                    <button
                                                        onClick={handleClickMinusQuanity}
                                                        data-id-product={product._id}
                                                        className='hover-basic'
                                                        style={
                                                            {
                                                                width: '30px',
                                                                height: '30px',
                                                                background: '#f7f0f0',
                                                                border: 'none'
                                                            }
                                                        }
                                                    >
                                                        -
                                                    </button>
                                                    <span
                                                        style={
                                                            {
                                                                display: 'inline-block',
                                                                minWidth: '30px',
                                                                maxHeight: '30px',
                                                                padding: '3px',
                                                                background: '#f7f0f0',
                                                                border: 'none',
                                                                textAlign: 'center'
                                                            }
                                                        }
                                                    >
                                                        {product.quanityProduct}
                                                    </span>
                                                    <button
                                                        onClick={handleClickPlusQuanity}
                                                        data-id-product={product._id}
                                                        className='hover-basic'
                                                        style={
                                                            {
                                                                width: '30px',
                                                                height: '30px',
                                                                background: '#f7f0f0',
                                                                border: 'none'
                                                            }
                                                        }
                                                    >
                                                        +
                                                    </button>
                                                </div>
                                            </Col>
                                            <Col xs='2' className='text-center'>
                                                <span><b>${Math.round(+product.promotionPrice * +product.quanityProduct).toLocaleString()}</b></span>
                                            </Col>
                                            <Col xs='1'>
                                                <span data-id-product={product._id} onClick={handleClickRemoveProduct} className='hover-underline'>x</span>
                                            </Col>
                                        </Row>
                                    </Col>
                                )
                            })
                        }
                        <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }} className='d-flex justify-content-end'>
                            <PaginationCartProducts numberPage={numberPage} />
                        </Col>
                    </>
                    :
                    <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                        <h5 className='text-center'>Have no product in cart!</h5>
                    </Col>
                }

                <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                    <div style={{
                        color: 'red',
                        display: cartReducer.resultCheckBuyQuantityProduct.result ? 'none' : 'block'
                    }}
                    >
                        <p>The buy quantity larger than the quantity of stock</p>
                        <p>Name product: {cartReducer.resultCheckBuyQuantityProduct.product.name}</p>
                        <p>Buy quantity: {cartReducer.resultCheckBuyQuantityProduct.buyQuantity}</p>
                        <p>Current stock: {cartReducer.resultCheckBuyQuantityProduct.product.amount}</p>
                    </div>
                </Col>
                <Col xs='12' style={{ paddingTop: '20px', paddingBottom: '20px' }} className='d-flex justify-content-between'>
                    <button
                        onClick={handleClickContinueShopping}
                        className='hover-basic'
                        style={{ background: '#f7f0f0', border: 'none', padding: '15px' }}
                    >
                        Continue Shopping
                    </button>
                    <button
                        onClick={handleClickUpdateCart}
                        className='hover-basic'
                        style={{ 
                            background: cartReducer.statusChangeCartProduct ? '#F46119' : '#f7f0f0', 
                            color: cartReducer.statusChangeCartProduct ? 'white' : '#333', 
                            border: 'none', 
                            padding: '15px',
                            transition: 'all ease .3s' 
                        }}
                    >
                        Update Cart
                    </button>
                </Col>
                <Col lg='6' md='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                    <h5>Discount Codes</h5>
                    <div className='d-flex align-items-center'>
                        <input value={cartReducer.voucherCode} onChange={handleChangeInputVoucher} type='text' style={{ border: '1px solid #ccc', borderRadius: '3px', padding: '10px', outline: 'none' }} placeholder='Enter your coupon code' />
                        <button
                            onClick={handleClickApplyCoupon}
                            className='hover-basic'
                            style={
                                {
                                    padding: '10px',
                                    marginLeft: '12px',
                                    background: '#6F6F6F',
                                    color: 'white',
                                    fontWeight: 500,
                                    border: 'none',
                                    minWidth: '122px',
                                    minHeight: '46px',
                                }
                            }
                        >
                            {cartReducer.fetchPendingCheckVoucher ?
                                <CircularProgress style={{ height: '20px', width: '20px', color: '#fff' }} />
                                :
                                'Apply coupon'
                            }
                        </button>
                    </div>
                    <span style={{ color: 'red', marginTop: '8px', display: cartReducer.statusVoucherNotFound ? 'inline-block' : 'none' }}>*Voucher not found</span>
                </Col>
                <Col lg='6' md='12' style={{ paddingTop: '20px', paddingBottom: '20px' }}>
                    <div style={{ background: '#f7f0f0', padding: '20px 45px' }}>
                        <p><b>Cart total</b></p>
                        <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                            <span><b>Total Quanity</b></span>
                            <span
                                style={{ color: 'red' }}
                            >
                                <b>{cartReducer.subTotalQuanity} items</b>
                            </span>
                        </div>
                        <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                            <span><b>Subtotal</b></span>
                            <span
                                style={{ color: 'red' }}
                            >
                                <b>${cartReducer.subTotalPrice.toLocaleString()}</b>
                            </span>
                        </div>
                        <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                            <span><b>Discount percent</b></span>
                            <span
                                style={{ color: 'red' }}
                            >
                                <b>{cartReducer.discountVoucher}%</b>
                            </span>
                        </div>
                        <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                            <span><b>Discount price</b></span>
                            <span
                                style={{ color: 'red' }}
                            >
                                <b>- ${Math.round(cartReducer.subTotalPrice * (cartReducer.discountVoucher / 100)).toLocaleString()}</b>
                            </span>
                        </div>
                        <hr />
                        <div className='d-flex justify-content-between' style={{ margin: '20px 0' }}>
                            <span><b>Total</b></span>
                            <span
                                style={{ color: 'red' }}
                            >
                                <b>
                                    ${cartReducer.totalPrice.toLocaleString()}
                                </b>
                            </span>
                        </div>
                        <button
                            onClick={handleClickCheckOut}
                            className= {!cartReducer.statusChangeCartProduct ? 'hover-basic' : ''}
                            style={
                                {
                                    width: '100%',
                                    display: 'block',
                                    border: 'none',
                                    padding: '8px 0',
                                    fontWeight: 500,
                                    background: !cartReducer.statusChangeCartProduct ? '#F46119' : '#ccc', 
                                    color: !cartReducer.statusChangeCartProduct ? 'white' : '#333',
                                    transition: 'all ease 0.3s'
                                }
                            }
                        >
                            Proceed to checkout
                        </button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}

export default CartProducts