import { Pagination } from '@mui/material'
import React from 'react'
import { useDispatch } from 'react-redux'
import { changePageCartProduct } from '../../../actions/cartAction'

const PaginationCartProducts = ({numberPage}) => {
    
    const dispatch = useDispatch()
    const handleChangePageCartProduct = (e, value) => {
        dispatch(changePageCartProduct(value))
    }
  return (
    <Pagination onChange={handleChangePageCartProduct} count={numberPage} />
  )
}

export default PaginationCartProducts