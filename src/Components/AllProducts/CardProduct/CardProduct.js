import classNames from "classnames/bind"
import styles from './CardProduct.module.css'
import { Link } from "react-router-dom"

let cx = classNames.bind(styles)

function CardProduct({idProduct, name, imgUrl, buyPrice, promotionPrice}) {

    return (
        <div className={cx('card-wrapper')}>
            <div >
                <Link to={`/products/${idProduct}`}>
                    <img className={cx('card-img')} src={imgUrl} alt="produc-img" />
                </Link>
            </div>
            <h3 className={cx('card-title')}>{name}</h3>
            <p className={cx('card-price')}>
                <span className={cx("old-price")}>${buyPrice}</span>
                <span className={cx("new-price")}>${promotionPrice}</span>
            </p>
        </div>
    )
}

export default CardProduct