import styled from "styled-components";

export const FilterWrapper = styled.div`
    margin: 40px 0;
    padding-bottom: 20px;
`

export const FilterTitle = styled.h5`
    margin: 0 0 20px;
`

export const FilterItem = styled.div`
    margin: 0 0 16px;
    input, label {
        cursor: pointer;
    }
`

export const FilterButton = styled.button`
    width: 100%;
    border-radius: 4px;
    background-color: #333;
    color: white;
    border: none;
    padding: 6px 0;
    text-align: center;
    &:hover {
        cursor: pointer;
        opacity: 0.8
    }
`