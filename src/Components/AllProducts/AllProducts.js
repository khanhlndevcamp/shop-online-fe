import { Row, Col } from "reactstrap";

import CardProduct from "./CardProduct/CardProduct";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { CircularProgress } from '@mui/material';
import { changeSizeWeb, changeStatusFirstTimeRender, fetchProduct } from "../../actions/productAction";
import { ProductFilter } from "./ProductFilter";
import PaginationProducts from "../PaginationProducts/PaginationProducts";
import { resetFilterCondition } from "../../actions/filterAction";

function AllProducts() {

    const fetchProductInfo = useSelector(reduxData => reduxData.productReducer)
    const filterReducer = useSelector(reduxData => reduxData.filterReducer)
    const dispatch = useDispatch()
    //get query from url, query have ?keyword=value
    // const [searchParams, setSearchParams] = useSearchParams();

    console.log('filter reducer', filterReducer)

    useEffect(() => {
        //param currentPage, itemPerPage, filterCondition
        console.log('run fetchProductInfo.itemPerPage fetchProductInfo.statusFirstTimeRender', fetchProductInfo.statusFirstTimeRender)
        if(!fetchProductInfo.statusFirstTimeRender) {
            console.log('run fetchProductInfo.itemPerPage', fetchProductInfo.itemPerPage)
            dispatch(fetchProduct(1, fetchProductInfo.itemPerPage, filterReducer))
        }

    }, [fetchProductInfo.itemPerPage])

    useEffect(() => {
        const handleResizeAllProduct = () => {
            console.log('run handleResizeAllProduct')

            if (window.innerWidth <= 575) {
                console.log('run handleResizeAllProduct window.innerWidth <= 575')
                //param itemPerPage
                dispatch(changeSizeWeb(5))
            }
            else if (window.innerWidth <= 765) {
                console.log('run handleResizeAllProduct window.innerWidth <= 765')
                dispatch(changeSizeWeb(8))
            }
            else if (window.innerWidth < 1200) {
                console.log('run handleResizeAllProduct window.innerWidth <= 1200', fetchProductInfo.itemPerPage)
                dispatch(changeSizeWeb(9))
            }
            else if (window.innerWidth >= 1200) {
                console.log('run handleResizeAllProduct window.innerWidth > 1200', fetchProductInfo.itemPerPage)
                dispatch(changeSizeWeb(8))
            }
        }
        window.scrollTo(0, 0)
        window.addEventListener('resize', handleResizeAllProduct)

        if (window.innerWidth <= 575) {
            //params: current page, limitItem, filterCondtion
            console.log('run window.innerWidth <= 575')
            //param currentPage, itemPerPage, filterCondition
            dispatch(fetchProduct(1, 5, filterReducer))
        }
        else if (window.innerWidth <= 765) {
            console.log('run window.innerWidth  <= 765')
            //param currentPage, itemPerPage, filterCondition
            dispatch(fetchProduct(1, 8, filterReducer))
        }
        else if (window.innerWidth < 1200) {
            console.log('run window.innerWidth<= 1200')
            //param currentPage, itemPerPage, filterCondition
            dispatch(fetchProduct(1, 9, filterReducer))
        }
        else if (window.innerWidth >= 1200) {
            console.log('run window.innerWidth >= 1200')
            //param currentPage, itemPerPage, filterCondition
            dispatch(fetchProduct(1, 8, filterReducer))
        }
        dispatch(changeStatusFirstTimeRender(false))
        return () => {
            dispatch(changeStatusFirstTimeRender(true))
            dispatch(resetFilterCondition())
            window.removeEventListener('resize', handleResizeAllProduct)
        }
    }, [])


    return (
        <div className="mt-5">

            <div style={{ textAlign: 'center', display: fetchProductInfo.fetchPending ? 'block' : 'none' }}>
                <CircularProgress />
            </div>
            <Row>
                <Col style={{ transition: 'all ease 0.3s' }} lg='3'>
                    <ProductFilter />
                </Col>
                <Col style={{ transition: 'all ease 0.3s' }} lg='9'>
                    <Row>
                        {fetchProductInfo.productsList.length > 0 ?
                            fetchProductInfo.productsList.map((product, index) => {
                                return (
                                    <Col style={{ transition: 'all ease 0.3s' }} className="justify-content-center d-flex" xl='3' md='4' sm='6' xs='12' key={index}>
                                        <CardProduct
                                            idProduct={product._id}
                                            name={product.name}
                                            imgUrl={product.imageUrl}
                                            buyPrice={product.buyPrice}
                                            promotionPrice={product.promotionPrice}
                                        />
                                    </Col>
                                )
                            })
                            :
                            <div style={{ textAlign: 'center', display: !fetchProductInfo.fetchPending ? 'block' : 'none' }}>
                                <h5>Have no products!</h5>
                            </div>
                        }
                    </Row>
                </Col>
            </Row>
            <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <PaginationProducts numberPage={fetchProductInfo.numberPage} page={fetchProductInfo.currentPage} />
            </div>
        </div>
    )
}

export default AllProducts