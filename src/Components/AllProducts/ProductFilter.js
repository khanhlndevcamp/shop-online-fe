import React, { useEffect, useState } from 'react'
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { FilterWrapper, FilterTitle, FilterItem, FilterButton } from './ProducFilterStyled.component'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProduct } from '../../actions/productAction'
import { changeCheckFilter, changePriceMax, changePriceMin } from '../../actions/filterAction'
import { useNavigate } from 'react-router-dom';

export const ProductFilter = () => {
    const fetchProductInfo = useSelector(reduxData => reduxData.productReducer)
    const filterCondition = useSelector(reduxData => reduxData.filterReducer)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    // const productList = fetchProductInfo.productsList.map((product) => { return { ...product } })
    const [isMobile, setIsMobile] = useState(window.innerWidth < 992)
    const [showFilterOnMobile, setShowFilterOnMobile] = useState(false)
    const [heightFilterSectionMobile, setHeightFilterSectionMobile] = useState(0)

    // if (productList.length !== 0) {
    //     productList.forEach((product, index) => {
    //         for (let i = index + 1; i < productList.length; i++) {
    //             if (productList[i].brand === product.brand) {
    //                 productList[i].brand = 'out'
    //             }
    //             if (productList[i].color === product.color) {
    //                 productList[i].color = 'out'
    //             }
    //         }
    //     })
    // }

    // const brandsList = productList
    //     .map((product) => {
    //         return product.brand
    //     })
    //     .filter((brand) => brand !== 'out')

    // const colorList = productList
    //     .map((product) => {
    //         return product.color
    //     })
    //     .filter((color) => color !== 'out')

    const handleCheckFilter = (e) => {
        dispatch(changeCheckFilter(e.target.checked, e.target.dataset.type, e.target.value))
    }

    const handleChangeFilterPriceMin = (e) => {
        dispatch(changePriceMin(e.target.value))
    }

    const handleChangeFilterPriceMax = (e) => {
        dispatch(changePriceMax(e.target.value))
    }

    const handleClickFilterIcon = (e) => {
        setShowFilterOnMobile(prev => !prev)
        let filterSectionMobile = document.getElementById('filter-section-mobile')
        setHeightFilterSectionMobile(filterSectionMobile.offsetHeight + 50)
    }

    const handleClickFilter = () => {
        // dispatch(filterProduct(filterCondition))
        dispatch(fetchProduct(1, fetchProductInfo.itemPerPage, filterCondition))

        if (filterCondition.brand.length === 0 &&
            filterCondition.color.length === 0 &&
            filterCondition.name === '' &&
            filterCondition.price.min === '' &&
            filterCondition.price.max === ''
        ) {
           navigate('/products')
        }
    }

    useEffect(() => {
        const handleResizeFilterMobile = (e) => {
            setIsMobile(window.innerWidth < 992)
        }
        window.addEventListener('resize', handleResizeFilterMobile)

        return () => {
            window.removeEventListener('resize', handleResizeFilterMobile)
        }
    }, [])

    return (
        <>
            <div>
                <div style={{ textAlign: 'right', display: isMobile ? 'block' : 'none' }}>
                    <span onClick={handleClickFilterIcon} style={{ padding: '12px' }}>
                        <FilterAltIcon />
                    </span>
                </div>
                <div
                    style={{ 
                        display: 'block', 
                        height: !isMobile ? '100%' : 
                        showFilterOnMobile ? heightFilterSectionMobile : '0', 
                        overflow: 'hidden', 
                        transition: 'height ease 0.3s' }}
                >
                    <div id='filter-section-mobile'>
                        <FilterWrapper>
                            <FilterTitle>Brands</FilterTitle>
                            {fetchProductInfo.brandsList.map((brand, index) => {
                                return (
                                    <FilterItem key={index}>
                                        <input checked={filterCondition.brand ? filterCondition.brand.includes(brand) : false} id={brand} value={brand} data-type='brand' onChange={handleCheckFilter} style={{ marginRight: '8px' }} type='checkbox' />
                                        <label htmlFor={brand}>{brand}</label>
                                    </FilterItem>
                                )
                            })}
                        </FilterWrapper>

                        <FilterWrapper>
                            <FilterTitle>Color</FilterTitle>
                            {fetchProductInfo.colorList.map((color, index) => {
                                return (
                                    <FilterItem key={index}>
                                        <input checked={filterCondition.color ? filterCondition.color.includes(color) : false} id={color} value={color} data-type='color' onChange={handleCheckFilter} style={{ marginRight: '8px' }} type='checkbox' />
                                        <label htmlFor={color}>{color}</label>
                                    </FilterItem>
                                )
                            })}
                        </FilterWrapper>


                        <FilterWrapper>
                            <FilterTitle>Prices</FilterTitle>
                            <FilterItem style={{ display: 'flex' }}>
                                <input value={filterCondition.price.min} onChange={handleChangeFilterPriceMin} type='number' data-type='min' placeholder='Min' style={{ width: '40%' }} />
                                <span style={{ margin: '0 8px' }}>-</span>
                                <input value={filterCondition.price.max} onChange={handleChangeFilterPriceMax} type='number' data-type='max' placeholder='Max' style={{ width: '40%' }} />
                            </FilterItem>
                        </FilterWrapper>

                        <FilterWrapper>
                            <FilterButton onClick={handleClickFilter}>Filter</FilterButton>
                        </FilterWrapper>
                    </div>
                </div>
            </div>
                {/* <div>
                    <FilterWrapper>
                        <FilterTitle>Brands</FilterTitle>
                        {brandsList.map((brand, index) => {
                            return (
                                <FilterItem key={index}>
                                    <input id={brand} value={brand} data-type='brand' onChange={handleCheckFilter} style={{ marginRight: '8px' }} type='checkbox' />
                                    <label htmlFor={brand}>{brand}</label>
                                </FilterItem>
                            )
                        })}
                    </FilterWrapper>

                    <FilterWrapper>
                        <FilterTitle>Color</FilterTitle>
                        {colorList.map((color, index) => {
                            return (
                                <FilterItem key={index}>
                                    <input id={color} value={color} data-type='color' onChange={handleCheckFilter} style={{ marginRight: '8px' }} type='checkbox' />
                                    <label htmlFor={color}>{color}</label>
                                </FilterItem>
                            )
                        })}
                    </FilterWrapper>


                    <FilterWrapper>
                        <FilterTitle>Prices</FilterTitle>
                        <FilterItem style={{ display: 'flex' }}>
                            <input value={filterCondition.price.min} onChange={handleChangeFilterPriceMin} type='number' data-type='min' placeholder='Min' style={{ width: '40%' }} />
                            <span style={{ margin: '0 8px' }}>-</span>
                            <input value={filterCondition.price.max} onChange={handleChangeFilterPriceMax} type='number' data-type='max' placeholder='Max' style={{ width: '40%' }} />
                        </FilterItem>
                    </FilterWrapper>

                    <FilterWrapper>
                        <FilterButton onClick={handleClickFilter}>Filter</FilterButton>
                    </FilterWrapper>
                </div> */}



        </>
    )
}
