import {
    LOG_IN_SUCCESS,
    LOG_OUT_SUCCESS,
    CHANGE_INPUT_PASSWORD,
    CHANGE_INPUT_USERNAME,
    PENDING_CALL_API_LOG_IN_WITH_ACCOUNT,
    SUCCESS_CALL_API_LOG_IN_WITH_ACCOUNT,
    FAIL_CALL_API_LOG_IN_WITH_ACCOUNT,
    LOG_OUT_ACCOUNT,
    LOAD_TOKEN_LOCAL_STORAGE,
    RESET_STATUS_LOG_IN,
    FETCH_PENDING_CALL_API_REGISTER_USER_DASHBOARD,
    FETCH_DONE_CALL_API_REGISTER_USER_DASHBOARD,
    CHANGE_STATUS_CALL_API_REGISTER_USER,
    SUCCESS_CALL_API_REGISTER_USER,
    RESET_DATA_REGISTER_USER,
    EXPIRED_DATE_ACCESS_TOKEN,
    CHANGE_STATUS_EXPIRED_DATE_ACCESS_TOKEN
} from "../constants/userConstant"

const initialValue = {
    user: '',
    username: '',
    password: '',
    statusPendingLogInWithAccount: false,
    statusLogin: true,
    statusPendingRegisterUser: false,
    resultRegisterUser: false,
    statusAccessTokenExpired: false
}

const userReducer = (state = initialValue, action) => {

    switch (action.type) {
        case EXPIRED_DATE_ACCESS_TOKEN:
            console.log('EXPIRED_DATE_ACCESS_TOKEN')
            state.user = null
            localStorage.removeItem('token')
            state.statusLogin = true
            break;
        case CHANGE_STATUS_EXPIRED_DATE_ACCESS_TOKEN:
            console.log('CHANGE_STATUS_EXPIRED_DATE_ACCESS_TOKEN')
            state.statusAccessTokenExpired = action.payload.status
            break;
        case RESET_DATA_REGISTER_USER:
            state.resultRegisterUser = false
            state.statusPendingRegisterUser = false
            break;
        case FETCH_PENDING_CALL_API_REGISTER_USER_DASHBOARD:
            state.statusPendingRegisterUser = true
            break;
        case FETCH_DONE_CALL_API_REGISTER_USER_DASHBOARD:
            state.statusPendingRegisterUser = false
            break;
        case CHANGE_STATUS_CALL_API_REGISTER_USER:
            state.resultRegisterUser = action.payload.status
            break;
        case SUCCESS_CALL_API_REGISTER_USER:
            break;
        case LOG_IN_SUCCESS:
            state.user = action.payload.user
            break;
        case LOG_OUT_SUCCESS:
            state.user = null
            break;
        case CHANGE_INPUT_USERNAME:
            state.username = action.payload.username
            break;
        case CHANGE_INPUT_PASSWORD:
            state.password = action.payload.password
            break;
        case PENDING_CALL_API_LOG_IN_WITH_ACCOUNT:
            console.log('user reducer: login pending')
            state.statusPendingLogInWithAccount = true
            break;
        case SUCCESS_CALL_API_LOG_IN_WITH_ACCOUNT:
            console.log('user reducer: login success')
            state.user = action.payload.user
            state.statusPendingLogInWithAccount = false
            localStorage.setItem('token', action.payload.token)
            state.statusLogin = true
            // state.statusAccessTokenExpired = false
            break;
        case FAIL_CALL_API_LOG_IN_WITH_ACCOUNT:
            console.log('user reducer: login fail')
            console.log('err:', action.payload.err)
            state.statusPendingLogInWithAccount = false
            state.user = null
            state.statusLogin = false
            // state.statusAccessTokenExpired = true
            break;
        case LOG_OUT_ACCOUNT:
            state.user = null
            localStorage.removeItem('token')
            state.statusLogin = true
            // state.statusAccessTokenExpired = false
            break;
        case LOAD_TOKEN_LOCAL_STORAGE:
            break;
        case RESET_STATUS_LOG_IN:
            state.statusLogin = true
            break;
        default:
            break;
    }

    return { ...state }
}

export default userReducer