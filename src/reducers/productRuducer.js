import {
    PENDING_FETCH_PRODUCTS,
    SUCCESS_FETCH_PRODUCTS,
    FAIL_FETCH_PRODUCTS,
    // CHANGE_PAGE,
    CHANGE_SIZE_WEB,
    // FILTER_PRODUCT,
    FETCH_PRODUCT_BY_ID,
    SUCCESS_FETCH_PRODUCT_LIST_RELATED,
    CHANGE_STATUS_FIRST_TIME_RENDER
} from "../constants/productConstant"

const initialValue = {
    fetchPending: false,
    productsList: [],
    brandsList: [],
    colorList: [],
    // productsFilterList: [],
    // productsListRender: [],
    productListRelated: [],
    productById: '',
    // limitItem: 8,
    itemPerPage: 8,
    numberPage: 1,
    currentPage: 1,
    limitRelateProduct: 6,
    statusFirstTimeRender: true,
}


const productReducer = (state = initialValue, action) => {

    switch (action.type) {
        case CHANGE_STATUS_FIRST_TIME_RENDER:
            state.statusFirstTimeRender = action.payload.status
            break;
        case SUCCESS_FETCH_PRODUCT_LIST_RELATED:
            state.productListRelated = action.payload.productListRelated
            break;
        case PENDING_FETCH_PRODUCTS:
            state.fetchPending = true
            break;
        case SUCCESS_FETCH_PRODUCTS:
            console.log('success fectch product')
            state.fetchPending = false;
            state.productsList = action.payload.productsList;
            state.brandsList = action.payload.brandsList;
            state.colorList = action.payload.colorList;
            // state.productsFilterList = action.payload.productsList;
            // state.productsListRender = action.payload.productsList;
            state.numberPage = action.payload.numberPage
            state.itemPerPage = action.payload.itemPerPage
            state.currentPage = action.payload.currentPage
            break;
        case FAIL_FETCH_PRODUCTS:
            console.log('get products list fail')
            // console.log('use backup data')
            // const cloneBackupData = backupData.map((product) => { return { ...product } })
            // state.fetchPending = false;
            // state.productsList = [...cloneBackupData]
            // state.productsFilterList = [...cloneBackupData]
            // state.productsListRender = cloneBackupData.slice(0, action.payload.itemPerPage);
            // state.numberPage = Math.ceil(cloneBackupData.length / action.payload.itemPerPage)
            // state.itemPerPage = action.payload.itemPerPage
            // state.currentPage = 1
            break;
        case FETCH_PRODUCT_BY_ID:
            state.fetchPending = false;
            state.productById = { ...action.payload.productById }
            break;
        // case CHANGE_PAGE:
        //     state.currentPage = action.payload.currentPage
        //     // state.productsListRender = state.productsFilterList.slice((action.payload.currentPage - 1) * state.itemPerPage, action.payload.currentPage * state.itemPerPage);
        //     break;
        case CHANGE_SIZE_WEB:
            state.itemPerPage = action.payload.itemPerPage;
            // state.productsListRender = state.productsFilterList.slice((state.currentPage - 1) * action.payload.itemPerPage, state.currentPage * action.payload.itemPerPage);
            // state.numberPage = Math.ceil(state.productsFilterList.length / action.payload.itemPerPage)
            // if (state.numberPage < state.currentPage) {
            //     state.currentPage = state.numberPage
            // }
            break;
        // case FILTER_PRODUCT:
        //     state.fetchPending = false;
        //     if (action.payload.filterCondition.brand.length === 0 &&
        //         action.payload.filterCondition.color.length === 0 &&
        //         action.payload.filterCondition.name === '' &&
        //         action.payload.filterCondition.price.min === '' &&
        //         action.payload.filterCondition.price.max === ''
        //     ) {
        //         state.productsList = action.payload.resultFilterList;
        //     }
        //     // state.productsFilterList = action.payload.resultFilterList
        //     state.currentPage = 1
        //     // state.productsListRender = action.payload.resultFilterList.slice(0, state.itemPerPage)
        //     state.numberPage = Math.ceil(action.payload.resultFilterList.length / state.itemPerPage)
        //     break;
        default:
            break;
    }

    return { ...state }
}

export default productReducer