import {
    CLICK_CHOOSE_CUSTOMER,
    CLICK_CHOOSE_DASHBOARD,
    CLICK_CHOOSE_ORDER,
    CLICK_CHOOSE_PRODUCT,
    CLICK_MENU_DASHBOARD,
    CLOSE_SIDEBAR_WHEN_RESIZE,
    OPEN_SIDEBAR_WHEN_RESIZE,
    FETCH_PENDING_DASHBOARD,
    FETCH_DONE_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_PRODUCTS,
    CHANGE_INPUT_NAME_PRODUCT_FILTER,
    CHANGE_INPUT_BRAND_PRODUCT_FILTER,
    SUCCESS_FETCH_INFO_TO_EDIT_PRODUCT_DASH_BOARD,
    CHANGE_INPUT_EDIT_PRODUCT_DASHBOARD,
    CHANGE_SELECT_TYPE_EDIT_PRODUCT_DASBOARD,
    SUCCESS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
    CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
    CHANGE_INPUT_ADD_PRODUCT_DASHBOARD,
    CHANGE_SELECT_TYPE_ADD_PRODUCT_DASBOARD,
    SUCCESS_CALL_API_ADD_PRODUCT_DASHBOARD,
    CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD,
    SUCCESS_CALL_API_GET_ALL_PRODUCT_TYPES,
    FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD,
    FETCH_DONE_CONTENT_PRODUCT_DASHBOARD,
    RESET_DATA_ADD_PRODUCT_DASHBOARD,
    RESET_DATA_EDIT_PRODUCT_DASHBOARD,
    FETCH_PENDING_CONTENT_ORDER_DASHBOARD,
    FETCH_DONE_CONTENT_ORDER_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_ORDERS,
    CHANGE_INPUT_ORDER_CODE_ORDER_FILTER,
    SUCCESS_FETCH_INFO_TO_EDIT_ORDER_DASH_BOARD,
    CHANGE_INPUT_EDIT_ORDER_DASHBOARD,
    CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
    CHANGE_SELECT_STATUS_EDIT_ORDER_DASBOARD,
    CHANGE_INPUT_QUANTITY_EDIT_ORDER_DASHBOARD,
    CHANGE_SELECT_ADD_MORE_PRODUCT_EDIT_ORDER_DASHBOARD,
    ADD_MORE_PRODUCT_TO_ORDER_DETAIL_EDIT_OREDER_DASHBOARD,
    SUCCESS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD,
    SUCCESS_CALL_API_UPDATE_ORDER_EDIT_ORDER_DASHBOARD,
    CHANGE_STATUS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD,
    REMOVE_PRODUCT_EDIT_ORDER_DASHBOARD,
    RESET_DATA_EDIT_ORDER_DASHBOARD,
    SUCCESS_FETCH_INFO_TO_ADD_ORDER_DASHBOARD,
    SUCCESS_CALL_API_GET_INFO_CUSTOMER_BY_ID_CREATE_ORDER_DASHBOARD,
    CHANGE_SELECT_CUSTOMER_ADD_ORDER_DASHBOARD,
    RESET_DATA_ADD_ORDER_DASHBOARD,
    SUCCESS_CALL_API_CREATE_ORDER_DASHBOARD,
    SUCCESS_DELETE_ORDER_DASHBOARD,
    CHANGE_STATUS_DELETE_ORDER_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_CUSTOMER_DASHBOARD,
    CHANGE_INPUT_FILTER_EMAIL_CUSTOMER_DASHBOARD,
    CHANGE_INPUT_FILTER_PHONE_NUMBER_CUSTOMER_DASHBOARD,
    CHANGE_INPUT_ADD_CUSTOMER_DASHBOARD,
    CHANGE_SELECT_ROLE_ADD_CUSTOMER_DASHBOARD,
    CHECK_DUP_USERNAME_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CHANGE_INPUT_USERNAME_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CHANGE_INPUT_PHONE_ADD_CUSTOMER_DASHBOARD,
    CHECK_DUP_PHONE_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CHANGE_INPUT_EMAIL_ADD_CUSTOMER_DASHBOARD,
    CHECK_DUP_EMAIL_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD,
    SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD,
    FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD,
    FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD,
    RESET_DATA_ADD_CUSTOMER_DASHBOARD,
    SUCCESS_CALL_API_GET_INFO_ACCOUNT_TO_EDIT_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_DELETE_CUSTOMER_DASHBOARD,
    SUCCESS_CALL_API_DELETE_CUSTOMER_DASHBOARD,
    RESET_DATA_DASHBOARD_GENERAL,
    CHANGE_SELECT_SORT_PRODUCT_DASHBOARD,
    RESET_DATA_CONTENT_PRODUCT_DASHBOARD,
    CHANGE_SELECT_SORT_CUSTOMER_DASHBOARD,
    CHANGE_SELECT_SORT_ORDER_DASHBOARD,
    CHANGE_INPUT_EMAIL_CUSTOMER_ORDER_FILTER,
    CHANGE_SELECT_STATUS_ORDER_FILTER,
    CHANGE_SELECT_RANGE_DATE_ORDER_FILTER,
    CHANGE_SELECT_YEAR_CHART_REVENEU_CONTENT_DASHBOARD,
    SUCCESS_CALL_API_GET_REVENEU_BY_MONTH_CONTENT_DASHBOARD,
    CHANGE_SELECT_YEAR_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD,
    CHANGE_SELECT_MONTH_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD,
    SUCCESS_CALL_API_GET_REVENEU_BY_DAY_CONTENT_DASHBOARD,
    CHANGE_SELECT_TYPE_PRODUCT_FILTER,
    CHANGE_SELECT_STATUS_PRODUCT_FILTER,
    VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD,
    VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD,
    CHANGE_SELECT_STATUS_CUSTOMER_FILTER
} from "../constants/dashboardConstant"

const initialValue = {
    fetchPendingDashboard: false,
    chooseDashboard: true,
    chooseProduct: false,
    chooseCustomer: false,
    chooseOrder: false,
    openSidebar: true,
    dataOfContentDashboard: {
        orderOverview: {
            totalOrders: '',
            pendingOrders: '',
            completedOrders: '',
            totalReveneu: '',
            totalSoldProducts: ''
        },
        numberOfTotalProduct: '',
        numberOfTotalCustomer: '',
        chartReveneuByMonth: {
            currentYear: new Date().getFullYear(),
            listOfReveneuByMonth: []
        },
        chartReveneuByDay: {
            currentMonth: new Date().getMonth() + 1,
            currentYear: new Date().getFullYear(),
            listOfReveneuByDay: []
        }
    },
    dataOfContentProduct: {
        fetchPendingContentProductDashboard: false,
        productList: [],
        numberPage: 1,
        currentPage: 1,
        limitItem: 5,
        filterConditon: {
            name: '',
            brand: '',
            type: '',
            status: ''
        },
        sortCondition: {
            nameSort: 1,
            priceSort: '',
            soldSort: '',
        },
        valueSelectSort: 'nameSort1',
        overal: {
            totalQuantitySold: 0,
            totalProducts: 0
        },
        infoEditProduct: {
            name: '',
            description: '',
            brand: '',
            color: '',
            type: '',
            imageUrl: '',
            buyPrice: '',
            promotionPrice: '',
            amount: 0
        },
        infoAddProduct: {
            name: '',
            description: '',
            brand: '',
            color: '',
            type: '',
            imageUrl: '',
            buyPrice: '',
            promotionPrice: '',
            amount: 0
        },
        statusCallApiUpdateProduct: false,
        statusChangeInputEditProduct: true,
        statusChangeInputAddProduct: true,
        statusCallApiAddProduct: false,
        allProductTypes: []
    },
    dataOfContentOrder: {
        fetchPendingContentOrderDashboard: false,
        orderList: [],
        numberPage: 1,
        currentPage: 1,
        limitItem: 10,
        filterConditon: {
            orderCode: '',
            email: '',
            date: '',
            statusOrder: '',
            fromDate: '01/01/2023',
            toDate: '12/31/2023',
        },
        sortCondition: {
            orderCodeSort: 1,
            priceSort: '',
            orderQuantitySort: '',
            createDateSort: ''
        },
        valueSelectSort: 'orderCodeSort1',
        statusCallApiUpdateOrder: false,
        statusChangeInputEditOrder: true,
        statusChangeInputAddOrder: true,
        statusCallApiAddOrder: false,
        statusClickCallApiCheckVoucherOrder: false,
        resultCallApiCheckVoucher: true,
        statusCallApiDeleteOrder: false,
        overal: {
            totalPrice: 0,
            totalQuantitySold: 0,
            totalOrders: 0
        },
        infoEditOrder: {
            customerName: '',
            phoneNumber: '',
            email: '',
            address: '',
            note: '',
            cost: '',
            status: '',
            orderDetail: [],
            cloneOrderDetail: [],
            voucher: '',
            voucherCode: '',
            step: '',
            currentStep: '',//use to store step when change select status
            totalQuantity: 0,
            discountPercent: 0,
            discountPrice: 0,
            totalPrice: 0,
            productList: [],
            infoProductAddMore: {
                productId: '',
                productName: 'Choose product',
                productPrice: 0
            }
        },
        infoAddOrder: {
            accountList: [],
            valueSelectCustomer: '',
            newOrderCode: ''
        },
        resultCheckBuyQuantityProduct: {
            result: true,
            product: {},
            buyQuantity: 0
        }
    },
    dataOfContentCustomer: {
        fetchPendingContentCustomerDashboard: false,
        customerList: [],
        numberPage: 1,
        currentPage: 1,
        limitItem: 10,
        filterConditon: {
            email: '',
            phoneNumber: '',
            status: ''
        },
        // confirmPassword: '',
        // resultCheckConfirmPassword: true,
        sortCondition: {
            nameSort: 1,
            reveneuSort: '',
            orderSort: '',
        },
        valueSelectSort: 'nameSort1',
        infoAddCustomer: {
            username: '',
            password: '',
            phone: '',
            email: '',
            fullName: '',
            role: '',
            imageURL: '',
            address: ''
        },
        infoEditCustomer: {
            currentAccountId: '',
            currentUsername: '', //use to check dup username in back end
            currentPhone: '',
            currentEmail: '',
        },
        resultCheckDupUsername: true, //true: user name valid, false, user name already exist
        resultCheckDupPhone: true, //true: phone valid, false, phone already exist
        resultCheckDupEmail: true, //true: email valid, false, email already exist
        statusCallApiDeleteCustomer: false,
        statusCallApiUpdateCustomer: false,
        statusChangeInputEditCustomer: true,
        statusChangeInputAddCustomer: true,
        statusCallApiAddCustomer: false,
        statusChangeInputUsernameAddCustomer: false, //input have value -> status true and reverse
        statusChangeInputPhoneAddCustomer: false, //input have value -> status true and reverse
        statusChangeInputEmailAddCustomer: false, //input have value -> status true and reverse
    }
}

const dashboardReducer = (state = initialValue, action) => {
    switch (action.type) {
        //general
        case FETCH_PENDING_DASHBOARD:
            console.log('dashboard reducer FETCH_PENDING_DASHBOARD true')
            state.fetchPendingDashboard = true
            break;
        case FETCH_DONE_DASHBOARD:
            console.log('dashboard reducer FETCH_PENDING_DASHBOARD false')
            state.fetchPendingDashboard = false
            break;
        case CLICK_CHOOSE_DASHBOARD:
            state.chooseDashboard = true
            state.chooseProduct = false
            state.chooseCustomer = false
            state.chooseOrder = false
            break;
        case CLICK_CHOOSE_CUSTOMER:
            state.chooseDashboard = false
            state.chooseProduct = false
            state.chooseCustomer = true
            state.chooseOrder = false
            break;
        case CLICK_CHOOSE_ORDER:
            state.chooseDashboard = false
            state.chooseProduct = false
            state.chooseCustomer = false
            state.chooseOrder = true
            break;
        case CLICK_CHOOSE_PRODUCT:
            state.chooseDashboard = false
            state.chooseProduct = true
            state.chooseCustomer = false
            state.chooseOrder = false
            break;
        case CLICK_MENU_DASHBOARD:
            state.openSidebar = !state.openSidebar
            break;
        case CLOSE_SIDEBAR_WHEN_RESIZE:
            state.openSidebar = false
            break;
        case OPEN_SIDEBAR_WHEN_RESIZE:
            state.openSidebar = true
            break;
        case RESET_DATA_DASHBOARD_GENERAL:
            state.chooseDashboard = true
            state.chooseProduct = false
            state.chooseCustomer = false
            state.chooseOrder = false
            break;
        //content dashboard
        case SUCCESS_FETCH_INFO_CONTENT_DASHBOARD:
            state.dataOfContentDashboard.orderOverview.totalOrders = action.payload.orderOverview.totalOrders
            state.dataOfContentDashboard.orderOverview.completedOrders = action.payload.orderOverview.completedOrders
            state.dataOfContentDashboard.orderOverview.pendingOrders = action.payload.orderOverview.pendingOrders
            state.dataOfContentDashboard.orderOverview.totalReveneu = action.payload.orderOverview.totalReveneu
            state.dataOfContentDashboard.orderOverview.totalSoldProducts = action.payload.orderOverview.totalSoldProducts
            state.dataOfContentDashboard.numberOfTotalProduct = action.payload.numberOfTotalProduct
            state.dataOfContentDashboard.numberOfTotalCustomer = action.payload.numberOfTotalCustomer
            break;
        case CHANGE_SELECT_YEAR_CHART_REVENEU_CONTENT_DASHBOARD:
            state.dataOfContentDashboard.chartReveneuByMonth.currentYear = action.payload.value
            break;
        case SUCCESS_CALL_API_GET_REVENEU_BY_MONTH_CONTENT_DASHBOARD:
            state.dataOfContentDashboard.chartReveneuByMonth.listOfReveneuByMonth = action.payload.dataReveneuByMonth
            break;
        case CHANGE_SELECT_YEAR_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD:
            state.dataOfContentDashboard.chartReveneuByDay.currentYear = action.payload.value
            break;
        case CHANGE_SELECT_MONTH_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD:
            state.dataOfContentDashboard.chartReveneuByDay.currentMonth = action.payload.value
            break;
        case SUCCESS_CALL_API_GET_REVENEU_BY_DAY_CONTENT_DASHBOARD:
            state.dataOfContentDashboard.chartReveneuByDay.listOfReveneuByDay = action.payload.dataReveneuByDay
            break;
        //content product
        case CHANGE_SELECT_TYPE_PRODUCT_FILTER:
            state.dataOfContentProduct.filterConditon.type = action.payload.value
            break;
        case CHANGE_SELECT_STATUS_PRODUCT_FILTER:
            state.dataOfContentProduct.filterConditon.status = action.payload.value
            break;
        case CHANGE_SELECT_SORT_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.sortCondition.nameSort = ''
            state.dataOfContentProduct.sortCondition.priceSort = ''
            state.dataOfContentProduct.sortCondition.soldSort = ''
            if (action.payload.value === 'nameSort1' ||
                action.payload.value === 'priceSort1' ||
                action.payload.value === 'soldSort1'
            ) {
                state.dataOfContentProduct.sortCondition[action.payload.typeOption] = '1'
            }
            else if (action.payload.value === 'nameSort-1' ||
                action.payload.value === 'priceSort-1' ||
                action.payload.value === 'soldSort-1'
            ) {
                state.dataOfContentProduct.sortCondition[action.payload.typeOption] = '-1'
            }
            state.dataOfContentProduct.valueSelectSort = action.payload.value
            break;
        case RESET_DATA_CONTENT_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.sortCondition.nameSort = 1
            state.dataOfContentProduct.sortCondition.priceSort = ''
            state.dataOfContentProduct.sortCondition.soldSort = ''
            break;
        case RESET_DATA_EDIT_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.statusChangeInputEditProduct = true
            state.dataOfContentProduct.statusCallApiUpdateProduct = false
            break;
        case RESET_DATA_ADD_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.infoAddProduct.name = ''
            state.dataOfContentProduct.infoAddProduct.description = ''
            state.dataOfContentProduct.infoAddProduct.brand = ''
            state.dataOfContentProduct.infoAddProduct.color = ''
            state.dataOfContentProduct.infoAddProduct.type = ''
            state.dataOfContentProduct.infoAddProduct.imageUrl = ''
            state.dataOfContentProduct.infoAddProduct.buyPrice = ''
            state.dataOfContentProduct.infoAddProduct.promotionPrice = ''
            state.dataOfContentProduct.infoAddProduct.amount = 0
            state.dataOfContentProduct.statusChangeInputAddProduct = true
            state.dataOfContentProduct.statusCallApiAddProduct = false
            break;
        case SUCCESS_CALL_API_GET_ALL_PRODUCT_TYPES:
            state.dataOfContentProduct.allProductTypes = action.payload.allProductTypes
            break;
        case FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD:
            console.log('dashboard reducer FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD true')
            state.dataOfContentProduct.fetchPendingContentProductDashboard = true
            break;
        case FETCH_DONE_CONTENT_PRODUCT_DASHBOARD:
            console.log('dashboard reducer FETCH_DONE_CONTENT_PRODUCT_DASHBOARD false')
            state.dataOfContentProduct.fetchPendingContentProductDashboard = false
            break;
        case CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.statusCallApiUpdateProduct = action.payload.status
            break;
        case SUCCESS_CALL_API_UPDATE_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.statusChangeInputEditProduct = false
            break;
        case CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.statusCallApiAddProduct = action.payload.status
            break;
        case SUCCESS_CALL_API_ADD_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.statusChangeInputAddProduct = false
            break;
        case SUCCESS_FETCH_INFO_CONTENT_PRODUCTS:
            state.dataOfContentProduct.productList = action.payload.productList
            state.dataOfContentProduct.numberPage = action.payload.numberPage
            state.dataOfContentProduct.currentPage = action.payload.currentPage
            state.dataOfContentProduct.overal = action.payload.overal
            state.dataOfContentProduct.allProductTypes = action.payload.allProductTypes
            break;
        case CHANGE_INPUT_BRAND_PRODUCT_FILTER:
            state.dataOfContentProduct.filterConditon.brand = action.payload.value
            break;
        case CHANGE_INPUT_NAME_PRODUCT_FILTER:
            state.dataOfContentProduct.filterConditon.name = action.payload.value
            break;
        case CHANGE_INPUT_EDIT_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.infoEditProduct[action.payload.typeInput] = action.payload.value
            state.dataOfContentProduct.statusChangeInputEditProduct = true
            break;
        case CHANGE_SELECT_TYPE_EDIT_PRODUCT_DASBOARD:
            state.dataOfContentProduct.infoEditProduct.type = action.payload.value
            state.dataOfContentProduct.statusChangeInputEditProduct = true
            break;
        case CHANGE_INPUT_ADD_PRODUCT_DASHBOARD:
            state.dataOfContentProduct.infoAddProduct[action.payload.typeInput] = action.payload.value
            state.dataOfContentProduct.statusChangeInputAddProduct = true
            break;
        case CHANGE_SELECT_TYPE_ADD_PRODUCT_DASBOARD:
            state.dataOfContentProduct.infoAddProduct.type = action.payload.value
            state.dataOfContentProduct.statusChangeInputAddProduct = true
            break;
        case SUCCESS_FETCH_INFO_TO_EDIT_PRODUCT_DASH_BOARD:
            state.dataOfContentProduct.infoEditProduct.name = action.payload.product.name
            state.dataOfContentProduct.infoEditProduct.description = action.payload.product.description
            state.dataOfContentProduct.infoEditProduct.brand = action.payload.product.brand
            state.dataOfContentProduct.infoEditProduct.color = action.payload.product.color
            state.dataOfContentProduct.infoEditProduct.type = action.payload.product.type._id
            state.dataOfContentProduct.infoEditProduct.imageUrl = action.payload.product.imageUrl
            state.dataOfContentProduct.infoEditProduct.buyPrice = action.payload.product.buyPrice
            state.dataOfContentProduct.infoEditProduct.promotionPrice = action.payload.product.promotionPrice
            state.dataOfContentProduct.infoEditProduct.buyPrice = action.payload.product.buyPrice
            state.dataOfContentProduct.infoEditProduct.amount = action.payload.product.amount
            state.dataOfContentProduct.allProductTypes = action.payload.allProductTypes
            break;

        //content customer
        case CHANGE_SELECT_STATUS_CUSTOMER_FILTER:
            state.dataOfContentCustomer.filterConditon.status = action.payload.value
            break;
        case CHANGE_SELECT_SORT_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.sortCondition.nameSort = ''
            state.dataOfContentCustomer.sortCondition.reveneuSort = ''
            state.dataOfContentCustomer.sortCondition.orderSort = ''
            if (action.payload.value === 'nameSort1' ||
                action.payload.value === 'reveneuSort1' ||
                action.payload.value === 'orderSort1'
            ) {
                state.dataOfContentCustomer.sortCondition[action.payload.typeOption] = '1'
            }
            else if (action.payload.value === 'nameSort-1' ||
                action.payload.value === 'reveneuSort-1' ||
                action.payload.value === 'orderSort-1') {
                state.dataOfContentCustomer.sortCondition[action.payload.typeOption] = '-1'
            }
            state.dataOfContentCustomer.valueSelectSort = action.payload.value
            break;
        case SUCCESS_CALL_API_DELETE_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusCallApiDeleteCustomer = true
            break;
        case CHANGE_STATUS_DELETE_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusCallApiDeleteCustomer = action.payload.status
            break;
        case SUCCESS_CALL_API_GET_INFO_ACCOUNT_TO_EDIT_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.infoAddCustomer.username = action.payload.infoAccount.username
            state.dataOfContentCustomer.infoAddCustomer.phone = action.payload.infoAccount.customer.phone
            state.dataOfContentCustomer.infoAddCustomer.email = action.payload.infoAccount.customer.email
            state.dataOfContentCustomer.infoAddCustomer.fullName = action.payload.infoAccount.customer.fullName
            state.dataOfContentCustomer.infoAddCustomer.role = action.payload.infoAccount.role
            state.dataOfContentCustomer.infoAddCustomer.imageURL = action.payload.infoAccount.imageURL
            state.dataOfContentCustomer.infoAddCustomer.address = action.payload.infoAccount.customer.address
            state.dataOfContentCustomer.infoEditCustomer.currentUsername = action.payload.infoAccount.username
            state.dataOfContentCustomer.infoEditCustomer.currentEmail = action.payload.infoAccount.customer.email
            state.dataOfContentCustomer.infoEditCustomer.currentPhone = action.payload.infoAccount.customer.phone
            state.dataOfContentCustomer.infoEditCustomer.currentAccountId = action.payload.infoAccount._id
            break;
        case RESET_DATA_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusChangeInputAddCustomer = true
            state.dataOfContentCustomer.statusCallApiAddCustomer = false
            state.dataOfContentCustomer.statusChangeInputEmailAddCustomer = false
            state.dataOfContentCustomer.statusChangeInputPhoneAddCustomer = false
            state.dataOfContentCustomer.statusChangeInputUsernameAddCustomer = false
            state.dataOfContentCustomer.infoAddCustomer.username = ''
            state.dataOfContentCustomer.infoAddCustomer.password = ''
            state.dataOfContentCustomer.infoAddCustomer.phone = ''
            state.dataOfContentCustomer.infoAddCustomer.email = ''
            state.dataOfContentCustomer.infoAddCustomer.fullName = ''
            state.dataOfContentCustomer.infoAddCustomer.role = ''
            state.dataOfContentCustomer.infoAddCustomer.imageURL = ''
            state.dataOfContentCustomer.infoAddCustomer.address = ''
            state.dataOfContentCustomer.resultCheckDupUsername = true
            state.dataOfContentCustomer.resultCheckDupPhone = true
            state.dataOfContentCustomer.resultCheckDupEmail = true
            break;
        case FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.fetchPendingContentCustomerDashboard = true
            break;
        case FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.fetchPendingContentCustomerDashboard = false
            break;
        case SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusChangeInputAddCustomer = false
            state.dataOfContentCustomer.statusChangeInputEmailAddCustomer = false
            state.dataOfContentCustomer.statusChangeInputPhoneAddCustomer = false
            state.dataOfContentCustomer.statusChangeInputUsernameAddCustomer = false
            break;
        case CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusCallApiAddCustomer = action.payload.status
            break;
        case CHANGE_STATUS_CHANGE_INPUT_EMAIL_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusChangeInputEmailAddCustomer = action.payload.status
            break;
        case CHECK_DUP_EMAIL_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.resultCheckDupEmail = action.payload.resultCheckDupEmail
            break;
        case CHANGE_STATUS_CHANGE_INPUT_PHONE_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusChangeInputPhoneAddCustomer = action.payload.status
            break;
        case CHECK_DUP_PHONE_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.resultCheckDupPhone = action.payload.resultCheckDupPhone
            break;
        case CHANGE_STATUS_CHANGE_INPUT_USERNAME_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusChangeInputUsernameAddCustomer = action.payload.status
            break;
        case CHECK_DUP_USERNAME_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.resultCheckDupUsername = action.payload.resultCheckDupUsername
            break;
        case CHANGE_SELECT_ROLE_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.infoAddCustomer.role = action.payload.value
            state.dataOfContentCustomer.statusChangeInputAddCustomer = true
            break;
        case CHANGE_INPUT_ADD_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.statusChangeInputAddCustomer = true
            state.dataOfContentCustomer.infoAddCustomer[action.payload.typeInput] = action.payload.value
            break;
        case SUCCESS_FETCH_INFO_CONTENT_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.numberPage = action.payload.numberPage
            state.dataOfContentCustomer.customerList = action.payload.customerList
            state.dataOfContentCustomer.currentPage = action.payload.currentPage
            break;
        case CHANGE_INPUT_FILTER_EMAIL_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.filterConditon.email = action.payload.value
            break;
        case CHANGE_INPUT_FILTER_PHONE_NUMBER_CUSTOMER_DASHBOARD:
            state.dataOfContentCustomer.filterConditon.phoneNumber = action.payload.value
            break;

        //content orders
        case VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD:
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.result = true
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.product = {}
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.buyQuantity = 0
            break;
        case VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD:
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.result = false
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.product = action.payload.product
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.buyQuantity = action.payload.buyQuantity
            break;
        case CHANGE_SELECT_RANGE_DATE_ORDER_FILTER:
            state.dataOfContentOrder.filterConditon.fromDate = action.payload.fromDate
            state.dataOfContentOrder.filterConditon.toDate = action.payload.toDate
            break;
        case CHANGE_SELECT_SORT_ORDER_DASHBOARD:
            state.dataOfContentOrder.sortCondition.orderCodeSort = ''
            state.dataOfContentOrder.sortCondition.priceSort = ''
            state.dataOfContentOrder.sortCondition.orderQuantitySort = ''
            if (action.payload.value === 'orderCodeSort1' ||
                action.payload.value === 'priceSort1' ||
                action.payload.value === 'orderQuantitySort1' ||
                action.payload.value === 'createDateSort1'
            ) {
                state.dataOfContentOrder.sortCondition[action.payload.typeOption] = '1'
            }
            else if (action.payload.value === 'orderCodeSort-1' ||
                action.payload.value === 'priceSort-1' ||
                action.payload.value === 'orderQuantitySort-1' ||
                action.payload.value === 'createDateSort-1'
            ) {
                state.dataOfContentOrder.sortCondition[action.payload.typeOption] = '-1'
            }
            state.dataOfContentOrder.valueSelectSort = action.payload.value
            break;
        case SUCCESS_FETCH_INFO_CONTENT_ORDERS:
            state.dataOfContentOrder.orderList = action.payload.orderList
            state.dataOfContentOrder.numberPage = action.payload.numberPage
            state.dataOfContentOrder.currentPage = action.payload.currentPage
            state.dataOfContentOrder.overal = action.payload.overal
            break;
        case CHANGE_INPUT_ORDER_CODE_ORDER_FILTER:
            state.dataOfContentOrder.filterConditon.orderCode = action.payload.value
            break;
        case CHANGE_INPUT_EMAIL_CUSTOMER_ORDER_FILTER:
            state.dataOfContentOrder.filterConditon.email = action.payload.value
            break;
        case CHANGE_SELECT_STATUS_ORDER_FILTER:
            state.dataOfContentOrder.filterConditon.statusOrder = action.payload.value
            break;
        case SUCCESS_FETCH_INFO_TO_EDIT_ORDER_DASH_BOARD:
            state.dataOfContentOrder.infoEditOrder.customerName = action.payload.order.customerName
            state.dataOfContentOrder.infoEditOrder.phoneNumber = action.payload.order.phoneNumber
            state.dataOfContentOrder.infoEditOrder.email = action.payload.order.email
            state.dataOfContentOrder.infoEditOrder.address = action.payload.order.address
            state.dataOfContentOrder.infoEditOrder.note = action.payload.order.note
            state.dataOfContentOrder.infoEditOrder.cost = action.payload.order.cost
            state.dataOfContentOrder.infoEditOrder.status = action.payload.order.status
            state.dataOfContentOrder.infoEditOrder.step = action.payload.order.step
            state.dataOfContentOrder.infoEditOrder.currentStep = action.payload.order.currentStep
            state.dataOfContentOrder.infoEditOrder.voucher = action.payload.order.voucher ? action.payload.order.voucher : ''
            state.dataOfContentOrder.infoEditOrder.voucherCode = action.payload.order.voucher ? action.payload.order.voucher.code : ''
            state.dataOfContentOrder.infoEditOrder.orderDetail = action.payload.orderDetail
            state.dataOfContentOrder.infoEditOrder.cloneOrderDetail = state.dataOfContentOrder.infoEditOrder.orderDetail.map((orderDetail) => {
                return orderDetail
            })
            if (state.dataOfContentOrder.infoEditOrder.voucher === '') {
                state.dataOfContentOrder.infoEditOrder.discountPercent = 0
            } else {
                state.dataOfContentOrder.infoEditOrder.discountPercent = state.dataOfContentOrder.infoEditOrder.voucher.discount
            }
            state.dataOfContentOrder.infoEditOrder.totalQuantity = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += +orderDetail.quantity
            }, 0)
            let totalPrice = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += (+orderDetail.quantity * +orderDetail.product.promotionPrice)
            }, 0)
            state.dataOfContentOrder.infoEditOrder.discountPrice = Math.round(totalPrice * (state.dataOfContentOrder.infoEditOrder.discountPercent / 100))
            state.dataOfContentOrder.infoEditOrder.totalPrice = Math.round(totalPrice - state.dataOfContentOrder.infoEditOrder.discountPrice)
            state.dataOfContentOrder.infoEditOrder.productList = action.payload.productList
            break;
        case SUCCESS_FETCH_INFO_TO_ADD_ORDER_DASHBOARD:
            state.dataOfContentOrder.infoEditOrder.productList = action.payload.productList
            state.dataOfContentOrder.infoAddOrder.accountList = action.payload.accountList
            break;
        case CHANGE_INPUT_EDIT_ORDER_DASHBOARD:
            state.dataOfContentOrder.infoEditOrder[action.payload.typeInput] = action.payload.value
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            break;
        case CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD:
            state.dataOfContentOrder.statusCallApiUpdateOrder = action.payload.status

            break;
        case CHANGE_SELECT_STATUS_EDIT_ORDER_DASBOARD:
            state.dataOfContentOrder.infoEditOrder.status = action.payload.value
            state.dataOfContentOrder.infoEditOrder.currentStep = action.payload.step
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            break;
        case CHANGE_INPUT_QUANTITY_EDIT_ORDER_DASHBOARD: {
            if (action.payload.value <= 0) {
                state.dataOfContentOrder.infoEditOrder.orderDetail[action.payload.index].quantity = 0
            } else {
                state.dataOfContentOrder.infoEditOrder.orderDetail[action.payload.index].quantity = action.payload.value
            }
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            state.dataOfContentOrder.infoEditOrder.totalQuantity = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += +orderDetail.quantity
            }, 0)
            let totalPrice = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += (+orderDetail.quantity * +orderDetail.product.promotionPrice)
            }, 0)
            state.dataOfContentOrder.infoEditOrder.cloneOrderDetail = state.dataOfContentOrder.infoEditOrder.cloneOrderDetail.map((orderDetailItem) => {
                if (orderDetailItem.product._id === state.dataOfContentOrder.infoEditOrder.orderDetail[action.payload.index].product._id) {
                    orderDetailItem.quantity = state.dataOfContentOrder.infoEditOrder.orderDetail[action.payload.index].quantity
                }
                return orderDetailItem
            })
            state.dataOfContentOrder.infoEditOrder.discountPrice = Math.round(totalPrice * (state.dataOfContentOrder.infoEditOrder.discountPercent / 100))
            state.dataOfContentOrder.infoEditOrder.totalPrice = Math.round(totalPrice - state.dataOfContentOrder.infoEditOrder.discountPrice)
        }
            break;
        case CHANGE_SELECT_ADD_MORE_PRODUCT_EDIT_ORDER_DASHBOARD:
            state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId = action.payload.productId
            state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productName = action.payload.productName
            state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productPrice = action.payload.productPrice
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            break;
        case ADD_MORE_PRODUCT_TO_ORDER_DETAIL_EDIT_OREDER_DASHBOARD: {

            const filterExistProductInOrderDetail = state.dataOfContentOrder.infoEditOrder.orderDetail.filter(orderDetail => {
                return orderDetail.product._id === state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId
            })
            if (filterExistProductInOrderDetail.length === 0) {
                let newOrderDetail = {
                    _id: '',
                    product: {
                        _id: state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId,
                        name: state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productName,
                        promotionPrice: state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productPrice,
                    },
                    quantity: 1
                }
                state.dataOfContentOrder.infoEditOrder.orderDetail.push(newOrderDetail)
            } else {
                state.dataOfContentOrder.infoEditOrder.orderDetail.forEach((orderDetail) => {
                    if (orderDetail.product._id === state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId) {
                        orderDetail.quantity = +orderDetail.quantity + 1
                    }
                })
            }

            const filterExistProductInCloneOrderDetail = state.dataOfContentOrder.infoEditOrder.cloneOrderDetail.filter(orderDetail => {
                return orderDetail.product._id === state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId
            })
            if (filterExistProductInCloneOrderDetail.length === 0) {
                let newOrderDetail = {
                    _id: '',
                    product: {
                        _id: state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId,
                        name: state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productName,
                        promotionPrice: state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productPrice,
                    },
                    quantity: 1
                }
                state.dataOfContentOrder.infoEditOrder.cloneOrderDetail.push(newOrderDetail)
            } else {
                state.dataOfContentOrder.infoEditOrder.cloneOrderDetail.forEach((orderDetail) => {
                    if (orderDetail.product._id === state.dataOfContentOrder.infoEditOrder.infoProductAddMore.productId) {
                        orderDetail.quantity = +orderDetail.quantity + 1
                    }
                })
            }

            state.dataOfContentOrder.infoEditOrder.totalQuantity = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += +orderDetail.quantity
            }, 0)
            let totalPrice = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += (+orderDetail.quantity * +orderDetail.product.promotionPrice)
            }, 0)
            state.dataOfContentOrder.infoEditOrder.discountPrice = Math.round(totalPrice * (state.dataOfContentOrder.infoEditOrder.discountPercent / 100))
            state.dataOfContentOrder.infoEditOrder.totalPrice = Math.round(totalPrice - state.dataOfContentOrder.infoEditOrder.discountPrice)
            state.dataOfContentOrder.statusChangeInputEditOrder = true
        }
            break;
        case REMOVE_PRODUCT_EDIT_ORDER_DASHBOARD: {
            state.dataOfContentOrder.infoEditOrder.cloneOrderDetail = state.dataOfContentOrder.infoEditOrder.cloneOrderDetail.map((orderDetailItem, index) => {
                if (orderDetailItem.product._id === action.payload.productId) {
                    orderDetailItem.quantity = 0
                }
                return orderDetailItem
            })
            state.dataOfContentOrder.infoEditOrder.orderDetail = state.dataOfContentOrder.infoEditOrder.orderDetail.filter((orderDetailItem) => {
                return orderDetailItem.product._id !== action.payload.productId
            })

            state.dataOfContentOrder.infoEditOrder.totalQuantity = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += +orderDetail.quantity
            }, 0)
            let totalPrice = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += (+orderDetail.quantity * +orderDetail.product.promotionPrice)
            }, 0)
            state.dataOfContentOrder.infoEditOrder.discountPrice = Math.round(totalPrice * (state.dataOfContentOrder.infoEditOrder.discountPercent / 100))
            state.dataOfContentOrder.infoEditOrder.totalPrice = Math.round(totalPrice - state.dataOfContentOrder.infoEditOrder.discountPrice)
            state.dataOfContentOrder.statusChangeInputEditOrder = true
        }
            break;
        case SUCCESS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD: {
            if (action.payload.voucher !== null) {
                state.dataOfContentOrder.resultCallApiCheckVoucher = true
                state.dataOfContentOrder.infoEditOrder.voucher = action.payload.voucher
                state.dataOfContentOrder.infoEditOrder.discountPercent = state.dataOfContentOrder.infoEditOrder.voucher.discount
            } else {
                state.dataOfContentOrder.resultCallApiCheckVoucher = false
                state.dataOfContentOrder.infoEditOrder.voucher = ''
                state.dataOfContentOrder.infoEditOrder.discountPercent = 0
            }

            state.dataOfContentOrder.infoEditOrder.totalQuantity = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += +orderDetail.quantity
            }, 0)
            let totalPrice = state.dataOfContentOrder.infoEditOrder.orderDetail.reduce((acc, orderDetail) => {
                return acc += (+orderDetail.quantity * +orderDetail.product.promotionPrice)
            }, 0)
            state.dataOfContentOrder.infoEditOrder.discountPrice = Math.round(totalPrice * (state.dataOfContentOrder.infoEditOrder.discountPercent / 100))
            state.dataOfContentOrder.infoEditOrder.totalPrice = Math.round(totalPrice - state.dataOfContentOrder.infoEditOrder.discountPrice)
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            state.dataOfContentOrder.statusClickCallApiCheckVoucherOrder = true
        }
            break;
        case SUCCESS_CALL_API_UPDATE_ORDER_EDIT_ORDER_DASHBOARD:
            state.dataOfContentOrder.statusCallApiUpdateOrder = true
            state.dataOfContentOrder.statusChangeInputEditOrder = false
            state.dataOfContentOrder.infoEditOrder.step = action.payload.step
            console.log('update order success')
            break;
        case SUCCESS_CALL_API_CREATE_ORDER_DASHBOARD:
            state.dataOfContentOrder.statusCallApiUpdateOrder = true
            state.dataOfContentOrder.statusChangeInputEditOrder = false
            state.dataOfContentOrder.infoAddOrder.newOrderCode = action.payload.newOrderCode
            console.log('create order success')
            break;
        case FETCH_PENDING_CONTENT_ORDER_DASHBOARD:
            state.dataOfContentOrder.fetchPendingContentOrderDashboard = true
            break;
        case FETCH_DONE_CONTENT_ORDER_DASHBOARD:
            state.dataOfContentOrder.fetchPendingContentOrderDashboard = false
            break;
        case CHANGE_STATUS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD:
            state.dataOfContentOrder.statusClickCallApiCheckVoucherOrder = action.payload.status
            break;
        case RESET_DATA_EDIT_ORDER_DASHBOARD:
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.result = true
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.product = {}
            state.dataOfContentOrder.resultCheckBuyQuantityProduct.buyQuantity = 0
            state.dataOfContentOrder.infoEditOrder = {
                customerName: '',
                phoneNumber: '',
                email: '',
                address: '',
                note: '',
                cost: '',
                status: '',
                orderDetail: [],
                cloneOrderDetail: [],
                voucher: '',
                voucherCode: '',
                totalQuantity: 0,
                discountPercent: 0,
                discountPrice: 0,
                totalPrice: 0,
                productList: [],
                infoProductAddMore: {
                    productId: '',
                    productName: 'Choose product',
                    productPrice: 0
                }
            }
            state.dataOfContentOrder.statusCallApiUpdateOrder = false
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            state.dataOfContentOrder.statusClickCallApiCheckVoucherOrder = false
            break;
        case SUCCESS_CALL_API_GET_INFO_CUSTOMER_BY_ID_CREATE_ORDER_DASHBOARD:
            state.dataOfContentOrder.infoEditOrder.customerName = action.payload.dataInfoCustomerById.fullName
            state.dataOfContentOrder.infoEditOrder.phoneNumber = action.payload.dataInfoCustomerById.phone
            state.dataOfContentOrder.infoEditOrder.email = action.payload.dataInfoCustomerById.email
            state.dataOfContentOrder.infoEditOrder.address = action.payload.dataInfoCustomerById.address
            //same logic with edit oder
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            break;
        case CHANGE_SELECT_CUSTOMER_ADD_ORDER_DASHBOARD:
            state.dataOfContentOrder.infoAddOrder.valueSelectCustomer = action.payload.value
            state.dataOfContentOrder.statusChangeInputEditOrder = true
            break;
        case RESET_DATA_ADD_ORDER_DASHBOARD:
            state.dataOfContentOrder.infoAddOrder.valueSelectCustomer = ''
            break;
        case SUCCESS_DELETE_ORDER_DASHBOARD:
            console.log('delete order dashboard success')
            state.dataOfContentOrder.statusCallApiDeleteOrder = true
            break;
        case CHANGE_STATUS_DELETE_ORDER_DASHBOARD:
            state.dataOfContentOrder.statusCallApiDeleteOrder = action.payload.status
            break;
        default:
            ;
    }

    return { ...state }
}

export default dashboardReducer