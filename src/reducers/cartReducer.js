import {
    ADD_TO_CART,
    CHANGE_PAGE_CART_PRODUCT,
    EDIT_MINUS_QUANITY,
    EDIT_PLUS_QUANITY,
    LOAD_DATA_LOCAL_STORAGE,
    REMOVE_PRODUCT_CART,
    RESET_STATUS_CHANGE_CART_PRODUCT,
    UPDATE_CART,
    GET_INFO_ORDER,
    CHANGE_VALUE_INPUT_INFO_ORDER,
    CHANGE_VALUE_INPUT_VOUCHER,
    FETCH_PENDING_VOUCHER_CODE,
    FETCH_CHECK_VOUCHER_CODE,
    FETCH_VOUCHER_CODE_FAIL,
    CALL_API_CREATE_ORDER_SUCCESS,
    CALL_API_CREATE_ORDER_FAIL,
    CALL_API_PENDING_CREATE_ORDER,
    RESET_STATUS_CREAT_ORDER,
    RESET_DATA_AFTER_CHECK_OUT,
    VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_CART_PRODUCT,
    VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_CART_PRODUCT
} from "../constants/cartConstant";

const initialValue = {
    totalQuanity: 0,
    subTotalQuanity: 0,
    totalQuantityProduct: 0,
    cartProducts: [],
    currentPage: 1,
    statusChangeCartProduct: false,
    statusVoucherNotFound: false,
    statusCreateOrder: false,
    fetchPendingCheckVoucher: false,
    callPendingCreateVoucher: false,
    orderCode: '',
    discountVoucher: 0,
    voucherCode: '',
    idVoucher: '',
    totalPrice: 0,
    subTotalPrice: 0,
    infoOrder: {
        fullName: '',
        address: '',
        phone: '',
        email: '',
        note: ''
    },
    resultCheckBuyQuantityProduct: {
        result: true,
        product: {},
        buyQuantity: 0
    }
}

const cartReducer = (state = initialValue, action) => {
    switch (action.type) {
        case VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_CART_PRODUCT:
            state.resultCheckBuyQuantityProduct.result = true
            state.resultCheckBuyQuantityProduct.product = {}
            state.resultCheckBuyQuantityProduct.buyQuantity = 0
            break;
        case VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_CART_PRODUCT:
            state.resultCheckBuyQuantityProduct.result = false
            state.resultCheckBuyQuantityProduct.product = action.payload.product
            state.resultCheckBuyQuantityProduct.buyQuantity = action.payload.buyQuantity
            break;
        case FETCH_PENDING_VOUCHER_CODE:
            state.fetchPendingCheckVoucher = true
            state.statusVoucherNotFound = false
            break;
        case LOAD_DATA_LOCAL_STORAGE: {
            console.log('load data local storage')
            const cartProducts = localStorage.getItem('cartProducts')
            const totalQuanity = localStorage.getItem('totalQuanity')
            if (JSON.parse(cartProducts)) {
                state.cartProducts = JSON.parse(cartProducts)
                let totalPrice = JSON.parse(cartProducts).reduce((acc, product) => {
                    return acc += +product.promotionPrice * +product.quanityProduct
                }, 0)
                state.totalPrice = Math.round(+totalPrice - (+totalPrice * (+state.discountVoucher / 100)))
                state.subTotalPrice = Math.round(totalPrice)
                state.totalQuantityProduct = state.cartProducts.length
            }
            if (totalQuanity) {
                state.totalQuanity = totalQuanity
                state.subTotalQuanity = totalQuanity
            }
        }
            break;
        case FETCH_CHECK_VOUCHER_CODE: {
            state.fetchPendingCheckVoucher = false
            state.statusVoucherNotFound = false
            state.discountVoucher = action.payload.discount
            state.idVoucher = action.payload.idVoucher
            let totalPrice = state.cartProducts.reduce((acc, product) => {
                return acc += +product.promotionPrice * +product.quanityProduct
            }, 0)
            state.totalPrice = Math.round(+totalPrice - (+totalPrice * (+state.discountVoucher / 100)))
        }
            break;
        case FETCH_VOUCHER_CODE_FAIL: {
            state.fetchPendingCheckVoucher = false
            state.statusVoucherNotFound = true
            state.discountVoucher = 0
            state.idVoucher = ''
            let totalPrice = state.cartProducts.reduce((acc, product) => {
                return acc += +product.promotionPrice * +product.quanityProduct
            }, 0)
            state.totalPrice = Math.round(totalPrice)
        }
            break;
        case ADD_TO_CART:
            const cartProducts = localStorage.getItem('cartProducts')
            const arrCartProducts = JSON.parse(cartProducts)
            if (arrCartProducts) {
                const checkProduct = arrCartProducts.filter((product) => {
                    return product._id === action.payload.productById._id
                })

                if (checkProduct.length > 0) {
                    arrCartProducts.forEach((product) => {
                        if (product._id === action.payload.productById._id) {
                            product.quanityProduct = +product.quanityProduct + action.payload.quanityProduct
                        }
                    })
                } else {
                    const newProduct = {
                        _id: action.payload.productById._id,
                        quanityProduct: action.payload.quanityProduct,
                        name: action.payload.productById.name,
                        promotionPrice: action.payload.productById.promotionPrice,
                        imageUrl: action.payload.productById.imageUrl
                    }
                    arrCartProducts.push(newProduct)
                }
                localStorage.setItem('cartProducts', JSON.stringify(arrCartProducts))
                state.cartProducts = arrCartProducts
            } else {
                const newProduct = {
                    _id: action.payload.productById._id,
                    quanityProduct: action.payload.quanityProduct,
                    name: action.payload.productById.name,
                    promotionPrice: action.payload.productById.promotionPrice,
                    imageUrl: action.payload.productById.imageUrl
                }
                localStorage.setItem('cartProducts', JSON.stringify([newProduct]))
                state.cartProducts = [newProduct]
            }
            const calTotalQuanity = state.cartProducts.reduce((acc, product) => {
                return acc += +product.quanityProduct
            }, 0)


            localStorage.totalQuanity = calTotalQuanity
            state.totalQuanity = calTotalQuanity
            let totalPrice = state.cartProducts.reduce((acc, product) => {
                return acc += +product.promotionPrice * +product.quanityProduct
            }, 0)
            state.subTotalQuanity = calTotalQuanity
            state.totalQuantityProduct = state.cartProducts.length
            state.totalPrice = Math.round(+totalPrice - (+totalPrice * (+state.discountVoucher / 100)))
            state.subTotalPrice = Math.round(totalPrice)
            break;
        case EDIT_MINUS_QUANITY: {
            state.cartProducts.forEach((product) => {
                if (product._id === action.payload.idProduct) {
                    const newQuanity = product.quanityProduct - 1
                    if (newQuanity <= 0) {
                        product.quanityProduct = 0
                    } else {
                        product.quanityProduct = newQuanity
                    }
                }
            })
            state.statusChangeCartProduct = true
            let totalPrice = state.cartProducts.reduce((acc, product) => {
                return acc += +product.promotionPrice * +product.quanityProduct
            }, 0)
            state.totalPrice = Math.round(+totalPrice - (+totalPrice * (+state.discountVoucher / 100)))
            state.subTotalPrice = Math.round(totalPrice)
            state.subTotalQuanity = state.cartProducts.reduce((acc, product) => {
                return acc += +product.quanityProduct
            }, 0)

        }
            break;
        case EDIT_PLUS_QUANITY: {
            state.cartProducts.forEach((product) => {
                if (product._id === action.payload.idProduct) {
                    product.quanityProduct += 1
                }
            })
            state.statusChangeCartProduct = true
            let totalPrice = state.cartProducts.reduce((acc, product) => {
                return acc += +product.promotionPrice * +product.quanityProduct
            }, 0)
            state.totalPrice = Math.round(+totalPrice - (+totalPrice * (+state.discountVoucher / 100)))
            state.subTotalPrice = Math.round(totalPrice)
            state.subTotalQuanity = state.cartProducts.reduce((acc, product) => {
                return acc += +product.quanityProduct
            }, 0)
        }
            break;
        case UPDATE_CART: {
            const calTotalQuanity = state.cartProducts.reduce((acc, product) => {
                return acc += +product.quanityProduct
            }, 0)
            const newCartProducts = state.cartProducts.filter((product) => {
                return product.quanityProduct !== 0
            })
            localStorage.cartProducts = JSON.stringify(newCartProducts)
            localStorage.totalQuanity = calTotalQuanity
            state.totalQuanity = calTotalQuanity
            state.subTotalQuanity = calTotalQuanity
            state.cartProducts = [...newCartProducts]
            state.statusChangeCartProduct = false
            state.totalQuantityProduct = state.cartProducts.length
        }
            break;
        case CHANGE_PAGE_CART_PRODUCT:
            state.currentPage = action.payload.currentPage
            break;
        case REMOVE_PRODUCT_CART: {
            const newCartProducts = state.cartProducts.filter((product) => {
                return product._id !== action.payload.idProduct
            })
            state.cartProducts = [...newCartProducts]
            state.statusChangeCartProduct = true
            let totalPrice = state.cartProducts.reduce((acc, product) => {
                return acc += +product.promotionPrice * +product.quanityProduct
            }, 0)
            state.totalPrice = Math.round(+totalPrice - (+totalPrice * (+state.discountVoucher / 100)))
            state.subTotalPrice = Math.round(totalPrice)
            state.subTotalQuanity = state.cartProducts.reduce((acc, product) => {
                return acc += +product.quanityProduct
            }, 0)
        }
            break;
        case RESET_STATUS_CHANGE_CART_PRODUCT:
            state.statusChangeCartProduct = false
            break;
        case GET_INFO_ORDER:
            if (action.payload.user !== null) {
                state.infoOrder.fullName = action.payload.user.displayName
                state.infoOrder.email = action.payload.user.email
                state.infoOrder.phone = action.payload.user.phone ?? ''
                state.infoOrder.address = action.payload.user.address 
                state.infoOrder.note = ''
            } else {
                state.infoOrder.fullName = ''
                state.infoOrder.email = ''
                state.infoOrder.phone = ''
                state.infoOrder.address = ''
                state.infoOrder.note = ''
            }
            break;
        case CHANGE_VALUE_INPUT_INFO_ORDER:
            state.infoOrder[action.payload.key] = action.payload.value
            break;
        case CHANGE_VALUE_INPUT_VOUCHER:
            state.voucherCode = action.payload.voucherCode
            break;
        case CALL_API_PENDING_CREATE_ORDER:
            state.callPendingCreateVoucher = true
            break
        case CALL_API_CREATE_ORDER_SUCCESS:
            state.callPendingCreateVoucher = false
            state.statusCreateOrder = true
            state.orderCode = action.payload.orderCode
            break;
        case CALL_API_CREATE_ORDER_FAIL:
            state.statusCreateOrder = false
            state.callPendingCreateVoucher = false
            console.log('create order fail: err', action.payload.err)
            break;
        case RESET_STATUS_CREAT_ORDER:
            state.statusCreateOrder = false
            break;
        case RESET_DATA_AFTER_CHECK_OUT:
            localStorage.removeItem('cartProducts')
            localStorage.removeItem('totalQuanity')
            state.totalQuanity = 0
            state.subTotalQuanity = 0
            state.totalQuantityProduct = 0
            state.cartProducts = []
            state.discountVoucher = 0
            state.voucherCode = ''
            state.idVoucher = ''
            state.totalPrice = 0
            state.subTotalPrice = 0
            state.infoOrder.fullName = ''
            state.infoOrder.address = ''
            state.infoOrder.phone = ''
            state.infoOrder.email = ''
            state.infoOrder.note = ''
            break;
        default:
            break;
    }

    return { ...state }
}

export default cartReducer