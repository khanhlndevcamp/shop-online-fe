import { 
    CHANGE_CHECK_FILTER, 
    CHANGE_PRICE_MAX, 
    CHANGE_PRICE_MIN, 
    RESET_FILTER_CONDITION,
    CHANGE_INPUT_SEARCH_HEADER
} from "../constants/filtterConstant"

const initialValue = {
    name: '',
    brand: [],
    color: [],
    price: {
        min: '',
        max: '',
    }    
}

const filterReducer = (state = initialValue, action) => {

    switch(action.type) {
        case CHANGE_CHECK_FILTER:
            if (action.payload.statusCheck) {
                state[action.payload.typeCheck].push(action.payload.valueCheck)
            }
            else {
                const newFilterConditione = state[action.payload.typeCheck].filter((condition) => condition !== action.payload.valueCheck)
                state[action.payload.typeCheck] = [...newFilterConditione]
            }
            break;
        case CHANGE_PRICE_MAX:
            state.price.max = action.payload.priceMax
            break;
        case CHANGE_PRICE_MIN:
            state.price.min = action.payload.priceMin
            break;
        case RESET_FILTER_CONDITION:
            state.brand = [];
            state.color = [];
            state.price.min = '';
            state.price.max = '';
            state.name = '';
            break;
        case CHANGE_INPUT_SEARCH_HEADER:
            state.name = action.payload.valueInput
            break;
        default:
            break;
    }

    return {...state}
}

export default filterReducer