import {
    FETCH_GET_ORDER_CUSTOMER_FAIL,
    FETCH_GET_ORDER_CUSTOMER_SUCCESS,
    FETCH_PENDING_GET_ORDER_CUSTOMER,
    FETCH_DELETE_ORDER_CUSTOMER_SUCCESS,
    FETCH_DELETE_ORDER_CUSTOMER_FAIL,
    CHANGE_PAGE_ORDERS_CUSTOMER,
    RESET_DATA_ORDER_CUSTOMER,
    CHANGE_VALUE_INPUT_ORDER_CODE,
    RESET_STATUS_DELETE_ORDER,
    FETCH_PENDING_GET_CURRENT_ORDER_INFO_DETAIL,
    FETCH_GET_CURRENT_ORDER_INFO_DETAIL_SUCCESS,
    FETCH_GET_CURRENT_ORDER_INFO_DETAIL_FAIL
} from "../constants/orderConstant";

const initalValue = {
    statusPendingFetchOrder: false,
    statusDeleteOrder: false,
    statusPedingGetInfoCurrentOrder: false,
    ordersOfCustomer: [],
    customerInfo: {},
    listRenderOrders: [],
    infoCurrentOrder: {
        totalPriceOfOrder: 0,
        discountPercent: 0,
        discountPrice: 0,
        totalQuantity: 0,
        order: '',
        orderDetails: '',
    },
    valueFilterOrder: '',
    currentPage: 1,
    numberPage: 1,
    itemPerPage: 5,
}

const orderReducer = (state = initalValue, action) => {
    switch (action.type) {
        case FETCH_PENDING_GET_ORDER_CUSTOMER:
            state.statusPendingFetchOrder = true
            // state.statusDeleteOrder = false
            break;
        case FETCH_GET_ORDER_CUSTOMER_SUCCESS:
            console.log('get order customer success')
            state.statusPendingFetchOrder = false
            state.ordersOfCustomer = action.payload.ordersOfCustomer
            state.numberPage = action.payload.numberPage
            state.currentPage = action.payload.currentPage
            // state.listRenderOrders = state.ordersOfCustomer.slice((state.currentPage - 1) * state.itemPerPage, state.currentPage * state.itemPerPage)
            state.customerInfo = action.payload.customerInfo
            break;
        case FETCH_GET_ORDER_CUSTOMER_FAIL:
            state.statusPendingFetchOrder = false
            state.ordersOfCustomer = []
            state.listRenderOrders = []
            console.log('get order customer fail')
            break;
        case FETCH_DELETE_ORDER_CUSTOMER_SUCCESS:
            console.log('order reducer FETCH_DELETE_ORDER_CUSTOMER_SUCCESS')
            state.statusPendingFetchOrder = false
            state.statusDeleteOrder = true
            // state.ordersOfCustomer = action.payload.ordersOfCustomer
            // state.numberPage = Math.ceil(state.ordersOfCustomer.length / state.itemPerPage)
            // if (state.ordersOfCustomer.length === state.numberPage * state.itemPerPage && state.currentPage >= state.numberPage) {
            //     state.currentPage = state.numberPage
            // }
            // state.listRenderOrders = state.ordersOfCustomer.slice((state.currentPage - 1) * state.itemPerPage, state.currentPage * state.itemPerPage)
            // state.customerInfo = action.payload.customerInfo
            break;
        case FETCH_DELETE_ORDER_CUSTOMER_FAIL:
            console.log('order reducer FETCH_DELETE_ORDER_CUSTOMER_FAIL')

            state.statusPendingFetchOrder = false
            state.statusDeleteOrder = false
            break;
        case RESET_STATUS_DELETE_ORDER:
            state.statusDeleteOrder = false
            break;
        case FETCH_PENDING_GET_CURRENT_ORDER_INFO_DETAIL:
            state.statusPedingGetInfoCurrentOrder = true
            break;
        case FETCH_GET_CURRENT_ORDER_INFO_DETAIL_SUCCESS:
            console.log('get current order info detail success')
            state.statusPedingGetInfoCurrentOrder = false
            state.infoCurrentOrder.order = action.payload.order
            state.infoCurrentOrder.orderDetails = action.payload.orderDetails
            if(action.payload.order.voucher) {
                state.infoCurrentOrder.discountPercent = action.payload.order.voucher.discount
            } else {
                state.infoCurrentOrder.discountPercent = 0
            }
            let totalPriceBeforeDiscount = state.infoCurrentOrder.orderDetails.reduce((acc, orderDetail) => {
                return acc += (orderDetail.quantity*orderDetail.product.promotionPrice)
            }, 0)
            state.infoCurrentOrder.discountPrice = totalPriceBeforeDiscount*(+state.infoCurrentOrder.discountPercent/100)
            state.infoCurrentOrder.totalPriceOfOrder = totalPriceBeforeDiscount - state.infoCurrentOrder.discountPrice
            state.infoCurrentOrder.totalQuantity = state.infoCurrentOrder.orderDetails.reduce((acc, orderDetail) => {
                return acc += orderDetail.quantity
            }, 0)
            break;
        case FETCH_GET_CURRENT_ORDER_INFO_DETAIL_FAIL:
            state.statusPedingGetInfoCurrentOrder = false
            console.log('get current order info detail fail')
            break;
        case CHANGE_PAGE_ORDERS_CUSTOMER:
            state.currentPage = action.payload.currentPage
            state.listRenderOrders = state.ordersOfCustomer.slice((state.currentPage - 1) * state.itemPerPage, state.currentPage * state.itemPerPage)
            break;
        case RESET_DATA_ORDER_CUSTOMER:
            state.currentPage = 1
            break;
        case CHANGE_VALUE_INPUT_ORDER_CODE:
            state.valueFilterOrder = action.payload.valueFilterOrder
            // if (state.valueFilterOrder === '') {
            //     state.numberPage = Math.ceil(state.ordersOfCustomer.length / state.itemPerPage)
            //     state.listRenderOrders = state.ordersOfCustomer.slice((state.currentPage - 1) * state.itemPerPage, state.currentPage * state.itemPerPage)
            // } else {
            //     state.listRenderOrders = state.ordersOfCustomer.filter((order) => {
            //         return order.orderCode.includes(state.valueFilterOrder)
            //     })
            //     state.numberPage = Math.ceil(state.listRenderOrders.length / state.itemPerPage) === 0 ? 1 : Math.ceil(state.listRenderOrders.length / state.itemPerPage)
            //     state.currentPage = 1
            // }
            break;
        default:
            break;
    }

    return { ...state }
}

export default orderReducer