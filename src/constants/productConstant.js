export const PENDING_FETCH_PRODUCTS = 'PENDING_FETCH_PRODUCTS'
export const SUCCESS_FETCH_PRODUCTS = 'SUCCESS_FETCH_PRODUCTS'
export const FAIL_FETCH_PRODUCTS = 'FAIL_FETCH_PRODUCTS'
export const CHANGE_PAGE = 'CHANGE_PAGE'
export const CHANGE_SIZE_WEB = 'CHANGE_SIZE_WEB'
export const FILTER_PRODUCT = 'FILTER_PRODUCT'
export const FETCH_PRODUCT_BY_ID = 'FETCH_PRODUCT_BY_ID'
export const SUCCESS_FETCH_PRODUCT_LIST_RELATED = 'SUCCESS_FETCH_PRODUCT_LIST_RELATED'
export const CHANGE_STATUS_FIRST_TIME_RENDER = 'CHANGE_STATUS_FIRST_TIME_RENDER'