//general
export const FETCH_PENDING_DASHBOARD = 'FETCH_PENDING_DASHBOARD'
export const FETCH_DONE_DASHBOARD = 'FETCH_PENDING_DASHBOARD'
export const CLICK_CHOOSE_DASHBOARD = 'CLICK_CHOOSE_DASHBOARD'
export const CLICK_CHOOSE_CUSTOMER = 'CLICK_CHOOSE_CUSTOMER'
export const CLICK_CHOOSE_ORDER = 'CLICK_CHOOSE_ORDER'
export const CLICK_CHOOSE_PRODUCT = 'CLICK_CHOOSE_PRODUCT'
export const CLICK_MENU_DASHBOARD = 'CLICK_MENU_DASHBOARD'
export const CLOSE_SIDEBAR_WHEN_RESIZE = 'CLOSE_SIDEBAR_WHEN_RESIZE'
export const OPEN_SIDEBAR_WHEN_RESIZE = 'OPEN_SIDEBAR_WHEN_RESIZE'
export const RESET_DATA_DASHBOARD_GENERAL = 'RESET_DATA_DASHBOARD_GENERAL'

//content dashboard
export const SUCCESS_FETCH_INFO_CONTENT_DASHBOARD = 'SUCCESS_FETCH_INFO_CONTENT_DASHBOARD'
export const CHANGE_SELECT_YEAR_CHART_REVENEU_CONTENT_DASHBOARD = 'CHANGE_SELECT_YEAR_CHART_REVENEU_CONTENT_DASHBOARD'
export const SUCCESS_CALL_API_GET_REVENEU_BY_MONTH_CONTENT_DASHBOARD = 'SUCCESS_CALL_API_GET_REVENEU_BY_MONTH_CONTENT_DASHBOARD'
export const CHANGE_SELECT_MONTH_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD = 'CHANGE_SELECT_MONTH_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD'
export const CHANGE_SELECT_YEAR_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD = 'CHANGE_SELECT_YEAR_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD'
export const SUCCESS_CALL_API_GET_REVENEU_BY_DAY_CONTENT_DASHBOARD = 'SUCCESS_CALL_API_GET_REVENEU_BY_Day_CONTENT_DASHBOARD'

//content product
export const FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD = 'FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD'
export const FETCH_DONE_CONTENT_PRODUCT_DASHBOARD = 'FETCH_DONE_CONTENT_PRODUCT_DASHBOARD'
export const SUCCESS_FETCH_INFO_CONTENT_PRODUCTS = 'SUCCESS_FETCH_INFO_CONTENT_PRODUCTS'
export const CHANGE_INPUT_NAME_PRODUCT_FILTER = 'CHANGE_INPUT_NAME_PRODUCT_FILTER'
export const CHANGE_INPUT_BRAND_PRODUCT_FILTER = 'CHANGE_INPUT_BRAND_PRODUCT_FILTER'
export const CHANGE_SELECT_TYPE_PRODUCT_FILTER = 'CHANGE_SELECT_TYPE_PRODUCT_FILTER'
export const CHANGE_SELECT_STATUS_PRODUCT_FILTER = 'CHANGE_SELECT_STATUS_PRODUCT_FILTER'
export const SUCCESS_FETCH_INFO_TO_EDIT_PRODUCT_DASH_BOARD = 'SUCCESS_FETCH_INFO_TO_EDIT_PRODUCT_DASH_BOARD'
export const CHANGE_INPUT_EDIT_PRODUCT_DASHBOARD = 'CHANGE_INPUT_EDIT_PRODUCT_DASHBOARD'
export const CHANGE_SELECT_TYPE_EDIT_PRODUCT_DASBOARD = 'CHANGE_SELECT_TYPE_EDIT_PRODUCT_DASBOARD'
export const SUCCESS_CALL_API_UPDATE_PRODUCT_DASHBOARD = 'SUCCESS_CALL_API_UPDATE_PRODUCT_DASHBOARD'
export const CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD = 'CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD'
export const CHANGE_INPUT_ADD_PRODUCT_DASHBOARD = 'CHANGE_INPUT_ADD_PRODUCT_DASHBOARD'
export const CHANGE_SELECT_TYPE_ADD_PRODUCT_DASBOARD = 'CHANGE_SELECT_TYPE_ADD_PRODUCT_DASBOARD'
export const SUCCESS_CALL_API_ADD_PRODUCT_DASHBOARD = 'SUCCESS_CALL_API_ADD_PRODUCT_DASHBOARD'
export const CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD = 'CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD'
export const SUCCESS_CALL_API_GET_ALL_PRODUCT_TYPES = 'SUCCESS_CALL_API_GET_ALL_PRODUCT_TYPES'
export const RESET_DATA_ADD_PRODUCT_DASHBOARD = 'RESET_DATA_ADD_PRODUCT_DASHBOARD'
export const RESET_DATA_EDIT_PRODUCT_DASHBOARD = 'RESET_DATA_EDIT_PRODUCT_DASHBOARD'
export const CHANGE_SELECT_SORT_PRODUCT_DASHBOARD = 'CHANGE_SELECT_SORT_PRODUCT_DASHBOARD'
export const RESET_DATA_CONTENT_PRODUCT_DASHBOARD = 'RESET_DATA_CONTENT_PRODUCT_DASHBOARD'

//content customer
export const FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD = 'FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD'
export const FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD = 'FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD'
export const SUCCESS_FETCH_INFO_CONTENT_CUSTOMER_DASHBOARD = 'SUCCESS_FETCH_INFO_CONTENT_CUSTOMER_DASHBOARD'
export const CHANGE_INPUT_FILTER_EMAIL_CUSTOMER_DASHBOARD = 'CHANGE_INPUT_FILTER_EMAIL_CUSTOMER_DASHBOARD'
export const CHANGE_INPUT_FILTER_PHONE_NUMBER_CUSTOMER_DASHBOARD = 'CHANGE_INPUT_FILTER_PHONE_NUMBER_CUSTOMER_DASHBOARD'
export const CHANGE_SELECT_STATUS_CUSTOMER_FILTER = 'CHANGE_SELECT_STATUS_CUSTOMER_FILTER'
export const CHANGE_INPUT_ADD_CUSTOMER_DASHBOARD = 'CHANGE_INPUT_ADD_CUSTOMER_DASHBOARD'
export const CHANGE_SELECT_ROLE_ADD_CUSTOMER_DASHBOARD = 'CHANGE_SELECT_ROLE_ADD_CUSTOMER_DASHBOARD'
export const CHECK_DUP_USERNAME_ADD_CUSTOMER_DASHBOARD = 'CHECK_DUP_USERNAME_ADD_CUSTOMER_DASHBOARD'
export const CHANGE_STATUS_CHANGE_INPUT_USERNAME_ADD_CUSTOMER_DASHBOARD = 'CHANGE_STATUS_CHANGE_INPUT_USERNAME_ADD_CUSTOMER_DASHBOARD'
export const CHECK_DUP_PHONE_ADD_CUSTOMER_DASHBOARD = 'CHECK_DUP_PHONE_ADD_CUSTOMER_DASHBOARD'
export const CHANGE_STATUS_CHANGE_INPUT_PHONE_ADD_CUSTOMER_DASHBOARD = 'CHANGE_STATUS_CHANGE_INPUT_PHONE_ADD_CUSTOMER_DASHBOARD'
export const CHECK_DUP_EMAIL_ADD_CUSTOMER_DASHBOARD = 'CHECK_DUP_EMAIL_ADD_CUSTOMER_DASHBOARD'
export const CHANGE_STATUS_CHANGE_INPUT_EMAIL_ADD_CUSTOMER_DASHBOARD = 'CHANGE_STATUS_CHANGE_INPUT_EMAIL_ADD_CUSTOMER_DASHBOARD'
export const SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD = 'SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD'
export const SUCCESS_CALL_API_UPDATE_CUSTOMER_DASHBOARD = 'SUCCESS_CALL_API_UPDATE_CUSTOMER_DASHBOARD'
export const CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD = 'CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD'
export const RESET_DATA_ADD_CUSTOMER_DASHBOARD = 'RESET_DATA_ADD_CUSTOMER_DASHBOARD'
export const SUCCESS_CALL_API_GET_INFO_ACCOUNT_TO_EDIT_CUSTOMER_DASHBOARD = 'SUCCESS_CALL_API_GET_INFO_ACCOUNT_TO_EDIT_CUSTOMER_DASHBOARD'
export const CHANGE_STATUS_DELETE_CUSTOMER_DASHBOARD = 'CHANGE_STATUS_DELETE_CUSTOMER_DASHBOARD'
export const SUCCESS_CALL_API_DELETE_CUSTOMER_DASHBOARD = 'SUCCESS_CALL_API_DELETE_CUSTOMER_DASHBOARD'
export const CHANGE_SELECT_SORT_CUSTOMER_DASHBOARD = 'CHANGE_SELECT_SORT_CUSTOMER_DASHBOARD'

//content order
export const VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD = 'VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD'
export const VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD = 'VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD'
export const FETCH_PENDING_CONTENT_ORDER_DASHBOARD = 'FETCH_PENDING_CONTENT_ORDER_DASHBOARD'
export const FETCH_DONE_CONTENT_ORDER_DASHBOARD = 'FETCH_DONE_CONTENT_ORDER_DASHBOARD' 
export const SUCCESS_FETCH_INFO_CONTENT_ORDERS = 'SUCCESS_FETCH_INFO_CONTENT_ORDERS'
export const CHANGE_INPUT_ORDER_CODE_ORDER_FILTER = 'CHANGE_INPUT_ORDER_CODE_ORDER_FILTER'
export const CHANGE_INPUT_EMAIL_CUSTOMER_ORDER_FILTER = 'CHANGE_INPUT_EMAIL_CUSTOMER_ORDER_FILTER'
export const CHANGE_SELECT_STATUS_ORDER_FILTER = 'CHANGE_SELECT_STATUS_ORDER_FILTER'
export const CHANGE_SELECT_RANGE_DATE_ORDER_FILTER = 'CHANGE_SELECT_RANGE_DATE_ORDER_FILTER'
export const SUCCESS_FETCH_INFO_TO_EDIT_ORDER_DASH_BOARD = 'SUCCESS_FETCH_INFO_TO_EDIT_ORDER_DASH_BOARD'
export const CHANGE_INPUT_EDIT_ORDER_DASHBOARD = 'CHANGE_INPUT_EDIT_ORDER_DASHBOARD'
export const CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD = 'CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD'
export const CHANGE_SELECT_STATUS_EDIT_ORDER_DASBOARD = 'CHANGE_SELECT_STATUS_EDIT_ORDER_DASBOARD'
export const CHANGE_INPUT_QUANTITY_EDIT_ORDER_DASHBOARD = 'CHANGE_INPUT_QUANTITY_EDIT_ORDER_DASHBOARD'
export const CHANGE_SELECT_ADD_MORE_PRODUCT_EDIT_ORDER_DASHBOARD = 'CHANGE_SELECT_ADD_MORE_PRODUCT_EDIT_ORDER_DASHBOARD'
export const REMOVE_PRODUCT_EDIT_ORDER_DASHBOARD = 'REMOVE_PRODUCT_EDIT_ORDER_DASHBOARD'
export const ADD_MORE_PRODUCT_TO_ORDER_DETAIL_EDIT_OREDER_DASHBOARD = 'ADD_MORE_PRODUCT_TO_ORDER_DETAIL_EDIT_OREDER_DASHBOARD'
export const SUCCESS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD = 'SUCCESS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD'
export const SUCCESS_CALL_API_UPDATE_ORDER_EDIT_ORDER_DASHBOARD = 'SUCCESS_CALL_API_UPDATE_ORDER_EDIT_ORDER_DASHBOARD'
export const CHANGE_STATUS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD = 'CHANGE_STATUS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD'
export const RESET_DATA_EDIT_ORDER_DASHBOARD = 'RESET_DATA_EDIT_ORDER_DASHBOARD'
export const RESET_DATA_ADD_ORDER_DASHBOARD = 'RESET_DATA_ADD_ORDER_DASHBOARD'
export const SUCCESS_FETCH_INFO_TO_ADD_ORDER_DASHBOARD = 'SUCCESS_FETCH_INFO_TO_ADD_ORDER_DASHBOARD'
export const SUCCESS_CALL_API_CREATE_ORDER_DASHBOARD = 'SUCCESS_CALL_API_CREATE_ORDER_DASHBOARD'
export const CHANGE_STATUS_CALL_API_CREATE_ORDER_DASHBOARD = 'CHANGE_STATUS_CALL_API_CREATE_ORDER_DASHBOARD'
export const SUCCESS_CALL_API_GET_INFO_CUSTOMER_BY_ID_CREATE_ORDER_DASHBOARD = 'SUCCESS_CALL_API_GET_INFO_CUSTOMER_BY_ID_CREATE_ORDER_DASHBOARD'
export const CHANGE_SELECT_CUSTOMER_ADD_ORDER_DASHBOARD = 'CHANGE_SELECT_CUSTOMER_ADD_ORDER_DASHBOARD'
export const SUCCESS_DELETE_ORDER_DASHBOARD = 'SUCCESS_DELETE_ORDER_DASHBOARD'
export const CHANGE_STATUS_DELETE_ORDER_DASHBOARD = 'CHANGE_STATUS_DELETE_ORDER_DASHBOARD'
export const CHANGE_SELECT_SORT_ORDER_DASHBOARD = 'CHANGE_SELECT_SORT_ORDER_DASHBOARD'