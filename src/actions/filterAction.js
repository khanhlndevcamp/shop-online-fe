import { 
    CHANGE_PRICE_MAX, 
    CHANGE_CHECK_FILTER, 
    CHANGE_PRICE_MIN, 
    RESET_FILTER_CONDITION, 
    CHANGE_INPUT_SEARCH_HEADER
} from "../constants/filtterConstant";


export const changeCheckFilter = (statusCheck, typeCheck, valueCheck) => {
    return {
        type: CHANGE_CHECK_FILTER,
        payload: {
            statusCheck,
            typeCheck,
            valueCheck
        }
    }
}

export const changePriceMin = (priceMin) => {
    return {
        type: CHANGE_PRICE_MIN,
        payload: {
            priceMin
        }
    }
}

export const changePriceMax = (priceMax) => {
    return {
        type: CHANGE_PRICE_MAX,
        payload: {
            priceMax
        }
    }
}

export const resetFilterCondition = () => {
    return {
        type: RESET_FILTER_CONDITION
    }
}

export const changeInputSearchHeader = (valueInput) => {
    return {
        type: CHANGE_INPUT_SEARCH_HEADER,
        payload: {
            valueInput
        }
    }
}