import {
    CHANGE_INPUT_PASSWORD,
    CHANGE_INPUT_USERNAME,
    LOG_IN_SUCCESS,
    LOG_OUT_SUCCESS,
    SUCCESS_CALL_API_LOG_IN_WITH_ACCOUNT,
    PENDING_CALL_API_LOG_IN_WITH_ACCOUNT,
    FAIL_CALL_API_LOG_IN_WITH_ACCOUNT,
    LOG_OUT_ACCOUNT,
    LOAD_TOKEN_LOCAL_STORAGE,
    RESET_STATUS_LOG_IN,
    SUCCESS_CALL_API_REGISTER_USER,
    FETCH_PENDING_CALL_API_REGISTER_USER_DASHBOARD,
    FETCH_DONE_CALL_API_REGISTER_USER_DASHBOARD,
    CHANGE_STATUS_CALL_API_REGISTER_USER,
    RESET_DATA_REGISTER_USER,
    CHANGE_STATUS_EXPIRED_DATE_ACCESS_TOKEN,
    EXPIRED_DATE_ACCESS_TOKEN
} from "../constants/userConstant";

export const logInSuccess = (user) => {
    return {
        type: LOG_IN_SUCCESS,
        payload: {
            user
        }
    }
}

export const logOutSuccess = () => {
    return {
        type: LOG_OUT_SUCCESS
    }
}

export const changeInputUsername = (username) => {
    return {
        type: CHANGE_INPUT_USERNAME,
        payload: {
            username
        }
    }
}

export const changeInputPassword = (password) => {
    return {
        type: CHANGE_INPUT_PASSWORD,
        payload: {
            password
        }
    }
}

export const callApiLogInWithAccount = (username, password) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: PENDING_CALL_API_LOG_IN_WITH_ACCOUNT })


            //call api login
            var headerLogin = new Headers();
            headerLogin.append("Content-Type", "application/json");

            var raw = JSON.stringify({
                "username": username,
                "password": password
            });

            var requestOptionsLogin = {
                method: 'POST',
                headers: headerLogin,
                body: raw,
                redirect: 'follow'
            };

            const resLogin = await fetch(`${process.env.REACT_APP_HOST_API}/login`, requestOptionsLogin)
            const dataLogin = await resLogin.json()
            const token = dataLogin.token

            //get info account
            var headersGetInfoAccount = new Headers();
            headersGetInfoAccount.append("Authentication", `Bearer ${token}`);
            headersGetInfoAccount.append("Content-Type", "application/json");


            var requestOptionsGetInfoAccount = {
                method: 'GET',
                headers: headersGetInfoAccount,
                redirect: 'follow'
            };

            const resInfoAccount = await fetch(`${process.env.REACT_APP_HOST_API}/account`, requestOptionsGetInfoAccount)
            const dataInfoAccount = await resInfoAccount.json()


            const user = {
                role: dataInfoAccount.account.role,
                photoURL: dataInfoAccount.account.imageURL,
                displayName: dataInfoAccount.account.customer.fullName,
                email: dataInfoAccount.account.customer.email,
                phone: dataInfoAccount.account.customer.phone,
                address: dataInfoAccount.account.customer.address,
                customer: dataInfoAccount.account.customer,
            }

            dispatch({
                type: SUCCESS_CALL_API_LOG_IN_WITH_ACCOUNT,
                payload: {
                    user,
                    token
                }
            })
        }
        catch (err) {
            dispatch({
                type: FAIL_CALL_API_LOG_IN_WITH_ACCOUNT,
                payload: {
                    err
                }
            })
        }
    }
}

export const logOutAccount = () => {
    return {
        type: LOG_OUT_ACCOUNT
    }
}

export const resetDataRegisterUser = () => {
    return {
        type: RESET_DATA_REGISTER_USER
    }
}

export const callApiRegisterUser = (infoCustomer) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CALL_API_REGISTER_USER_DASHBOARD })


            let headers = new Headers();
            headers.append("Content-Type", "application/json");

            var raw = JSON.stringify(infoCustomer);

            let requestOptionsCreateCustomer = {
                method: 'POST',
                headers: headers,
                body: raw
            };

            let resNewCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/account`, requestOptionsCreateCustomer)
            // let dataNewCustomer = await resNewCustomer.json()
            if (resNewCustomer.status === 201) {
                console.log('callApiRegisterUser status 201:')
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_REGISTER_USER,
                    payload: {
                        status: true
                    }
                })
            } else {
                console.log('callApiRegisterUser status different 201:')
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_REGISTER_USER,
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CALL_API_REGISTER_USER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_REGISTER_USER
            })
        }
        catch (err) {
            console.log('callApiAddCusomerDashboard error:', err)
            await dispatch({
                type: CHANGE_STATUS_CALL_API_REGISTER_USER,
                payload: {
                    status: false
                }
            })
            await dispatch({ type: FETCH_DONE_CALL_API_REGISTER_USER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_REGISTER_USER
            })
        }
    }
}

export const changeStatusExpiredDateAccessToken = (status) => {
    return {
        type: CHANGE_STATUS_EXPIRED_DATE_ACCESS_TOKEN,
        payload: {
            status
        }
    }
}

export const loadTokenLocalStorage = () => {
    return async (dispatch) => {
        const tokenLocalStorage = localStorage.getItem('token')

        if (tokenLocalStorage) {
            try {
                await dispatch({ type: PENDING_CALL_API_LOG_IN_WITH_ACCOUNT })
                const token = tokenLocalStorage
                //get info account
                var headersGetInfoAccount = new Headers();
                headersGetInfoAccount.append("Authentication", `Bearer ${token}`);
                headersGetInfoAccount.append("Content-Type", "application/json");


                var requestOptionsGetInfoAccount = {
                    method: 'GET',
                    headers: headersGetInfoAccount,
                    redirect: 'follow'
                };

                //check expired token
                const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetInfoAccount)
                const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

                if (dataResultCheckExpiredToken.expired) {
                    await dispatch(changeStatusExpiredDateAccessToken(true))
                    return dispatch({
                        type: EXPIRED_DATE_ACCESS_TOKEN
                    })
                }

                //end check expired token

                //if access token not expired, call api get info account
                const resInfoAccount = await fetch(`${process.env.REACT_APP_HOST_API}/account`, requestOptionsGetInfoAccount)
                const dataInfoAccount = await resInfoAccount.json()


                const user = {
                    role: dataInfoAccount.account.role,
                    photoURL: dataInfoAccount.account.imageURL,
                    displayName: dataInfoAccount.account.customer.fullName,
                    email: dataInfoAccount.account.customer.email,
                    phone: dataInfoAccount.account.customer.phone,
                    customer: dataInfoAccount.account.customer,
                    address: dataInfoAccount.account.customer.address,
                }

                dispatch({
                    type: SUCCESS_CALL_API_LOG_IN_WITH_ACCOUNT,
                    payload: {
                        user,
                        token
                    }
                })
            }
            catch (err) {
                dispatch({
                    type: FAIL_CALL_API_LOG_IN_WITH_ACCOUNT,
                    payload: {
                        err
                    }
                })
            }
        } else {
            return dispatch({
                type: LOAD_TOKEN_LOCAL_STORAGE,
                user: null
            })
        }
    }
}

export const resetStatusLogin = () => {
    return {
        type: RESET_STATUS_LOG_IN
    }
}