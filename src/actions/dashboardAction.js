import {
    CLICK_CHOOSE_CUSTOMER,
    CLICK_CHOOSE_DASHBOARD,
    CLICK_CHOOSE_ORDER,
    CLICK_CHOOSE_PRODUCT,
    CLICK_MENU_DASHBOARD,
    CLOSE_SIDEBAR_WHEN_RESIZE,
    OPEN_SIDEBAR_WHEN_RESIZE,
    FETCH_PENDING_DASHBOARD,
    FETCH_DONE_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_PRODUCTS,
    CHANGE_INPUT_NAME_PRODUCT_FILTER,
    CHANGE_INPUT_BRAND_PRODUCT_FILTER,
    SUCCESS_FETCH_INFO_TO_EDIT_PRODUCT_DASH_BOARD,
    CHANGE_INPUT_EDIT_PRODUCT_DASHBOARD,
    CHANGE_SELECT_TYPE_EDIT_PRODUCT_DASBOARD,
    SUCCESS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
    CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
    CHANGE_INPUT_ADD_PRODUCT_DASHBOARD,
    CHANGE_SELECT_TYPE_ADD_PRODUCT_DASBOARD,
    SUCCESS_CALL_API_ADD_PRODUCT_DASHBOARD,
    CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD,
    SUCCESS_CALL_API_GET_ALL_PRODUCT_TYPES,
    FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD,
    FETCH_DONE_CONTENT_PRODUCT_DASHBOARD,
    RESET_DATA_ADD_PRODUCT_DASHBOARD,
    RESET_DATA_EDIT_PRODUCT_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_ORDERS,
    CHANGE_INPUT_ORDER_CODE_ORDER_FILTER,
    SUCCESS_FETCH_INFO_TO_EDIT_ORDER_DASH_BOARD,
    CHANGE_INPUT_EDIT_ORDER_DASHBOARD,
    CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
    CHANGE_SELECT_STATUS_EDIT_ORDER_DASBOARD,
    CHANGE_INPUT_QUANTITY_EDIT_ORDER_DASHBOARD,
    CHANGE_SELECT_ADD_MORE_PRODUCT_EDIT_ORDER_DASHBOARD,
    ADD_MORE_PRODUCT_TO_ORDER_DETAIL_EDIT_OREDER_DASHBOARD,
    SUCCESS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD,
    SUCCESS_CALL_API_UPDATE_ORDER_EDIT_ORDER_DASHBOARD,
    FETCH_DONE_CONTENT_ORDER_DASHBOARD,
    FETCH_PENDING_CONTENT_ORDER_DASHBOARD,
    CHANGE_STATUS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD,
    REMOVE_PRODUCT_EDIT_ORDER_DASHBOARD,
    RESET_DATA_EDIT_ORDER_DASHBOARD,
    SUCCESS_FETCH_INFO_TO_ADD_ORDER_DASHBOARD,
    SUCCESS_CALL_API_GET_INFO_CUSTOMER_BY_ID_CREATE_ORDER_DASHBOARD,
    CHANGE_SELECT_CUSTOMER_ADD_ORDER_DASHBOARD,
    RESET_DATA_ADD_ORDER_DASHBOARD,
    SUCCESS_CALL_API_CREATE_ORDER_DASHBOARD,
    SUCCESS_DELETE_ORDER_DASHBOARD,
    CHANGE_STATUS_DELETE_ORDER_DASHBOARD,
    SUCCESS_FETCH_INFO_CONTENT_CUSTOMER_DASHBOARD,
    CHANGE_INPUT_FILTER_EMAIL_CUSTOMER_DASHBOARD,
    CHANGE_INPUT_FILTER_PHONE_NUMBER_CUSTOMER_DASHBOARD,
    CHANGE_INPUT_ADD_CUSTOMER_DASHBOARD,
    CHANGE_SELECT_ROLE_ADD_CUSTOMER_DASHBOARD,
    CHECK_DUP_USERNAME_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CHANGE_INPUT_USERNAME_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CHANGE_INPUT_PHONE_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CHANGE_INPUT_EMAIL_ADD_CUSTOMER_DASHBOARD,
    CHECK_DUP_PHONE_ADD_CUSTOMER_DASHBOARD,
    CHECK_DUP_EMAIL_ADD_CUSTOMER_DASHBOARD,
    SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD,
    FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD,
    FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD,
    RESET_DATA_ADD_CUSTOMER_DASHBOARD,
    SUCCESS_CALL_API_GET_INFO_ACCOUNT_TO_EDIT_CUSTOMER_DASHBOARD,
    CHANGE_STATUS_DELETE_CUSTOMER_DASHBOARD,
    SUCCESS_CALL_API_DELETE_CUSTOMER_DASHBOARD,
    RESET_DATA_DASHBOARD_GENERAL,
    CHANGE_SELECT_SORT_PRODUCT_DASHBOARD,
    RESET_DATA_CONTENT_PRODUCT_DASHBOARD,
    CHANGE_SELECT_SORT_CUSTOMER_DASHBOARD,
    CHANGE_SELECT_SORT_ORDER_DASHBOARD,
    CHANGE_INPUT_EMAIL_CUSTOMER_ORDER_FILTER,
    CHANGE_SELECT_STATUS_ORDER_FILTER,
    CHANGE_SELECT_RANGE_DATE_ORDER_FILTER,
    CHANGE_SELECT_YEAR_CHART_REVENEU_CONTENT_DASHBOARD,
    SUCCESS_CALL_API_GET_REVENEU_BY_MONTH_CONTENT_DASHBOARD,
    CHANGE_SELECT_YEAR_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD,
    CHANGE_SELECT_MONTH_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD,
    SUCCESS_CALL_API_GET_REVENEU_BY_DAY_CONTENT_DASHBOARD,
    CHANGE_SELECT_TYPE_PRODUCT_FILTER,
    CHANGE_SELECT_STATUS_PRODUCT_FILTER,
    VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD,
    VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD,
    CHANGE_SELECT_STATUS_CUSTOMER_FILTER
} from "../constants/dashboardConstant"
import { EXPIRED_DATE_ACCESS_TOKEN } from "../constants/userConstant"
import { changeStatusExpiredDateAccessToken } from "./userAction"


//general
export const clickChooseDashboard = () => {
    return { type: CLICK_CHOOSE_DASHBOARD }
}

export const clickChooseProduc = () => {
    return async (dispatch) => {
        try {

            return dispatch({ type: CLICK_CHOOSE_PRODUCT })
        } catch (err) {
            console.log('err')
        }
    }
}

export const clickChooseOrder = () => {
    return async (dispatch) => {
        try {

            return dispatch({ type: CLICK_CHOOSE_ORDER })
        } catch (err) {
            console.log('err')
        }
    }
}

export const clickChooseCustomer = () => {
    return async (dispatch) => {
        try {

            return dispatch({ type: CLICK_CHOOSE_CUSTOMER })
        } catch (err) {
            console.log('err')
        }
    }
}

export const clickMenuDashboard = () => {
    return {
        type: CLICK_MENU_DASHBOARD
    }
}

export const closeSidebarWhenResize = () => {
    return {
        type: CLOSE_SIDEBAR_WHEN_RESIZE
    }
}

export const openSidebarWhenResize = () => {
    return {
        type: OPEN_SIDEBAR_WHEN_RESIZE
    }
}

export const resetDataDashboardGeneral = () => {
    return {
        type: RESET_DATA_DASHBOARD_GENERAL
    }
}

//content dashboard
export const fetchDataOfContentDashboard = () => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_DASHBOARD })

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //if access token not expired get order overview
            var requestOptionsGetOrderOverview = {
                method: 'GET',
                headers: headers,
            };

            const resOrderOverview = await fetch(`${process.env.REACT_APP_HOST_API}/orders/overview`, requestOptionsGetOrderOverview)
            const dataOrderOverview = await resOrderOverview.json()

            //get number of total product
            const resNumberOfTotalProduct = await fetch(`${process.env.REACT_APP_HOST_API}/products/total`, requestOptionsGetOrderOverview)
            const dataNumberOfTotalProduct = await resNumberOfTotalProduct.json()

            //get number of total product
            const resNumberOfTotalCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/customers/total`, requestOptionsGetOrderOverview)
            const dataNumberOfTotalCustomer = await resNumberOfTotalCustomer.json()
            console.log('dashboard action: clickChooseDashboard: dataOrderOverview', dataOrderOverview)

            await dispatch({ type: FETCH_DONE_DASHBOARD })

            return dispatch({
                type: SUCCESS_FETCH_INFO_CONTENT_DASHBOARD,
                payload: {
                    orderOverview: dataOrderOverview.data,
                    numberOfTotalProduct: dataNumberOfTotalProduct.numberOfTotalProduct,
                    numberOfTotalCustomer: dataNumberOfTotalCustomer.numberOfTotalCustomer
                }
            })

        } catch (err) {
            console.log('err')
        }
    }
}

export const changeSelectYearChartReveneuByDayContentDashboard = (value) => {

    return {
        type: CHANGE_SELECT_YEAR_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD,
        payload: {
            value
        }
    }
}

export const changeSelectMonthChartReveneuByDayContentDashboard = (value) => {

    return {
        type: CHANGE_SELECT_MONTH_CHART_REVENEU_BY_DAY_CONTENT_DASHBOARD,
        payload: {
            value
        }
    }
}

export const changeSelectYearChartReveneuContentDashboard = (value) => {
    return {
        type: CHANGE_SELECT_YEAR_CHART_REVENEU_CONTENT_DASHBOARD,
        payload: {
            value
        }
    }
}

export const callApiGetReveneuByMonthContentDashboard = (year) => {
    return async (dispatch) => {
        try {

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //if token not expired, get info reveneu by month
            var requestOptions = {
                method: 'GET',
                headers: headers,
            };

            const resReveneuByMonth = await fetch(`${process.env.REACT_APP_HOST_API}/orders/month/${year}`, requestOptions)
            const dataReveneuByMonth = await resReveneuByMonth.json()


            return dispatch({
                type: SUCCESS_CALL_API_GET_REVENEU_BY_MONTH_CONTENT_DASHBOARD,
                payload: {
                    dataReveneuByMonth: dataReveneuByMonth.listReveneuOfMonth
                }
            })
        }
        catch (err) {
            console.log('callApiGetReveneuByMonthContentDashboard err:', err)
        }
    }
}

export const callApiGetReveneuByDayContentDashboard = (year, month) => {
    return async (dispatch) => {
        try {

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //if access token not expired, et reveneu by day
            var requestOptions = {
                method: 'GET',
                headers: headers,
            };

            const resReveneuByDay = await fetch(`${process.env.REACT_APP_HOST_API}/orders/day/${year}/${month}`, requestOptions)
            const dataReveneuByDay = await resReveneuByDay.json()


            return dispatch({
                type: SUCCESS_CALL_API_GET_REVENEU_BY_DAY_CONTENT_DASHBOARD,
                payload: {
                    dataReveneuByDay: dataReveneuByDay.listReveneuDaysInMonth
                }
            })
        }
        catch (err) {
            console.log('callApiGetReveneuByMonthContentDashboard err:', err)
        }
    }
}

//content product

export const changeSelectTypeProductFilter = (value) => {
    return {
        type: CHANGE_SELECT_TYPE_PRODUCT_FILTER,
        payload: {
            value
        }
    }
}

export const changeSelectStatusProductFilter = (value) => {
    return {
        type: CHANGE_SELECT_STATUS_PRODUCT_FILTER,
        payload: {
            value
        }
    }
}

export const resetDataContentProductDashboard = () => {
    return {
        type: RESET_DATA_CONTENT_PRODUCT_DASHBOARD
    }
}

export const changeSelectSortProductDashboard = (typeOption, value) => {
    return {
        type: CHANGE_SELECT_SORT_PRODUCT_DASHBOARD,
        payload: {
            typeOption,
            value
        }
    }
}

export const resetDataAddProductDashBoard = () => {
    return { type: RESET_DATA_ADD_PRODUCT_DASHBOARD }
}

export const resetDataEditProductDashBoard = () => {
    return { type: RESET_DATA_EDIT_PRODUCT_DASHBOARD }
}

export const callApitGetAllProductTypes = () => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_DASHBOARD })

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            var requestOptions = {
                method: 'GET',
                headers: headers,
            };

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //get all product type
            const resAllProductTypes = await fetch(`${process.env.REACT_APP_HOST_API}/product-type`, requestOptions)
            const dataAllProductTypes = await resAllProductTypes.json()


            await dispatch({ type: FETCH_DONE_DASHBOARD })
            return dispatch({
                type: SUCCESS_CALL_API_GET_ALL_PRODUCT_TYPES,
                payload: {
                    allProductTypes: dataAllProductTypes.allProductTypes
                }
            })
        }
        catch (err) {
            console.log('dashboard action call api et all product types error:', err)
        }
    }
}

export const changeStatusCallApiUpdateProductDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
        payload: {
            status
        }
    }
}

export const changeStatusCallApiAddProductDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD,
        payload: {
            status
        }
    }
}

export const callApiUpdateProductDashboard = (newInfoProduct, productId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD })

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //update product
            var raw = JSON.stringify(newInfoProduct);

            var requestOptionsEditProduct = {
                method: 'PUT',
                headers: headers,
                body: raw
            };

            const resEditProduct = await fetch(`${process.env.REACT_APP_HOST_API}/products/${productId}`, requestOptionsEditProduct)

            if (resEditProduct.status === 201) {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
                    payload: {
                        status: true
                    }
                })
            } else {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CONTENT_PRODUCT_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_UPDATE_PRODUCT_DASHBOARD
            })

        } catch (err) {
            console.log('callApiUpdateProductDashboard error', err)
        }
    }
}

export const callApiUpdateStatusProductDashboard = (status, productId, currentPage, limitItem, filterConditon, sortConditon) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD })

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //update product
            var raw = JSON.stringify({ active: status });

            var requestOptionsEditProduct = {
                method: 'PUT',
                headers: headers,
                body: raw
            };

            await fetch(`${process.env.REACT_APP_HOST_API}/products/${productId}`, requestOptionsEditProduct)

            // if (resEditProduct.status === 201) {
            //     await dispatch({
            //         type: CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
            //         payload: {
            //             status: true
            //         }
            //     })
            // } else {
            //     await dispatch({
            //         type: CHANGE_STATUS_CALL_API_UPDATE_PRODUCT_DASHBOARD,
            //         payload: {
            //             status: false
            //         }
            //     })
            // }

            // await dispatch({ type: FETCH_DONE_CONTENT_PRODUCT_DASHBOARD })


            await dispatch(fetchDataOfContentProduct(currentPage, limitItem, filterConditon, sortConditon))
            return dispatch({
                type: FETCH_DONE_CONTENT_PRODUCT_DASHBOARD
            })

        } catch (err) {
            console.log('callApiUpdateStatusProductDashboard error', err)
        }
    }
}

export const callApiAddProductDashboard = (newInfoProduct) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD })

            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //add new product
            var raw = JSON.stringify(newInfoProduct);

            var requestOptionsAddProduct = {
                method: 'POST',
                headers: headers,
                body: raw
            };

            const resAddProduct = await fetch(`${process.env.REACT_APP_HOST_API}/products`, requestOptionsAddProduct)

            if (resAddProduct.status === 201) {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD,
                    payload: {
                        status: true
                    }
                })
            } else {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_ADD_PRODUCT_DASHBOARD,
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CONTENT_PRODUCT_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_ADD_PRODUCT_DASHBOARD
            })

        } catch (err) {
            console.log('callApiAddProductDashboard error', err)
        }
    }
}

export const changeSelectTypeEditProductDashboard = (value) => {
    return {
        type: CHANGE_SELECT_TYPE_EDIT_PRODUCT_DASBOARD,
        payload: {
            value
        }
    }
}

export const changeInputEditProductDashboard = (typeInput, value) => {
    return {
        type: CHANGE_INPUT_EDIT_PRODUCT_DASHBOARD,
        payload: {
            typeInput,
            value
        }
    }
}

export const changeSelectTypeAddProductDashboard = (value) => {
    return {
        type: CHANGE_SELECT_TYPE_ADD_PRODUCT_DASBOARD,
        payload: {
            value
        }
    }
}

export const changeInputAddProductDashboard = (typeInput, value) => {
    return {
        type: CHANGE_INPUT_ADD_PRODUCT_DASHBOARD,
        payload: {
            typeInput,
            value
        }
    }
}

export const changeInputBrandProductFilter = (value) => {
    return {
        type: CHANGE_INPUT_BRAND_PRODUCT_FILTER,
        payload: {
            value
        }
    }
}

export const changeInputNameProductFilter = (value) => {
    return {
        type: CHANGE_INPUT_NAME_PRODUCT_FILTER,
        payload: {
            value
        }
    }
}

export const fetchDataOfContentProduct = (currentPage, limitItem, filterConditon, sortConditon) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD })
            console.log('fetchDataOfContentProduct')
            const token = localStorage.getItem('token')
            //get order overview
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //get product list
            var requestOptionsGetProductList = {
                method: 'GET',
                headers: headers,
            };

            //get product list
            const resProductList = await fetch(`${process.env.REACT_APP_HOST_API}/products/filter?keyword=${filterConditon.name}&brand=${filterConditon.brand}&type=${filterConditon.type}&onlyActive=${filterConditon.status}&limit=${limitItem}&page=${currentPage}&nameSort=${sortConditon.nameSort}&priceSort=${sortConditon.priceSort}&soldSort=${sortConditon.soldSort}`, requestOptionsGetProductList)
            const dataProductList = await resProductList.json()
            console.log('call api product', dataProductList)

            //call api get all type of product
            const resAllProductTypes = await fetch(`${process.env.REACT_APP_HOST_API}/product-type`, requestOptionsGetProductList)
            const dataAllProductTypes = await resAllProductTypes.json()

            await dispatch({ type: FETCH_DONE_CONTENT_PRODUCT_DASHBOARD })

            return dispatch({
                type: SUCCESS_FETCH_INFO_CONTENT_PRODUCTS,
                payload: {
                    productList: dataProductList.productList,
                    numberPage: dataProductList.numberPage,
                    overal: dataProductList.overal,
                    currentPage,
                    allProductTypes: dataAllProductTypes.allProductTypes
                }
            })

        } catch (err) {
            console.log('fetchDataOfContentProduct err:', err)
        }
    }
}

export const fetchInfoToEditProductDashboard = (productId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_PRODUCT_DASHBOARD })

            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //get info product by id
            var requestOptionsGetInfoProduct = {
                method: 'GET',
                headers: headers,
            };

            //call api get info product
            const resInfoProduct = await fetch(`${process.env.REACT_APP_HOST_API}/products/${productId}`, requestOptionsGetInfoProduct)
            const dataInfoProduct = await resInfoProduct.json()

            //call api get all type of product
            const resAllProductTypes = await fetch(`${process.env.REACT_APP_HOST_API}/product-type`, requestOptionsGetInfoProduct)
            const dataAllProductTypes = await resAllProductTypes.json()

            await dispatch({ type: FETCH_DONE_CONTENT_PRODUCT_DASHBOARD })
            return dispatch({
                type: SUCCESS_FETCH_INFO_TO_EDIT_PRODUCT_DASH_BOARD,
                payload: {
                    product: dataInfoProduct.product,
                    allProductTypes: dataAllProductTypes.allProductTypes
                }
            })
        }
        catch (err) {
            console.log('dashboard action fetchInfoToEditProductDashboard', err)
        }
    }
}

//content customer

export const changeSelectStatusCustomerFilter = (value) => {
    return {
        type: CHANGE_SELECT_STATUS_CUSTOMER_FILTER,
        payload: {
            value
        }
    }
}

export const changeSelectSortCustomerDashboard = (typeOption, value) => {
    return {
        type: CHANGE_SELECT_SORT_CUSTOMER_DASHBOARD,
        payload: {
            typeOption,
            value
        }
    }
}

export const changeStatusCallApiDeleteCustomerDashboard = (status) => {
    return {
        type: CHANGE_STATUS_DELETE_CUSTOMER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const callApiGetInfoAcccountToEditCusomerDashboard = (customerId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD })

            const token = localStorage.getItem('token')

            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //get info account by customer Id
            let requestOptionsGetInfoAccount = {
                method: 'GET',
                headers: headers,
            };

            let resInfoAccount = await fetch(`${process.env.REACT_APP_HOST_API}/account/customer/${customerId}`, requestOptionsGetInfoAccount)
            let dataInfoAccount = await resInfoAccount.json()

            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_GET_INFO_ACCOUNT_TO_EDIT_CUSTOMER_DASHBOARD,
                payload: {
                    infoAccount: dataInfoAccount.account
                }
            })
        }
        catch (err) {
            console.log('callApiAddCusomerDashboard error:', err)
            await dispatch({
                type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD,
                payload: {
                    status: false
                }
            })
            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD
            })
        }
    }
}

export const resetDataAddCustomerDashboard = () => {
    return {
        type: RESET_DATA_ADD_CUSTOMER_DASHBOARD
    }
}

export const callApiUpdateCustomerDashboard = (infoCustomer, accountId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD })

            const token = localStorage.getItem('token')

            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //edit customer
            var raw = JSON.stringify(infoCustomer);

            let requestOptionsUpdateCustomer = {
                method: 'PUT',
                headers: headers,
                body: raw
            };

            let resUpdatedCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/account/choose-role/${accountId}`, requestOptionsUpdateCustomer)
            // let dataUpdatedCustomer = await resUpdatedCustomer.json()
            if (resUpdatedCustomer.status === 201) {
                await dispatch({
                    type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD, //same logic with add customer
                    payload: {
                        status: true
                    }
                })
            } else {
                await dispatch({
                    type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD, //same logic with add customer
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD //same logic with add customer
            })
        }
        catch (err) {
            console.log('callApiAddCusomerDashboard error:', err)
            await dispatch({
                type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD, //same logic with add customer
                payload: {
                    status: false
                }
            })
            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD }) //same logic with add customer

            return dispatch({
                type: SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD //same logic with add customer
            })
        }
    }
}


export const callApiUpdateStatusCustomerDashboard = (status, customerId, currentPage, limitItem, filterConditon, sortCondition) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD })

            const token = localStorage.getItem('token')

            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //edit customer
            var raw = JSON.stringify({ active: status });

            let requestOptionsUpdateCustomer = {
                method: 'PUT',
                headers: headers,
                body: raw
            };

            await fetch(`${process.env.REACT_APP_HOST_API}/customers/${customerId}`, requestOptionsUpdateCustomer)

            //param: current page, limitItem, filterCondition, sortCondition
            await dispatch(fetchDataOfContentCustomerDashboard(currentPage, limitItem, filterConditon, sortCondition))
            return dispatch({
                type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD //same logic with add customer
            })
        }
        catch (err) {
            console.log('callApiAddCusomerDashboard error:', err)
        }
    }
}


export const callApiAddCusomerDashboard = (infoCustomer) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD })

            const token = localStorage.getItem('token')

            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");
            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //create new customer
            var raw = JSON.stringify(infoCustomer);

            let requestOptionsCreateCustomer = {
                method: 'POST',
                headers: headers,
                body: raw
            };

            let resNewCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/account/choose-role`, requestOptionsCreateCustomer)
            // let dataNewCustomer = await resNewCustomer.json()
            if (resNewCustomer.status === 201) {
                await dispatch({
                    type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD,
                    payload: {
                        status: true
                    }
                })
            } else {
                await dispatch({
                    type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD,
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD
            })
        }
        catch (err) {
            console.log('callApiAddCusomerDashboard error:', err)
            await dispatch({
                type: CHANGE_STATUS_CALL_APIT_ADD_CUSTOMER_DASHBOARD,
                payload: {
                    status: false
                }
            })
            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_ADD_CUSTOMER_DASHBOARD
            })
        }
    }
}

export const changeStatusChangeInputUsernameCustomerDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CHANGE_INPUT_USERNAME_ADD_CUSTOMER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const changeStatusChangeInputPhoneCustomerDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CHANGE_INPUT_PHONE_ADD_CUSTOMER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const changeStatusChangeInputEmailCustomerDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CHANGE_INPUT_EMAIL_ADD_CUSTOMER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const checkDupUsernameAddcustomerDashboard = (username, currentUsername) => {
    return async (dispatch) => {
        try {
            const token = localStorage.getItem('token')
            //get result check dup username
            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            let requestOptionsGetResultCheckDupUsername = {
                method: 'GET',
                headers: headers,
            };

            let resResultCheckDupUsername = await fetch(`${process.env.REACT_APP_HOST_API}/account/check/username/${username}`, requestOptionsGetResultCheckDupUsername)

            if (currentUsername) {
                resResultCheckDupUsername = await fetch(`${process.env.REACT_APP_HOST_API}/account/check/username/${username}?currentUsername=${currentUsername}`, requestOptionsGetResultCheckDupUsername)
            } else {
                resResultCheckDupUsername = await fetch(`${process.env.REACT_APP_HOST_API}/account/check/username/${username}`, requestOptionsGetResultCheckDupUsername)
            }
            let dataResultCheckDupUsername = await resResultCheckDupUsername.json()

            return dispatch({
                type: CHECK_DUP_USERNAME_ADD_CUSTOMER_DASHBOARD,
                payload: {
                    resultCheckDupUsername: dataResultCheckDupUsername.result
                }
            })
        }
        catch (err) {
            console.log('checkDupUsernameAddcustomerDashboard error:', err)
        }

    }
}

export const checkDupPhoneAddcustomerDashboard = (phone, currentPhone) => {
    return async (dispatch) => {
        try {
            const token = localStorage.getItem('token')
            //get result check dup phone
            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            let requestOptionsGetResultCheckDupPhone = {
                method: 'GET',
                headers: headers,
            };

            let resResultCheckDupPhone

            if (currentPhone) {
                resResultCheckDupPhone = await fetch(`${process.env.REACT_APP_HOST_API}/customers/check/phone/${phone}?currentPhone=${currentPhone}`, requestOptionsGetResultCheckDupPhone)
            } else {
                resResultCheckDupPhone = await fetch(`${process.env.REACT_APP_HOST_API}/customers/check/phone/${phone}`, requestOptionsGetResultCheckDupPhone)
            }

            let dataResultCheckDupPhone = await resResultCheckDupPhone.json()

            return dispatch({
                type: CHECK_DUP_PHONE_ADD_CUSTOMER_DASHBOARD,
                payload: {
                    resultCheckDupPhone: dataResultCheckDupPhone.result
                }
            })
        }
        catch (err) {
            console.log('checkDupPhoneAddcustomerDashboard error:', err)
        }

    }
}


export const checkDupEmailAddcustomerDashboard = (email, currentEmail) => {
    return async (dispatch) => {
        try {
            const token = localStorage.getItem('token')
            //get result check dup email
            let headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            let requestOptionsGetResultCheckDupEmail = {
                method: 'GET',
                headers: headers,
            };
            let resResultCheckDupEmail

            if (currentEmail) {
                resResultCheckDupEmail = await fetch(`${process.env.REACT_APP_HOST_API}/customers/check/email/${email}?currentEmail=${currentEmail}`, requestOptionsGetResultCheckDupEmail)
            } else {
                resResultCheckDupEmail = await fetch(`${process.env.REACT_APP_HOST_API}/customers/check/email/${email}`, requestOptionsGetResultCheckDupEmail)
            }

            let dataResultCheckDupEmail = await resResultCheckDupEmail.json()

            return dispatch({
                type: CHECK_DUP_EMAIL_ADD_CUSTOMER_DASHBOARD,
                payload: {
                    resultCheckDupEmail: dataResultCheckDupEmail.result
                }
            })
        }
        catch (err) {
            console.log('checkDupPhoneAddcustomerDashboard error:', err)
        }

    }
}

export const changeSelectRoleAddCustomerDashboard = (value) => {
    return {
        type: CHANGE_SELECT_ROLE_ADD_CUSTOMER_DASHBOARD,
        payload: {
            value
        }
    }
}

export const changeInputAddCustomerDashboard = (value, typeInput) => {
    return {
        type: CHANGE_INPUT_ADD_CUSTOMER_DASHBOARD,
        payload: {
            value,
            typeInput
        }
    }
}

export const changeInputFilterEmailCustomerDashboard = (value) => {
    return {
        type: CHANGE_INPUT_FILTER_EMAIL_CUSTOMER_DASHBOARD,
        payload: {
            value
        }
    }
}

export const changeInputFilterPhoneNumberCustomerDashboard = (value) => {
    return {
        type: CHANGE_INPUT_FILTER_PHONE_NUMBER_CUSTOMER_DASHBOARD,
        payload: {
            value
        }
    }
}

export const fetchDataOfContentCustomerDashboard = (currentPage, limitItem, filterConditon, sortCondition) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD })

            const token = localStorage.getItem('token')


            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");
            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token


            var requestOptionsGetCustomerList = {
                method: 'GET',
                headers: headers,
            };

            //get customer list
            let resCustomerList = await fetch(`${process.env.REACT_APP_HOST_API}/customers/filter?email=${filterConditon.email}&phoneNumber=${filterConditon.phoneNumber}&onlyActive=${filterConditon.status}&page=${currentPage}&limit=${limitItem}&nameSort=${sortCondition.nameSort}&reveneuSort=${sortCondition.reveneuSort}&orderSort=${sortCondition.orderSort}`, requestOptionsGetCustomerList)
            let dataCustomerList = await resCustomerList.json()

            if (dataCustomerList.numberPage < currentPage && dataCustomerList.numberPage !== 0) { //case delete the last item of page
                currentPage = dataCustomerList.numberPage
                resCustomerList = await fetch(`${process.env.REACT_APP_HOST_API}/customers/filter?email=${filterConditon.email}&phoneNumber=${filterConditon.phoneNumber}&onlyActive=${filterConditon.status}&page=${currentPage}&limit=${limitItem}&nameSort=${sortCondition.nameSort}&reveneuSort=${sortCondition.reveneuSort}&orderSort=${sortCondition.orderSort}`, requestOptionsGetCustomerList)
                dataCustomerList = await resCustomerList.json()
            }
            console.log('call api customer', dataCustomerList)


            await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })

            return dispatch({
                type: SUCCESS_FETCH_INFO_CONTENT_CUSTOMER_DASHBOARD,
                payload: {
                    customerList: dataCustomerList.data,
                    numberPage: dataCustomerList.numberPage,
                    currentPage,
                }
            })

        } catch (err) {
            console.log('err')
        }
    }
}

export const callApiDeleteCustomerDashboard = (customerId, currentPage, limitItem, filterConditon, sortCondition) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_CUSTOMER_DASHBOARD })

            const token = localStorage.getItem('token')
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            let requestOptionsGetInfoAccountCustomer = {
                method: 'GET',
                headers: headers,
            };

            //get info account & customer by customer id
            const resInfoAccount = await fetch(`${process.env.REACT_APP_HOST_API}/account/customer/${customerId}`, requestOptionsGetInfoAccountCustomer)
            const dataInfoAccount = await resInfoAccount.json()
            let accountId = dataInfoAccount.account._id
            console.log('step 1 get info account & customer by customer id')
            //step 1: delete all order and order details of customer
            let requestOptionsDeleteOrderOfCustomer = {
                method: 'DELETE',
                headers: headers,
            };

            var requestOptionsGetInfoOrder = {
                method: 'GET',
                headers: headers,
            };
            let requestOptionsDeleteOrderDetails = {
                method: 'DELETE',
                headers: headers,
            };


            await dataInfoAccount.account.customer.orders.forEach(async (orderId, index) => {
                //call api get list order details by order id
                const resInfoOrderById = await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}`, requestOptionsGetInfoOrder)
                const dataInfoOrderById = await resInfoOrderById.json()

                console.log('step 2 get dataInfoOrderById', dataInfoOrderById)
                //call api delete order details
                await dataInfoOrderById.order.orderDetails.forEach(async (orderDetail) => {
                    // console.log('step 3: delete order detail')
                    await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}/order-details/${orderDetail._id}`, requestOptionsDeleteOrderDetails)
                })
                console.log('step 3 delete order detail')
                console.log('step 4 set timeout delete order')
                setTimeout(async () => {
                    console.log('step 5 delete order inside set time out')
                    await fetch(`${process.env.REACT_APP_HOST_API}/customers/${customerId}/orders/${orderId}`, requestOptionsDeleteOrderOfCustomer)
                    // console.log('step 6 call api get order list')
                    // //param: current page, limitItem, filterCondition, sortCondition
                    // await dispatch(fetchDataOfContentOrder(currentPage, limitItem, filterConditon, sortCondition))
                }, 250)

                // await fetch(`${process.env.REACT_APP_HOST_API}/customers/${customerId}/orders/${orderId}`, requestOptionsDeleteOrderOfCustomer)
            })

            setTimeout(async () => {
                console.log('step 6: delete customer')
                //step2: delete customer
                let requestOptionsDeleteCustomer = {
                    method: 'DELETE',
                    headers: headers,
                };
                let resDeleteCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/account/${accountId}/customers/${customerId}`, requestOptionsDeleteCustomer)
                console.log('step 7: delete account')

                //step 3: delete account
                let requestOptionsDeleteAccount = {
                    method: 'DELETE',
                    headers: headers,
                };
                let resDeleteAccount = await fetch(`${process.env.REACT_APP_HOST_API}/account/${accountId}`, requestOptionsDeleteAccount)
                console.log('step 8: get customer list')
                //step 4: get customer list
                if (resDeleteCustomer.status === 204 && resDeleteAccount.status === 204) {
                    console.log('%c========Complete delete cutomer========', 'color: yellow')
                    console.log('resDeleteCustomer.status', resDeleteCustomer.status)
                    console.log('resDeleteAccount.status', resDeleteAccount.status)
                }
                await dispatch(fetchDataOfContentCustomerDashboard(currentPage, limitItem, filterConditon, sortCondition))

                await dispatch({ type: FETCH_DONE_CONTENT_CUSTOMER_DASHBOARD })
            }, 150)

            console.log('step 9: return dispatch')
            return dispatch({
                type: SUCCESS_CALL_API_DELETE_CUSTOMER_DASHBOARD
            })
        }
        catch (err) {
            console.log('callApiDeleteOrderDashboard error:', err)
        }
    }
}

//content order

export const changeSelectSortOrderDashboard = (typeOption, value) => {
    return {
        type: CHANGE_SELECT_SORT_ORDER_DASHBOARD,
        payload: {
            typeOption,
            value
        }
    }
}

export const changeSelectRangeDateOrderFilter = (fromDate, toDate) => {
    return {
        type: CHANGE_SELECT_RANGE_DATE_ORDER_FILTER,
        payload: {
            fromDate,
            toDate
        }
    }
}

export const changeSelectCustomerAddOrderDashboard = (value) => {
    return {
        type: CHANGE_SELECT_CUSTOMER_ADD_ORDER_DASHBOARD,
        payload: {
            value
        }
    }
}

export const resetDataAddOrderDashboard = () => {
    return {
        type: RESET_DATA_ADD_ORDER_DASHBOARD
    }
}

export const fetchInfoCustomerByIdAddOrderDashboard = (customerId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_DASHBOARD })

            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //get info customer by customer Id
            var requestOptionsGetInfoCustomerById = {
                method: 'GET',
                headers: headers,
            };
            const resInfoCustomerById = await fetch(`${process.env.REACT_APP_HOST_API}/customers/${customerId}`, requestOptionsGetInfoCustomerById)
            const dataInfoCustomerById = await resInfoCustomerById.json()
            console.log('dataInfoCustomerById', dataInfoCustomerById)
            return dispatch({
                type: SUCCESS_CALL_API_GET_INFO_CUSTOMER_BY_ID_CREATE_ORDER_DASHBOARD,
                payload: {
                    dataInfoCustomerById: dataInfoCustomerById.customer
                }
            })
        }
        catch (err) {
            console.log('fetch info customer by id add order dashboard err:', err)
        }
    }
}

export const fetchInfoToAddOrderDashboard = () => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_DASHBOARD })

            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            var requestOptionsGetProductList = {
                method: 'GET',
                headers: headers,
            };
            //call api get product list
            const resProductList = await fetch(`${process.env.REACT_APP_HOST_API}/products`, requestOptionsGetProductList)
            const dataProductList = await resProductList.json()
            console.log('call api product', dataProductList)

            var requestOptionsGetAccountList = {
                method: 'GET',
                headers: headers,
            };
            //call api get account list
            const resAccountList = await fetch(`${process.env.REACT_APP_HOST_API}/accounts`, requestOptionsGetAccountList)
            const dataAccountList = await resAccountList.json()

            await dispatch({ type: FETCH_DONE_DASHBOARD })

            return dispatch({
                type: SUCCESS_FETCH_INFO_TO_ADD_ORDER_DASHBOARD,
                payload: {
                    productList: dataProductList.data,
                    accountList: dataAccountList.account
                }
            })
        }
        catch (err) {
            console.log('fetch info add order dashboard error:', err)
        }
    }
}

export const    callApiUpdateOrderEditOrderDashboard = (infoEditOrder, orderId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_ORDER_DASHBOARD })

            const token = localStorage.getItem('token')
            var headersUpdateOrder = new Headers();
            headersUpdateOrder.append("Authentication", `Bearer ${token}`);
            headersUpdateOrder.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headersUpdateOrder,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            // update order section
            let totalQuantity = infoEditOrder.cloneOrderDetail.reduce((acc, orderDetailItem) => {
                return acc += +orderDetailItem.quantity
            }, 0)
            const infoOrder = {
                cost: infoEditOrder.totalPrice,
                note: infoEditOrder.note,
                customerName: infoEditOrder.customerName,
                phoneNumber: infoEditOrder.phoneNumber,
                email: infoEditOrder.email,
                address: infoEditOrder.address,
                status: infoEditOrder.status,
                totalQuantity
            }

            if (infoEditOrder.voucher !== '') {
                infoOrder.voucher = infoEditOrder.voucher._id
            } else {
                infoOrder.voucher = ''
            }

            let rawInfoOrder = JSON.stringify(infoOrder);

            var requestOptionsUpdateOrder = {
                method: 'PUT',
                headers: headersUpdateOrder,
                body: rawInfoOrder,
            };

            const resOrder = await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}`, requestOptionsUpdateOrder)

            //order detail section

            await infoEditOrder.cloneOrderDetail.forEach(async (orderDetailItem) => {
                let rawOrderDetail = JSON.stringify({
                    "product": orderDetailItem.product._id,
                    "quantity": +orderDetailItem.quantity
                });

                if (orderDetailItem.quantity !== 0) {
                    if (orderDetailItem._id !== '') {
                        let requestOptionsOrderDetail = {
                            method: 'PUT',
                            headers: headersUpdateOrder,
                            body: rawOrderDetail,
                            // redirect: 'follow'
                        };
                        await fetch(`${process.env.REACT_APP_HOST_API}/order-details/${orderDetailItem._id}`, requestOptionsOrderDetail)

                    } else {
                        let requestOptionsOrderDetail = {
                            method: 'POST',
                            headers: headersUpdateOrder,
                            body: rawOrderDetail,
                            // redirect: 'follow'
                        };
                        await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}/order-details`, requestOptionsOrderDetail)
                    }
                } else {
                    if (orderDetailItem._id !== '') {
                        let requestOptionsOrderDetail = {
                            method: 'DELETE',
                            headers: headersUpdateOrder
                            // redirect: 'follow'
                        };
                        await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}/order-details/${orderDetailItem._id}`, requestOptionsOrderDetail)

                    }
                }

            })

            if (resOrder.status === 201) {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
                    payload: {
                        status: true
                    }
                })
            } else {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CONTENT_ORDER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_UPDATE_ORDER_EDIT_ORDER_DASHBOARD,
                payload: {
                    step: infoEditOrder.currentStep
                }
            })

        }
        catch (err) {
            console.log('dashboard action call api update order: error:', err)
        }
    }
}

export const validateFalseBuyQuantityProductOrderDashboard = (product, buyQuantity) => {
    return {
        type: VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD,
        payload: {
            product,
            buyQuantity
        }
    }
}

export const validateTrueBuyQuantityProductOrderDashboard = () => {
    return {
        type: VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_ORDER_DASHBOARD
    }
}

export const callApiCreateOrderDashboard = (infoEditOrder, customerId) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_ORDER_DASHBOARD })

            const token = localStorage.getItem('token')
            var headersCreateOrder = new Headers();
            headersCreateOrder.append("Authentication", `Bearer ${token}`);
            headersCreateOrder.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headersCreateOrder,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token



            console.log('callApiCreateOrderDashboard infoEditOrder', infoEditOrder)
            let totalQuantity = infoEditOrder.cloneOrderDetail.reduce((acc, orderDetailItem) => {
                return acc += +orderDetailItem.quantity
            }, 0)
            console.log('callApiCreateOrderDashboard totalQuantity', totalQuantity)

            // create order section
            const infoOrder = {
                cost: infoEditOrder.totalPrice,
                note: infoEditOrder.note,
                customerName: infoEditOrder.customerName,
                phoneNumber: infoEditOrder.phoneNumber,
                email: infoEditOrder.email,
                address: infoEditOrder.address,
                status: infoEditOrder.status,
                totalQuantity
            }

            if (infoEditOrder.voucher !== '') {
                infoOrder.voucher = infoEditOrder.voucher._id
            } else {
                infoOrder.voucher = ''
            }

            let rawInfoOrder = JSON.stringify(infoOrder);

            var requestOptionsCreateOrder = {
                method: 'POST',
                headers: headersCreateOrder,
                body: rawInfoOrder,
            };

            const resOrder = await fetch(`${process.env.REACT_APP_HOST_API}/customers/${customerId}/orders`, requestOptionsCreateOrder)

            const dataOrder = await resOrder.json()

            //order detail section

            await infoEditOrder.cloneOrderDetail.forEach(async (orderDetailItem) => {
                let rawOrderDetail = JSON.stringify({
                    "product": orderDetailItem.product._id,
                    "quantity": +orderDetailItem.quantity
                });

                if (orderDetailItem.quantity !== 0) {
                    let requestOptionsOrderDetail = {
                        method: 'POST',
                        headers: headersCreateOrder,
                        body: rawOrderDetail,
                        // redirect: 'follow'
                    };
                    await fetch(`${process.env.REACT_APP_HOST_API}/orders/${dataOrder.order._id}/order-details`, requestOptionsOrderDetail)
                }
            })

            if (resOrder.status === 201) {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
                    payload: {
                        status: true
                    }
                })
            } else {
                await dispatch({
                    type: CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
                    payload: {
                        status: false
                    }
                })
            }

            await dispatch({ type: FETCH_DONE_CONTENT_ORDER_DASHBOARD })

            return dispatch({
                type: SUCCESS_CALL_API_CREATE_ORDER_DASHBOARD,
                payload: {
                    newOrderCode: dataOrder.order.orderCode
                }
            })

        }
        catch (err) {
            console.log('dashboard action call api update order: error:', err)
        }
    }
}

export const callApiDeleteOrderDashboard = (orderId, currentPage, limitItem, filterConditon, sortCondition) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_ORDER_DASHBOARD })

            console.log('step 1 get data', orderId)
            const token = localStorage.getItem('token')
            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            var requestOptionsGetInfoCustomer = {
                method: 'GET',
                headers: headers,
            };

            //get info customer by orderId
            const resInfoCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/customers/order/${orderId}`, requestOptionsGetInfoCustomer)
            const dataInfoCustomer = await resInfoCustomer.json()
            console.log('step 2 get dataInfoCustomer', dataInfoCustomer)

            //call api get list order details by order id
            var requestOptionsGetInfoOrder = {
                method: 'GET',
                headers: headers,
            };
            const resInfoOrderById = await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}`, requestOptionsGetInfoOrder)
            const dataInfoOrderById = await resInfoOrderById.json()

            console.log('step 3 get dataInfoOrderById', dataInfoOrderById)

            //call api delete order details
            let requestOptionsDeleteOrderDetails = {
                method: 'DELETE',
                headers: headers,
            };
            await dataInfoOrderById.order.orderDetails.forEach(async (orderDetail) => {
                console.log('')
                await fetch(`${process.env.REACT_APP_HOST_API}/orders/${orderId}/order-details/${orderDetail._id}`, requestOptionsDeleteOrderDetails)
            })
            console.log('step 4 delete order detail', dataInfoCustomer)


            //call api delete order
            let customerId = dataInfoCustomer.data._id
            var requestOptionsDeleteOrder = {
                method: 'DELETE',
                headers: headers,
            };
            console.log('step 5 set timeout delete order')
            setTimeout(async () => {
                console.log('step 6 delete order inside set time out', dataInfoCustomer)
                await fetch(`${process.env.REACT_APP_HOST_API}/customers/${customerId}/orders/${orderId}`, requestOptionsDeleteOrder)
                console.log('step 7 call api get order list')
                //param: current page, limitItem, filterCondition, sortCondition
                await dispatch(fetchDataOfContentOrder(currentPage, limitItem, filterConditon, sortCondition))
            }, 150)

            //get order list
            console.log('step 8 return dispatch')
            return dispatch({
                type: SUCCESS_DELETE_ORDER_DASHBOARD
            })

        }
        catch (err) {
            console.log('callApiDeleteOrderDashboard error:', err)
        }
    }
}

export const changeStatusCallApiDeleteOrderDashboard = (status) => {
    return {
        type: CHANGE_STATUS_DELETE_ORDER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const changeStatusCallApiCheckVoucherEditOrderDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const callApiCheckVoucherEditOrderDashboard = (voucherCode) => {
    return async (dispatch) => {
        try {
            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            var requestOptionsGetInfoVoucher = {
                method: 'GET',
                headers: headers,
            };

            //call api get info product
            const resInfoVoucher = await fetch(`${process.env.REACT_APP_HOST_API}/vouchers/code/${voucherCode}`, requestOptionsGetInfoVoucher)
            const dataInfoVoucher = await resInfoVoucher.json()

            return dispatch({
                type: SUCCESS_CALL_API_CHECK_VOUCHER_EDIT_ORDER_DASHBOARD,
                payload: {
                    voucher: dataInfoVoucher.voucher
                }
            })
        }
        catch (err) {
            console.log('dashboard action call api check voucher: error:', err)
        }
    }
}

export const addMoreProductToOrderDetailEditOrderDashboard = () => {
    return {
        type: ADD_MORE_PRODUCT_TO_ORDER_DETAIL_EDIT_OREDER_DASHBOARD
    }
}

export const removeProductToOrderDetailEditOrderDashboard = (productId) => {
    return {
        type: REMOVE_PRODUCT_EDIT_ORDER_DASHBOARD,
        payload: {
            productId
        }
    }
}

export const changeSelectAddMoreProductEditOrderDashboard = (productName, productPrice, productId) => {
    return {
        type: CHANGE_SELECT_ADD_MORE_PRODUCT_EDIT_ORDER_DASHBOARD,
        payload: {
            productName,
            productId,
            productPrice
        }
    }
}

export const changeInputEditOrderDashboard = (typeInput, value) => {
    return {
        type: CHANGE_INPUT_EDIT_ORDER_DASHBOARD,
        payload: {
            typeInput,
            value
        }
    }
}

export const resetDataEditOrderDashboard = () => {
    return {
        type: RESET_DATA_EDIT_ORDER_DASHBOARD
    }
}

export const changeStatusCallApiUpdateOrderDashboard = (status) => {
    return {
        type: CHANGE_STATUS_CALL_API_UPDATE_ORDER_DASHBOARD,
        payload: {
            status
        }
    }
}

export const changeSelectStatusEditOrderDashboard = (value, step) => {
    return {
        type: CHANGE_SELECT_STATUS_EDIT_ORDER_DASBOARD,
        payload: {
            value,
            step
        }
    }
}

export const changeInputQuantityEditOrderDashboard = (value, index) => {
    return {
        type: CHANGE_INPUT_QUANTITY_EDIT_ORDER_DASHBOARD,
        payload: {
            value,
            index
        }
    }
}

export const fetchDataOfContentOrder = (currentPage, limitItem, filterConditon, sortCondition) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_ORDER_DASHBOARD })

            const token = localStorage.getItem('token')


            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            var requestOptionsGetOrderList = {
                method: 'GET',
                headers: headers,
            };

            // console.log(`url filter order: ${process.env.REACT_APP_HOST_API}/orders/filter?page=${currentPage}&limit=${limitItem}&orderCode=${filterConditon.orderCode}&email=${filterConditon.email}&orderCodeSort=${sortCondition.orderCodeSort}&priceSort=${sortCondition.priceSort}&orderQuantitySort=${sortCondition.orderQuantitySort}&createDateSort=${sortCondition.createDateSort}`)
            //get order list
            let resOrderList = await fetch(`${process.env.REACT_APP_HOST_API}/orders/filter?page=${currentPage}&limit=${limitItem}&orderCode=${filterConditon.orderCode}&email=${filterConditon.email}&statusOrder=${filterConditon.statusOrder}&fromDate=${filterConditon.fromDate}&toDate=${filterConditon.toDate}&orderCodeSort=${sortCondition.orderCodeSort}&priceSort=${sortCondition.priceSort}&orderQuantitySort=${sortCondition.orderQuantitySort}&createDateSort=${sortCondition.createDateSort}`, requestOptionsGetOrderList)
            let dataOrderList = await resOrderList.json()
            if (dataOrderList.numberPage < currentPage && dataOrderList.numberPage !== 0) { //case delete the last item of page
                currentPage = dataOrderList.numberPage
                resOrderList = await fetch(`${process.env.REACT_APP_HOST_API}/orders/filter?page=${currentPage}&limit=${limitItem}&orderCode=${filterConditon.orderCode}&email=${filterConditon.email}&statusOrder=${filterConditon.statusOrder}&fromDate=${filterConditon.fromDate}&toDate=${filterConditon.toDate}orderCodeSort=${sortCondition.orderCodeSort}&priceSort=${sortCondition.priceSort}&orderQuantitySort=${sortCondition.orderQuantitySort}&createDateSort=${sortCondition.createDateSort}`, requestOptionsGetOrderList)
                dataOrderList = await resOrderList.json()
            }
            console.log('call api Orderrrrrrrrrrrr', dataOrderList)

            await dispatch({ type: FETCH_DONE_CONTENT_ORDER_DASHBOARD })

            return dispatch({
                type: SUCCESS_FETCH_INFO_CONTENT_ORDERS,
                payload: {
                    orderList: dataOrderList.orders,
                    numberPage: dataOrderList.numberPage,
                    overal: dataOrderList.overal,
                    currentPage
                }
            })

        } catch (err) {
            console.log('err')
        }
    }
}

export const changeInputOrderCodeOrderFilter = (value) => {
    return {
        type: CHANGE_INPUT_ORDER_CODE_ORDER_FILTER,
        payload: {
            value
        }
    }
}

export const changeInputEmailCustomerOrderFilter = (value) => {
    return {
        type: CHANGE_INPUT_EMAIL_CUSTOMER_ORDER_FILTER,
        payload: {
            value
        }
    }
}

export const changeSelectStatusOrderFilter = (value) => {
    return {
        type: CHANGE_SELECT_STATUS_ORDER_FILTER,
        payload: {
            value
        }
    }
}

export const fetchInfoToEditOrderDashboard = (orderCode) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_CONTENT_ORDER_DASHBOARD })

            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            var requestOptionsGetInfoOrder = {
                method: 'GET',
                headers: headers,
            };

            //call api get info order by order code
            const resInfoOrder = await fetch(`${process.env.REACT_APP_HOST_API}/orders/code/${orderCode}`, requestOptionsGetInfoOrder)
            const dataInfoOrder = await resInfoOrder.json()

            var requestOptionsGetProductList = {
                method: 'GET',
                headers: headers,
            };
            //call api get product list
            const resProductList = await fetch(`${process.env.REACT_APP_HOST_API}/products`, requestOptionsGetProductList)
            const dataProductList = await resProductList.json()
            console.log('call api product', dataProductList)

            await dispatch({ type: FETCH_DONE_CONTENT_ORDER_DASHBOARD })
            return dispatch({
                type: SUCCESS_FETCH_INFO_TO_EDIT_ORDER_DASH_BOARD,
                payload: {
                    order: dataInfoOrder.order,
                    orderDetail: dataInfoOrder.orderDetail,
                    productList: dataProductList.data
                }
            })
        }
        catch (err) {
            console.log('dashboard action fetchInfoToEditProductDashboard', err)
        }
    }
}





