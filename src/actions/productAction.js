import {
    PENDING_FETCH_PRODUCTS,
    SUCCESS_FETCH_PRODUCTS,
    FAIL_FETCH_PRODUCTS,
    // CHANGE_PAGE,
    CHANGE_SIZE_WEB,
    // FILTER_PRODUCT,
    FETCH_PRODUCT_BY_ID,
    SUCCESS_FETCH_PRODUCT_LIST_RELATED,
    CHANGE_STATUS_FIRST_TIME_RENDER
} from "../constants/productConstant";

export const changeStatusFirstTimeRender = (status) => {
    return {
        type: CHANGE_STATUS_FIRST_TIME_RENDER,
        payload: {
            status
        }
    }
}

export const fetchProduct = (currentPage, itemPerPage, filterCondition) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: PENDING_FETCH_PRODUCTS })
            // console.log(`%c${process.env.REACT_APP_HOST_API}/products/filter/?brand=${filterCondition.brand}&color=${filterCondition.color}&min=${filterCondition.price.min}&max=${filterCondition.price.max}&keyword=${filterCondition.name}&limit=${itemPerPage}&page=${currentPage}`, 'color: yellow' )
            let resProductList = await fetch(`${process.env.REACT_APP_HOST_API}/products/filter/?brand=${filterCondition.brand}&color=${filterCondition.color}&min=${filterCondition.price.min}&max=${filterCondition.price.max}&keyword=${filterCondition.name}&limit=${itemPerPage}&page=${currentPage}&onlyActive=true`)

            let data = await resProductList.json()

            // console.log('data response product list', data)
            return dispatch({
                type: SUCCESS_FETCH_PRODUCTS,
                payload: {
                    productsList: data.productList,
                    numberPage: data.numberPage,
                    brandsList: data.brandsList,
                    colorList: data.colorList,
                    itemPerPage,
                    currentPage
                }
            })
        }
        catch (err) {
            console.log('fetchProduct error:', err)
            return dispatch({ type: FAIL_FETCH_PRODUCTS, payload: { itemPerPage } })
        }
    }
}

export const fetchProductListRelated = (limitRelateProduct) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: PENDING_FETCH_PRODUCTS })
            console.log('limitRelateProduct', limitRelateProduct)
            // console.log(`%cCurrent page product action ${currentPage}`, 'color: yellow' )
            let resProductListRelated = await fetch(`${process.env.REACT_APP_HOST_API}/products?limit=${limitRelateProduct}&random=true&onlyActive=true`)

            let data = await resProductListRelated.json()

            console.log('data response product list related', data)
            return dispatch({
                type: SUCCESS_FETCH_PRODUCT_LIST_RELATED,
                payload: {
                        productListRelated: data.data
                }
            })
        }
        catch (err) {
            console.log('fetchProduct error:', err)
            return dispatch({ type: FAIL_FETCH_PRODUCTS})
        }
    }
}

// export const changePage = (currentPage) => {
//     return {
//         type: CHANGE_PAGE,
//         payload: {
//             currentPage
//         }
//     }
// }

export const changeSizeWeb = (itemPerPage) => {
    return {
        type: CHANGE_SIZE_WEB,
        payload: {
            itemPerPage
        }
    }
}


// export const filterProduct = (filterCondition) => {
//     return async (dispatch) => {
//         try {
//             await dispatch({ type: PENDING_FETCH_PRODUCTS })

//             let res = await fetch(`${process.env.REACT_APP_HOST_API}/products/filter/?brand=${filterCondition.brand}&color=${filterCondition.color}&min=${filterCondition.price.min}&max=${filterCondition.price.max}&keyword=${filterCondition.name}`)

//             let data = await res.json()


//             return dispatch({
//                 type: FILTER_PRODUCT,
//                 payload: {
//                     resultFilterList: data.data,
//                     filterCondition
//                 }
//             })
//         }
//         catch (err) {
//             return dispatch({ type: FAIL_FETCH_PRODUCTS, payload: { itemPerPage: 8 } })
//         }
//     }
// }

export const fetchProductById = (idProduct) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: PENDING_FETCH_PRODUCTS })

            let res = await fetch(`${process.env.REACT_APP_HOST_API}/products/${idProduct}`)
            let data = await res.json()

            return dispatch({
                type: FETCH_PRODUCT_BY_ID,
                payload: {
                    productById: data.product
                }
            })
        }
        catch (err) {
            console.log('fetchProductById error:', err)
            return dispatch({ type: FAIL_FETCH_PRODUCTS, payload: { itemPerPage: 8 } })
        }
    }
}