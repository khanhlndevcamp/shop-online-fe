import {
    ADD_TO_CART,
    CHANGE_PAGE_CART_PRODUCT,
    CHANGE_VALUE_INPUT_INFO_ORDER,
    CHANGE_VALUE_INPUT_VOUCHER,
    EDIT_MINUS_QUANITY,
    EDIT_PLUS_QUANITY,
    FETCH_CHECK_VOUCHER_CODE,
    FETCH_PENDING_VOUCHER_CODE,
    FETCH_VOUCHER_CODE_FAIL,
    GET_INFO_ORDER,
    LOAD_DATA_LOCAL_STORAGE,
    REMOVE_PRODUCT_CART,
    RESET_STATUS_CHANGE_CART_PRODUCT,
    UPDATE_CART,
    CALL_API_PENDING_CREATE_ORDER,
    CALL_API_CREATE_ORDER_SUCCESS,
    CALL_API_CREATE_ORDER_FAIL,
    RESET_STATUS_CREAT_ORDER,
    RESET_DATA_AFTER_CHECK_OUT,
    VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_CART_PRODUCT,
    VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_CART_PRODUCT
} from "../constants/cartConstant"
import { EXPIRED_DATE_ACCESS_TOKEN } from "../constants/userConstant"
import { changeStatusExpiredDateAccessToken } from "./userAction"


export const addToCart = (quanityProduct, productById) => {
    return {
        type: ADD_TO_CART,
        payload: {
            quanityProduct,
            productById
        }
    }
}

export const loadDataLocalStorage = () => {
    return {
        type: LOAD_DATA_LOCAL_STORAGE
    }
}

export const editPlusQuanity = (idProduct) => {
    return {
        type: EDIT_PLUS_QUANITY,
        payload: {
            idProduct
        }
    }
}

export const editMinusQuanity = (idProduct) => {
    return {
        type: EDIT_MINUS_QUANITY,
        payload: {
            idProduct
        }
    }
}

export const removeProductCart = (idProduct) => {
    return {
        type: REMOVE_PRODUCT_CART,
        payload: {
            idProduct
        }
    }
}

export const updateCart = () => {
    return {
        type: UPDATE_CART
    }
}

export const changePageCartProduct = (currentPage) => {
    return {
        type: CHANGE_PAGE_CART_PRODUCT,
        payload: {
            currentPage
        }
    }
}

export const resetStatusChangeCartProduct = () => {
    return {
        type: RESET_STATUS_CHANGE_CART_PRODUCT
    }
}

export const getInfoOrder = (user) => {
    return {
        type: GET_INFO_ORDER,
        payload: {
            user
        }
    }
}

export const changeValueInputInfoOrder = (key, value) => {
    return {
        type: CHANGE_VALUE_INPUT_INFO_ORDER,
        payload: {
            key,
            value
        }
    }
}

export const changeValueInputVoucher = (voucherCode) => {
    return {
        type: CHANGE_VALUE_INPUT_VOUCHER,
        payload: {
            voucherCode
        }
    }
}

export const fetchPendingCheckVoucher = () => {
    return {
        type: FETCH_PENDING_VOUCHER_CODE
    }
}

export const fetchCheckVoucherCode = (voucherCode) => {
    return async (dispatch) => {
        try {
            await dispatch(fetchPendingCheckVoucher())

            console.log('action voucher code', voucherCode)
            const res = await fetch(`${process.env.REACT_APP_HOST_API}/vouchers/code/${voucherCode}`)

            const data = await res.json()

            return dispatch({
                type: FETCH_CHECK_VOUCHER_CODE,
                payload: {
                    discount: data.voucher.discount,
                    idVoucher: data.voucher._id
                }
            })

        }
        catch (err) {
            console.log('fail check voucher')
            return dispatch({
                type: FETCH_VOUCHER_CODE_FAIL
            })
        }
    }
}

export const callApiCreateOrder = (cartReducer) => {
    return async (dispatch) => {
        try {
            console.log('------------cartAction-------------')
            console.log('cart reducer', cartReducer)
            await dispatch({ type: CALL_API_PENDING_CREATE_ORDER })

            const token = localStorage.getItem('token')
            //get info account
            var headersGetInfoAccount = new Headers();
            headersGetInfoAccount.append("Authentication", `Bearer ${token}`);
            headersGetInfoAccount.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headersGetInfoAccount,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token
            

            //get current info account by account Id store at headers access token
            var requestOptionsGetInfoAccount = {
                method: 'GET',
                headers: headersGetInfoAccount,
                // redirect: 'follow'
            };

            const resInfoAccount = await fetch(`${process.env.REACT_APP_HOST_API}/account`, requestOptionsGetInfoAccount)
            const dataInfoAccount = await resInfoAccount.json()

            console.log('1 dataInfoAccount', dataInfoAccount)
            // order section: create order for customer id
            let totalQuantity = cartReducer.cartProducts.reduce((acc, cartProduct) => {
                return acc += +cartProduct.quanityProduct
            }, 0)

            const infoOrder = {
                cost: cartReducer.totalPrice,
                note: cartReducer.infoOrder.note,
                customerName: cartReducer.infoOrder.fullName,
                phoneNumber: cartReducer.infoOrder.phone,
                email: cartReducer.infoOrder.email,
                address: cartReducer.infoOrder.address,
                totalQuantity
            }

            if (cartReducer.idVoucher !== '') {
                infoOrder.voucher = cartReducer.idVoucher
            }
            var rawInfoOrder = JSON.stringify(infoOrder);

            var requestOptionsOrder = {
                method: 'POST',
                headers: headersGetInfoAccount,
                body: rawInfoOrder,
                // redirect: 'follow'
            };

            console.log('call api create order')
            const resOrder = await fetch(`${process.env.REACT_APP_HOST_API}/customers/${dataInfoAccount.account.customer._id}/orders`, requestOptionsOrder)

            const dataOrder = await resOrder.json()
            console.log('2 dataOrder', dataOrder)

            //order detail section

            await cartReducer.cartProducts.forEach(async (cartProduct) => {

                var rawOrderDetail = JSON.stringify({
                    "product": cartProduct._id,
                    "quantity": +cartProduct.quanityProduct
                });

                var requestOptionsOrderDetail = {
                    method: 'POST',
                    headers: headersGetInfoAccount,
                    body: rawOrderDetail,
                    // redirect: 'follow'
                };
                await fetch(`${process.env.REACT_APP_HOST_API}/orders/${dataOrder.order._id}/order-details`, requestOptionsOrderDetail)
            })
            var requestOptionsGetOrderDetail = {
                method: 'GET',
                headers: headersGetInfoAccount,
                // redirect: 'follow'
            };

            const resAllInforOfOrder = await fetch(`${process.env.REACT_APP_HOST_API}/orders/${dataOrder.order._id}/order-details`, requestOptionsGetOrderDetail)
            const dataAllInforOfOrder = await resAllInforOfOrder.json()
            console.log('3 dataAllInforOfOrder', dataAllInforOfOrder)
            console.log('-------------------------')

            await dispatch({
                type: CALL_API_CREATE_ORDER_SUCCESS,
                payload: {
                    orderCode: dataAllInforOfOrder.order.orderCode
                }
            })

            return dispatch({ type: RESET_DATA_AFTER_CHECK_OUT })
        }
        catch (err) {
            return dispatch({
                type: CALL_API_CREATE_ORDER_FAIL,
                payload: {
                    err
                }
            })
        }
    }
}

export const resetStatusCreateOrder = () => {
    return {
        type: RESET_STATUS_CREAT_ORDER
    }
}

export const validateFalseBuyQuantityProductCartProduct = (product, buyQuantity) => {
    return {
        type: VALIDATE_FALSE_BUY_QUANTITY_PRODUCT_CART_PRODUCT,
        payload: {
            product,
            buyQuantity
        }
    }
}

export const validateTrueBuyQuantityProductCartProduct = () => {
    return {
        type: VALIDATE_TRUE_BUY_QUANTITY_PRODUCT_CART_PRODUCT
    }
}