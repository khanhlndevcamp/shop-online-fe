import {
    FETCH_GET_ORDER_CUSTOMER_FAIL,
    FETCH_GET_ORDER_CUSTOMER_SUCCESS,
    FETCH_PENDING_GET_ORDER_CUSTOMER,
    CHANGE_PAGE_ORDERS_CUSTOMER,
    RESET_DATA_ORDER_CUSTOMER,
    CHANGE_VALUE_INPUT_ORDER_CODE,
    FETCH_DELETE_ORDER_CUSTOMER_SUCCESS,
    FETCH_DELETE_ORDER_CUSTOMER_FAIL,
    RESET_STATUS_DELETE_ORDER,
    FETCH_GET_CURRENT_ORDER_INFO_DETAIL_SUCCESS,
    FETCH_PENDING_GET_CURRENT_ORDER_INFO_DETAIL,
    FETCH_GET_CURRENT_ORDER_INFO_DETAIL_FAIL
} from "../constants/orderConstant"
import { EXPIRED_DATE_ACCESS_TOKEN } from "../constants/userConstant"
import { changeStatusExpiredDateAccessToken } from "./userAction"

export const fetchOrderCustomer = (emailCustomer, currentPage, limitItem, orderCode) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_GET_ORDER_CUSTOMER })

            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            var requestOptionsGetOrderCustomer = {
                method: 'GET',
                headers: headers,
            };

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }

            //end check expired token

            //if token not expired get order of customer
            let resOrderCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/customers/email/${emailCustomer}/orders?page=${currentPage}&limit=${limitItem}&orderCode=${orderCode}`, requestOptionsGetOrderCustomer)
            let dataOrderCustomer = await resOrderCustomer.json()

            if (dataOrderCustomer.numberPage < currentPage && dataOrderCustomer.numberPage !== 0) {
                currentPage = dataOrderCustomer.numberPage
                resOrderCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/customers/email/${emailCustomer}/orders?page=${currentPage}&limit=${limitItem}&orderCode=${orderCode}`, requestOptionsGetOrderCustomer)
                dataOrderCustomer = await resOrderCustomer.json()
            }

            console.log('dataOrderCustomer', dataOrderCustomer)
            return dispatch({
                type: FETCH_GET_ORDER_CUSTOMER_SUCCESS,
                payload: {
                    ordersOfCustomer: dataOrderCustomer.orders,
                    customerInfo: dataOrderCustomer.customerInfo,
                    numberPage: dataOrderCustomer.numberPage,
                    currentPage
                }
            })
        }
        catch (err) {
            return dispatch({ type: FETCH_GET_ORDER_CUSTOMER_FAIL })
        }
    }
}

export const changePageOrdersCustomer = (currentPage) => {
    return {
        type: CHANGE_PAGE_ORDERS_CUSTOMER,
        payload: {
            currentPage
        }
    }
}

export const resetDataOrderCustomer = () => {
    return {
        type: RESET_DATA_ORDER_CUSTOMER
    }
}

export const changeValueInputOrderCode = (valueFilterOrder) => {
    return {
        type: CHANGE_VALUE_INPUT_ORDER_CODE,
        payload: {
            valueFilterOrder
        }
    }
}

export const deleteOderOfCustomer = (idCustomer, idOder, emailCustomer, currentPage, limitItem, orderCode) => {
    return async (dispatch) => {
        try {
            await dispatch({ type: FETCH_PENDING_GET_ORDER_CUSTOMER })
            const token = localStorage.getItem('token')


            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token

            //call api get list order details by order id
            var requestOptionsGetInfoOrder = {
                method: 'GET',
                headers: headers,
            };
            const resInfoOrderById = await fetch(`${process.env.REACT_APP_HOST_API}/orders/${idOder}`, requestOptionsGetInfoOrder)
            const dataInfoOrderById = await resInfoOrderById.json()

            // console.log('callApiDeleteOrderDashboard dataInfoOrderById', dataInfoOrderById)

            //call api delete order details
            let requestOptionsDeleteOrderDetails = {
                method: 'DELETE',
                headers: headers,
            };

            await dataInfoOrderById.order.orderDetails.forEach(async (orderDetail) => {
                await fetch(`${process.env.REACT_APP_HOST_API}/orders/${idOder}/order-details/${orderDetail._id}`, requestOptionsDeleteOrderDetails)
            })

            var requestOptions = {
                method: 'DELETE',
                redirect: 'follow',
                headers: headers,
            };

            setTimeout(async () => {
                console.log('step 6 delete order inside set time out')
                //delete order by orderId of customer
                await fetch(`${process.env.REACT_APP_HOST_API}/customers/${idCustomer}/orders/${idOder}`, requestOptions)
                console.log('step 7 call api get order list')
                // //get order of customer
                await dispatch(fetchOrderCustomer(emailCustomer, currentPage, limitItem, orderCode))
            }, 150)
            // const resOrderCustomer = await fetch(`${process.env.REACT_APP_HOST_API}/customers/email/${emailCustomer}/orders`)
            // const dataOrderCustomer = await resOrderCustomer.json()

            return dispatch({
                type: FETCH_DELETE_ORDER_CUSTOMER_SUCCESS
            })
        }
        catch (err) {
            return dispatch({ type: FETCH_DELETE_ORDER_CUSTOMER_FAIL })
        }
    }
}

export const resetStatusDeleteOrder = () => {
    return { type: RESET_STATUS_DELETE_ORDER }
}

export const getCurrentOrderInfoDetail = (idOrder) => {
    return async (dispatch) => {
        try {

            await dispatch({ type: FETCH_PENDING_GET_CURRENT_ORDER_INFO_DETAIL })
            const token = localStorage.getItem('token')

            var headers = new Headers();
            headers.append("Authentication", `Bearer ${token}`);
            headers.append("Content-Type", "application/json");

            var requestOptionsGetOrderCustomer = {
                method: 'GET',
                headers: headers,
            };
            //check expired token
            var requestOptionsGetResultCheckExpiredToken = {
                method: 'GET',
                headers: headers,
            };
            const resResultCheckExpiredToken = await fetch(`${process.env.REACT_APP_HOST_API}/check-expired`, requestOptionsGetResultCheckExpiredToken)
            const dataResultCheckExpiredToken = await resResultCheckExpiredToken.json()

            if (dataResultCheckExpiredToken.expired) {
                await dispatch(changeStatusExpiredDateAccessToken(true))
                return dispatch({
                    type: EXPIRED_DATE_ACCESS_TOKEN
                })
            }
            //end check expired token
            
            //get info current order by order id
            const resInfoCurrentOrder = await fetch(`${process.env.REACT_APP_HOST_API}/orders/${idOrder}/order-details`, requestOptionsGetOrderCustomer)
            const dataInfoCurrentOrder = await resInfoCurrentOrder.json()

            return dispatch({
                type: FETCH_GET_CURRENT_ORDER_INFO_DETAIL_SUCCESS,
                payload: {
                    order: dataInfoCurrentOrder.order,
                    orderDetails: dataInfoCurrentOrder.orderDetails
                }
            })
        }
        catch (err) {
            return dispatch({ type: FETCH_GET_CURRENT_ORDER_INFO_DETAIL_FAIL })
        }
    }
}